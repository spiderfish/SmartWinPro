﻿<!DOCTYPE html>
<!-- saved from url=(0065)https://www.17sucai.com/preview/1673365/2019-06-02/dll/index.html -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title id="T_logo2"></title>
	
	
	<!--图标-->
	<link rel="stylesheet" type="text/css" href="logins_files/font-awesome.min.css">
	
	<!--布局框架-->
	<link rel="stylesheet" type="text/css" href="logins_files/util.css">
	
	<!--主要样式-->
	<link rel="stylesheet" type="text/css" href="logins_files/main.css">

   <meta content="ie=edge chrome=1" http-equiv="X-UA-Compatible">
    
    <link rel="shortcut icon" type="image/x-icon" href="images/logo/favicon.ico"/>
     <link href="lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" />
    <link href="lib/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" />
    <link href="CSS/input.css" rel="stylesheet" type="text/css" />
        <link href="css/login.css" type="text/css" rel="stylesheet" />
   <%-- <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>--%>
    <script src="lib/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="lib/ligerUI/js/ligerui.min.js" type="text/javascript"></script>

    <script src="lib/jquery-validation/jquery.validate.js" type="text/javascript"></script>
    <script src="lib/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="lib/jquery-validation/messages_cn.js" type="text/javascript"></script>
    <script src="lib/ligerUI/js/common.js" type="text/javascript"></script>
     <script src="JS/jquery.localize.js" type="text/javascript" charset="utf-8"></script> 
    <script src="JS/language_cookie.js" type="text/javascript" charset="utf-8"></script> 
    <script src="JS/jquery.md5.js" type="text/javascript"></script>
    <script src="JS/XHD.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $.metadata.setType("attr", "validate");
            XHD.validate($(form1));
            //var data = {};
            //langdata.callback 

            $("input[ltype=text],input[ltype=password]", this).ligerTextBox();
                  //获取LOG
            $.ajax({
                type: 'post',
                dataType: 'json',
                url: 'login.getsysinfo.xhd',
                data: { rnd: Math.random() },
                success: function (result) {
                    var obj = eval(result);
                    var rows = obj.Rows;
                    $("#logo").attr("src", rows[2].sys_value);
                      $("#T_logo").html(rows[1].sys_value+"-客户自助订单系统");
                     $("#T_logo2").html(rows[1].sys_value);
                },
                error: function () {
                    //javascript: location.replace("login.aspx");
                }
            });
      
       

            $("#btn_login").hover(function () {
                $(this).addClass("btn_login_over");
            }, function () {
                $(this).removeClass("btn_login_over");
            }).mousedown(function () {
                check();
            });

            if (getCookie("xhdcrm_uid") && getCookie("xhdcrm_uid") != null)
                $("#T_uid").val(getCookie("xhdcrm_uid"))

            $(document).keydown(function (e) {
                if (e.keyCode == 13) {
                    check();
                }
            });

            $("#reset").click(function () {
                $(":input", "#form1").not(":button,:submit:reset:hidden").val("");
            });

            function check() {
                if ($(form1).valid()) {
                    dologin();
                }
            }

            function dologin() {
                var company = $("#T_company").val();
                var uid = $("#T_uid").val();
                var pwd = $("#T_pwd").val();
                var validate = $("#T_validate").val();
                if (validate == "") {
                    $.ligerDialog.warn("验证码不能为空！");
                    $("#T_validate").focus();
                    return;
                }
                else if (validate.length != 4) {
                    $.ligerDialog.warn("验证码错误！");
                    $("#T_validate").focus();
                    return;
                }

                if (company == "") {
                    $.ligerDialog.warn("单位不能为空！");
                    $("#T_company").focus();
                    return;
                }

                if (uid == "") {
                    $.ligerDialog.warn("账号不能为空！");
                    $("#T_uid").focus();
                    return;
                }
                if (pwd == "") {
                    $.ligerDialog.warn("密码不能为空！");
                    $("#T_pwd").focus();
                    return;
                }



                $.ajax({
                    type: 'post', dataType: 'json',
                    url: 'login.checkOnline.xhd',
                    data: [
                      
                        { name: 'username', value: uid },
                        { name: 'password', value: $.md5(pwd) },
                        { name: 'validate', value: validate },
                        { name: 'rnd', value: Math.random() }
                    ],
                    dataType:'json',
                    success: function (result) {
                        $.ligerDialog.closeWaitting();

                        var obj = eval(result);

                        if (obj.isSuccess) {                         
                            SetCookie("xhdcrm_uid", uid, 30);                           
                            location.href = "mainOnline.aspx";
                        }
                        else {
                            $("#validate").click();
                            $.ligerDialog.error(obj.Message);
                            
                        }                       
                    },
                    error: function () {
                        $.ligerDialog.closeWaitting();
                        $("#validate").click();
                        $.ligerDialog.warn('发生系统错误,请与系统管理员联系!');
                    },
                    beforeSend: function () {
                        $.ligerDialog.waitting("正在登录中,请稍后...");
                        $("#btn_lgoin").attr("disabled", true);
                    },
                    complete: function () {
                        $("#btn_login").attr("disabled", false);
                    }
                });
            }
        })


    </script>
   
    <script type="text/javascript">
        if (top.location != self.location) top.location = self.location;
    </script>
</head>

<body>

<div class="login">
	<div class="container-login100">
		<div class="wrap-login100">
			<div class="login100-pic js-tilt" data-tilt="">
			
                <img id="logo" alt="" style=" margin-left: 5px; margin-top: 2px;" />
                   <%--<img src="/images/winproxz.png" style=" margin-left: 5px; margin-top: 2px;" />--%>
                	  
               <%-- <img src="logins_files/winprologo.png" style=" margin-left: 5px; margin-top: 2px;" />--%>
			</div>

			<div class="login100-form ">
				<span id="T_logo" class="login100-form-title">
					
				</span>
                <form id="form1" name="form1">
				<div class="wrap-input100 validate-input">
					<input class="input100" type="text"  id="T_uid" name="T_uid" placeholder="用户名">
					<span class="focus-input100"></span>
					<span class="symbol-input100">
						<i class="fa fa-user" aria-hidden="true"></i>
					</span>
				</div>

				<div class="wrap-input100 validate-input">
					<input class="input100" type="password"id="T_pwd" name="T_pwd"  type="password" placeholder="密码">
					<span class="focus-input100"></span>
					<span class="symbol-input100">
						<i class="fa fa-lock" aria-hidden="true"></i>
					</span>
				</div>
                <div class="wrap-input100 validate-input">
					<input class="input100" type="text" id="T_validate" name="T_validte" placeholder="验证码">
					<span class="focus-input100"></span>
					<span class="symbol-input100">
						<i class="fa fa-sort-numeric-asc" aria-hidden="true"></i>
					</span>
				</div>
								<div >
<img src="ValidateCode.aspx" alt="看不清楚，换一张" name="validate" width="250" height="50" id="validate" title="看不清楚，换一张" onclick="this.src=this.src+'?'" />
				</div>
				<div class="container-login100-form-btn">
                    <div id="btn_login" style="background: url(logins_files/logindo.png); width: 300px; height: 50px; cursor: pointer;"></div>

				</div>
                       </form>
				<div class="text-center p-t-12">
					<a class="txt2" >
					Smart Management System By WINPRO V1.0
					</a>
				</div>

				
			</div>
		</div>
	</div>
</div>


</body></html>