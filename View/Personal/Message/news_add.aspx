﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta http-equiv="X-UA-Compatible" content="ie=edge chrome=1" />
    <link href="../../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" />
    <link href="../../lib/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" />

    <script src="../../lib/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/ligerui.min.js" type="text/javascript"></script>

    <script src="../../lib/jquery-validation/jquery.validate.js" type="text/javascript"></script>
    <script src="../../lib/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="../../lib/jquery-validation/messages_cn.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/common.js" type="text/javascript"></script>
    <script src="../../lib/jquery.form.js" type="text/javascript"></script>

    <script src="../../JS/XHD.js" type="text/javascript"></script>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<%--    <script type="text/javascript" charset="utf-8" src="../../ueditor/ueditor.config.js"></script>
    <script src="../../ueditor/ueditor.all.min.js" type="text/javascript"></script>
    <script src="../../ueditor/lang/zh-cn/zh-cn.js" type="text/javascript"></script>
    <link href="../../ueditor/themes/default/css/ueditor.css" rel="stylesheet" />--%>
    
<link href="https://unpkg.com/@wangeditor/editor@latest/dist/css/style.css" rel="stylesheet"/>
     <script src="https://unpkg.com/@wangeditor/editor@latest/dist/index.js"></script>
    <style type="text/css">
  #editor—wrapper {
    border: 1px solid #ccc;
    z-index: 50; /* 按需定义 */
  }
  #toolbar-container { border-bottom: 1px solid #ccc; }
  #editor-container { height: 500px; }
</style>

  
</head>
<body style="overflow: hidden;">
    <form id="form1" onsubmit="return false">
        <div style="position: relative; z-index: 900;">


            <table style="width: 650px"  class="aztable">
                <tr>
                    <td style="width: 85px">
                        <div style="width: 80px; text-align: right;">新闻标题：</div>
                    </td>
                    <td>
                        <input type="text" id="T_title" name="T_title" ltype="text" ligerui="{width:300}" validate="{required:true}" /></td>
                    <td style="width: 85px">
                        <div style="width: 80px; text-align: right;">类型：</div>
                    </td>
                    <td>
                          <input type="text" id="T_type" name="T_type" ltype="select" ligerui="{width:80,data:[{id:'B',text:'Banner'},{id:'N',text:'新闻'},{id:'C',text:'案例'}]}"  validate="{required:true}"  />
                     </td>
                </tr>
                     <tr>
                    <td style="width: 85px">
                        <div style="width: 80px; text-align: right;">资源地址：</div>
                    </td>
                    <td colspan="3">
                        <input type="text" id="T_url" name="T_url" ltype="text" ligerui="{width:600}" validate="{required:true}" /></td>
                    
                </tr>
            </table>
          <table><tr><td> <div id="editor—wrapper">
    <div id="toolbar-container"><!-- 工具栏 --></div>
    <div id="editor-container"><!-- 编辑器 --></div>
</div></td><td>
                   <img id="userheadimg" src="a.gif" onerror="noheadimg()" style="border-radius: 4px; box-shadow: 1px 1px 3px #111; width: 220px; height: 150px; margin-left: 5px; background: #d2d2f2; border: 3px solid #fff; behavior: url(../css/pie.htc);" />

          <input type="hidden" id="headurl" name="headurl" />
                        <div style="text-align: center">
                            <div id="uploadimg"></div>
                        </div></td></tr></table>
             <%--<script id="editor"></script>--%>
        </div>

        

    </form>

      <script type="text/javascript">
          // var editor;
          var Richhtml = "";
          const { createEditor, createToolbar } = window.wangEditor
          const editorConfig = {
              placeholder: 'Type here...',
              MENU_CONF: {},
              onChange(editor) {
                  Richhtml = editor.getHtml()
                  console.log('editor content', Richhtml)
                  // 也可以同步到 <textarea>
              }
          }

          editorConfig.MENU_CONF['uploadImage'] = {
              //debug:true,
              server: 'public_news.RichUpload.xhd',
              onSuccess(file, res) {
                  console.log('onSuccess', file, res)
              },
              onFailed(file, res) {
                  alert(res.message)
                  console.log('onFailed', file, res)
              },
              onError(file, err, res) {
                  alert(err.message)
                  console.error('onError', file, err, res)
              },
              /*  base64LimitSize: 5 * 1024 *1024 // 5M*/
          }
          const editor = createEditor({
              selector: '#editor-container',
              html: '<p><br></p>',
              config: editorConfig,
              mode: 'default', // or 'simple'
          })

          const toolbarConfig = {}

          const toolbar = createToolbar({
              editor,
              selector: '#toolbar-container',
              config: toolbarConfig,
              mode: 'default', // or 'simple'
          })
          $(function () {
              $.metadata.setType("attr", "validate");
              XHD.validate($(form1));

              $("form").ligerForm();
              //editor = UE.getEditor('editor', {
              //    //initialFrameWidth: null,
              //    //initialFrameHeight: 293,
              //    toolbars: [
              //       ['fullscreen', 'source', '|', 'undo', 'redo', '|',
              //        'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'superscript', 'subscript', 'removeformat', 'formatmatch', 'autotypeset', 'blockquote', 'pasteplain', '|',
              //        'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', 'cleardoc', '|',
              //        'rowspacingtop', 'rowspacingbottom', 'lineheight', '|',
              //        'customstyle', 'paragraph', 'fontfamily', 'fontsize', '|',
              //        'directionalityltr', 'directionalityrtl', 'indent', '|',
              //        'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|', 'touppercase', 'tolowercase', '|',
              //        'link', 'unlink', 'anchor', '|', 'imagenone', 'imageleft', 'imageright', 'imagecenter', '|',
              //        'simpleupload', 'insertimage', 'emotion', 'scrawl', 'music', 'map', 'gmap', 'insertcode', 'pagebreak', 'template', 'background', '|',
              //        'horizontal', 'date', 'time', 'spechars', '|',
              //        'inserttable', 'deletetable', 'insertparagraphbeforetable', 'insertrow', 'deleterow', 'insertcol', 'deletecol', 'mergecells', 'mergeright', 'mergedown', 'splittocells', 'splittorows', 'splittocols', '|',
              //        'print', 'preview', 'searchreplace', 'help', 'drafts'
              //       ]
              //    ],
              //    autoHeightEnabled: false
              //});


              if (getparastr("nid")) {
                  loadForm(getparastr("nid"));
              }
              $("#uploadimg").ligerButton({ text: "上传照片", width: 130, click: uploadimg });
              onResize();
              $(window).resize(function () {
                  onResize();
              });
          })

          function onResize() {
              //var winH = $(window).height();
              //var winW = $(window).width();

              //editor.ready(function () {
              //    $('.edui-editor').width(winW - 20);
              //    $('#edui1_iframeholder').width(winW - 20);
              //    editor.setHeight(winH - 170);
              //});
          }

          function f_save() {
              if ($(form1).valid()) {
                  var arr = [];
                  // arr.push(UE.getEditor('editor').getContent());
                  //+ "&richText=" + escape(Richhtml)
                  console.log($("#headurl").val())
                  var sendtxt = "&nid=" + getparastr("nid")   + "&T_content=" + escape(Richhtml)// escape(arr);
                  return $("form :input").fieldSerialize() + sendtxt;
              }
          }

          function loadForm(oaid) {
              $.ajax({
                  type: "GET",
                  url: "public_news.form.xhd", /* 注意后面的名字对应CS的方法名称 */
                  data: { id: oaid, rnd: Math.random() }, /* 注意参数的格式和名称 */
                  contentType: "application/json; charset=utf-8",
                  dataType: "json",
                  success: function (result) {
                      var obj = eval(result);
                      for (var n in obj) {

                      }
                      var h = unescape(obj.news_content);
                      console.log(escape2Html(h))
                      editor.setHtml(escape2Html(h))
                      //$("#T_product_category").ligerGetComboBoxManager().selectValue(obj.category_id);
                      $("#headurl").val(obj.bannerImgUrl);
                      $("#userheadimg").attr("src", "../../Images/upload/news/" + obj.bannerImgUrl);
                      //alert(obj.constructor); //String 构造函数
                      $("#T_title").val(obj.news_title);
                      $("#T_url").val(obj.caseVideoUrl);
                      $("#T_type").val(obj.news_type);
                      liger.get("T_type").selectValue(obj.news_type);
                      //editor_a.setContent($('#content').val());  //赋值给UEditor
                      //editor.ready(function () {
                      //    editor.setContent(myHTMLDeCode(obj.news_content));
                      //});

                  }
              });

          }
          function escape2Html(str) {
              var arrEntities = { 'lt': '<', 'gt': '>', 'nbsp': ' ', 'amp': '&', 'quot': '"' };
              return str.replace(/&(lt|gt|nbsp|amp|quot);/ig, function (all, t) { return arrEntities[t]; });
          } 

          function uploadimg() {
              top.$.ligerDialog.open({
                  zindex: 9004,
                  width: 800, height: 500,
                  title: '上传新闻Banner图片',
                  url: 'Product/uploadimage.aspx',
                  buttons: [
                      {
                          text: '保存', onclick: function (item, dialog) {
                              saveheadimg(item, dialog);
                          }
                      },
                      {
                          text: '关闭', onclick: function (item, dialog) {
                              dialog.close();
                          }
                      }
                  ],
                  isResize: true
              });
          }

          function saveheadimg(item, dialog) {
              var upfile = dialog.frame.f_save();
              //alert(upfile);
              if (upfile) {
                  dialog.close();
                  $.ligerDialog.waitting('数据保存中,请稍候...');

                  $.ajax({
                      url: "public_news.Base64Upload.xhd", type: "POST",
                      data: upfile,
                      dataType: "json",
                      success: function (result) {
                          $.ligerDialog.closeWaitting();

                          var obj = eval(result);
                          if (obj.isSuccess) {
                              console.log(obj.Message)
                              $("#headurl").val(obj.Message);
                              $("#userheadimg").attr("src", "../../Images/upload/news/" + obj.Message);
                          }
                          else {
                              $.ligerDialog.error(obj.Message);
                          }


                      },
                      error: function () {
                          $.ligerDialog.closeWaitting();
                          $.ligerDialog.error('操作失败！');
                      }
                  });
              }
          }
      </script>
   
</body>

</html>
