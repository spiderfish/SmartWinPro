<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <meta http-equiv="X-UA-Compatible" content="ie=edge chrome=1" />

    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" />
    <link href="../lib/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" />
    <link href="../CSS/input.css" rel="stylesheet" />

    <script src="../lib/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/ligerui.min.js" type="text/javascript"></script>
    <script src="../lib/jquery.form.js" type="text/javascript"></script>
    <script src="../JS/XHD.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {

            initLayout();
            $(window).resize(function () {
                initLayout();
            });

            $("#maingrid4").ligerGrid({
                columns: [
                    //{ display: '序号', width: 50, render: function (item,i) { return item.n; } },
                    {
                        display: '公告标题', name: 'notice_title', width: 460, align: 'left', render: function (item) {
                            return "<a href='javascript:void(0)' onclick=view('" + item.id + "')>" + item.notice_title + "</a>";
                        }
                    },
                    { display: '发布部门', name: 'dep_name', width: 100 },
                    { display: '发布人', name: 'create_name', width: 100 },

                    {
                        display: '发布时间', name: 'create_time', width: 90, render: function (item) {
                            return formatTimebytype(item.create_time, 'yyyy-MM-dd');
                        }
                    },
                    {
                        display: '查看', render: function (item) {
                            return "<a href='javascript:void(0)' onclick=view('" + item.id + "')>查看</a>";
                        }
                    }

                ],
                //fixedCellHeight:false,
                rownumbers:true,
                onSelectRow: function (data, rowindex, rowobj) {
                    $("#framecenter").html(myHTMLDeCode(data.notice_content));
                },
                dataAction: 'server',   pageSize: 30, 
                pageSizeOptions: [20, 30, 50, 100],
                method: 'get',  
                url: "ScanApp.asmx/GetListNotice?Viewtype=List&id=&rnd=" + Math.random(),
                //title:'公告列表',
                width: '100%', height: '100%',
                heightDiff: -10,
                onRClickToSelect: true,

                onContextmenu: function (parm, e) {
                    actionCustomerID = parm.data.id;
                    menu.show({ top: e.pageY, left: e.pageX });
                    return false;
                }
            });

            $("#grid").height(document.documentElement.clientHeight - $(".toolbar").height());

            toolbar();
        });

        function toolbar() {
          
        }

        function doserch() {
            var sendtxt = "&rnd=" + Math.random();
            var serchtxt = $("#form1 :input").fieldSerialize() + sendtxt;
            //alert(serchtxt);           
            var manager = $("#maingrid4").ligerGetGridManager();

            manager._setUrl("ScanApp.asmx/GetListNotice&Viewtype=List&id=&" + serchtxt);
        }
        function doclear() {
            //var serchtxt = $("#serchform :input").reset();
            $("#form1").each(function () {
                this.reset();
            });
        }

        //查看
        function view(title_id) {
            var dialogOptions = {
                width: 770, height: 510, title: "查看公告", url: 'notice_view.aspx?nid=' + title_id, buttons: [
                        {
                            text: '关闭', onclick: function (item, dialog) {
                                dialog.close();
                            }
                        }
                ], isResize: true, timeParmName: 'a'
            };
            activeDialog = parent.jQuery.ligerDialog.open(dialogOptions);
        }

    

        
 
        function f_reload() {
            $("#maingrid4").ligerGetGridManager().loadData(true);
        };


    </script>
</head>
<body>
    <form id="form1" onsubmit="return false">
        <div style="padding: 10px;">
            <div id="toolbar"></div>
            <div id="grid">
                <div id="maingrid4" style="margin: -1px; min-width: 800px;"></div>
            </div>
        </div>
    </form>

</body>
</html>
