﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Xml.Serialization;
using Microsoft.CSharp;
using XHD.Common;
using System.Web.Script.Services;
using XHD.Model;

namespace XHD.View.WebService
{
    /// <summary>
    /// ScanApp 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允许使用 ASP.NET AJAX 从脚本中调用此 Web 服务，请取消注释以下行。 
    [System.Web.Script.Services.ScriptService]
    public class ScanApp : System.Web.Services.WebService
    {
        ActionResult AR = new ActionResult();

        [WebMethod]
        [XmlInclude(typeof(object))]
        public DataTable GetDataTable_Sys_info()
        {
            var r = (ScannerApp.DataBase.ScanAppSQLLogic.GetSys_info());
            r.Item3.TableName = "Sys_info";
            return r.Item3;
        }

        [WebMethod]
        [XmlInclude(typeof(object))]
        public string UpdateIsStart(string yesorno)
        {
            var r = ScannerApp.DataBase.ScanAppSQLLogic.UpdateIsStart(yesorno);
            return r.Item2;
        }



        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="barcode"></param>
        /// <returns></returns>
        [WebMethod]
        [XmlInclude(typeof(object))]
        public  DataTable  GetDatable_hr_employee(string barcode,string mac)
        {
            var r=(ScannerApp.DataBase.ScanAppSQLLogic.GetDatable_hr_employee(barcode, mac));
            r.Item3.TableName = "hr";
            return r.Item3;
        }

        #region 第一次导航
        /// <summary>
        /// 获取可使用的机器名
        /// </summary>
        /// <param name="DevID"></param>
        /// <returns></returns>
        [WebMethod]
        [XmlInclude(typeof(object))]
        public DataTable GetList_Basic_Device()
        {
            var r = (ScannerApp.DataBase.ScanAppSQLLogic.GetList_Basic_Device());
            r.Item3.TableName = "Basic_Device";
            return r.Item3;
        }
        [WebMethod]
        [XmlInclude(typeof(object))]
        public string Sys_log(string paras)
        {

            var r = ScannerApp.DataBase.ScanAppSQLLogic.Sys_log(paras);
            //ReturnJsonStr(r.Item1, r.Item2, null);
            return r.Item2;
        }
        /// <summary>
        /// 此电脑还未绑定
        /// </summary>
        /// <param name="paras"></param>
        /// <returns></returns>
        [WebMethod]
        [XmlInclude(typeof(object))]
        public DataTable GetLogin(string paras)
        {
            var r = ScannerApp.DataBase.ScanAppSQLLogic.GetLogin(paras);
            r.Item3.TableName = "hr";
            return r.Item3;

        }
        /// <summary>
        /// 绑定PC端MAC与编号地址
        /// </summary>
        /// <param name="paras"></param>
        /// <returns></returns>
        [WebMethod]
        [XmlInclude(typeof(object))]
        public string PC_DeviceName(string type, string paras)
        {
            var r = ScannerApp.DataBase.ScanAppSQLLogic.PC_DeviceName(type, paras);
            //ReturnJsonStr(r.Item1, r.Item2, null);
            return r.Item2;
        }
        /// <summary>
        /// 公告
        /// </summary>
        /// <param name="paras"></param>
        /// <returns></returns>
        [WebMethod]
        [XmlInclude(typeof(object))]
        public DataTable GetTableNotice(string paras)
        {
            BLL.public_notice bll = new BLL.public_notice();
            string Total = "";
             DataSet   ds = bll.GetList(5, 1, "", " create_time desc", out Total);
 
            var r = ds.Tables[0];
            ds.Tables[0].TableName = "TableNotice";
            return ds.Tables[0];

        }
        [WebMethod]
        [XmlInclude(typeof(object))]
        public DataTable GetLastNotice()
        {
            string sql = "SELECT   TOP	 1 * FROM  dbo.public_notice ORDER BY	 create_time DESC	";
            DataSet ds = DBUtility.DbHelperSQL.Query(sql);

            var r = ds.Tables[0];
            ds.Tables[0].TableName = "LastNotice";
            return ds.Tables[0];

        }
        /// <summary>
        /// 首页--Notice公告和公告明细
        /// </summary>
        /// <param name="Viewtype">类型</param>
        /// <param name="id">单号</param>
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [WebMethod]
        public void GetListNotice(string Viewtype, string id)
        {
            string retstr = "";
            XHD.Server.public_notice notice = new XHD.Server.public_notice();
            DataSet ds;
            if (Viewtype == "List")
            {
                BLL.public_notice bll = new BLL.public_notice();
                string Total = "";
                ds = bll.GetList(30, 1, "", " create_time desc", out Total);

                retstr = (GetGridJSON.DataTableToJSON1(ds.Tables[0], Total));

            }
            else if (Viewtype == "form")
            {
                retstr = notice.form(id);
            }
            else
            {
                retstr = "{}";
            }



            ReturnStr(retstr);

        }


        #endregion

        #region 通用类型（比如退回单据）
        [WebMethod]
        [XmlInclude(typeof(object))]
        public string Update_boar_collection_back(  string id, string status)
        {
            var r = (ScannerApp.DataBase.ScanAppSQLLogic.Update_boar_collection_back( id,status));
            return r.Item2;
        }
        /// <summary>
        /// 各种状态更新
        /// </summary>
        /// <param name="type">density/VIT/dilution/packing/In/Out</param> 
        /// <param name="id"></param>
        /// <returns></returns>
        [WebMethod]
        [XmlInclude(typeof(object))]
        public string Update_boar_collection_status(string type, string id)
        {
            var r = (ScannerApp.DataBase.ScanAppSQLLogic.Update_boar_collection_status(type, id));
            return r.Item2;
        }
        /// <summary>
        /// 条件查询单据
        /// </summary>
        /// <param name="barcode"></param>
        /// <param name="type">density/VIT/dilution/packing/In/Out</param> 
        /// <returns></returns>
          [WebMethod]
        [XmlInclude(typeof(object))]
        public DataTable GetDataTable_collection_condition(string barcode,string type)
        {
            var r = (ScannerApp.DataBase.ScanAppSQLLogic.GetListboar_collection_condition(barcode,type));
            r.Item3.TableName = "Basic_boar11";
            return r.Item3;
        }
        #endregion
        /// <summary>
        /// 获取种猪的详细信息
        /// </summary>
        /// <param name="barcode"></param>
        /// <returns></returns>
        [WebMethod]
        [XmlInclude(typeof(object))]
        public DataTable GetDataTable_Basic_boar_collection(string barcode)
        {
            var r = (ScannerApp.DataBase.ScanAppSQLLogic.GetListBasic_boar(barcode));
            r.Item3.TableName = "Basic_boar";
            return r.Item3;
        }

        #region 称重
        /// <summary>
        /// 获取信息
        /// </summary>
        /// <param name="barcode"></param>
        /// <param name="status"></param>
        /// <returns></returns>

        [WebMethod]
        [XmlInclude(typeof(object))]
        public DataTable GetDataTable_boar_collection(string barcode, string status)
        {
            var r = (ScannerApp.DataBase.ScanAppSQLLogic.GetDataTable_boar_collection(barcode, status));
            r.Item3.TableName = "boar";
            return r.Item3;
        }
        [WebMethod]
        [XmlInclude(typeof(object))]
        public DataTable GetList_boar_collection(string DevID,string type)
        {
            var r = (ScannerApp.DataBase.ScanAppSQLLogic.GetList_boar_collection(DevID,type));
            r.Item3.TableName = "boar";
            return r.Item3;
        }
        /// <summary>
        /// 更新称重
        /// </summary>
        /// <param name="weights"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [WebMethod]
        [XmlInclude(typeof(object))]
        public string Update_boar_collection_Weights(string weights, string id)
        {
            var r = (ScannerApp.DataBase.ScanAppSQLLogic.Update_boar_collection_Weights(weights, id));
            return r.Item2;
        }
        /// <summary>
        /// 获取种猪对应订单信息
        /// </summary>
        /// <param name="barcode"></param>
        /// <returns></returns>
        [WebMethod]
        [XmlInclude(typeof(object))]
        public DataTable GetDataTable_boar_collection_collection(string barcode)
        {
            var r = (ScannerApp.DataBase.ScanAppSQLLogic.GetListboar_collection(barcode));
            r.Item3.TableName = "Basic_boar";
            return r.Item3;
        }

        #endregion




        #region  collection采集

        /// <summary>
        /// 获取所有订单状态信息
        /// </summary>
        /// <param name="barcode"></param>
        /// <returns></returns>
        [WebMethod]
        [XmlInclude(typeof(object))]
        public DataSet GetCollection_all_data()
        {
            var r = (ScannerApp.DataBase.ScanAppSQLLogic.GetCollection_all_data());
            r.Item3.Tables[0].TableName = "Basic_boar1";
            r.Item3.Tables[1].TableName = "Basic_boar2";
            //r.Item3.Tables[2].TableName = "Basic_boar3";
            return r.Item3;
        }
        /// <summary>
        /// /// <summary>
        /// 条件筛选
        /// </summary>
        /// <param name="IsStatus">IsStatus -A0-_-10-</param>
        /// <returns></returns>
        /// </summary>
        /// <param name="IsStatus"></param>
        /// <returns></returns>
        [WebMethod]
        [XmlInclude(typeof(object))]
        public DataSet GetCollection_ByConditon(string IsStatus)
        {
            var r = (ScannerApp.DataBase.ScanAppSQLLogic.GetCollection_ByConditon(IsStatus));
            r.Item3.Tables[0].TableName = "Basic_boar1"; 
            return r.Item3;
        }
        /// <summary>
        /// 是否存在未处理订单
        /// </summary>
        /// <param name="barcode"></param>
        /// <returns></returns>
        [WebMethod]
        [XmlInclude(typeof(object))]
        public bool IsExist_collectionOrder( string barcode)
        {
            var r = (ScannerApp.DataBase.ScanAppSQLLogic.IsExist_collection(barcode));
            return r.Item1;
        }

        /// <summary>
        /// 更新状态
        /// </summary>
        /// <param name="type">collection_save/collection_submit</param>
        ///<param name="para"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [WebMethod]
        [XmlInclude(typeof(object))]
        public string Update_boar_collection_collection(string type, string para, string id)
        {
            var r = (ScannerApp.DataBase.ScanAppSQLLogic.Update_boar_collection(type, para, id));
            return r.Item2;
        }
        [WebMethod]
        [XmlInclude(typeof(object))]
        public ActionResult Save_boar_collection_collection(string type, string collector, string deviceid, string boarid, string collectStation, string WorkstationNo, string doperson)
        {
            var r = (ScannerApp.DataBase.ScanAppSQLLogic.Save_boar_collection(type, collector, deviceid, boarid, collectStation, WorkstationNo, doperson));
            if (r.Item1) AR.type = ResultType.success;
            else AR.type = ResultType.error; 
            AR.message = r.Item2;
            return AR;
        }

        #endregion

        #region 密度/活力分析
        /// <summary>
        /// 新增分析报告
        /// </summary>
        /// <param name="bi"></param>
        /// <returns></returns>
        [WebMethod]
        [XmlInclude(typeof(object))]
        public string Add_boar_ISASpSUS(Boar_ISASpSUS bi)
        {
            var r = (ScannerApp.DataBase.ScanAppSQLLogic.Insert_boar_ISASpSUS(bi));
            return r.Item2;
        }
        #endregion

        #region 打包
        [WebMethod]
        [XmlInclude(typeof(object))]
        public string Update_boar_collection_statusPacking(string id,int lsh,string IsPrint,string remarks)
        {
            var r = (ScannerApp.DataBase.ScanAppSQLLogic.Update_boar_collection_statusPacking( id,lsh,IsPrint, remarks));
            return r.Item2;
        }

        #endregion



        /// <summary>
        /// 返回JSON格式
        /// </summary>
        /// <param name="flag"></param>
        /// <param name="data"></param>
        private void ReturnJsonStr(bool flag, string msg, DataTable data)
        {
            string rstr = "";
            if (!flag)//StringToUnicodeHex
            {
                rstr = " {\"code\":-1,\"message\":\"" + msg + "\",\"data\":null}";
            }
            else
            {
                DataSet ds = new DataSet();
                ds.Tables.Add(data);

                rstr = " {\"code\":0,\"message\":\"" + msg + "\",\"data\":" + Common.DataToJson.GetJson(ds) + "}";
            }
               

            Context.Response.Charset = "utf-8"; //设置字符集类型  
            Context.Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
            Context.Response.Write(rstr);

            Context.Response.End();
        }
        private void ReturnStr(object data)
        {
          
            Context.Response.Charset = "utf-8"; //设置字符集类型  
            Context.Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
            Context.Response.Write(data);

            Context.Response.End();
        }



        [WebMethod] 
        public void  ISASpSUS_Info(string id)
        {
            var r = (ScannerApp.DataBase.ScanAppSQLLogic.GetISASpSUS(id));
            if (r.Item1)
                ReturnJsonStr(r.Item1,  r.Item2,r.Item3);
            else ReturnJsonStr(r.Item1,r.Item2,null);
            //  return r.Item2;
        }


       


    }
}
