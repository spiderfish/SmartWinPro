﻿using Newtonsoft.Json.Linq;
using ScannerApp.DataBase;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using XHD.Model;

namespace XHD.CRM.webserver
{
    /// <summary>
    /// Handler1 的摘要说明
    /// </summary>
    public class Handler1 : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            //context.Response.ContentType = "text/plain";
            //context.Response.Write("Hello World");
            context.Response.ContentType = "text/plain";
            HttpRequest request = context.Request;
            if (request["Action"] == "UploadFile")
            {
                context.Response.ContentType = "text/plain";
                context.Response.Charset = "utf-8";

                var files = context.Request.Files;
                if (files.Count <= 0)
                {
                    return;
                }

                HttpPostedFile file = files[0];

                if (file == null)
                {
                    context.Response.Write("error|file is null");
                    return;
                }
                else
                {
                    string path = context.Server.MapPath("~/uploadedFiles/");  //存储图片的文件夹
                    string originalFileName = file.FileName;
                    string fileExtension = originalFileName.Substring(originalFileName.LastIndexOf('.'), originalFileName.Length - originalFileName.LastIndexOf('.'));
                    string currentFileName = (new Random()).Next() + fileExtension;  //文件名中不要带中文，否则会出错
                    //生成文件路径
                    if (Directory.Exists(path) == false)//如果不存在就创建file文件夹
                    {
                        Directory.CreateDirectory(path);
                    }
                    string imagePath = path + currentFileName;
                    //保存文件
                    file.SaveAs(imagePath);
                    //返回图片url地址
                    context.Response.Write(currentFileName);
                    return;
                }
            }
            if (request["Action"] == "UploadFileArry")
            {
                context.Response.ContentType = "text/plain";
                context.Response.Charset = "utf-8";

                var files = context.Request.Files;
                if (files.Count <= 0)
                {
                    return;
                }

                string s = "";
                //foreach (HttpPostedFile file in files)
                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFile file = files[i];

                    if (file == null)
                    {
                        s += ";" + "error|file is null";
                    }
                    else
                    {
                        string path = context.Server.MapPath("~/uploadedFiles/");  //存储图片的文件夹
                        string originalFileName = file.FileName;
                        string fileExtension = originalFileName.Substring(originalFileName.LastIndexOf('.'), originalFileName.Length - originalFileName.LastIndexOf('.'));
                        string currentFileName = (new Random()).Next() + fileExtension;  //文件名中不要带中文，否则会出错
                        if (Directory.Exists(path) == false)//如果不存在就创建file文件夹
                        {
                            Directory.CreateDirectory(path);
                        }                                                           //生成文件路径
                        string imagePath = path + currentFileName;
                        //保存文件
                        file.SaveAs(imagePath);
                        //返回图片url地址
                        s += ";" + currentFileName;

                    }
                }
                context.Response.Write(s);
                return;
            }

        }     //base64编码的文本 转为    图片
        private string Base64StringToImage(string base64string, string path)
        {
            try
            {
                byte[] bt = Convert.FromBase64String(base64string);
                File.WriteAllBytes(path, bt);
                return "";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}