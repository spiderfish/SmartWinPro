﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace XHD.View.WebService
{
   
    public partial class callback : System.Web.UI.Page
    {
        public String output;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //快递信息通过param获取(订阅时指定的格式是json这里就是json)
                String param = Request.Form["param"].ToString();
                //......这里可以保存您的快递信息
                txtresult.Text = param.ToString();
                ScannerApp.DataBase.ScanAppSQLLogic.Insert_test(param.ToString());
                
                //一定要返回成功，不返回就是失败
                output = "{\"code\":\"{\"errcode\":\"200\",\"errmsg\":\"success\"}\"";
            }
            catch
            {
                //如果快递信息保存失败，这里返回失败信息，过30分钟会重推
                output = "{\"code\":\"{\"errcode\":\"201\",\"errmsg\":\"服务器错误\"}\"";
            }
        }
    }
}