<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta content="ie=edge chrome=1" http-equiv="X-UA-Compatible">
    <title data-localize="login.title">WINPRO管理系统登录</title>
    <link rel="shortcut icon" type="image/x-icon" href="images/logo/favicon.ico"/>
     <link href="lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" />
    <link href="lib/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" />
    <link href="CSS/input.css" rel="stylesheet" type="text/css" />
        <link href="css/login.css" type="text/css" rel="stylesheet" />
   <%-- <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>--%>
    <script src="lib/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="lib/ligerUI/js/ligerui.min.js" type="text/javascript"></script>

    <script src="lib/jquery-validation/jquery.validate.js" type="text/javascript"></script>
    <script src="lib/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="lib/jquery-validation/messages_cn.js" type="text/javascript"></script>
    <script src="lib/ligerUI/js/common.js" type="text/javascript"></script>
     <script src="JS/jquery.localize.js" type="text/javascript" charset="utf-8"></script> 
    <script src="JS/language_cookie.js" type="text/javascript" charset="utf-8"></script> 
    <script src="JS/jquery.md5.js" type="text/javascript"></script>
    <script src="JS/XHD.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $.metadata.setType("attr", "validate");
            XHD.validate($(form1));
            //var data = {};
            //langdata.callback 

            $("input[ltype=text],input[ltype=password]", this).ligerTextBox();
                  //获取LOG
            $.ajax({
                type: 'post',
                dataType: 'json',
                url: 'login.getsysinfo.xhd',
                data: { rnd: Math.random() },
                success: function (result) {
                    var obj = eval(result);
                    var rows = obj.Rows;
                    $("#logo").attr("src", rows[2].sys_value);
                },
                error: function () {
                    //javascript: location.replace("login.aspx");
                }
            });
      
       

            $("#btn_login").hover(function () {
                $(this).addClass("btn_login_over");
            }, function () {
                $(this).removeClass("btn_login_over");
            }).mousedown(function () {
                check();
            });

            if (getCookie("xhdcrm_uid") && getCookie("xhdcrm_uid") != null)
                $("#T_uid").val(getCookie("xhdcrm_uid"))

            $(document).keydown(function (e) {
                if (e.keyCode == 13) {
                    check();
                }
            });

            $("#reset").click(function () {
                $(":input", "#form1").not(":button,:submit:reset:hidden").val("");
            });

            function check() {
                if ($(form1).valid()) {
                    dologin();
                }
            }

            function dologin() {
                var company = $("#T_company").val();
                var uid = $("#T_uid").val();
                var pwd = $("#T_pwd").val();
                var validate = $("#T_validate").val();
                if (validate == "") {
                    $.ligerDialog.warn("验证码不能为空！");
                    $("#T_validate").focus();
                    return;
                }
                else if (validate.length != 4) {
                    $.ligerDialog.warn("验证码错误！");
                    $("#T_validate").focus();
                    return;
                }

                if (company == "") {
                    $.ligerDialog.warn("单位不能为空！");
                    $("#T_company").focus();
                    return;
                }

                if (uid == "") {
                    $.ligerDialog.warn("账号不能为空！");
                    $("#T_uid").focus();
                    return;
                }
                if (pwd == "") {
                    $.ligerDialog.warn("密码不能为空！");
                    $("#T_pwd").focus();
                    return;
                }



                $.ajax({
                    type: 'post', dataType: 'json',
                    url: 'login.check.xhd',
                    data: [
                      
                        { name: 'username', value: uid },
                        { name: 'password', value: $.md5(pwd) },
                        { name: 'validate', value: validate },
                        { name: 'rnd', value: Math.random() }
                    ],
                    dataType:'json',
                    success: function (result) {
                        $.ligerDialog.closeWaitting();

                        var obj = eval(result);

                        if (obj.isSuccess) {                         
                            SetCookie("xhdcrm_uid", uid, 30);                           
                            location.href = "main.aspx";
                        }
                        else {
                            $("#validate").click();
                            $.ligerDialog.error(obj.Message);
                            
                        }                       
                    },
                    error: function () {
                        $.ligerDialog.closeWaitting();
                        $("#validate").click();
                        $.ligerDialog.warn('发生系统错误,请与系统管理员联系!');
                    },
                    beforeSend: function () {
                        $.ligerDialog.waitting("正在登录中,请稍后...");
                        $("#btn_lgoin").attr("disabled", true);
                    },
                    complete: function () {
                        $("#btn_login").attr("disabled", false);
                    }
                });
            }
        })


    </script>
   
    <script type="text/javascript">
        if (top.location != self.location) top.location = self.location;
    </script>
</head>
<body>
     <div class="box1">
    <div class="box2">
      <h1> <img id="logo" alt="" style="height: 42px; margin-left: 5px; margin-top: 2px;" /></h1>

    <form id="form1" name="form1">
        
         <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
    <td width="30%" align="right" style="font-size:15px"  data-localize="login.userid" >用户：</td>
    <td width="40%" colspan="2"> <input id="T_uid" name="T_uid" type="text" style="width: 160px; font-size:20px" ltype="text" validate="{required:true}" /></td>

    <td width="30%"></td>
  </tr>
                  <tr>
    <td align="right" style="font-size:15px" data-localize="login.pwd" >密码：</td>
    <td colspan="2"> <input id="T_pwd" name="T_pwd" type="password" style="width: 160px;font-size:20px" ltype="text" validate="{required:true}" /></td>
    <td> </td>
  </tr> 
               <tr>
    <td align="right" style="font-size:15px" data-localize="login.yzm" >验证码：</td>
    <td> <input id="T_validate" name="T_validte" type="text" style="width: 80px;font-size:20px" ltype="text" validate="{required:true}" />
      </td>
    <td>  <img id="validate" onClick="this.src=this.src+'?'" src="ValidateCode.aspx" style="cursor: pointer; border: 1px solid #AECAF0; height: 22px;" alt="看不清楚，换一张" title="看不清楚，换一张" /></td>
    <td>&nbsp;</td>
  </tr>
  
                   
         

                  <tr>
    <td>&nbsp;</td>
    <td colspan="2">  <div id="btn_login" style="background: url(images/login06.png); width: 180px; height: 41px; cursor: pointer;"></div></td>
    <td> 
   </td>
  </tr>      
<%--       <tr>
           <td>选择语言</td>
           <td>
            <select id="ddlSomoveLanguage" onchange="chgLang();">  
                    <option value="zh">选择</option>
                    <option value="zh">中文</option>  
                    <option value="en">ENGLISH</option>  
                    <option value="ja">日本語</option>
            </select>  
           </td>

       </tr>--%>
                       
      
    
                  

      </table>
    </form>
         <div class="copy">Smart Management System By WINPRO V1.0</div>
        </div>
         </div>
</body>

</html>
