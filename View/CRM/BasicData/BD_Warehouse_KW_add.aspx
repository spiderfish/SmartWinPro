<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../../CSS/input.css" rel="stylesheet" />
    <link href="../../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" />
    <link href="../../lib/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" />

    <meta http-equiv="X-UA-Compatible" content="ie=edge chrome=1" />
    <script src="../../lib/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/ligerui.min.js" type="text/javascript"></script>

    <script src="../../lib/jquery-validation/jquery.validate.js" type="text/javascript"></script>
    <script src="../../lib/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="../../lib/jquery-validation/messages_cn.js" type="text/javascript"></script>
    <script src="../../JS/XHD.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/common.js" type="text/javascript">></script>
    <script src="../../lib/jquery.form.js" type="text/javascript">></script>
    <script type="text/javascript">
        $(function () {
            $.metadata.setType("attr", "validate");
            XHD.validate($("#form1"));
            $("form").ligerForm();

         
            
               // $("#T_category_parent").ligerComboBox({ width: 150, url: '../../data/CKKW.ashx?Action=combo&type=CK&rnd=' + Math.random() })

            
                loadForm(getparastr("CkID"), getparastr("KWID"));
         
            jiconlist = $("body > .iconlist:first");
            if (!jiconlist.length) jiconlist = $('<ul class="iconlist"></ul>').appendTo('body');
        })


        
        function f_save() {
            if ($(form1).valid()) {
                var sendtxt = "&Action=savekw&KWID=" + getparastr("KWID") + "&CkID=" + getparastr("CkID");
                return $("form :input").fieldSerialize() + sendtxt;
            }
        }

        function loadForm(CkID, KWID) {
            $.ajax({
                type: "GET",
                url: "BD_Warehouse.formkw.xhd", /* 注意后面的名字对应CS的方法名称 */
                data: {   CkID: CkID, KWID: KWID, rnd: Math.random() }, /* 注意参数的格式和名称 */
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    var obj = eval(result);

                    for (var n in obj) {
                        
                    }
                   // alert(obj.KWID); //String 构造函数
                    if (obj.KWID != undefined) {
                        $("#T_KW_name").attr("readonly", true)//将input元素设置为readonly
                    }
                    $("#T_KW_name").val(obj.KWID);
                    $("#T_Name").val(obj.Name);
                    $("#T_Remark").val(obj.Remark);
                    //$("#T_style").val(obj.c_style);

                    $('#T_category_parent').ligerComboBox({
                        width: 180, value: obj.CkID, emptyText: '（空）',
                        url: "BD_Warehouse.combo.xhd?Action=combo&type=CK&rnd=" + Math.random()
                         
                    });
             

                }
            });
        }

        function f_selectContact() {
            if (winicons) {
                winicons.show();
                //return;
            }
            winicons = $.ligerDialog.open({
                title: '选取图标',
                target: jiconlist,
                width: 400, height: 220, modal: true
            });
            if (!jiconlist.attr("loaded")) {
                $.ajax({
                    url: "../../data/Base.ashx", type: "get",
                    data: { Action: "GetIcons", icontype: "1", rnd: Math.random() },
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var obj = eval(data);
                        //alert(obj.length);
                        for (var i = 0, l = obj.length; i < l; i++) {
                            var src = obj[i];
                            var reg = /(images\\icon)(.+)/;
                            var match = reg.exec(src);
                            // alert(match);
                            jiconlist.append("<li><img src='../../images/icon/" + src.Name + "' /></li>");
                            if (!match) continue;
                        }
                        jiconlist.attr("loaded", true);
                    }
                });
            }
        }
        $(".iconlist li").live('mouseover', function () {
            $(this).addClass("over");
        }).live('mouseout', function () {
            $(this).removeClass("over");
        }).live('click', function () {
            if (!winicons) return;
            var src = $("img", this).attr("src");
            var name = src.substr(src.lastIndexOf("/")).toLowerCase();
            $("#menuicon").attr("src", "../../images/icon" + name);
            $("#T_category_icon").val("images/icon" + name);

            winicons.close();
        });
    </script>
    <style type="text/css">
        .iconlist {
            width: 360px;
            padding: 3px;
        }

            .iconlist li {
                border: 1px solid #FFFFFF;
                float: left;
                display: block;
                padding: 2px;
                cursor: pointer;
            }

                .iconlist li.over {
                    border: 1px solid #516B9F;
                }

                .iconlist li img {
                    height: 16px;
                    height: 16px;
                }
    </style>


    </script>
</head>
<body style="">
    <div style="padding: 0px 10px;">
        <form id="form1" onsubmit="return false">
               <table border="0" cellpadding="3" cellspacing="1" style="background: #fff; width: 400px;">
              <tr>
                <td height="23" colspan="2">

                    <div align="left" style="width: 62px">仓库：</div>
                </td>
                <td height="23">
                    <input type="text" id="T_category_parent" name="T_category_parent" validate="{required:true}" />
                </td>
            </tr>
            <tr>
                <td height="23" style="width: 85px" colspan="2">

                    <div align="left" style="width: 62px">库位：</div>
                </td>
                <td height="23">

                    <input type="text" id="T_KW_name" name="T_KW_name" ltype="text" ligerui="{width:180}" validate="{required:true}" />

                </td>
            </tr>
                <tr>
                <td height="23" colspan="2">

                    <div align="left" style="width: 62px">库位名称：</div>
                </td>
                <td height="23">
                    <input type="text" id="T_Name" name="T_Name" ltype="text" ligerui="{width:180}" validate="{required:true}" />
                
                </td>
            </tr>
          
          <%--  <tr>
                <td height="23" style="width: 62px">

                    <div align="left" style="width: 62px">类别图标：</div>
                </td>
                <td height="23" style="width: 27px">
                    <img id="menuicon" style="width: 16px; height: 16px;" /></td>
                <td height="23">
                    <input type="text" id="T_category_icon" name="T_category_icon" ltype="text" ligerui="{width:180}" validate="{required:true}" />
                </td>
            </tr>--%>
         
             <tr>
                <td height="23" colspan="2">

                    <div align="left" style="width: 62px">备注：</div>
                </td>
                <td height="23">
                    <input type="text" id="T_Remark" name="T_Remark" ltype="text" ligerui="{width:180}" validate="{required:true}" />
                
                </td>
            </tr>
           <%-- <tr>
                <td height="23" colspan="2">

                    <div align="left" style="width: 62px">主要类型：</div>
                </td>
                <td height="23"> --%>
                    <%--,data:[{id:'主材',text:'主材'},{id:'基建',text:'基建'}]  ltype="select"--%>
                   <%--  <input id="T_style" name="T_style" type="text"  ligerui="{width:176}" validate="{required:true}" /></td>
             
            </tr>--%>
        </table>
   
        </form>
    </div>
</body>
</html>
