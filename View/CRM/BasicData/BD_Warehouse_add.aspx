<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../../CSS/input.css" rel="stylesheet" />
    <link href="../../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" />
    <link href="../../lib/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" />

    <meta http-equiv="X-UA-Compatible" content="ie=edge chrome=1" />
    <script src="../../lib/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/ligerui.min.js" type="text/javascript"></script>

    <script src="../../lib/jquery-validation/jquery.validate.js" type="text/javascript"></script>
    <script src="../../lib/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="../../lib/jquery-validation/messages_cn.js" type="text/javascript"></script>
    <script src="../../JS/XHD.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/common.js" type="text/javascript">></script>
    <script src="../../lib/jquery.form.js" type="text/javascript">></script>
    <script type="text/javascript">
        $(function () {
            $.metadata.setType("attr", "validate");
            XHD.validate($("#form1"));
            $("form").ligerForm();
           
            loadForm(getparastr("id"));
            $('#lxr').ligerComboBox({ width: 150, onBeforeOpen: f_selectlxr });

        })

        function loadForm(id) {
            $.ajax({
                type: "GET",
                url: "BD_Warehouse.form.xhd", /* 注意后面的名字对应CS的方法名称 */
                data: { id: id, rnd: Math.random() }, /* 注意参数的格式和名称 */
                contentType: "application/json; charset=utf-8",
                dataType: "json",
               
                success: function (data) {
                    var obj = eval(data);
                  // alert(JSON.stringify(obj))
                    for (var n in obj) {
                        if (obj[n] == "null" || obj[n] == null)
                            obj[n] = "";
                    }

                    $("#Name").val(obj.Name);
                    $("#Address").val(obj.Address);
                    $("#lxrid").val(obj.Lxrid);
                    $("#lxr").val(obj.Lxr);
                    $("#lxrdh").val(obj.Lxrdh);
                    $("#Remark").val(obj.Remark);




                },
                error: function (ex) {
                    top.$.ligerDialog.error('操作失败，错误信息：' + ex.responseText);
                }
            });
        }
        function f_selectlxr() {
            top.$.ligerDialog.open({
                zindex: 9003,
                title: '选择员工', width: 850, height: 400, url: "hr/Getemp_Auth.aspx?auth=1", buttons: [
                    { text: '确定', onclick: f_selectlxrOK },
                    { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
                ]
            });
            return false;
        }

        function f_selectlxrOK(item, dialog) {
            var data = dialog.frame.f_select();
            if (!data) {
                alert('请选择员工!');
                return;
            }
            $("#lxr").val("【" + data.dep_name + "】" + data.name);
            $("#lxrid").val(data.id);
            dialog.close();
        }
        function f_save() {
            if ($(form1).valid()) {
                var sendtxt = "&Action=save&id=" + getparastr("id");
                return $("form :input").fieldSerialize() + sendtxt ;
            }
        }


    </script>
</head>
<body style="">
    <div style="padding: 0px 10px;">
        <form id="form1" onsubmit="return false">
                 <table style="width: 550px; margin: 5px;">
            <tr>
                <td>
                    <div style="width: 100px; text-align: right; float: right">仓库名称：</div>
                </td>
                <td colspan="3">
                   
                
                     <div style="float: left; width: 420px;">
                       <input type="text" id="Name" name="Name" ltype="text" ligerui="{width:420}" /> 
                    </div>
                   


                </td>
                </tr>
            <tr>
                <td>
                    <div style="width: 100px; text-align: right; float: right">仓库地址：</div>
                </td>
                <td colspan="3">
                   
                
                     <div style="float: left; width: 365px;">
                       <input type="text" id="Address" name="Address" ltype="text" ligerui="{width:420}" /> 
                    </div>
                    

                </td>

               
            </tr>
            
            <tr>
                <td>
                    <div style="width: 100px; text-align: right; float: right">负责人：</div>
                </td>
                <td>
                  <input type="text" id="lxr" name="lxr" validate="{required:true}" />
                    <input id="lxrid" name="lxrid" type="hidden" />
                </td>
                <td>
                    <div style="width: 100px; text-align: right; float: right">仓库电话：</div>
                </td>
                <td>
                    <input type="text" id="lxrdh" name="lxrdh" ltype="text" ligerui="{width:130}" />
                </td>
            </tr>
           
            <tr>
                <td>
                    <div style="width: 100px; text-align: right; float: right">备&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;注：</div>
                </td>
                <td colspan="3">
                    <textarea id="Remark" name="Remark" cols="100" rows="4" class="l-textarea" style="width:420px"></textarea>
             
                </td>
            </tr>
        </table>
   
        </form>
    </div>
</body>
</html>
