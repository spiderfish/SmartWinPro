<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
    <link href="../../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" />
    <link href="../../lib/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" />
    <link href="../../CSS/input.css" rel="stylesheet" />

    <script src="../../lib/jquery/jquery-1.11.3.min.js" type="text/javascript"> </script>
    <script src="../../lib/ligerUI/js/ligerui.min.js" type="text/javascript"> </script>
    <script src="../../lib/jquery.form.js" type="text/javascript"></script>
    <script src="../../JS/XHD.js" type="text/javascript"> </script>
    <script type="text/javascript">
        if (top.location == self.location) top.location = "../../default.aspx";
        var manager;
        var manager1;
        var loaddiff = 150;
        $(function () {

            initLayout();
            $(window).resize(function () {
                initLayout();
            });

            grid();
            $("#grid").height(document.documentElement.clientHeight - $(".toolbar").height());
            $('#serchform').ligerForm();
            toolbar();
             
            //var tt = test(jsonObj.Rows, 3);
            //alert(JSON.stringify(tt));
        });

        function grid() {
            $("#maingrid4").ligerGrid({
                columns: [
                    //{
                    //    display: '序号', width: 50, render: function (rowData, rowindex, value, column, rowid, page, pagesize) {
                    //        //debugger;
                    //        return (page - 1) * pagesize + rowindex + 1;
                    //    }
                    //},
                    { display: '仓库名称', name: 'Name', width: 150, align: 'left' },
                    { display: '仓库地址', name: 'Address', width: 200, align: 'left' },
                    { display: '负责人', name: 'Lxr', width: 60 },
                    { display: '仓库电话', name: 'Lxrdh', width: 150 },
                    { display: '备注', name: 'Remark', width: 200, align: 'left' },
                    // { display: '登记人', name: 'InEmpID', width: 80 },
                    {
                        display: '登记日期',
                        name: 'InDate',
                        width: 90,
                        render: function (item) {
                            var InDate = formatTimebytype(item.InDate, 'yyyy-MM-dd');
                            return InDate;
                        }
                    }
                    //{ display: '登记日期', name: 'InDate', width: 150 }
                ],
                rownumbers: true,
                onbeforeLoaded: function (grid, data) {
                    startTime = new Date();
                },
                //fixedCellHeight:false,  
                rowtype: "BD_Warehouse",
                dataAction: 'server',
                pageSize: 30,
                pageSizeOptions: [20, 30, 50, 100],
                url: "BD_Warehouse.list.xhd?rnd=" + Math.random(),
                //width: '100%',
                height: '100%',
                heightDiff: -11,
                onRClickToSelect: true,
                onContextmenu: function (parm, e) {
                    actionCustomerID = parm.data.id;
                    menu.show({ top: e.pageY, left: e.pageX });
                    return false;
                },
                onAfterShowData: function (grid) {
                    $("tr[rowtype='已成交']").addClass("l-treeleve1").removeClass("l-grid-row-alt");
                    var nowTime = new Date();
                    //loaddiff = nowTime - startTime;
                    //alert('加载数据耗时：' + (nowTime - startTime));
                }
            });

        }

        function toolbar() {
            $.ajax({
                url: "toolbar.GetSys.xhd",
                type: "POST",
                data: { mid: 'basicdata_warehouse', rnd: Math.random() },
                //dataType: "json",
                success: function (result) {

                    var data = eval('(' + result + ')');;

                    var items = [];
                    var arr = data.Items;
                    for (var i = 0; i < arr.length; i++) {
                        arr[i].icon = "../../" + arr[i].icon;
                        items.push(arr[i]);
                    }
            
                    //items.push({ type: "filter", icon: '../../images/icon/51.png', title: "帮助", click: function () { help(); } })
                    $("#toolbar").ligerToolBar({
                        items: items
                    });
                    menu = $.ligerMenu({
                        width: 120,
                        items: getMenuItems(data)
                    });
      
                    $("#maingrid4").ligerGetGridManager()._onResize();
                },
                error: function () {
                    $.ligerDialog.error('！');
                }
            });
        }
 
       



 

        function add() {
            f_openWindow('CRM/BasicData/BD_Warehouse_add.aspx', "新增仓库", 770, 490, f_save);
        }

        function edit() {
            var manager = $("#maingrid4").ligerGetGridManager();
            var row = manager.getSelectedRow();
            if (row) {
                f_openWindow('CRM/BasicData/BD_Warehouse_add.aspx?id=' + row.ID, "修改仓库", 770, 490, f_save);
            } else {
                $.ligerDialog.warn('请选择行！');
            }
        }

        function del() {
            var manager = $("#maingrid4").ligerGetGridManager();
            var row = manager.getSelectedRow();

            if (row) {
                $.ligerDialog.confirm("确定删除？", function (yes) {
                    if (yes) {
                        $.ligerDialog.waitting('数据删除中,请稍候...');
                        $.ajax({
                            url: "BD_Warehouse.del.xhd",
                            type: "POST",
                            data: { id: row.ID, rnd: Math.random() },
                            dataType: "json",
                            success: function (result) {
                                $.ligerDialog.closeWaitting();

                                var obj = eval(result);

                                if (obj.isSuccess) {
                                    f_reload();
                                }
                                else {
                                    top.$.ligerDialog.error(obj.Message);
                                }
                            },
                            error: function () {
                                $.ligerDialog.error('删除失败！');
                            }
                        });
                    }
                });
            } else {
                $.ligerDialog.warn("请选择数据");
            }
        }
 

        function f_save(item, dialog) {
            var issave = dialog.frame.f_save();
            if (issave) {

                $.ajax({
                    url: "BD_Warehouse.save.xhd",
                    type: "POST",
                    data: issave,
                    dataType: "json",
                    beforesend: function () {
                        $.ligerDialog.waitting('数据保存中,请稍候...');
                    },
                    success: function (result) {
                        $.ligerDialog.closeWaitting();

                        var obj = eval(result);

                        if (obj.isSuccess) {
                            dialog.close();
                            f_reload();
                        }
                        else {
                          alert('操作失败，错误信息：' + obj.Message);
                        //  top.$.ligerDialog.error(obj.Message);
                        }

                        //f_openWindow('CRM/Customer/Customer_add.aspx?cid=' + row.id, "添加联系人", 770, 490, f_save);
                    },
                    error: function  (XMLHttpRequest, textStatus, errorThrown)  {
                        $.ligerDialog.closeWaitting();
                        $.ligerDialog.error('操作失败！' + XMLHttpRequest.status + textStatus + errorThrown);
                    }
                });

            }
        }

        function f_reload() {
            var manager = $("#maingrid4").ligerGetGridManager();
            manager.loadData(true);
        }



    </script>
    <style type="text/css">
        .l-treeleve1 { background: yellow; }

        .l-treeleve2 { background: yellow; }

        .l-treeleve3 { background: #eee; }
    </style>
</head>
<body>
    <div style="padding: 10px;">
        <div id="toolbar"></div>

        <div id="grid" style="">
            <form id="form1" onsubmit=" return false ">
                <div id="maingrid4" style=" min-width: 800px;"></div>
            </form>

        </div>
    </div>




    <div class="az" style="padding-left: 10px;">
        <form id='serchform'>

            <div style="">
                <table style='width: 960px' class="aztable">
                    <tr>
                        <td>
                            <div style='float: right; text-align: right; width: 60px;'>客户名称：</div>
                        </td>
                        <td>
                            <input type='text' id='company' name='company' ltype='text' ligerui='{width:120}' /></td>

                        <td>
                            <div style='float: right; text-align: right; width: 60px;'>客户类型：</div>
                        </td>
                        <td>
                            <div style='float: left; width: 100px;'>
                                <input type='text' id='customertype' name='customertype' />
                            </div>
                            <div style='float: left; width: 98px;'>
                                <input type='text' id='customerlevel' name='customerlevel' />
                            </div>
                        </td>
                        <td>
                            <div style='float: right; text-align: right; width: 60px;'>录入时间：</div>
                        </td>
                        <td>
                            <div style='float: left; width: 100px;'>
                                <input type='text' id='startdate' name='startdate' ltype='date' ligerui='{width:97}' />
                            </div>
                            <div style='float: left; width: 98px;'>
                                <input type='text' id='enddate' name='enddate' ltype='date' ligerui='{width:96}' />
                            </div>
                        </td>
                        <td>
                            <%--<input id='keyword' name="keyword" type='text' ltype='text' ligerui='{width:196, nullText: "输入关键词搜索地址、描述、备注"}' />--%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style='float: right; text-align: right; width: 60px;'>所属行业：</div>
                        </td>
                        <td>
                            <input id='industry' name="industry" type='text' /></td>

                        <td>
                            <div style='float: right; text-align: right; width: 60px;'>所属地区：</div>
                        </td>
                        <td>
                            <div style='float: left; width: 100px;'>
                                <input type='text' id='T_Provinces' name='T_Provinces' />
                            </div>
                            <div style='float: left; width: 98px;'>
                                <input type='text' id='T_City' name='T_City' />
                            </div>
                        </td>
                        <td>
                            <div style='float: right; text-align: right; width: 60px;'>最后跟进：</div>
                        </td>
                        <td>
                            <div style='float: left; width: 100px;'>
                                <input type='text' id='startfollow' name='startfollow' ltype='date' ligerui='{width:97}' />
                            </div>
                            <div style='float: left; width: 98px;'>
                                <input type='text' id='endfollow' name='endfollow' ltype='date' ligerui='{width:96}' />
                            </div>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <div style='float: right; text-align: right; width: 60px;'>电话：</div>
                        </td>
                        <td>
                            <input type='text' id='tel' name='tel' ltype='text' ligerui='{width:120}' />
                        </td>

                        <td>
                            <div style='float: right; text-align: right; width: 60px;'>客户来源：</div>
                        </td>
                        <td>
                            <input type='text' id='cus_sourse' name='cus_sourse' />
                        </td>
                        <td>
                            <div style='float: right; text-align: right; width: 60px;'>业务员：</div>
                        </td>
                        <td>
                            <div style='float: left; width: 100px;'>
                                <input type='text' id='department' name='department' />
                            </div>
                            <div style='float: left; width: 98px;'>
                                <input type='text' id='employee' name='employee' />
                            </div>
                        </td>
                        <td>
                            <div id="btn_serch"></div>
                            <div id="btn_reset"></div>
                        </td>
                    </tr>
                </table>

            </div>
        </form>
    </div>


    <form id='toexcel'></form>
</body>
</html>
