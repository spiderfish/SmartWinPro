<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>

    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" />
    <link href="../lib/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" />
    <link href="../CSS/input.css" rel="stylesheet" type="text/css" />
    <script src="../lib/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/ligerui.min.js" type="text/javascript"></script> 
    <script src="../JS/XHD.js" type="text/javascript"></script>
    <script src="../lib/jquery.form.js" type="text/javascript"></script>

    <script type="text/javascript" id="checkData">

        var manager = "",g1,g2;
        var treemanager;
        $(function () {
            $("#layout1").ligerLayout({ leftWidth: 200, allowLeftResize: false, allowLeftCollapse: true, space: 2, heightDiff: -5 });
            $("#tree1").ligerTree({
                url: 'Basic_Dorm.tree.xhd?rnd=' + Math.random(),
                onSelect: onSelect,
                idFieldName: 'id',
                //parentIDFieldName: 'pid',
                usericon: 'd_icon',
                checkbox: false,
                itemopen: false,
                onSuccess: function () {
                    $(".l-first div:first").click();
                }
            });

            treemanager = $("#tree1").ligerGetTreeManager();

            initLayout();
            $(window).resize(function () {
                initLayout();
            });

            $("#maingrid4").ligerGrid({
                columns: [
                    //{ display: '序号', width: 50, render: function (item,i) { return item.n; } },
                    { display: '舍栏名称', name: 'corralName', width: 120 },
                    { display: '所住家禽', name: 'barcode', width: 120 },
                    { display: '温度℃', name: 'temperature', width: 120 },
                    { display: '湿度', name: 'humidity', width: 120 }, 
                    {
                        display: '检测时间', name: 'checkData', width: 190, render: function (item) {
                            return formatTimebytype(item.checkData, 'yyyy-MM-dd');
                        }
                    }

                ],
                 
                dataAction: 'server',
                url: "Basic_Corral.gridHealth.xhd?dornId=0&rnd=" + Math.random(),
                pageSize: 30,
                pageSizeOptions: [20, 30, 50, 100],
                width: '100%',
                height: '100%',
                heightDiff: -8, 
                onContextmenu: function (parm, e) {
                    actionCustomerID = parm.data.id;
                    menu.show({ top: e.pageY, left: e.pageX });
                    return false;
                } 
            });
            
            toolbar();
        });

        function toolbar() {

            var items = [];

            items.push({ type: 'button', text: '添加', icon: '../images/add.gif', disable: true, click: function () { add() } });

            items.push({ type: 'button', text: '日志', icon: '../images/search.gif', disable: true, click: function () { viewLog() } });
            $("#toolbar").ligerToolBar({
                items: items

            });

        }

        function onSelect(note) {
            var manager = $("#maingrid4").ligerGetGridManager();
            var url = "Basic_Corral.gridHealth.xhd?dornId=" + note.data.id + "&rnd=" + Math.random();
            manager._setUrl(url);
        }
        //查询
        function doserch() {
            var sendtxt = "&rnd=" + Math.random();
            var serchtxt = $("#form1 :input").fieldSerialize() + sendtxt;

            var manager = $("#maingrid4").ligerGetGridManager();
            manager._setUrl("Basic_Corral.gridHealth.xhd?" + serchtxt);
        }

        //重置
        function doclear() {
            //var serchtxt = $("#serchform :input").reset();
            $("#form1").each(function () {
                this.reset();
            });
        }
      
        function add() {
            var notes = $("#tree1").ligerGetTreeManager().getSelected();

            if (notes != null && notes != undefined) {
                var manager = $("#maingrid4").ligerGetGridManager();
                var rows = manager.getSelectedRow();
                if (rows && rows != undefined) {
                    f_openWindow('product/boar_health_bc_add_add.aspx?cid=' + rows.id + "&bid=" + notes.data.id, "更新记录", 700, 380, f_save);
                }
                else {
                    $.ligerDialog.warn('请选择成品！');
                }
            }
        }
        function viewLog() {
            var notes = $("#tree1").ligerGetTreeManager().getSelected();

            if (notes != null && notes != undefined) {
                var manager = $("#maingrid4").ligerGetGridManager();
                var rows = manager.getSelectedRow();
                if (rows && rows != undefined) {
                    f_openWindow('product/boar_health_bc_add_log.aspx?cid=' + rows.id + "&bid=" + notes.data.id, "记录列表", 700, 380, f_save);
                }
                else {
                    $.ligerDialog.warn('请选择成品！');
                }
            }
        }
          
        function f_save(item, dialog) {
            var issave = dialog.frame.f_save();
            if (issave) {
                dialog.close();
                $.ligerDialog.waitting('数据保存中,请稍候...');
                $.ajax({
                    url: "Basic_Corral.saveLog.xhd", type: "POST",
                    data: issave,
                    dataType: 'json',
                    success: function (result) {
                        $.ligerDialog.closeWaitting();

                        var obj = eval(result);

                        if (obj.isSuccess) {
                            f_load();
                        }
                        else {
                            $.ligerDialog.error(obj.Message);
                        }
                        //f_load();     
                    },
                    error: function () {
                        $.ligerDialog.closeWaitting();
                        $.ligerDialog.error('操作失败！');
                    }
                });

            }
        }
        function f_load() {
            var manager = $("#maingrid4").ligerGetGridManager();
            manager.loadData(true);
        }

    </script>
</head>
<body style="padding: 0px; overflow: hidden;">
    <div style="padding: 5px 10px 0px 5px;">
         <div id="toolbar"></div>
        <form id="form1" onsubmit="return false">
            <div id="layout1" style="">
                <div position="left" title="畜舍">
                    <div id="treediv" style="width: 200px; height: 100%; margin: -1px; float: left; overflow: auto;margin-top:2px;">
                        <ul id="tree1"></ul>
                    </div>
                </div>
                <div position="center">
                    <div id="toolbar" style="margin-top: 10px;"></div>
                    <div id="maingrid4" style="margin: -1px;"></div>

                </div>
            </div>
        </form>

    </div>
</body>
</html>
