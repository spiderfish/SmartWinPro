<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" />
    <link href="../lib/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" />

    <script src="../lib/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/ligerui.min.js" type="text/javascript"></script>

    <script src="../lib/jquery-validation/jquery.validate.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/messages_cn.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/common.js" type="text/javascript"></script>
    <script src="../lib/jquery.form.js" type="text/javascript"></script>
     <%--<script src="../JS/JsBarcode.all.min.js"></script>--%>
    <script src="../JS/XHD.js" type="text/javascript"></script>
    <script type="text/javascript">
        //图标
        var jiconlist, winicons, currentComboBox;
        $(function () {
            $.metadata.setType("attr", "validate");
            XHD.validate($(form1));

            //$("#T_Contract_name").focus();
            $("form").ligerForm();  

            loadForm(getparastr("bid"));
            toolbar();
           
        });
        function toolbar() {
           
                var items = []; 
             items.push({ type: 'button', text: '系谱图', icon: '../images/search.gif', disable: true, click: function () { xpt() } });
            items.push({ type: 'button', text: '系谱图编辑', icon: '../images/edit.gif', disable: true, click: function () { xptedit() } });

                $("#toolbar").ligerToolBar({
                    items: items

                });
           
        }
        function f_save() {
            if ($(form1).valid()) {
                var sendtxt = "&bid=" + getparastr("bid") + "&id=" + getparastr("id");
                return $("form :input").fieldSerialize() + sendtxt;
            }
        }

        function loadForm(oaid) {
            $.ajax({
                type: "GET",
                url: "Basic_Boar_sys.form.xhd", /* 注意后面的名字对应CS的方法名称 */
                data: { id: oaid, rnd: Math.random() }, /* 注意参数的格式和名称 */
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    var obj = eval(result);
                    for (var n in obj) { 
                    } 
                    $("#T_license").val(obj.license);
                    $("#T_description").val(obj.description);
                    $("#T_level").val(obj.levels);
                    $("#T_outDate").val(formatTimebytype(obj.outDate,"yyyy-MM-dd"));
                    $("#T_bithsNumber").val(obj.bithsNumber);
                    $("#T_sameNestsNumber").val(obj.sameNestsNumber);
                    $("#T_stopmilkweight").val(obj.stopMilkWeight);
                    $("#T_stopmilkdate").val(formatTimebytype(obj.stopMilkDate, "yyyy-MM-dd")); 
                    $("#T_rightNip").val(obj.rightNip);
                    $("#T_leftNip").val(obj.leftNip);   
                    $("#T_bithPlace").val(obj.birthPlace);
                    $("#T_weight").val(obj.weight);
                }
            });
        }
        function xpt() {
            f_openWindow('product/boar_sysId_map.aspx?bid=' + getparastr("bid"), "系谱图", 1000, 600);

        }
    
        function xptedit() {
            f_openWindow('product/boar_sysId_map_edit.aspx?bid=' + getparastr("bid"), "系谱图编辑", 1000, 600);
        }

    </script>
    <style type="text/css">
        .iconlist { width: 360px; padding: 3px; }
            .iconlist li { border: 1px solid #FFFFFF; float: left; display: block; padding: 2px; cursor: pointer; }
                .iconlist li.over { border: 1px solid #516B9F; }
                .iconlist li img { height: 16px; height: 16px; }
    </style>

</head>
<body style="padding: 0px">
       <div id="toolbar"></div>
    <form id="form1" onsubmit="return false">
        <table border="0" cellpadding="3" cellspacing="1" style="background: #fff; width: 400px;" class="aztable">

            <tr>
                <td height="23" style="width: 85px"  >

                    <div align="right" style="width: 100px">体重(kg)：</div>
                </td>
                <td height="23">

                    <input type="text" id="T_weight" name="T_weight" ltype="float" ligerui="{width:180}" validate="{required:true}" />

                </td>
                <td height="23" style="width: 85px"  >

                    <div align="right" style="width: 100px"> 出生地：</div>
                </td>
                <td height="23">

                    <input type="text" id="T_bithPlace" name="T_bithPlace" ltype="text" ligerui="{width:180}" validate="{required:true}" />

                </td>
            </tr>
            <tr>
                <td height="23"  >

                    <div align="right" style="width: 100px">左乳头：</div>
                </td>
                <td height="23">
                    <input type="text" id="T_leftNip" name="T_leftNip" ltype="int" ligerui="{width:180}" validate="{required:true}" />
                </td>
                <td height="23"  >

                    <div align="right" style="width: 100px">右乳头：</div>
                </td>
                <td height="23">
                    <input type="text" id="T_rightNip" name="T_rightNip" ltype="int" ligerui="{width:180}" validate="{required:true}" />
                </td>
            </tr>
            <tr>
                <td height="23"  >

                    <div align="right" style="width: 100px">断乳日期：</div>
                </td>
                <td height="23">
                    <input type="text" id="T_stopmilkdate" name="T_stopmilkdate" ltype="date" ligerui="{width:180}" validate="{required:true}" />
                </td>
                <td height="23"  >

                    <div align="right" style="width: 100px">断乳体重：</div>
                </td>
                <td height="23">
                    <input type="text" id="T_stopmilkweight" name="T_stopmilkweight" ltype="float" ligerui="{width:180}" validate="{required:true}" />
                </td>
            </tr>
            <tr>
                <td height="23"  >

                    <div align="right" style="width: 100px">同窝头数：</div>
                </td>
                <td height="23">
                    <input type="text" id="T_sameNestsNumber" name="T_sameNestsNumber" ltype="int" ligerui="{width:180}" validate="{required:true}" />
                </td>
                <td height="23"  >

                    <div align="right" style="width: 100px">出生胎次：</div>
                </td>
                <td height="23">
                    <input type="text" id="T_bithsNumber" name="T_bithsNumber" ltype="int" ligerui="{width:180}" validate="{required:true}" />
                </td>
            </tr>
                <tr>
                <td height="23"  >

                    <div align="right" style="width: 100px">出场日期：</div>
                </td>
                <td height="23">
                    <input type="text" id="T_outDate" name="T_outDate" ltype="date" ligerui="{width:180}" validate="{required:true}" />
                </td>
                <td height="23"  >

                    <div align="right" style="width: 100px">级别：</div>
                </td>
                <td height="23">
                    <input type="text" id="T_level" name="T_level" ltype="text" ligerui="{width:180}" validate="{required:true}" />
                </td>
            </tr> 
                <tr>
                <td height="23"  >

                    <div align="right" style="width: 100px"> 外形特征：</div>
                </td>
                <td height="23">
                    <input type="text" id="T_description" name="T_description" ltype="text" ligerui="{width:180}" validate="{required:true}" />
                </td>
                <td height="23"  >

                    <div align="right" style="width: 100px">许可证编号：</div>
                </td>
                <td height="23">
                    <input type="text" id="T_license" name="T_license" ltype="text" ligerui="{width:180}" validate="{required:true}" />
                </td>
            </tr>
         
            
        </table>
    </form>
</body>
</html>
