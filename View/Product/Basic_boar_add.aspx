<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" />
    <link href="../lib/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" />
    <link href="../CSS/input.css" rel="stylesheet" type="text/css" />

    <script src="../lib/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/ligerui.min.js" type="text/javascript"></script>

    <script src="../lib/jquery-validation/jquery.validate.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/messages_cn.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/common.js" type="text/javascript"></script>
    <script src="../JS/XHD.js" type="text/javascript"></script>
    <script src="../lib/jquery.form.js" type="text/javascript"></script>
    <script src="../JS/JsBarcode.all.min.js"></script>
    <script src="../JS/printTable/jquery.jqprint-0.3.js"></script> 
    <script type="text/javascript">
        $(function () {
            $.metadata.setType("attr", "validate");
            XHD.validate($(form1));

            //$("#T_Contract_name").focus();
       
            loadForm(getparastr("pid"));
            if (getparastr("isprint") == "Y") {
                var items = [];
                items.push({ type: 'button', text: '打印', icon: '../images/search.gif', disable: true, click: function () { print() } });
                $("#toolbar").ligerToolBar({
                    items: items

                });
                //menu = $.ligerMenu({
                //    width: 120, items: getMenuItems(data)
                //});
            }
            $("#uploadimg").ligerButton({ text: "上传照片", width: 130, click: uploadimg });
        });
        function print() {
            $("#barcode").jqprint({
                debug: false,
                importCSS: true,
                printContainer: true,
                operaSupport: false
            });
        }
        function f_save() {
            if ($(form1).valid()) {
                var sendtxt = "&pid=" + getparastr("pid");
                return $("form :input").fieldSerialize() + sendtxt;
            }
        }

        function loadForm(oaid) {
            $.ajax({
                type: "get",
                url: "Basic_boar.form.xhd", /* 注意后面的名字对应CS的方法名称 */
                data: { Action: 'form', id: oaid, rnd: Math.random() }, /* 注意参数的格式和名称 */
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    var obj = eval(result);
                    for (var n in obj) {
                        if (obj[n] == "null" || obj[n] == null)
                            obj[n] = "";
                    }
                    //alert(obj.constructor); //String 构造函数

                    //$("#T_product_category").ligerGetComboBoxManager().selectValue(obj.category_id);
                   // alert(getparastr("specifications"));
                    if (!obj.category_id) {
                        obj.category_id = getparastr("categoryid");
                        obj.product_name = urldecode( getparastr("specifications"));
                    }

                    if (!obj.img_url)
                        obj.img_url = "images/noheader.png";
                    //alert(obj.img_url);
                    $("#headurl").val(obj.img_url);
                    $("#userheadimg").attr("src", "../Images/upload/Basicboar/" + obj.img_url);
                    var NowDate = getNowFormatDate();
                    $("#form1").ligerAutoForm({
                        labelWidth: 80, inputWidth: 180, space: 20,
                        fields: [
                            {
                                display: '种禽维护', type: 'group', icon: '',
                                rows: [
                                    
                                    [
                                        { display: "ID", name: "T_txm", type: "text", width: 180, validate: "{required:true,minlength:15,maxlength:15}", initValue: obj.barcode },
                                         { display: "简短编码", name: "T_shortcode", type: "text",  width: 180, validate: "{required:true}", initValue: obj.shortcode },
                                    ],
                                    [
                                        { display: "种源名称", name: "T_product_name", type: "text", options: "{width:180}", validate: "{required:true}", initValue: obj.product_name },
                                        { display: "种源类型", name: "T_product_category",   type: "select", options: "{width:180,treeLeafOnly: false,tree:{url:'Product.tree.xhd',idFieldName: 'id',checkbox: false},value:'" + obj.category_id + "'}", validate: "{required:true}" }
                                        //{ display: "产品类别", name: "T_product_category", type: "select", options: "{width:180,treeLeafOnly: false,tree:{url:'Sys_Param.tree.xhd?type=poultry_type',idFieldName: 'id',checkbox: false},value:'" + obj.category_id + "'}", validate: "{required:true}" }
                                    ],
                                    [
                                        //{ display: "成本价", name: "T_cost", type: "text", options: "{width:180,onChangeValue:function(value){ $('#T_cost').val(toMoney(value)); }}", validate: "{required:true}", initValue: toMoney( obj.cost) },
                                        { display: "种源规格", name: "T_specifications", type: "text", options: "{width:180}", initValue: obj.specifications },
                                        { display: "单位", name: "T_product_unit", type: "select", options: "{width:180,treeLeafOnly: false,tree:{url:'Sys_Param.tree.xhd?type=units',idFieldName: 'id',checkbox: false},value:'" + obj.unit_id + "'}", validate: "{required:true}" } 

                                    ],
                                    [
                                        { display: "来源", name: "T_origin", type: "text", options: "{width:180}", initValue: obj.origin },
                                        { display: "场地", name: "T_place", type: "text", options: "{width:180}", validate: "{required:true}", initValue: obj.place }

                                    ],
                                    [
                                        { display: "畜舍", name: "T_dormId",},
                                        { display: "畜栏", name: "T_corralId" }

                                    ],
                                    [
                                        { display: "进场时间", name: "T_InDate", type: "date", options: "{width:180}", initValue: formatTimebytype(obj.InDate, "yyyy-MM-dd") == "" ? NowDate:formatTimebytype(obj.InDate, "yyyy-MM-dd") , validate: "{required:true}"},
                                        { display: "系谱编号", name: "T_sysId", type: "text", options: "{width:180}",  initValue: obj.sysId  }

                                    ],
                                    [
                                        { display: "性别", name: "T_sex", type: "select", options: "{width:180,treeLeafOnly: false,tree:{data:[{id:'公',text:'公'},{id:'母',text:'母'}],idFieldName: 'id',checkbox: false},value:'" + (obj.sex=='null'?'公':obj.sex) + "'}" },
                                        { display: "品系", name: "T_Pro_SeriesId" }
                                        //{ display: "天数", name: "T_day", type: "int", options: "{width:180,onChangeValue:function(value){ set_tobir(value); }}", validate: "{required:true}",initValue: obj.nlday },
                                    ],
                                    [
                                        //{ display: "品种", name: "T_productId" },
                                       

                                    ],
                                    [
                                        { display: "等级", name: "T_Pro_Series_LevelId"},

                                        { display: "出生日期", name: "T_bir", type: "text", validate: "{required:true}", initValue: formatTimebytype(obj.birthday, "yyyy-MM-dd")  }
                                    ],
                                    [
                                        { display: "备注", name: "T_remarks", type: "textarea", cols: 73, rows: 4, width: 465, cssClass: "l-textarea", initValue: obj.remarks }
                                    ]
                                ]
                            }
                        ]
                    });
                    //alert(obj.barcode);
                    $("#T_product_category").ligerGetComboBoxManager()._setReadonly(1);
                    JsBarcode("#barcode", obj.barcode, {
                        width: 2,
                        height: 20,
                        displayValue: true
                    });

                    f = $('#T_corralId').ligerComboBox({
                        width: 180,
                        url: "Basic_Corral.combo.xhd?id=" + obj.dormId + "&rnd=" + Math.random(),
                        value: obj.corralId,
                        emptyText: '（空）'
                    });
                    e = $('#T_dormId').ligerComboBox({
                        width: 180,
                        url: "Basic_Dorm.combo.xhd?rnd=" + Math.random(),
                        value: obj.dormId,
                        onBeforeSelect: function (newvalue) { 
                            url = "Basic_Corral.combo.xhd?id=" + newvalue + "&rnd=" + Math.random(),
                                f.setUrl(url);
                        },
                        emptyText: '（空）'
                    });


                    d = $('#T_Pro_Series_LevelId').ligerComboBox({
                        width: 180,
                        url: "Basic_Series_Level.combo.xhd?id=" + obj.Pro_SeriesId + "&rnd=" + Math.random(),
                        value: obj.Pro_Series_LevelId,
                        emptyText: '（空）'
                    });
                    c = $('#T_Pro_SeriesId').ligerComboBox({
                        width: 180,
                        url: "Basic_Series.combo.xhd?id=" + obj.category_id + "&rnd=" + Math.random(),
                        value: obj.Pro_SeriesId,
                        onBeforeSelect: function (newvalue) { 
                            url = "Basic_Series_Level.combo.xhd?id=" + newvalue + "&rnd=" + Math.random(),
                                d.setUrl(url);
                        },
                        emptyText: '（空）'
                    });
                    //var url222 = "Basic_Series.combo.xhd?id=" + obj.category_id + "&rnd=" + Math.random();
                    //    c.setUrl(url222);
                    
                    //b = $('#T_product_category').ligerComboBox({
                    //    width: 180,
                    //      value: obj.productId,
                    //    url: "Product.combo.xhd?rnd=" + Math.random(),
                    //    onBeforeSelect: function (newvalue) { 
                    //        url = "Basic_Series.combo.xhd?id=" + newvalue + "&rnd=" + Math.random(),
                    //            c.setUrl(url);
                    //    }, emptyText: '（空）'
                    //});


                }
            });

          
        }

        function set_tomoney(value) {
            $("#T_price").val(toMoney(value));
        }
        function set_tobir(value) { 
            var v = GetDateStr(- value);
            $("#T_bir").val(v);
        }
        function GetDateStr(AddDayCount) {
            var dd = new Date();
            dd.setDate(dd.getDate() + AddDayCount);//获取AddDayCount天后的日期
            var y = dd.getFullYear();
            var m = (dd.getMonth() + 1) < 10 ? "0" + (dd.getMonth() + 1) : (dd.getMonth() + 1);//获取当前月份的日期，不足10补0
            var d = dd.getDate() < 10 ? "0" + dd.getDate() : dd.getDate();//获取当前几号，不足10补0
            return y + "-" + m + "-" + d;
        }

        function uploadimg() {
            top.$.ligerDialog.open({
                zindex: 9004,
                width: 800, height: 500,
                title: '上传种源图片',
                url: 'Product/uploadimage.aspx',
                buttons: [
                    {
                        text: '保存', onclick: function (item, dialog) {
                            saveheadimg(item, dialog);
                        }
                    },
                    {
                        text: '关闭', onclick: function (item, dialog) {
                            dialog.close();
                        }
                    }
                ],
                isResize: true
            });
        }

        function saveheadimg(item, dialog) {
            var upfile = dialog.frame.f_save();
            //alert(upfile);
            if (upfile) {
                dialog.close();
                $.ligerDialog.waitting('数据保存中,请稍候...');

                $.ajax({
                    url: "Basic_boar.Base64Upload.xhd", type: "POST",
                    data: upfile,
                    dataType: "json",
                    success: function (result) {
                        $.ligerDialog.closeWaitting();

                        var obj = eval(result);
                        if (obj.isSuccess) {
                            $("#headurl").val(obj.Message);
                            $("#userheadimg").attr("src", "../Images/upload/Basicboar/" + obj.Message);
                        }
                        else {
                            $.ligerDialog.error(obj.Message);
                        }


                    },
                    error: function () {
                        $.ligerDialog.closeWaitting();
                        $.ligerDialog.error('操作失败！');
                    }
                });
            }
        }


    </script>
</head>
<body>
     <div style="padding: 10px;">
         
           <div id="toolbar" style="margin-top: 10px;"></div>
    <form id="form1" onsubmit="return false">
   
                   
    </form>
         </div>
    <div>  <img id="barcode"  height="0" /> 
          </div>
    <div>
         <form id="form2" onsubmit="return false">
                   <img id="userheadimg" src="a.gif" onerror="noheadimg()" style="border-radius: 4px; box-shadow: 1px 1px 3px #111; width: 220px; height: 150px; margin-left: 5px; background: #d2d2f2; border: 3px solid #fff; behavior: url(../css/pie.htc);" />

          <input type="hidden" id="headurl" name="headurl" />
                        <div style="text-align: center">
                            <div id="uploadimg"></div>
                        </div>
             </form>
    </div>
</body>
</html>
