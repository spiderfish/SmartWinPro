<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" />
    <link href="../lib/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" />

    <script src="../lib/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/ligerui.min.js" type="text/javascript"></script>

    <script src="../lib/jquery-validation/jquery.validate.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/messages_cn.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/common.js" type="text/javascript"></script>
    <script src="../lib/jquery.form.js" type="text/javascript"></script>
     <%--<script src="../JS/JsBarcode.all.min.js"></script>--%>
    <script src="../JS/XHD.js" type="text/javascript"></script>
    <script type="text/javascript">
        //图标
        var jiconlist, winicons, currentComboBox;
        var boarId = "";
        $(function () {
            $.metadata.setType("attr", "validate");
            XHD.validate($(form1));

            //$("#T_Contract_name").focus();
            $("form").ligerForm();

            loadForm(getparastr("bid"));

            $("#maingrid4").ligerGrid({
                columns: [
                    { display: '序号', width: 50, render: function (item,i) { return item.n; } }, 
                    { display: '体重', name: 'weight', width: 59 },
                    { display: '背膘', name: 'backfat', width: 59 },
                    { display: '眼肌面积', name: 'eyeArea', width: 59 },
                    { display: '体温', name: 'temperature', width: 59 },
                    {
                        display: 'PCR检测', columns:
                            [

                                { display: '血清', name: 'serum', width: 59 },
                                { display: '精液', name: 'semen', width: 59 },
                            ]
                    },
                    {
                        display: 'ELISA检测', columns:
                            [

                                { display: '饮水', name: 'drinkWater', width: 59 },
                                { display: '饲料', name: 'feed', width: 59 },
                                { display: '粪便', name: 'Feces', width: 59 },
                                { display: '唾液', name: 'saliva', width: 59 },
                            ]
                    },
                    {
                        display: '其他检测', columns:
                            [
                                { display: '猪体卫生', name: 'pigHeath', width: 59 },
                                { display: '舍内卫生', name: 'dormHeath', width: 59 },
                                { display: '其他', name: 'others', width: 59 }, 
                            ]
                    },
                   
                    { display: '备注', name: 'remarks', width: 59 }, 
                    { display: '监测人', name: 'monitorPerson', width: 59 }, 
                    { display: '监测日期', name: 'monitorDate', width: 120 }

                ],
                dataAction: 'server',
                url: "Basic_Boar_Heath.grid.xhd?id=" + getparastr("bid")+"&rnd=" + Math.random(),
                pageSize: 30,
                pageSizeOptions: [20, 30, 50, 100],
                width: '100%',
                height: '100%',
                heightDiff: -8,

                onContextmenu: function (parm, e) {
                    actionCustomerID = parm.data.id;
                    menu.show({ top: e.pageY, left: e.pageX });
                    return false;
                }
            });
            if (getparastr("IsView") != 'Y') 
            toolbar();

        });
        function toolbar() {

            var items = [];
            items.push({ type: 'button', text: '选择种畜', icon: '../images/arrow.gif', disable: true, click: function () { select() } });
            items.push({ type: 'button', text: '添加监测', icon: '../images/edit.gif', disable: true, click: function () { add() } });
             

            $("#toolbar").ligerToolBar({
                items: items

            });

        }


        function select() {
            $.ligerDialog.open({
                zindex: 19009, title: '选择种禽', width: 650, height: 590, url: 'GetBasic_boar.aspx', buttons: [
                    { text: '确定', onclick: f_selectorderOK },
                    { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
                ]
            });
        }
        function f_selectorderOK(item, dialog) {
            var obj = dialog.frame.f_select(); 
            if (!obj) {
                alert('请选择行!');
                return;
            } 
            boarId = obj.id
            loadForm(obj.id) 
            doserch(obj.id)
            dialog.close();
        }
        //新增监测
        function add() {
            if (boarId == "") {
                $.ligerDialog.warn('请先选择一个有效种禽！');
                return;
            }
            f_openWindow('product/boar_health_board_add.aspx?boarId=' + boarId, "新增种禽监测", 700, 500,f_saveadd);
          //  f_openWindow("boar_health_board_add");
        }
        function f_save() {
            if ($(form1).valid()) {
                var sendtxt = "&id=" + getparastr("bid");
                return $("form :input").fieldSerialize() + sendtxt;
            }
        }
        function doserch(id) { 
            var manager = $("#maingrid4").ligerGetGridManager();
            manager._setUrl("Basic_Boar_Heath.grid.xhd?id=" + id+"&rnd=" + Math.random());
        }
        function loadForm(oaid) {
            $.ajax({
                type: "GET",
                url: "Basic_Boar_Heath.formFull.xhd", /* 注意后面的名字对应CS的方法名称 */
                data: { id: oaid, rnd: Math.random() }, /* 注意参数的格式和名称 */
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    var obj = eval(result);
                    for (var n in obj) {

                    } 
                    $("#T_barcode").val(obj.barcode);
                    $("#T_name").val(obj.product_name1);
                    $("#T_pz").val(obj.product_name);
                    $("#T_pxlb").val(obj.product_category);
                    $("#T_px").val(obj.SeriesName);
                    $("#T_birthday").val(obj.birthday);
                    $("#T_ylts").val(obj.nlday);
                    $("#T_weight").val(obj.weight);
                    //
                    $("#T_InDate").val(obj.InDate);
                    $("#T_source").val(obj.origin);
                    $("#T_zt").val(obj.CollectionStatus);

                    $("#T_xpbh").val(obj.sysId);
                    $("#T_cd").val(obj.place);
                    $("#T_dorm").val(obj.dormName);
                    $("#T_corral").val(obj.corralName);
                    $("#T_fjh").val(obj.shortcode);
                    
                }
            });
        }
        function f_saveadd(item, dialog) {

            var issave = dialog.frame.f_save();
            if (issave) {
                dialog.close(); 
                $.ligerDialog.waitting('数据保存中,请稍候...');
                $.ajax({
                    url: "Basic_Boar_Heath.save.xhd", type: "POST",
                    data: issave,
                    dataType: 'json',
                    success: function (result) {
                        $.ligerDialog.closeWaitting();
                        var obj = eval(result);

                        if (obj.isSuccess) {
                            f_reload();
                        }
                        else {
                            $.ligerDialog.error(obj.Message);
                        }
                        //f_load();     
                    },
                    error: function () {
                        $.ligerDialog.closeWaitting();
                        $.ligerDialog.error('操作失败！');
                    }
                });
            }
        }
        function f_reload() {
            var manager = $("#maingrid4").ligerGetGridManager();
            manager.loadData(true);
        }
    </script>
    <style type="text/css">
        .iconlist { width: 360px; padding: 3px; }
            .iconlist li { border: 1px solid #FFFFFF; float: left; display: block; padding: 2px; cursor: pointer; }
                .iconlist li.over { border: 1px solid #516B9F; }
                .iconlist li img { height: 16px; height: 16px; }
    </style>

</head>
<body style="padding: 0px">
       <div id="toolbar"></div>
    <form id="form1" onsubmit="return false">
        <table border="0" cellpadding="3" cellspacing="1" style="background: #fff; width: 590px;" class="aztable">

            <tr>
                <td height="23" style="width: 85px"  >

                    <div align="left" style="width: 62px">耳标：</div>
                </td>
                <td height="23">

                    <input type="text" id="T_barcode" name="T_barcode" ltype="text" ligerui="{width:180,readonly:true}" validate="{required:true}" />

                </td>
                <td height="23" style="width: 85px"  >

                    <div align="left" style="width: 62px"> 名称：</div>
                </td>
                <td height="23">

                    <input type="text" id="T_name" name="T_name" ltype="text" ligerui="{width:180,readonly:true}" validate="{required:true}" />

                </td>
          
                <td height="23"  >

                    <div align="left" style="width: 62px">品种类别：</div>
                </td>
                <td height="23">
                    <input type="text" id="T_pzlb" name="T_pzlb" ltype="text" ligerui="{width:180,readonly:true}" validate="{required:true}" />
                </td>
                <td height="23"  >

                    <div align="left" style="width: 62px">品系：</div>
                </td>
                <td height="23">
                    <input type="text" id="T_px" name="T_px" ltype="text" ligerui="{width:180,readonly:true}" validate="{required:true}" />
                </td>
            </tr>
            <tr>
                <td height="23"  >

                    <div align="left" style="width: 62px">品种：</div>
                </td>
                <td height="23">
                    <input type="text" id="T_pz" name="T_pz" ltype="text" ligerui="{width:180,readonly:true}" validate="{required:true}" />
                </td>
                <td height="23"  >

                    <div align="left" style="width: 62px">出生日期：</div>
                </td>
                <td height="23">
                    <input type="text" id="T_birthday" name="T_birthday" ltype="text" ligerui="{width:180,readonly:true}" validate="{required:true}" />
                </td>
            
                <td height="23"  >

                    <div align="left" style="width: 62px">月龄天数：</div>
                </td>
                <td height="23">
                    <input type="text" id="T_ylts" name="T_ylts" ltype="text" ligerui="{width:180,readonly:true}" validate="{required:true}" />
                </td>
                <td height="23"  >

                    <div align="left" style="width: 62px">体重：</div>
                </td>
                <td height="23">
                    <input type="text" id="T_weight" name="T_weight" ltype="text" ligerui="{width:180,readonly:true}" validate="{required:true}" />
                </td>
            </tr>
                <tr>
                <td height="23"  >

                    <div align="left" style="width: 62px">进场日期：</div>
                </td>
                <td height="23">
                    <input type="text" id="T_InDate" name="T_InDate" ltype="text" ligerui="{width:180,readonly:true}" validate="{required:true}" />
                </td>
                <td height="23"  >

                    <div align="left" style="width: 62px">来源：</div>
                </td>
                <td height="23">
                    <input type="text" id="T_source" name="T_source" ltype="text" ligerui="{width:180,readonly:true}" validate="{required:true}" />
                </td>
            
                <td height="23"  >

                    <div align="left" style="width: 62px"> 状态：</div>
                </td>
                <td height="23">
                    <input type="text" id="T_zt" name="T_zt" ltype="text" ligerui="{width:180,readonly:true}" validate="{required:true}" />
                </td>
                <td height="23"  >

                    <div align="left" style="width: 62px">系谱编号：</div>
                </td>
                <td height="23">
                    <input type="text" id="T_xpbh" name="T_xpbh" ltype="text" ligerui="{width:180,readonly:true}" validate="{required:true}" />
                </td>
            </tr>
          <tr>
                <td height="23"  >

                    <div align="left" style="width: 62px">场地：</div>
                </td>
                <td height="23">
                    <input type="text" id="T_cd" name="T_cd" ltype="date" ligerui="{width:180,readonly:true}" validate="{required:true}" />
                </td>
                <td height="23"  >

                    <div align="left" style="width: 62px">畜舍：</div>
                </td>
                <td height="23">
                    <input type="text" id="T_dorm" name="T_dorm" ltype="text" ligerui="{width:180,readonly:true}" validate="{required:true}" />
                </td>
            
                <td height="23"  >

                    <div align="left" style="width: 62px"> 畜栏：</div>
                </td>
                <td height="23">
                    <input type="text" id="T_corral" name="T_corral" ltype="text" ligerui="{width:180,readonly:true}" validate="{required:true}" />
                </td>
                <td height="23"  >

                    <div align="left" style="width: 62px">附加号：</div>
                </td>
                <td height="23">
                    <input type="text" id="T_fjh" name="T_fjh" ltype="text" ligerui="{width:180,readonly:true}" validate="{required:true}" />
                </td>
            </tr>
            
        </table>
         <div id="maingrid4" style="margin: -1px;"></div>
    </form>
</body>
</html>
