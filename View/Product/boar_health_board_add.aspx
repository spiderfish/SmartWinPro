<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" />
    <link href="../lib/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" />

    <script src="../lib/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/ligerui.min.js" type="text/javascript"></script>

    <script src="../lib/jquery-validation/jquery.validate.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/messages_cn.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/common.js" type="text/javascript"></script>
    <script src="../lib/jquery.form.js" type="text/javascript"></script>
     <%--<script src="../JS/JsBarcode.all.min.js"></script>--%>
    <script src="../JS/XHD.js" type="text/javascript"></script>
    <script type="text/javascript">
        //图标
        var jiconlist, winicons, currentComboBox;
        $(function () {
            $.metadata.setType("attr", "validate");
            XHD.validate($(form1));

            //$("#T_Contract_name").focus();
            $("form").ligerForm();
            $("#T_time").val(getNowFormatDate());  
          //  loadForm(getparastr("boarId")); 
           
        });
      
        function f_save() {
            if ($(form1).valid()) {
                var sendtxt = "&bid=" + getparastr("boarId") + "&id=" + getparastr("id");
                return $("form :input").fieldSerialize() + sendtxt;
            }
        }

        function loadForm(oaid) {
            $.ajax({
                type: "GET",
                url: "Basic_Boar_sys.form.xhd", /* 注意后面的名字对应CS的方法名称 */
                data: { id: oaid, rnd: Math.random() }, /* 注意参数的格式和名称 */
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    var obj = eval(result);
                    for (var n in obj) { 
                    } 
                    $("#T_license").val(obj.license);
                    $("#T_description").val(obj.description);
                    $("#T_level").val(obj.levels);
                    $("#T_outDate").val(obj.outDate);
                    $("#T_bithsNumber").val(obj.bithsNumber);
                    $("#T_sameNestsNumber").val(obj.sameNestsNumber);
                    $("#T_stopmilkweight").val(obj.stopMilkWeight);
                    $("#T_stopmilkdate").val(obj.stopMilkDate); 
                    $("#T_rightNip").val(obj.rightNip);
                    $("#T_leftNip").val(obj.leftNip);
                    $("#T_bithPlace").val(obj.birthPlace);
                    $("#T_weight").val(obj.weight);
                }
            });
        }
        function f_save() {
            if ($(form1).valid()) {
                var sendtxt = "&bid=" + getparastr("boarId");
                return $("form :input").fieldSerialize() + sendtxt;
            }
        }
    
        
    </script>
    <style type="text/css">
        .iconlist { width: 360px; padding: 3px; }
            .iconlist li { border: 1px solid #FFFFFF; float: left; display: block; padding: 2px; cursor: pointer; }
                .iconlist li.over { border: 1px solid #516B9F; }
                .iconlist li img { height: 16px; height: 16px; }
    </style>

</head>
<body style="padding: 0px">
       <div id="toolbar"></div>
    <form id="form1" onsubmit="return false">
        <table border="0" cellpadding="3" cellspacing="1" style="background: #fff; width: 400px;" class="aztable">

            <tr>
                <td height="23" style="width: 85px"  >

                    <div align="left" style="width: 62px">体重(kg)：</div>
                </td>
                <td height="23">

                    <input type="text" id="T_weight" name="T_weight" ltype="float" ligerui="{width:180}" validate="{required:true}" />

                </td>
                <td height="23" style="width: 85px"  >

                    <div align="left" style="width: 62px"> 背膘：</div>
                </td>
                <td height="23">

                    <input type="text" id="T_backfat" name="T_backfat" ltype="float" ligerui="{width:180}" validate="{required:true}" />

                </td>
            </tr>
            <tr>
                <td height="23"  >

                    <div align="left" style="width: 62px"> 眼肌面积：</div>
                </td>
                <td height="23">
                    <input type="text" id="T_eyeArea" name="T_eyeArea" ltype="float" ligerui="{width:180}" validate="{required:true}" />
                </td>
                <td height="23"  >

                    <div align="left" style="width: 62px"> 体温：</div>
                </td>
                <td height="23">
                    <input type="text" id="T_temperature" name="T_temperature" ltype="int" ligerui="{width:180}" validate="{required:true}" />
                </td>
            </tr>
            <tr>
                <td height="23"  >

                    <div align="left" style="width: 62px">血清：</div>
                </td>
                <td height="23">
                      <input type="text" id="T_serum" name="T_serum" ltype="select" ligerui="{width:80,data:[{id:'Y',text:'阴性'},{id:'P',text:'阳性'}]}"  />
                     
                </td>
                <td height="23"  >

                    <div align="left" style="width: 62px">精液：</div>
                </td>
                <td height="23">
                        <input type="text" id="T_semen" name="T_semen" ltype="select" ligerui="{width:80,data:[{id:'Y',text:'阴性'},{id:'P',text:'阳性'}]}"  />
                    
                </td>
            </tr>
            <tr>
                <td height="23"  >

                    <div align="left" style="width: 62px"> 饮水：</div>
                </td>
                <td height="23">
                       <input type="text" id="T_drinkWater" name="T_drinkWater" ltype="select" ligerui="{width:80,data:[{id:'Y',text:'阴性'},{id:'P',text:'阳性'}]}"  />
 
                </td>
                <td height="23"  >

                    <div align="left" style="width: 62px"> 饲料：</div>
                </td>
                <td height="23">
                <input type="text" id="T_feed" name="T_feed" ltype="select" ligerui="{width:80,data:[{id:'Y',text:'阴性'},{id:'P',text:'阳性'}]}"  />
                     
                </td>
            </tr>
                <tr>
                <td height="23"  >

                    <div align="left" style="width: 62px"> 粪便：</div>
                </td>
                <td height="23">
                          <input type="text" id="T_Feces" name="T_Feces" ltype="select" ligerui="{width:80,data:[{id:'Y',text:'阴性'},{id:'P',text:'阳性'}]}"  />
                     
                </td>
                <td height="23"  >

                    <div align="left" style="width: 62px"> 唾液：</div>
                </td>
                <td height="23">
                   <input type="text" id="T_saliva" name="T_saliva" ltype="select" ligerui="{width:80,data:[{id:'Y',text:'阴性'},{id:'P',text:'阳性'}]}"  /> 
                </td>
            </tr> 
                <tr>
                <td height="23"  >

                    <div align="left" style="width: 62px"> 猪体卫生：</div>
                </td>
                <td height="23">
                           <input type="text" id="T_pigHeath" name="T_pigHeath" ltype="select" ligerui="{width:80,data:[{id:'Y',text:'合格'},{id:'P',text:'不合格'}]}"  /> 
         
                </td>
                <td height="23"  >

                    <div align="left" style="width: 62px"> 舍内卫生：</div>
                </td>
                <td height="23">
                       <input type="text" id="T_dormHeath" name="T_dormHeath" ltype="select" ligerui="{width:80,data:[{id:'Y',text:'合格'},{id:'P',text:'不合格'}]}"  /> 
          
                </td>
            </tr>
              <tr>
                <td height="23"  >

                    <div align="left" style="width: 62px"> 其他：</div>
                </td>
                <td height="23">
                    <input type="text" id="T_others" name="T_others" ltype="text" ligerui="{width:180}" validate="{required:true}" />
                </td>
                <td height="23"  >

                    <div align="left" style="width: 62px"> 备注：</div>
                </td>
                <td height="23">
                    <input type="text" id="T_remarks" name="T_remarks" ltype="text" ligerui="{width:180}" validate="{required:true}" />
                </td>
            </tr>
             <tr>
                <td height="23"  >

                    <div align="left" style="width: 62px"> 监测时间：</div>
                </td>
                <td height="23">
                    <input type="text" id="T_time" name="T_time" ltype="date" ligerui="{width:180}" validate="{required:true}" />
                </td>
                <td height="23"  >

                    
                </td>
                <td height="23">
                    
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
