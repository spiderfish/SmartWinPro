<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" />
    <link href="../lib/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" />
    <link href="../CSS/input.css" rel="stylesheet" type="text/css" />

    <script src="../lib/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/ligerui.min.js" type="text/javascript"></script>
    <script src="../lib/json2.js" type="text/javascript"></script>
    <script src="../JS/XHD.js" type="text/javascript"></script>
    <script src="../JS/JsBarcode.all.min.js"></script>
    <script type="text/javascript">
        var baritems = []; //创建一个空数组
        var g1;
        $(function () {
            $("#maingrid4").ligerGrid({
                columns: [
                    { display: '名称', name: 'dormName', width: 250, align: 'left' },
                    { display: '面积', name: 'area', width: 250, align: 'left' },
                    { display: '备注', name: 'remarks', width: 120 },
                    {
                        display: '图标', name: 'product_icon', width: 50, render: function (item) {
                            return "<div style='margin-top:8px;'><img class='imgbar' src='../" + item.product_icon + "'/></div>";
                        }
                    },

                ],
                dataAction: 'local',
                pageSize: 30,
                pageSizeOptions: [20, 30, 50, 100],
                url: "Basic_Dorm.grid.xhd?grid=tree",
                width: '100%',
                height: '100%',
                tree: { columnName: 'product_category' },
                heightDiff: -10,
                onRClickToSelect: true,

                onContextmenu: function (parm, e) {
                    actionCustomerID = parm.data.id;
                    menu.show({ top: e.pageY, left: e.pageX });
                    return false;
                },
                detail: {
                    height: 'auto',
                    onShowDetail: function (r, p) {
                        for (var n in r) {
                            if (r[n] == null) r[n] = "";
                        }
                        g1 = document.createElement('div');
                        $(p).append(g1);
                        $(g1).css('margin', 3).ligerGrid({
                            columns: [
                                //{ display: '序号', width: 50, render: function (item,i) { return item.n; } },
                                { display: '名称', name: 'corralName', width: 120 },
                                { display: '面积', name: 'area', width: 120 },
                                { display: '备注', name: 'remarks', width: 120 }

                            ],
                            dataAction: 'server',
                            url: "Basic_Corral.grid.xhd?dornId=" + r.id + "&rnd=" + Math.random(),
                            toolbar: {
                                items: [
                                    { text: '增加', tag: 'add', click: itemclick, icon: '../images/icon/11.png', disable: true, data: r },
                                    { text: '修改', tag: 'update', click: itemclick, icon: '../images/icon/33.png', disable: true, data: r },
                                    { text: '删除', tag: 'del', click: itemclick, icon: '../images/icon/12.png', disable: true, data: r }
                                ]
                            },
                            usePager: true,
                            checkbox: false,
                            pageSize: 999   ,
                            pageSizeOptions: [20, 30, 50, 100],
                            width: '99%', height: '280',
                            heightDiff: 0
                        })

                    }
                },

            });
            initLayout();
            $(window).resize(function () {
                initLayout();
            });
            toolbar();
        });
        function toolbar() {
            $.get("toolbar.GetSys.xhd?mid=product_category&rnd=" + Math.random(), function (data, textStatus) {
                var data = eval('(' + data + ')');
                //alert(data);
                var items = [];
                var arr = data.Items;
                for (var i = 0; i < arr.length; i++) {
                    arr[i].icon = "../" + arr[i].icon;
                    items.push(arr[i]);
                }
                $("#toolbar").ligerToolBar({
                    items: items

                });
                menu = $.ligerMenu({
                    width: 120, items: getMenuItems(data)
                });

                $("#maingrid4").ligerGetGridManager()._onResize();
            });
        }

        function itemclick(item) {

            if (item.tag == "add") {
                if (item.data.id) {
                    f_openWindow('product/corral_add.aspx?pid=' + item.data.id, "新增畜栏", 380, 250, f_save_Corrral);

                }
            }
            else if (item.tag == "update") {
                var manager = $(g1).ligerGetGridManager();
                var rows = manager.getSelectedRow();
                if (rows && rows != undefined) {
                    f_openWindow('product/corral_add.aspx?pid=' + item.data.id + "&id=" + rows.id, "修改畜栏", 380, 250, f_save_Corrral);
                }
                else {
                    $.ligerDialog.warn('请选择品系！');
                }
            }
            else if (item.tag == "del") {
                var manager = $(g1).ligerGetGridManager();
                var rows = manager.getSelectedRow();
                if (rows && rows != undefined) {
                    delPara('Basic_Corral.del.xhd', rows.id);
                }
                else {
                    $.ligerDialog.warn('请选择品系！');
                }

            }
        }
        function add() {
            f_openWindow("product/dorm_add.aspx", "新增畜舍", 380, 250, f_save);
        }

        function edit() {
            var manager = $("#maingrid4").ligerGetGridManager();
            var row = manager.getSelectedRow();
            if (row) {
                f_openWindow('product/dorm_add.aspx?id=' + row.id, "修改畜舍", 380, 250, f_save);
            } else {
                $.ligerDialog.warn('请选择行！');
            }
        }
        function f_save_Corrral(item, dialog) {
            var issave = dialog.frame.f_save();
            if (issave) {
                dialog.close();
                $.ligerDialog.waitting('数据保存中,请稍候...');
                $.ajax({
                    url: "Basic_Corral.save.xhd", type: "POST",
                    data: issave,
                    dataType: 'json',
                    success: function (result) {
                        $.ligerDialog.closeWaitting();

                        var obj = eval(result);

                        if (obj.isSuccess) {
                            f_reload();
                        }
                        else {
                            $.ligerDialog.error(obj.Message);
                        }
                        //f_load();     
                    },
                    error: function () {
                        $.ligerDialog.closeWaitting();
                        $.ligerDialog.error('操作失败！');
                    }
                });

            }
        }
        function del() {
            var manager = $("#maingrid4").ligerGetGridManager();
            var row = manager.getSelectedRow();
            if (row) {

                $.ligerDialog.confirm("删除不能恢复，确定删除？", function (yes) {
                    if (yes) {
                        $.ajax({
                            url: "Basic_Dorm.del.xhd", type: "POST",
                            data: { id: row.id, rnd: Math.random() },
                            dataType: 'json',
                            success: function (result) {
                                $.ligerDialog.closeWaitting();

                                var obj = eval(result);

                                if (obj.isSuccess) {
                                    f_reload();
                                }
                                else {
                                    $.ligerDialog.error(obj.Message);
                                }
                            },
                            error: function () {
                                top.$.ligerDialog.closeWaitting();
                                top.$.ligerDialog.error('删除失败！', "", null, 9003);
                            }
                        });
                    }
                })
            } else {
                $.ligerDialog.warn("请选择产品类别！");
            }
        }
        function delPara(url, id) {
            $.ligerDialog.confirm("删除不能恢复，确定删除？", function (yes) {
                if (yes) {
                    $.ajax({
                        url: url, type: "POST",
                        data: { id: id, rnd: Math.random() },
                        dataType: 'json',
                        success: function (result) {
                            $.ligerDialog.closeWaitting();

                            var obj = eval(result);

                            if (obj.isSuccess) {
                                f_reload();
                            }
                            else {
                                $.ligerDialog.error(obj.Message);
                            }
                        },
                        error: function () {
                            top.$.ligerDialog.closeWaitting();
                            top.$.ligerDialog.error('删除失败！');
                        }
                    });
                }
            })

        }
        function f_save(item, dialog) {
            var issave = dialog.frame.f_save();
            if (issave) {
                dialog.close();
                $.ligerDialog.waitting('数据保存中,请稍候...');
                $.ajax({
                    url: "Basic_Dorm.save.xhd", type: "POST",
                    data: issave,
                    dataType: 'json',
                    success: function (result) {
                        $.ligerDialog.closeWaitting();

                        var obj = eval(result);

                        if (obj.isSuccess) {
                            f_reload();
                        }
                        else {
                            $.ligerDialog.error(obj.Message);
                        }
                    },
                    error: function () {
                        $.ligerDialog.closeWaitting();

                    }
                });

            }
        }
        function f_reload() {
            var manager = $("#maingrid4").ligerGetGridManager();
            manager.loadData(true);
        };
    </script>
    <style type="text/css">
        .l-leaving { background: #eee; color: #999; }
    </style>

</head>
<body>

    <form id="form1" onsubmit="return false">
        <div style="padding:10px;">
            <div id="toolbar"></div>
            
            <div id="maingrid4" style="margin: -1px;"></div>
            <img id="bar" />
        </div>
    </form>


</body>
</html>
