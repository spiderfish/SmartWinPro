<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
   <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" />
    <link href="../lib/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" />

    <script src="../lib/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/ligerui.min.js" type="text/javascript"></script>

    <script src="../lib/jquery-validation/jquery.validate.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/messages_cn.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/common.js" type="text/javascript"></script>

    <script src="../lib/jquery.form.js" type="text/javascript"></script>
    <script src="../JS/XHD.js" type="text/javascript"></script>

    <script type="text/javascript">
        //图标
        var jiconlist, winicons, currentComboBox;
        $(function () {
            $.metadata.setType("attr", "validate");
            XHD.validate($(form1));

            //$("#T_Contract_name").focus();
            $("form").ligerForm();
            //$("#T_fbh").ligerComboBox({
            //    width: 180,
            //    onBeforeOpen: f_selectorder
            //})
            //$("#T_mbh").ligerComboBox({
            //    width: 180,
            //    onBeforeOpen: f_selectorderM
            //})
            if (getparastr("bid"))
            loadForm(getparastr("oid"), getparastr("bid"), getparastr("level"));
           

        });
         
        function f_save() {
            if ($(form1).valid()) {
                var sendtxt = "&oid=" + getparastr("oid") + "&bid=" + getparastr("bid") + "&id=" + getparastr("id") + "&level=" + getparastr("level");
                return $("form :input").fieldSerialize() + sendtxt;
            }
        }

        function loadForm(oaid,bid,level) {
            $.ajax({
                type: "GET",
                url: "Basic_Boar_sys_Genealogy.form2.xhd", /* 注意后面的名字对应CS的方法名称 */
                data: { oid: oaid, bid: bid, level, level, id: getparastr("id"), rnd: Math.random() }, /* 注意参数的格式和名称 */
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    var obj = eval(result);
                    for (var n in obj) {

                    }
                    //alert(obj.constructor); //String 构造函数
                    $("#T_fbh").val(obj.fbarcode);
                    $("#T_fbh_val").val(obj.fid);//订单明细编号（产品编号）
                    $("#T_fname").val(obj.fname);
                    $("#T_flb").val(obj.flb);
                    $("#T_fbz").val(obj.fbz);
                    $("#T_mbh").val(obj.mbarcode);
                    $("#T_mbh_val").val(obj.mid);//订单明细编号（产品编号）
                    $("#T_mname").val(obj.mname);
                    $("#T_mlb").val(obj.mlb);
                    $("#T_mbz").val(obj.mbz);
                
                }
            });
        }

        function f_selectorder() {
            $.ligerDialog.open({
                zindex: 19009, title: '选择种禽-父', width: 650, height: 400, url: 'GetBasic_boar.aspx', buttons: [
                    { text: '确定', onclick: f_selectorderOK },
                    { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
                ]
            });
            return false;
        }
        function f_selectorderM() {
            $.ligerDialog.open({
                zindex: 19009, title: '选择种禽-母', width: 650, height: 400, url: 'GetBasic_boar.aspx', buttons: [
                    { text: '确定', onclick: f_selectorderMOK },
                    { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
                ]
            });
            return false;
        }
        function f_selectorderOK(item, dialog) {
            var obj = dialog.frame.f_select();
            console.log(obj)
            if (!obj) {
                alert('请选择行!');
                return;
            }
            if ($("#T_mbh").val() == obj.barcode) {
                alert('父和母不能相同!');
                return;
            }
            $("#T_fbh").val(obj.barcode);
            $("#T_fbh_val").val(obj.id);
            $("#T_fname").val(obj.product_name);
            $("#T_flb").val(obj.specifications);
            $("#T_fbz").val(obj.remarks); 
            dialog.close();
        }
        function f_selectorderMOK(item, dialog) {
            var obj = dialog.frame.f_select();
            if (!obj) {
                alert('请选择行!');
                return;
            } if ($("#T_fbh").val() == obj.barcode) {
                alert('父和母不能相同!');
                return;
            }
            $("#T_mbh").val(obj.barcode);
            $("#T_mbh_val").val(obj.id);
            $("#T_mname").val(obj.product_name);
            $("#T_mlb").val(obj.specifications);
            $("#T_mbz").val(obj.remarks);
            dialog.close();
        }
    </script>
    <style type="text/css">
        .iconlist { width: 360px; padding: 3px; }
            .iconlist li { border: 1px solid #FFFFFF; text: left; display: block; padding: 2px; cursor: pointer; }
                .iconlist li.over { border: 1px solid #516B9F; }
                .iconlist li img { height: 16px; height: 16px; }
    </style>

</head>
<body style="padding: 0px">
       <div id="toolbar"></div>
    <form id="form1" onsubmit="return false">
        <table border="0" cellpadding="3" cellspacing="1" style="background: #fff; width: 400px;" class="aztable">

            <tr>
                <td height="23" style="width: 85px"  >

                    <div align="right" style="width: 82px">父系：</div>                </td>
                <td height="23">

                    <input type="text" id="T_fbh" name="T_fbh" ltype="text" ligerui="{width:180}" validate="{required:true}"  />                </td>
                <td height="23" style="width: 85px"  >

                    <div align="right"  style="width: 82px"> 父-名称：</div>                </td>
                <td height="23">

                    <input type="text" id="T_fname" name="T_fname" ltype="text" ligerui="{width:180}" validate="{required:true}" />                </td>
            </tr>
            <tr>
                
                <td height="23"  >

                    <div align="right"  style="width: 82px">父-备注：</div>                </td>
                <td height="23" colspan="3">
                    <input type="text" id="T_fbz" name="T_fbz" ltype="text" ligerui="{width:474}"   />                                </td>
		    </tr>
            <tr>
              <td height="23" style="width: 85px"  >&nbsp;</td>
              <td height="23">&nbsp;</td>
              <td height="23" style="width: 85px"  >&nbsp;</td>
              <td height="23">&nbsp;</td>
            </tr>
            <tr>
                <td height="23" style="width: 85px"  >

                    <div align="right"  style="width: 82px">母系：</div>                </td>
                <td height="23">

                    <input type="text" id="T_mbh" name="T_mbh" ltype="text" ligerui="{width:180}" validate="{required:true}" />                </td>
                <td height="23" style="width: 85px"  >

                    <div align="right"  style="width: 82px"> 母-名称：</div>                </td>
                <td height="23">

                    <input type="text" id="T_mname" name="T_mname" ltype="text" ligerui="{width:180}" validate="{required:true}" />                </td>
            </tr>
            <tr>
               
                <td height="23"  >

                    <div align="right"  style="width: 82px">母-备注：</div>                </td>
                <td height="23"  colspan="3">
                    <input type="text" id="T_mbz" name="T_mbz" ltype="text" ligerui="{width:474}"   />                </td>
            </tr>
              
            
        </table>

    </form>
</body>
</html>
