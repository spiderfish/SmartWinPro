<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" />
    <link href="../lib/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" />
    <link href="../CSS/input.css" rel="stylesheet" type="text/css" />

    <script src="../lib/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/ligerui.min.js" type="text/javascript"></script>

    <script src="../lib/jquery-validation/jquery.validate.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/messages_cn.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/common.js" type="text/javascript"></script>
    <script src="../JS/XHD.js" type="text/javascript"></script>

    <script src="../lib/jquery.form.js" type="text/javascript"></script>
    <!-- 引入 css -->
<link href="https://unpkg.com/@wangeditor/editor@latest/dist/css/style.css" rel="stylesheet"/>
<style type="text/css">
  #editor—wrapper {
    border: 1px solid #ccc;
    z-index: 50; /* 按需定义 */
  }
  #toolbar-container { border-bottom: 1px solid #ccc; }
  #editor-container { height: 500px; }
</style>

</head>
<body>
     <div style="padding: 10px;">
    <form id="form1" onsubmit="return false">
   
    </form>
 
             <div>
			 <table><tr><td> <div id="editor—wrapper">
    <div id="toolbar-container"><!-- 工具栏 --></div>
    <div id="editor-container"><!-- 编辑器 --></div>
</div></td><td>
                   <img id="userheadimg" src="a.gif" onerror="noheadimg()" style="border-radius: 4px; box-shadow: 1px 1px 3px #111; width: 220px; height: 150px; margin-left: 5px; background: #d2d2f2; border: 3px solid #fff; behavior: url(../css/pie.htc);" />

          <input type="hidden" id="headurl" name="headurl" />
                        <div style="text-align: center">
                            <div id="uploadimg"></div>
                        </div></td></tr></table>
     
           
    </div>
         </div>
    <script src="https://unpkg.com/@wangeditor/editor@latest/dist/index.js"></script>
    <script type="text/javascript">

        var Richhtml = "";
        const { createEditor, createToolbar } = window.wangEditor 
        const editorConfig = {
            placeholder: 'Type here...',
            MENU_CONF: {},
            onChange(editor) {
                Richhtml = editor.getHtml()
                console.log('editor content', Richhtml)
                // 也可以同步到 <textarea>
            }
        }
       
        editorConfig.MENU_CONF['uploadImage'] = {
            //debug:true,
            server: 'Product.RichUpload.xhd',
            onSuccess(file, res) {
                console.log('onSuccess', file, res)
            },
            onFailed(file, res) {
                alert(res.message)
                console.log('onFailed', file, res)
            },
            onError(file, err, res) {
                alert(err.message)
                console.error('onError', file, err, res)
            },
          /*  base64LimitSize: 5 * 1024 *1024 // 5M*/
        }
        const editor = createEditor({
            selector: '#editor-container',
            html: '<p><br></p>',
            config: editorConfig,
            mode: 'default', // or 'simple'
        })

        const toolbarConfig = {}

        const toolbar = createToolbar({
            editor,
            selector: '#toolbar-container',
            config: toolbarConfig,
            mode: 'default', // or 'simple'
        })
        $(function () {
            $.metadata.setType("attr", "validate");
            XHD.validate($(form1));

            //$("#T_Contract_name").focus();

            loadForm(getparastr("pid"));
            $("#uploadimg").ligerButton({ text: "上传照片", width: 130, click: uploadimg });
        });

        function f_save() {
            if ($(form1).valid()) {
                console.log(Richhtml)
                var sendtxt = "&pid=" + getparastr("pid") + "&headurl=" + $("#headurl").val() + "&richText=" + escape(Richhtml);
                return $("form :input").fieldSerialize() + sendtxt;
            }
        }
        function escape2Html(str) {
            var arrEntities = { 'lt': '<', 'gt': '>', 'nbsp': ' ', 'amp': '&', 'quot': '"' };
            return str.replace(/&(lt|gt|nbsp|amp|quot);/ig, function (all, t) { return arrEntities[t]; });
        } 

        function loadForm(oaid) {
            $.ajax({
                type: "get",
                url: "Product.form.xhd", /* 注意后面的名字对应CS的方法名称 */
                data: { Action: 'form', id: oaid, rnd: Math.random() }, /* 注意参数的格式和名称 */
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    var obj = eval(result);
                    for (var n in obj) {
                        if (obj[n] == "null" || obj[n] == null)
                            obj[n] = "";
                    }
                    //alert(obj.constructor); //String 构造函数
                    var h = unescape(obj.richText);
                    console.log(escape2Html(h))
                    
                    editor.setHtml(escape2Html(h))
                    //$("#T_product_category").ligerGetComboBoxManager().selectValue(obj.category_id);
                    $("#headurl").val(obj.img);
                    $("#userheadimg").attr("src", "../Images/upload/product/" + obj.img);
                    if (!obj.category_id)
                        obj.category_id = getparastr("categoryid");

                    $("#form1").ligerAutoForm({
                        labelWidth: 80, inputWidth: 80, space: 20,
                        fields: [
                            {
                                display: '品种设置', type: 'group', icon: '',
                                rows: [
                                    [
                                        { display: "品种名称", name: "T_product_name", type: "text", options: "{width:80}", validate: "{required:true}", initValue: obj.product_name },
                                        { display: "品种类别", name: "T_product_category", type: "select", options: "{width:80,treeLeafOnly: false,tree:{url:'Product_category.tree.xhd',idFieldName: 'id',checkbox: false},value:'" + obj.category_id + "'}", validate: "{required:true}" },
                                   
                                        { display: "品系", name: "T_px", options: "{width:80}" },
                                        { display: "等级", name: "T_dj", options: "{width:80}" },
                                        //   { display: "品系", name: "T_px", type: "select", options: "{width:180,treeLeafOnly: false,tree:{url:'Basic_Series.tree.xhd?type=poultry_px',idFieldName: 'id',checkbox: false},value:'" + obj.Series_Id + "'}", validate: "{required:true}"   },
                                        //   { display: "等级", name: "T_dj", type: "select", options: "{width:180,treeLeafOnly: false,tree:{url:'Basic_Series_Level.tree.xhd?type=poultry_level',idFieldName: 'id',checkbox: false},value:'" + obj.Level_Id + "'}", validate: "{required:true}"   },
                                  
                                        { display: "成本价", name: "T_cost", type: "text", options: "{width:80,onChangeValue:function(value){ $('#T_cost').val(toMoney(value)); }}", validate: "{required:true}", initValue: toMoney(obj.cost) },
                                        { display: "单位", name: "T_product_unit", type: "select", options: "{width:80,treeLeafOnly: false,tree:{url:'Sys_Param.tree.xhd?type=units',idFieldName: 'id',checkbox: false},value:'" + obj.unit_id + "'}", validate: "{required:true}" },

                                   
                                        { display: "每份剂量", name: "T_price", type: "int", options: "{width:80}", validate: "{required:true}", initValue: obj.price ?? 25 }    ],
                                        //{ display: "销售价", name: "T_price", type: "text", options: "{width:180,onChangeValue:function(value){ $('#T_price').val(toMoney(value)); }}", validate: "{required:true}", initValue:toMoney( obj.price )},
                                     [    { display: "采集种禽", name: "T_specifications", type: "select", options: "{width:80,treeLeafOnly: false,tree:{url:'Sys_Param.tree.xhd?type=poultry_type',idFieldName: 'id',checkbox: false},value:'" + obj.specifications_id + "'}", },
                                
                                   
                                        { display: "产出份数", name: "T_dailyoutput", type: "int", options: "{width:80}", validate: "{required:true}", initValue: obj.dailyoutput ?? 80 },
                                        { display: "剂量单位", name: "T_jldw", type: "select", options: "{width:80,treeLeafOnly: false,tree:{url:'Sys_Param.tree.xhd?type=units',idFieldName: 'id',checkbox: false},value:'" + obj.dosageUnit_id + "'}", validate: "{required:true}" },
                                   
                                        { display: "采集周期", name: "T_pickcycle", type: "int", options: "{width:80}", validate: "{required:true}", initValue: obj.pickcycle },
                                        { display: "销售单价", name: "T_agio", type: "text", options: "{width:80,onChangeValue:function(value){ $('#T_agio').val(toMoney(value)); }}", validate: "{required:true}", initValue: toMoney(obj.agio) },

                                         { display: "可采天数", name: "T_waitDay", type: "int", options: "{width:80}", validate: "{required:true}", initValue: obj.waitDay ?? 1 } ,

                                        { display: "备注", name: "T_Remark", type: "textarea", cols: 73, rows:1, width: 100, cssClass: "l-textarea", initValue: obj.Remark }
                                    ],
                                    [
                                        { display: "有效精子(百万)", name: "T_EffectiveSperm", type: "int", options: "{width:80}", initValue: obj.EffectiveSperm ?? 1 },

                                        { display: "每份剂量(ML)", name: "T_Dose", type: "int", options: "{width:80}", initValue: obj.Dose ?? 1 },

                                        { display: "数量(份)", name: "T_Quantity", type: "int", options: "{width:80}", initValue: obj.Quantity ?? 1 }

                                    ]
                                ]
                            }
                        ]
                    });


                    d = $('#T_dj').ligerComboBox({
                        width: 80,
                        url: "Basic_Series_Level.combo.xhd?id=" + obj.Series_Id + "&rnd=" + Math.random(),
                        value: obj.Level_Id,
                        emptyText: '（空）'
                    });
                    c = $('#T_px').ligerComboBox({
                        width: 80,
                        url: "Basic_Series.combo.xhd?rnd=" + Math.random(),
                        value: obj.Series_Id,
                        onBeforeSelect: function (newvalue) {
                            url = "Basic_Series_Level.combo.xhd?id=" + newvalue + "&rnd=" + Math.random(),
                                d.setUrl(url);
                        },
                        emptyText: '（空）'
                    });
                }
            });

        }

        function set_tomoney(value) {
            $("#T_price").val(toMoney(value));
        }


        function uploadimg() {
            top.$.ligerDialog.open({
                zindex: 9004,
                width: 800, height: 500,
                title: '上传品种图片',
                url: 'Product/uploadimage.aspx?id=' + getparastr("pid"),
                buttons: [
                    {
                        text: '保存', onclick: function (item, dialog) {
                            saveheadimg(item, dialog);
                        }
                    },
                    {
                        text: '关闭', onclick: function (item, dialog) {
                            dialog.close();
                        }
                    }
                ],
                isResize: true
            });
        }

        function saveheadimg(item, dialog) {
            var upfile = dialog.frame.f_save();
            //alert(upfile);
            if (upfile) {
                dialog.close();
                $.ligerDialog.waitting('数据保存中,请稍候...');

                $.ajax({
                    url: "Product.Base64Upload.xhd", type: "POST",
                    data: upfile,
                    dataType: "json",
                    success: function (result) {
                        $.ligerDialog.closeWaitting();

                        var obj = eval(result);
                        if (obj.isSuccess) {
                            $("#headurl").val(obj.Message);
                            $("#userheadimg").attr("src", "../Images/upload/product/" + obj.Message);
                        }
                        else {
                            $.ligerDialog.error(obj.Message);
                        }


                    },
                    error: function () {
                        $.ligerDialog.closeWaitting();
                        $.ligerDialog.error('操作失败！');
                    }
                });
            }
        }
    </script>
</body>
</html>
