<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" />
    <link href="../lib/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" />

    <script src="../lib/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/ligerui.min.js" type="text/javascript"></script>

    <script src="../lib/jquery-validation/jquery.validate.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/messages_cn.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/common.js" type="text/javascript"></script>
    <script src="../lib/jquery.form.js" type="text/javascript"></script>
     <script src="../JS/JsBarcode.all.min.js"></script>
    <script src="../JS/XHD.js" type="text/javascript"></script>
    <script type="text/javascript">
        //图标
        var jiconlist, winicons, currentComboBox;
        $(function () {
            $.metadata.setType("attr", "validate");
            XHD.validate($(form1));

            //$("#T_Contract_name").focus();
            $("form").ligerForm();
            //$("#menuicon").bind("click", f_selectIcon);
            //$("#T_category_icon").bind("click", f_selectIcon);

            loadForm(getparastr("id"));

            //jiconlist = $("body > .iconlist:first");
            //if (!jiconlist.length) jiconlist = $('<ul class="iconlist"></ul>').appendTo('body');
        });

        function f_save() {
            if ($(form1).valid()) {
                var sendtxt = "&id=" + getparastr("id");
                return $("form :input").fieldSerialize() + sendtxt;
            }
        }

        function loadForm(oaid) {
            $.ajax({
                type: "GET",
                url: "Basic_Drug.form.xhd", /* 注意后面的名字对应CS的方法名称 */
                data: { id: oaid, rnd: Math.random() }, /* 注意参数的格式和名称 */
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    var obj = eval(result);
                    for (var n in obj) {

                    }
                    //alert(obj.constructor); //String 构造函数
                    $("#T_drug_name").val(obj.drugName);
                    $("#T_drugcycle").val(obj.drugcycle);
                    $("#T_remarks").val(obj.Remarks);

                    d = $('#T_unit').ligerComboBox({
                        width: 180,
                        url: "Sys_Param.tree.xhd?type=units&rnd=" + Math.random(),
                        value: obj.unit_id,
                        emptyText: '（空）'
                    });
                    //if (!obj.product_icon)
                    //    obj.product_icon = 'images/icon/21.png ';
                    //$("#menuicon").attr("src", "../" + obj.product_icon);
 
                    //JsBarcode("#barcode", obj.id, { 
                    //    width: 1,
                    //    height: 20,
                    //    displayValue: true
                    //});

                }
            });
       

        
        }


    </script>
    <style type="text/css">
        .iconlist { width: 360px; padding: 3px; }
            .iconlist li { border: 1px solid #FFFFFF; float: left; display: block; padding: 2px; cursor: pointer; }
                .iconlist li.over { border: 1px solid #516B9F; }
                .iconlist li img { height: 16px; height: 16px; }
    </style>

</head>
<body style="padding: 0px">
    <form id="form1" onsubmit="return false">
        <table border="0" cellpadding="3" cellspacing="1" style="background: #fff; width: 400px;" class="aztable">

            <tr>
                <td height="23" style="width: 85px" colspan="2">

                    <div align="left" style="width: 62px">药品名称：</div>
                </td>
                <td height="23">

                    <input type="text" id="T_drug_name" name="T_drug_name" ltype="text" ligerui="{width:180}" validate="{required:true}" />

                </td>
            </tr>
            <tr>
                <td height="23" colspan="2">

                    <div align="left" style="width: 62px">周期(天)：</div>
                </td>
                <td height="23">
                   <input type="text" id="T_drugcycle" name="T_drugcycle" ltype="float" ligerui="{width:180}" validate="{required:true}" />
                
                </td>
            </tr>
                  <tr>
                <td height="23" colspan="2">

                    <div align="left" style="width: 62px">单位：</div>
                </td>
                <td height="23">
                   <input type="text" id="T_unit" name="T_unit"   />
                
                </td>
            </tr>
            <tr>
                <td height="23" colspan="2">

                    <div align="left" style="width: 62px">备注：</div>
                </td>
                 <td height="23">
                    <input type="text" id="T_remarks" name="T_remarks" ltype="text" ligerui="{width:180}" validate="{required:true}" /> 
                </td> 
            </tr>
           <%-- <tr>
                <td colspan="3">
                     <img id="barcode"  /> 
                </td>
            </tr>--%>
            
        </table>
    </form>
</body>
</html>
