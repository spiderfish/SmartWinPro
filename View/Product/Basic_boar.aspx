<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>

    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" />
    <link href="../lib/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" />
    <link href="../CSS/input.css" rel="stylesheet" type="text/css" />
    <script src="../lib/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/ligerui.min.js" type="text/javascript"></script>
    <script src="../JS/XHD.js" type="text/javascript"></script>
    <script src="../lib/jquery.form.js" type="text/javascript"></script>

    <script type="text/javascript">

        var manager = "";
        var treemanager;
        $(function () {
            $("#layout1").ligerLayout({ leftWidth: 200, allowLeftResize: false, allowLeftCollapse: true, space: 2, heightDiff: -5 });
            $("#tree1").ligerTree({
                //url: 'Sys_Param.tree.xhd?type=poultry_type&rnd=' + Math.random(),
                url: 'Product.tree.xhd?rnd=' + Math.random(),
                onSelect: onSelect,
                idFieldName: 'id', 
                //parentIDFieldName: 'pid',
                //usericon: 'd_icon',
                checkbox: false,
                itemopen: false,
                onSuccess: function () {
                    $(".l-first div:first").click();
                }
            });

            treemanager = $("#tree1").ligerGetTreeManager();

            initLayout();
            $(window).resize(function () {
                initLayout();
            });

            $("#maingrid4").ligerGrid({
                columns: [
                    //{ display: '序号', width: 50, render: function (item,i) { return item.n; } },

					{
                        display: '系统ID尾号', name: 'id', width: 120, render: function (item) {
                            var html = item.id.substring(20,36);
;
                            return html;
                        }
                    },
                    { display: 'ID', name: 'barcode', width: 200 },
                     { display: '耳号', name: 'shortcode', width: 100 },
					{ display: '种源名称', name: 'product_name', width: 120 },
					{
                        display: '最后采集时间', name: 'LastCollecTime', width: 90, render: function (item) {
                            return formatTimebytype(item.LastCollecTime, 'yyyy-MM-dd');
                        }
                    },
                    //{ display: '最后用药', name: 'drugName', width: 120 },
                    // { display: '药效期', name: 'drugcycle', width: 120 },

                    //{
                    //    display: '最后用药', name: 'LastEfficaTime', width: 90, render: function (item) {
                    //        return formatTimebytype(item.LastEfficaTime, 'yyyy-MM-dd');
                    //    }
                    //},
                    //{ display: '健康信息', name: 'zhiliaoqi', width: 120 },
                    //   { display: '治疗原因', name: 'zhiliaoyuanyin', width: 120 },
                    //{ display: '种禽类别', name: 'category_name', width: 120 },
                  //  { display: '种禽规格', name: 'specifications', width: 120 },
                    /*{
                        display: '成本价（￥）', name: 'cost', width: 120, align: 'right', render: function (item) {
                            return toMoney(item.cost);
                        }
                    },
                    {
                        display: '报价（￥）', name: 'price', width: 120, align: 'right', render: function (item) {
                            return toMoney(item.price);
                        }
                    },
                    {
                        display: '销售价（￥）', name: 'agio', width: 120, align: 'right', render: function (item) {
                            return toMoney(item.agio);
                        }
                    },*/
                    { display: '单位', name: 'unit', width: 60 },
                    { display: '周期(天)', name: 'pickcycle', width: 60 },
                    {
                            display: '采集状态', name: 'CollectionStatus', width: 60, render: function (item) {

                                var html;
                                if (item.CollectionStatus == "1") {
                                    html = "<div style='color:#339900'>";
                                    html += "可采集";
                                    html += "</div>";
                                }
                                else if (item.CollectionStatus == "3") {
                                    html = "<div style='color:#FF0000'>";
                                    html += "禁采期";
                                    html += "</div>";
                                }
                               
                               
                                else {
                                    html = item.CollectionStatus;
                                }
                                return html;
                            }
                        },
                {
                            display: '出禁天数', name: 'okdate', width: 60, render: function (item) {

                                var html;
                                if (item.okdate <= 0) {
                                    html = "<div style='color:#339900'>";
                                    html += "0";
                                    html += "</div>";
                                }
                               
                                else {
                                      html = "<div style='color:#FF0000'>";
                                    html += item.okdate;
                                    html += "</div>";
                                }
                                return html;
                            }
                    },
                 { display: '年龄(天)', name: 'nlday', width: 60 },
                 {
                        display: '创建时间', name: 'create_time', width: 120, render: function (item) {
                            return formatTimebytype(item.create_time, 'yyyy-MM-dd hh:mm');
                        }
                    },
                    { display: '备注', name: 'remarks', width: 120 }

                ],
                dataAction: 'server',
                url: "Basic_boar.grid.xhd&categoryid=0rnd=" + Math.random(),
                pageSize: 30,
                pageSizeOptions: [20, 30, 50, 100],
                width: '100%',
                height: '100%',
                heightDiff: -8,

                onContextmenu: function (parm, e) {
                    actionCustomerID = parm.data.id;
                    menu.show({ top: e.pageY, left: e.pageX });
                    return false;
                }
            });
            toolbar();

        });
        function toolbar() {
            $.get("toolbar.GetSys.xhd?mid=basic_boar_list&rnd=" + Math.random(), function (data, textStatus) {
                var data = eval('(' + data + ')');
                //alert(data);
                var items = [];
                var arr = data.Items;
                for (var i = 0; i < arr.length; i++) {
                    arr[i].icon = "../" + arr[i].icon;
                    items.push(arr[i]);
                }
                items.push({ type: 'textbox', id: 'stext', text: '' });
                items.push({ type: 'textbox', id: 'T_sectype', text: '状态' });
                items.push({ type: 'button', text: '搜索', icon: '../images/search.gif', disable: true, click: function () { doserch() } });

                $("#toolbar").ligerToolBar({
                    items: items

                });
                $('#T_sectype').ligerComboBox({
                    width: 80,
                    selectBoxWidth: 120,
                    selectBoxHeight: 160,
                    valueField: 'id',
                    textField: 'text',
                    tree: {
                        data: [
                            { id: 0, text: '全部' },
                            { id: 1, text: '可采集' },
                            { id: 2, text: '禁采期' } 


                        ],
                        checkbox: false
                    }
                });
                menu = $.ligerMenu({
                    width: 120, items: getMenuItems(data)
                });
                $("#stext").ligerTextBox({ width: 200 });
                $("#maingrid4").ligerGetGridManager()._onResize();
            });
        }


        function onSelect(note) {
            var manager = $("#maingrid4").ligerGetGridManager();
            var url = "Basic_boar.grid.xhd?categoryid=" + note.data.id + "&rnd=" + Math.random();
            manager._setUrl(url);
        }
        $(document).keydown(function (e) {
            if (e.keyCode == 13) {//&& e.target.applyligerui
                doserch();
            }
        });
        //查询
        function doserch() {
            var sendtxt = "&rnd=" + Math.random();
            var serchtxt = $("#form1 :input").fieldSerialize() + sendtxt;

            var manager = $("#maingrid4").ligerGetGridManager();
            manager._setUrl("Basic_boar.grid.xhd?" + serchtxt);
        }

        //重置
        function doclear() {
            //var serchtxt = $("#serchform :input").reset();
            $("#form1").each(function () {
                this.reset();
            });
        }
        //系统档案
        function sysbtn() {
            openUrl('product/boar_sysId_add.aspx?rnd=' + Math.random(), "系谱档案",f_saveDA);

        }
        //转舍
        function changebtn() { openUrl('product/boar_change_add.aspx?rnd=' + Math.random(), "转舍档案",""); }
        //健康
        //function healthbcbtn() { openUrl('product/boar_health_bc_add.aspx?rnd=' + Math.random(), "舍栏环境检测", ""); }

        function healthbtn() { openUrl('product/boar_health_board.aspx?IsView=Y&rnd=' + Math.random(), "健康档案", "");}
        //用药
        function drugbtn() { openUrl('product/boar_drug_board.aspx?IsView=Y&rnd=' + Math.random(), "用药档案", ""); };


        function openUrl(url, title, f_savesss) {

            var manager = $("#maingrid4").ligerGetGridManager();
            var rows = manager.getSelectedRow();
            if (rows && rows != undefined) { 
                f_openWindow(url + '&bid=' + rows.id + "&dormId=" + rows.dormId + "&corralId=" + rows.corralId , title, 1000, 600, f_savesss);
            }
            else {
                $.ligerDialog.warn('请选择种禽！');
            }
        }

        function print() {
            var manager = $("#maingrid4").ligerGetGridManager();
            var rows = manager.getSelectedRow();
            if (rows && rows != undefined) {
                f_openWindow('product/Basic_boar_add.aspx?isprint=Y&pid=' + rows.id, "打印种禽", 700, 650);
            }
            else {
                $.ligerDialog.warn('请选择种禽！');
            }
        }
        function edit() {
            var manager = $("#maingrid4").ligerGetGridManager();
            var rows = manager.getSelectedRow();
            if (rows && rows != undefined) {
                f_openWindow('product/Basic_boar_add.aspx?pid=' + rows.id, "维护种禽", 700, 650, f_save);
            }
            else {
                $.ligerDialog.warn('请选择种禽！');
            }
        }
        function add() {
            var notes = $("#tree1").ligerGetTreeManager().getSelected();
              
            if (notes != null && notes != undefined) {
                f_openWindow('product/Basic_boar_add.aspx?specifications=' + urlencode(  notes.data.specifications)+'&categoryid=' + notes.data.id, "新增种禽", 700, 650, f_save);
            }
            else {
                $.ligerDialog.warn('请选择种禽类别！');
            }
        }

        function del() {
            var manager = $("#maingrid4").ligerGetGridManager();
            var row = manager.getSelectedRow();
            if (row) {
                $.ligerDialog.confirm("种禽删除不能恢复，确定删除？", function (yes) {
                    if (yes) {
                        $.ajax({
                            url: "Basic_boar.del.xhd", type: "POST",
                            data: { id: row.id, rnd: Math.random() },
                            dataType: 'json',
                            success: function (result) {
                                $.ligerDialog.closeWaitting();

                                var obj = eval(result);

                                if (obj.isSuccess) {
                                    f_load();
                                }
                                else {
                                    $.ligerDialog.error(obj.Message);
                                }
                            },
                            error: function () {
                                top.$.ligerDialog.closeWaitting();
                                top.$.ligerDialog.error('删除失败！');
                            }
                        });
                    }
                })
            }
            else {
                $.ligerDialog.warn("请选择种禽");
            }

        } 

        function f_saveDA(item, dialog) {
            var issave = dialog.frame.f_save();
            if (issave) {
                dialog.close();
               
                save("Basic_Boar_sys.save.xhd",issave);
            }
        }

        function save(url, issave) {

            $.ligerDialog.waitting('数据保存中,请稍候...');
            $.ajax({
                url: url, type: "POST",
                data: issave,
                dataType: 'json',
                success: function (result) {
                    $.ligerDialog.closeWaitting(); 
                    var obj = eval(result); 
                    if (obj.isSuccess) {
                        f_load();
                    }
                    else {
                        $.ligerDialog.error(obj.Message);
                    }
                    //f_load();     
                },
                error: function () {
                    $.ligerDialog.closeWaitting();
                    $.ligerDialog.error('操作失败！');
                }
            });}

        function f_save(item, dialog) {
            var issave = dialog.frame.f_save();
            //alert(JSON.stringify(issave))
            if (issave) {
                dialog.close();
                $.ligerDialog.waitting('数据保存中,请稍候...');
                $.ajax({
                    url: "Basic_boar.save.xhd", type: "POST",
                    data: issave,
                    dataType: 'json',
                    success: function (result) {
                        $.ligerDialog.closeWaitting();

                        var obj = eval(result);

                        if (obj.isSuccess) {
                            f_load();
                        }
                        else {
                            $.ligerDialog.error(obj.Message);
                        }
                        //f_load();     
                    },
                    error: function () {
                        $.ligerDialog.closeWaitting();
                        $.ligerDialog.error('操作失败！');
                    }
                });

            }
        }
        function f_load() {
            var manager = $("#maingrid4").ligerGetGridManager();
            manager.loadData(true);
        }

    </script>
</head>
<body style="padding: 0px; overflow: hidden;">
    <div style="padding: 5px 10px 0px 5px;">
        <form id="form1" onsubmit="return false">
            <div id="layout1" style="">
                <div position="left" title="种源类型">
                    <div id="treediv" style="width: 200px; height: 100%; margin: -1px; float: left; overflow: auto;margin-top:2px;">
                        <ul id="tree1"></ul>
                    </div>
                </div>
                <div position="center">
                    <div id="toolbar" style="margin-top: 10px;"></div>
                    <div id="maingrid4" style="margin: -1px;"></div>

                </div>
            </div>
        </form>

    </div>
</body>
</html>
