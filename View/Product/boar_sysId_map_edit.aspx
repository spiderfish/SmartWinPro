<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>

    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" />
    <link href="../lib/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" />
    <link href="../CSS/input.css" rel="stylesheet" type="text/css" />
    <script src="../lib/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/ligerui.min.js" type="text/javascript"></script> 
    <script src="../JS/XHD.js" type="text/javascript"></script>
    <script src="../lib/jquery.form.js" type="text/javascript"></script>

    <script type="text/javascript">

        var manager = "",g1,g2;
        var treemanager;
        $(function () {
   
            initLayout();
            $(window).resize(function () {
                initLayout();
            });

            manager=  $("#maingrid4").ligerGrid({
                columns: [
                    { display: '级别', name: 'levels', width: 80 },
                    {
                        display: '名称', name: 'name', width: 80, align: 'right'
                    },
                    { display: '备注', name: 'remark', width: 80 },
                    { display: '公母', name: 'gmsex', width: 80 },
                    //{ display: '类型', name: 'specifications', width: 80 },
                    //{
                    //    display: '日期', name: 'create_time', width: 80, width: 120, render: function (item) {
                    //        return formatTimebytype(item.create_time, 'yyyy-MM-dd hh:mm');
                    //    }
                    //},
                    {
                        display: '人员', name: 'create_id', width: 100
                    }

                ],
                 
                dataAction: 'server',
                url: "Basic_Boar_sys_Genealogy.grid.xhd?bid=" + getparastr("bid")+"&level=1&rnd=" + Math.random(),
                pageSize: 30,
                pageSizeOptions: [20, 30, 50, 100],
                width: '100%',
                height: '100%',
                heightDiff: -8, 
                toolbar: {
                    items: [
                        { text: '增加一级', tag: 'add', click: itemclick_, icon: '../images/icon/11.png', disable: true},
                        { text: '修改一级', tag: 'update', click: itemclick_, icon: '../images/icon/33.png', disable: true },
                        { text: '删除', tag: 'del', click: itemclick_, icon: '../images/icon/12.png', disable: true }
                    ]
                },
                onContextmenu: function (parm, e) {
                    actionCustomerID = parm.data.id;
                    menu.show({ top: e.pageY, left: e.pageX });
                    return false;
                },
                detail: {
                    height: 'auto',
                    onShowDetail: function (r, p) {
                        for (var n in r) {
                            if (r[n] == null) r[n] = "";
                        }
                        g1 = document.createElement('div');
                        $(p).append(g1);
                        $(g1).css('margin', 3).ligerGrid({
                            columns: [
                                { display: '级别', name: 'levels', width: 80 },
                                {
                                    display: '名称', name: 'name', width: 80, align: 'right'
                                },
                                { display: '备注', name: 'remark', width: 80 },
                                { display: '公母', name: 'gmsex', width: 80 },
                                //{ display: '类型', name: 'specifications', width: 80 },  
                                //{
                                //    display: '日期', name: 'create_time', width: 80, width: 120, render: function (item) {
                                //        return formatTimebytype(item.create_time, 'yyyy-MM-dd hh:mm');
                                //    }
                                //},
                                {
                                    display: '人员', name: 'create_id', width: 100
                                }

                            ],
                            toolbar: {
                                items: [
                                    { text: '增加二级',tag:'add', click: itemclick, icon: '../images/icon/11.png', disable:true,data:r},
                                    { text: '修改二级',tag:'update', click: itemclick, icon: '../images/icon/33.png', disable: true, data: r },
                                    { text: '删除', tag:'del', click: itemclick, icon: '../images/icon/12.png', disable: true, data: r}
                                ]
                            },
                            usePager: false,
                            checkbox: false,
                            url: "Basic_Boar_sys_Genealogy.grid.xhd?bid=" + getparastr("bid") + "&pid=" + r.id+"&level=2&rnd=" + Math.random(),
                            width: '99%', height: '580',
                            heightDiff: 0,
                            detail: {
                                height: 'auto',
                                onShowDetail: function (r, p) {
                                    for (var n in r) {
                                        if (r[n] == null) r[n] = "";
                                    }
                                    g2 = document.createElement('div');
                                    $(p).append(g2);
                                    $(g2).css('margin', 3).ligerGrid({
                                        columns: [
                                            { display: '级别', name: 'levels', width: 80 },
                                            {
                                                display: '名称', name: 'name', width: 80, align: 'right'
                                            },
                                            { display: '备注', name: 'remark', width: 80 },
                                            { display: '公母', name: 'gmsex', width: 80 },
                                            ////{ display: '类型', name: 'specifications', width: 80 },
                                            //{
                                            //    display: '日期', name: 'create_time', width: 80, width: 120, render: function (item) {
                                            //        return formatTimebytype(item.create_time, 'yyyy-MM-dd hh:mm');
                                            //    }
                                            //},
                                            {
                                                display: '人员', name: 'create_id', width: 100
                                            }

                                        ],
                                        toolbar: {
                                            items: [
                                                { text: '增加三级', tag: 'add', click: itemclick2, icon: '../images/icon/11.png', disable: true, data: r },
                                                { text: '修改三级', tag: 'update', click: itemclick2, icon: '../images/icon/33.png', disable: true, data: r },
                                                { text: '删除', tag: 'del', click: itemclick2, icon: '../images/icon/12.png', disable: true, data: r }
                                            ]
                                        },
                                        usePager: false,
                                        checkbox: false,
                                        url: "Basic_Boar_sys_Genealogy.grid.xhd?bid=" + getparastr("bid") + "&pid=" + r.id + "&level=3&rnd=" + Math.random(),
                                        width: '99%', height: '180',
                                        heightDiff: 0
                                    })

                                }
                            },
                        })

                    }
                },
            }); 

        });
        function itemclick_(item) {

            if (item.tag == "add") { 
                var manager = $("#maingrid4").ligerGetGridManager();
                var data = manager.getData(); 
                if (data.length > 1) { $.ligerDialog.error("不允许新增!"); return false; }
                f_openWindow('product/boar_sysId_map_edit_add.aspx?level=1&oid=' + getparastr("bid") + '&bid=' + getparastr("bid"), "新增一级", 600, 300, f_save_Series);
 
            }
            else if (item.tag == "update") {

                var manager = $("#maingrid4").ligerGetGridManager();
                var rows = manager.getSelectedRow();
                if (rows && rows != undefined) {
                    f_openWindow('product/boar_sysId_map_edit_add.aspx?level=1&id=' + rows.gid + '&oid=' + getparastr("bid") + '&bid=' + getparastr("bid"), "修改一级", 600, 300, f_save_Series);
                }
                else {
                    $.ligerDialog.warn('请选择品系！');
                }
            }
            else if (item.tag == "del") {
                var manager = $("#maingrid4").ligerGetGridManager();
                var rows = manager.getSelectedRow();
                if (rows && rows != undefined) {
                    delPara('Basic_Boar_sys_Genealogy.del.xhd', rows.id);
                }
                else {
                    $.ligerDialog.warn('请选择品系！');
                }

            }
        } 
        function itemclick(item) {
           
            if (item.tag == "add") { 
                var manager = $(g1).ligerGetGridManager(); 
                var data = manager.getData();

                if (data.length > 1) { $.ligerDialog.error("不允许新增!"); return false; }
                if (item.data.id) {
                    f_openWindow('product/boar_sysId_map_edit_add.aspx?level=2&bid=' + item.data.pid + '&oid=' + getparastr("bid") , "新增二级", 600, 300, f_save_Series);

                }
            }
            else if (item.tag == "update") {
                var manager = $(g1).ligerGetGridManager();
                var rows = manager.getSelectedRow();
                if (rows && rows != undefined) {
                    f_openWindow('product/boar_sysId_map_edit_add.aspx?level=2&bid=' + item.data.pid + "&id=" + rows.gid + '&oid=' + getparastr("bid"), "修改二级", 600, 300, f_save_Series);
                }
                else {
                    $.ligerDialog.warn('请选择品系！');
                }
            }
            else if (item.tag == "del") { 
                var manager = $(g1).ligerGetGridManager();
                var rows = manager.getSelectedRow();
                if (rows && rows != undefined) {
                    delPara('Basic_Boar_sys_Genealogy.del.xhd', rows.id);
                }
                else {
                    $.ligerDialog.warn('请选择品系！');
                }
               
            }
        }
        function itemclick2(item, id) {
            if (item.tag == "add") { 
                var manager = $(g2).ligerGetGridManager();
                var data = manager.getData();

                if (data.length > 1) { $.ligerDialog.error("不允许新增!"); return false; }
                if (item.data.id) {
                    f_openWindow('product/boar_sysId_map_edit_add.aspx?level=3&bid=' + item.data.pid + '&oid=' + getparastr("bid"), "新增三级", 600, 300, f_save_Series); 
                }
            }
            else if (item.tag == "update") {
                var manager = $(g2).ligerGetGridManager();
                var rows = manager.getSelectedRow();
                if (rows && rows != undefined) {
                    f_openWindow('product/boar_sysId_map_edit_add.aspx?level=3&id=' + rows.gid + "&bid=" + item.data.pid + '&oid=' + getparastr("bid"), "修改三级", 600, 300, f_save_Series);
                }
                else {
                    $.ligerDialog.warn('请选择等级！');
                }
            }
            else if (item.tag == "del") {
                var manager = $(g2).ligerGetGridManager();
                var rows = manager.getSelectedRow();
                if (rows && rows != undefined) {
                    delPara('Basic_Boar_sys_Genealogy.del.xhd',rows.id);
                }
                else {
                    $.ligerDialog.warn('请选择等级！');
                }

            }
        }
        function toolbar() {
            $.get("toolbar.GetSys.xhd?mid=product_list&rnd=" + Math.random(), function (data, textStatus) {
                var data = eval('(' + data + ')');
                //alert(data);
                var items = [];
                var arr = data.Items;
                for (var i = 0; i < arr.length; i++) {
                    arr[i].icon = "../" + arr[i].icon;
                    items.push(arr[i]);
                }
                items.push({ type: 'textbox', id: 'stext', text: '成品名：' });
                items.push({ type: 'button', text: '搜索', icon: '../images/search.gif', disable: true, click: function () { doserch() } });

                $("#toolbar").ligerToolBar({
                    items: items

                });
                menu = $.ligerMenu({
                    width: 120, items: getMenuItems(data)
                });
                $("#stext").ligerTextBox({ width: 200 });
                $("#maingrid4").ligerGetGridManager()._onResize();
            });
        }

 
        function delPara(url,id) {
            $.ligerDialog.confirm("删除不能恢复，确定删除？", function (yes) {
                if (yes) {
                    $.ajax({
                        url: url, type: "POST",
                        data: { id:id, rnd: Math.random() },
                        dataType: 'json',
                        success: function (result) {
                            $.ligerDialog.closeWaitting();

                            var obj = eval(result);

                            if (obj.isSuccess) {
                                f_load();
                            }
                            else {
                                $.ligerDialog.error(obj.Message);
                            }
                        },
                        error: function () {
                            top.$.ligerDialog.closeWaitting();
                            top.$.ligerDialog.error('删除失败！');
                        }
                    });
                }
            })

        } 
        function f_save_Series(item, dialog) {
            var issave = dialog.frame.f_save();
            console.log(1111)
            if (issave) {
                dialog.close();
                $.ligerDialog.waitting('数据保存中,请稍候...');
                $.ajax({
                    url: "Basic_Boar_sys_Genealogy.save2.xhd", type: "POST",
                    data: issave,
                    dataType: 'json',
                    success: function (result) {
                        $.ligerDialog.closeWaitting();

                        var obj = eval(result);

                        if (obj.isSuccess) {
                            f_load();
                        }
                        else {
                            $.ligerDialog.error(obj.Message);
                        }
                        //f_load();     
                    },
                    error: function () {
                        $.ligerDialog.closeWaitting();
                        $.ligerDialog.error('操作失败！');
                    }
                });

            }
        }

      
        function f_load() {
            var manager = $("#maingrid4").ligerGetGridManager();
            manager.loadData(true);
        }

    </script>
</head>
<body style="padding: 0px; overflow: hidden;">
    <div style="padding: 5px 10px 0px 5px;">
        <form id="form1" onsubmit="return false">
            <div id="layout1" style="">
           
                <div position="center">
                    <div id="toolbar" style="margin-top: 10px;"></div>
                    <div id="maingrid4" style="margin: -1px;"></div>

                </div>
            </div>
        </form>

    </div>
</body>
</html>
