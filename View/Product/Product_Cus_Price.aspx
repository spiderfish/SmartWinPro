<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" />
    <link href="../lib/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" />

    <script src="../lib/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/ligerui.min.js" type="text/javascript"></script>

    <script src="../lib/jquery-validation/jquery.validate.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/messages_cn.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/common.js" type="text/javascript"></script>
    <script src="../lib/jquery.form.js" type="text/javascript"></script>
     <%--<script src="../JS/JsBarcode.all.min.js"></script>--%>
    <script src="../JS/XHD.js" type="text/javascript"></script>
    <script type="text/javascript">
        //图标
        var jiconlist, winicons, currentComboBox;
        $(function () {
            $.metadata.setType("attr", "validate");
            XHD.validate($(form1));

            //$("#T_Contract_name").focus();
            $("form").ligerForm(); 

            $("#T_customer").ligerComboBox({
                width: 465,
                onBeforeOpen: f_selectCustomer
            })
            loadForm(getparastr("pid"), getparastr("cid"));
            $("#T_customer").val(getparastr("cid"))
            $("#T_payType").ligerComboBox({ url: 'Sys_Param.combo.xhd?type=pay_type', value: getparastr("payType")});
            //$("#T_payType").val(getparastr("payType"))
            $("#T_payCycle").val(getparastr("payCycle"))
            $("#T_payType").ligerGetComboBoxManager().setDisabled();
        });
        function f_selectCustomer() {
            $.ligerDialog.open({
                zindex: 9005, title: '选择客户', width: 500, height: 500, url: '../crm/customer/getCustomer.aspx', buttons: [
                    { text: '确定', onclick: f_selectCustomerOK },
                    { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
                ]
            });
            return false;
        }
        function f_selectCustomerOK(item, dialog) {
            var data = dialog.frame.f_select();
            if (!data) {
                alert('请选择行!');
                return;
            }

            $("#T_customer").val(data.cus_name);
            $("#T_customer_val").val(data.id);
            $("#T_payCycle").val(data.payCycle)
            $("#T_payType").ligerComboBox({
                url: 'Sys_Param.combo.xhd?type=pay_type', value: data.payType
            });
            //$("#T_payType").val(getparastr("payType"))
           
            dialog.close();
        }

        function f_save() {
            if ($(form1).valid()) {
                 
                    var sendtxt = "&cid=" + $("#T_customer_val").val() + "&pid=" + getparastr("pid");
                return $("form :input").fieldSerialize() + sendtxt;
            }
        }

        function loadForm(pid,cid) {
            $.ajax({
                type: "GET",
                url: "Product.formPrice.xhd", /* 注意后面的名字对应CS的方法名称 */
                data: { pid: pid,cid:cid, rnd: Math.random() }, /* 注意参数的格式和名称 */
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    var obj = eval(result);
                    for (var n in obj) {

                    }
                    console.log(obj)
                    $("#T_customer").val(obj.cus_name);
                    $("#T_customer_val").val(obj.customer_id); 
                    $("#T_Price").val(obj.price);
                    $("#T_remarks").val(obj.remarks); 
                   
                   

                }
            });
        }

        


    </script>
 

</head>
<body style="padding: 0px">
    <form id="form1" onsubmit="return false">
        <table border="0" cellpadding="3" cellspacing="1" style="background: #fff; width: 400px;" class="aztable">

            <tr>
                <td height="23" style="width: 85px" colspan="2">

                    <div align="left" style="width: 80px">客户：</div>
                </td>
                <td height="23">
                    
                    <input type="text" id="T_customer" name="T_customer"  validate="{required:true}" />

                </td>
            </tr>
               <tr>
                <td height="23" style="width: 85px" colspan="2">

                    <div align="left" style="width: 80px">付款方式：</div>
                </td>
                <td height="23">
                    
                    <input type="text" id="T_payType" name="T_payType"  validate="{required:true}" />

                </td>
            </tr>
               <tr>
                <td height="23" style="width: 85px" colspan="2">

                    <div align="left" style="width: 80px">付款周期：</div>
                </td>
                <td height="23">
                    
                    <input type="text" id="T_payCycle" name="T_payCycle"  validate="{required:true,disable:disable}" />

                </td>
            </tr>
            <tr>
                <td height="23" colspan="2">

                    <div align="left" style="width: 80px">价格</div>
                </td>
                <td height="23">
                    <input type="text" id="T_Price" name="T_Price"  ltype="float" validate="{required:true,disable:disable}" />
                </td>
            </tr>
                <tr>
                <td height="23" colspan="2">

                    <div align="left" style="width: 80px">备注：</div>
                </td>
                <td height="23">
                    <input type="text" id="T_remarks" name="T_remarks"  ltype="text" />
                </td>
            </tr>
     
            
            
        </table>
    </form>
</body>
</html>
