<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" />
    <link href="../lib/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" />
    <link href="../CSS/input.css" rel="stylesheet" type="text/css" />

    <script src="../lib/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/ligerui.min.js" type="text/javascript"></script>
    <script src="../lib/json2.js" type="text/javascript"></script>
    <script src="../JS/XHD.js" type="text/javascript"></script>
    <script src="../JS/JsBarcode.all.min.js"></script>
    <script type="text/javascript">
        var baritems = []; //创建一个空数组
        var g1; var Nobid = "N";
        $(function () {
            $("#maingrid4").ligerGrid({
                columns: [
                    {
                        display: '入舍日期', name: 'InTime', width: 150, render: function (item) {
                            return formatTimebytype(item.InTime, 'yyyy-MM-dd hh:mm');
                        }
                    },
                    //{ display: '转舍日期', name: 'InTime', width: 150, align: 'left' },
                    { display: '转入畜舍', name: 'InBorm', width: 80, align: 'center' },
                    { display: '转入栏号', name: 'InCorral', width: 120, align: 'center' },
                    { display: '入舍人员', name: 'accpetPerson', width: 80, align: 'center' },
                    {
                        display: '出舍日期', name: 'OutTime', width: 150, render: function (item) {
                            return item.OutTime>'1970-01-01'? formatTimebytype(item.OutTime, 'yyyy-MM-dd  hh:mm'):'';
                        }
                    },
                    { display: '出舍人', name: 'OutDormId', width: 80, align: 'left' },
                    //{ display: '转入栏号', name: 'OutCorral', width: 120 },
                   
                    { display: '备注', name: 'remarks', width: 120 } 
                   
                ],
                dataAction: 'local',
                pageSize: 30,
                pageSizeOptions: [20, 30, 50, 100],
                url: "Basic_Boar_changeLog.grid.xhd?bid=" + getparastr("bid"),
                width: '100%',
                height: '100%', 
                heightDiff: -10,
                onRClickToSelect: true,
              
                onContextmenu: function (parm, e) {
                    actionCustomerID = parm.data.id;
                    menu.show({ top: e.pageY, left: e.pageX });
                    return false;
                }, toolbar: {
                    items: [
                        { text: '入舍', tag: 'add', click: itemclick, icon: '../images/icon/11.png', disable: true },
                        { text: '出舍', tag: 'update', click: itemclick, icon: '../images/icon/33.png', disable: true },
                        { text: '调舍', tag: 'del', click: itemclick, icon: '../images/icon/12.png', disable: true }
                    ]
                },

            });
            initLayout();
            $(window).resize(function () {
                initLayout();
            }); 
        });
   
        function itemclick(item) { 

            var S_dormId = "", S_corralId=""; 
            $.ajax({
                url: "Basic_Boar_changeLog.GetChangeCheckData.xhd", type: "POST",
                data: { bid: getparastr("bid") },
                dataType: 'json',
                success: function (result) {
                    var obj = eval(result); 
                    S_dormId = obj.dormId;
                    S_corralId = obj.corralId; 
                    if (item.tag == "add") {
                        if (StringIsNull(S_dormId) || StringIsNull(S_corralId)) {
                            f_openWindow('product/getdorm.aspx?bid=' + getparastr("bid"), "入舍", 700, 380, f_save_In);
                        }
                        else {
                            $.ligerDialog.warn('已入栏，不能进行入舍操作。！');
                        }
                    }
                    else if (item.tag == "update") {
                        if (StringIsNull(S_dormId) || StringIsNull(S_corralId)) {
                            $.ligerDialog.warn('未分配舍栏，不能进行出舍操作！');
                        }
                        else {
                            f_save_Out()
                        }
                    }
                    else if (item.tag == "del") {
                        if (StringIsNull(S_dormId) || StringIsNull(S_corralId)) {
                            $.ligerDialog.warn('未分配舍栏，不能进行调舍操作！');
                        }
                        else {
                       
                            f_openWindow('product/getdorm.aspx?bid=' + getparastr("bid") + "&Nobid=N", "调舍", 700, 380, f_save_Change); 

                        }

                    }

                },
                error: function () {
                    $.ligerDialog.closeWaitting();
                    $.ligerDialog.error('操作失败！');
                }
            });
                    
           
        }
       
        function f_save_In(item, dialog) {
            var issave = dialog.frame.f_select();
            //console.log(issave)
            //return;
            if (issave) {
                dialog.close();
                var data= eval(issave)
                ajax("In", data.SelectTree.id, data.SelectRow.id); 
            }
        }
        function f_save_Out(bid) {   
                ajax("Out", "", "");
            
        }
        function f_save_Change(item, dialog) {
            var issave = dialog.frame.f_select(); 
            if (issave) {
                dialog.close();
                var data = eval(issave)
                $.ajax({
                    url: "Basic_Boar_changeLog.ChangeCheck.xhd", type: "POST",
                    data: { DormId: data.SelectTree.id, CorralId: data.SelectRow.id, id: getparastr("bid") },
                    dataType: 'json',
                    success: function (result) {
                        $.ligerDialog.closeWaitting();
                        var obj = eval(result); 
                        if (obj.isSuccess) {
                            Nobid = "Y"; 
                            ajax("Change", data.SelectTree.id, data.SelectRow.id);
                        }
                        else {
                            $.ligerDialog.confirm(obj.Message + "是否调换？", function (yes) {
                                if (yes) {
                                    Nobid = "N";
                                    ajax("Change", data.SelectTree.id, data.SelectRow.id);
                                }
                            })
                        }
                      
                        //f_load();     
                    },
                    error: function () {
                        $.ligerDialog.closeWaitting();
                        $.ligerDialog.error('操作失败！');
                    }
                });

              
            }
        }

        function ajax(type, DormId, CorralId) { 
            $.ligerDialog.waitting('数据保存中,请稍候...');
            $.ajax({
                url: "Basic_Boar_changeLog.save.xhd", type: "POST",
                data: { bid: getparastr("bid"), type: type, DormId: DormId, CorralId: CorralId, Nobid: Nobid},
                dataType: 'json',
                success: function (result) {
                    $.ligerDialog.closeWaitting(); 
                    var obj = eval(result);

                    if (obj.isSuccess) {
                        f_reload();
                    }
                    else {
                        $.ligerDialog.error(obj.Message);
                    }
                    //f_load();     
                },
                error: function () {
                    $.ligerDialog.closeWaitting();
                    $.ligerDialog.error('操作失败！');
                }
            });
        }
          
        function f_reload() {
            var manager = $("#maingrid4").ligerGetGridManager();
            manager.loadData(true);
        };
    </script>
    <style type="text/css">
        .l-leaving { background: #eee; color: #999; }
    </style>

</head>
<body>

    <form id="form1" onsubmit="return false">
        <div style="padding:10px;">
            <div id="toolbar"></div>
            
            <div id="maingrid4" style="margin: -1px;"></div>
            <img id="bar" />
        </div>
    </form>


</body>
</html>
