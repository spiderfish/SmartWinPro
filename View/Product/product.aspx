<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>

    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" />
    <link href="../lib/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" />
    <link href="../CSS/input.css" rel="stylesheet" type="text/css" />
    <script src="../lib/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/ligerui.min.js" type="text/javascript"></script> 
    <script src="../JS/XHD.js" type="text/javascript"></script>
    <script src="../lib/jquery.form.js" type="text/javascript"></script>

    <script type="text/javascript">

        var manager = "",g1,g2;
        var treemanager;
        $(function () {
            $("#layout1").ligerLayout({ leftWidth: 200, allowLeftResize: false, allowLeftCollapse: true, space: 2, heightDiff: -5 });
            $("#tree1").ligerTree({
                url: 'Product_category.tree.xhd?rnd=' + Math.random(),
                onSelect: onSelect,
                idFieldName: 'id',
                //parentIDFieldName: 'pid',
                usericon: 'd_icon',
                checkbox: false,
                itemopen: false,
                onSuccess: function () {
                    $(".l-first div:first").click();
                }
            });

            treemanager = $("#tree1").ligerGetTreeManager();

            initLayout();
            $(window).resize(function () {
                initLayout();
            });

            $("#maingrid4").ligerGrid({
                columns: [
                    //{ display: '序号', width: 50, render: function (item,i) { return item.n; } },
                    { display: '种源类型', name: 'product_name', width: 120 },
                    { display: '品种名称', name: 'category_name', width: 120 },
                    { display: '种禽', name: 'specifications', width: 60 },
                    { display: '品系', name: 'Series_Name', width: 80 },
                    { display: '等级', name: 'Level_Name', width: 80 },
                    { display: '单位', name: 'unit', width: 80 },
                     { display: '每份剂量', name: 'price', width: 80 },
                     
					{ display: '产出份数', name: 'dailyoutput', width: 80 },
					
                  {
                        display: '成本价（￥）', name: 'cost', width: 80, align: 'right', render: function (item) {
                            return toMoney(item.cost);
                        }
                    },
                  /*  
                    {
                        display: '报价（￥）', name: 'price', width: 120, align: 'right', render: function (item) {
                            return toMoney(item.price);
                        }
                    },*/
                    {
                        display: '销售价（￥）', name: 'agio', width: 80, align: 'right', render: function (item) {
                            return toMoney(item.agio);
                        }
                    },
                   
                    { display: '采集周期(天)', name: 'pickcycle', width: 100 },
                    { display: '备注', name: 'remarks', width: 120 }

                ],
                 
                dataAction: 'server',
                url: "Product_category.grid.xhd?categoryid=0&rnd=" + Math.random(),
                pageSize: 30,
                pageSizeOptions: [20, 30, 50, 100],
                width: '100%',
                height: '100%',
                heightDiff: -8, 
                onContextmenu: function (parm, e) {
                    actionCustomerID = parm.data.id;
                    menu.show({ top: e.pageY, left: e.pageX });
                    return false;
                },
                detail: {
                    height: 'auto',
                    onShowDetail: function (r, p) {
                        for (var n in r) {
                            if (r[n] == null) r[n] = "";
                        }
                        g1 = document.createElement('div');
                        $(p).append(g1);
                        $(g1).css('margin', 3).ligerGrid({
                            columns: [
                              
                                {
                                    display: '客户', name: 'cus_name', width: 180, align: 'right'
                                },
                                { display: '价格', name: 'price', width: 80 },
                                {
                                    display: '备注', name: 'remarks', width: 100
                                },
                                {
                                    display: '支付方式', name: 'payTypeName', width: 100
                                },
                                {
                                    display: '支付周期', name: 'payCycle', width: 100
                                },
                                {
                                    display: '日期', name: 'createTime', width: 80, width: 120, render: function (item) {
                                        return formatTimebytype(item.createTime, 'yyyy-MM-dd hh:mm');
                                    }
                                },
                                {
                                    display: '人员', name: 'createName', width: 100
                                }

                            ],
                            toolbar: {
                                items: [
                                    { text: '增加', tag: 'add', click: itemclickPrice, icon: '../images/icon/11.png', disable:true,data:r},
                                    { text: '修改', tag: 'update', click: itemclickPrice, icon: '../images/icon/33.png', disable: true, data: r },
                                    { text: '删除', tag: 'del', click: itemclickPrice, icon: '../images/icon/12.png', disable: true, data: r}
                                ]
                            },
                            usePager: false,
                            checkbox: false,
                            url: "Product.gridPrice.xhd?pid=" + r.id,
                            width: '99%', height: '380',
                            heightDiff: 0,
                  
                        })

                    }
                },
            });
            toolbar();

        });
        function itemclickPrice(item) {

            if (item.tag == "add") {
                if (item.data.id) {
                    f_openWindow('product/Product_Cus_Price.aspx?pid=' + item.data.id, "新增产品客户价格", 900, 680, f_save_Price);

                }
            }
            else if (item.tag == "update") {
                var manager = $(g1).ligerGetGridManager();
                var rows = manager.getSelectedRow();
                if (rows && rows != undefined) {
                    f_openWindow('product/Product_Cus_Price.aspx?pid=' + item.data.id + "&cid=" + rows.customer_id + '&payType=' + rows.payType + '&payCycle=' + rows.payCycle, "修改产品客户价格", 900, 680, f_save_Price);
                }
                else {
                    $.ligerDialog.warn('请选择产品！');
                }
            }
            else if (item.tag == "del") {
                var manager = $(g1).ligerGetGridManager();
                var rows = manager.getSelectedRow();
                if (rows && rows != undefined) {
                    delPara('Product.delPrice.xhd', rows.id);
                }
                else {
                    $.ligerDialog.warn('请选择产品！');
                }

            }
        }
        function itemclick(item) {
           
            if (item.tag == "add") { 
                if (item.data.id) {
                    f_openWindow('product/Product_Series_add.aspx?pid=' + item.data.id, "新增品系", 700, 380, f_save_Series);

                }
            }
            else if (item.tag == "update") {
                var manager = $(g1).ligerGetGridManager();
                var rows = manager.getSelectedRow();
                if (rows && rows != undefined) {
                    f_openWindow('product/Product_Series_add.aspx?pid=' + item.data.id + "&id=" + rows.id, "修改品系", 700, 380, f_save_Series);
                }
                else {
                    $.ligerDialog.warn('请选择品系！');
                }
            }
            else if (item.tag == "del") { 
                var manager = $(g1).ligerGetGridManager();
                var rows = manager.getSelectedRow();
                if (rows && rows != undefined) {
                    delPara('Basic_Series.del.xhd', rows.id);
                }
                else {
                    $.ligerDialog.warn('请选择品系！');
                }
               
            }
        }
        function itemclick2(item, id) {
            if (item.tag == "add") { 
                if (item.data.id) {
                    f_openWindow('product/Product_Series_Level_add.aspx?pid=' + item.data.id, "新增等级", 700, 380, f_save_Level); 
                }
            }
            else if (item.tag == "update") {
                var manager = $(g2).ligerGetGridManager();
                var rows = manager.getSelectedRow();
                if (rows && rows != undefined) {
                    f_openWindow('product/Product_Series_Level_add.aspx?id=' + rows.id + "&pid=" + item.data.id, "修改等级", 700, 380, f_save_Level);
                }
                else {
                    $.ligerDialog.warn('请选择等级！');
                }
            }
            else if (item.tag == "del") {
                var manager = $(g2).ligerGetGridManager();
                var rows = manager.getSelectedRow();
                if (rows && rows != undefined) {
                    delPara('Basic_Series_Level.del.xhd',rows.id);
                }
                else {
                    $.ligerDialog.warn('请选择等级！');
                }

            }
        }
        function toolbar() {
            $.get("toolbar.GetSys.xhd?mid=product_list&rnd=" + Math.random(), function (data, textStatus) {
                var data = eval('(' + data + ')');
                //alert(data);
                var items = [];
                var arr = data.Items;
                for (var i = 0; i < arr.length; i++) {
                    arr[i].icon = "../" + arr[i].icon;
                    items.push(arr[i]);
                }
                items.push({ type: 'textbox', id: 'stext', text: '成品名：' });
                items.push({ type: 'button', text: '搜索', icon: '../images/search.gif', disable: true, click: function () { doserch() } });

                $("#toolbar").ligerToolBar({
                    items: items

                });
                menu = $.ligerMenu({
                    width: 120, items: getMenuItems(data)
                });
                $("#stext").ligerTextBox({ width: 200 });
                $("#maingrid4").ligerGetGridManager()._onResize();
            });
        }


        function onSelect(note) {
            var manager = $("#maingrid4").ligerGetGridManager();
            var url = "Product.grid.xhd?categoryid=" + note.data.id + "&rnd=" + Math.random();
            manager._setUrl(url);
        }
        //查询
        function doserch() {
            var sendtxt = "&rnd=" + Math.random();
            var serchtxt = $("#form1 :input").fieldSerialize() + sendtxt;

            var manager = $("#maingrid4").ligerGetGridManager();
            manager._setUrl("Product.grid.xhd?" + serchtxt);
        }

        //重置
        function doclear() {
            //var serchtxt = $("#serchform :input").reset();
            $("#form1").each(function () {
                this.reset();
            });
        }
      
        function edit() {
            var manager = $("#maingrid4").ligerGetGridManager();
            var rows = manager.getSelectedRow();
            if (rows && rows != undefined) {
                f_openWindow('product/product_add.aspx?pid=' + rows.id, "修改成品", 1440, 600, f_save);
            }
            else {
                $.ligerDialog.warn('请选择成品！');
            }
        }
        function add() {
            var notes = $("#tree1").ligerGetTreeManager().getSelected();

            if (notes != null && notes != undefined) {
                f_openWindow('product/product_add.aspx?categoryid=' + notes.data.id, "新增成品", 700, 780, f_save);
            }
            else {
                $.ligerDialog.warn('请选择成品类别！');
            }
        }
        function delPara(url,id) {
            $.ligerDialog.confirm("删除不能恢复，确定删除？", function (yes) {
                if (yes) {
                    $.ajax({
                        url: url, type: "POST",
                        data: { id:id, rnd: Math.random() },
                        dataType: 'json',
                        success: function (result) {
                            $.ligerDialog.closeWaitting();

                            var obj = eval(result);

                            if (obj.isSuccess) {
                                f_load();
                            }
                            else {
                                $.ligerDialog.error(obj.Message);
                            }
                        },
                        error: function () {
                            top.$.ligerDialog.closeWaitting();
                            top.$.ligerDialog.error('删除失败！');
                        }
                    });
                }
            })

        }
        function del() {
            var manager = $("#maingrid4").ligerGetGridManager();
            var row = manager.getSelectedRow();
            if (row) {
                   if (row.product_name == '自动生成') {

                    $.ligerDialog.error("此项目禁止删除！");
                    return;
                }
                $.ligerDialog.confirm("成品删除不能恢复，确定删除？", function (yes) {
                    if (yes) {
                        $.ajax({
                            url: "Product.del.xhd", type: "POST",
                            data: { id: row.id, rnd: Math.random() },
                            dataType: 'json',
                            success: function (result) {
                                $.ligerDialog.closeWaitting();

                                var obj = eval(result);

                                if (obj.isSuccess) {
                                    f_load();
                                }
                                else {
                                    $.ligerDialog.error(obj.Message);
                                }
                            },
                            error: function () {
                                top.$.ligerDialog.closeWaitting();
                                top.$.ligerDialog.error('删除失败！');
                            }
                        });
                    }
                })
            }
            else {
                $.ligerDialog.warn("请选择成品");
            }

        }

        function f_save_Price(item,dialog) {
            var issave = dialog.frame.f_save();
            if (issave) {
                dialog.close();
                $.ligerDialog.waitting('数据保存中,请稍候...');
                $.ajax({
                    url: "Product.savePrice.xhd", type: "POST",
                    data: issave,
                    dataType: 'json',
                    success: function (result) {
                        $.ligerDialog.closeWaitting();

                        var obj = eval(result);

                        if (obj.isSuccess) {
                            f_load();
                        }
                        else {
                            $.ligerDialog.error(obj.Message);
                        }
                        //f_load();     
                    },
                    error: function () {
                        $.ligerDialog.closeWaitting();
                        $.ligerDialog.error('操作失败！');
                    }
                });

            }
        }

        function f_save_Series(item, dialog) {
           
        }

        function f_save_Level(item, dialog) {
            var issave = dialog.frame.f_save();
            if (issave) {
                dialog.close();
                $.ligerDialog.waitting('数据保存中,请稍候...');
                $.ajax({
                    url: "Basic_Series_Level.save.xhd", type: "POST",
                    data: issave,
                    dataType: 'json',
                    success: function (result) {
                        $.ligerDialog.closeWaitting();

                        var obj = eval(result);

                        if (obj.isSuccess) {
                            f_load();
                        }
                        else {
                            $.ligerDialog.error(obj.Message);
                        }
                        //f_load();     
                    },
                    error: function () {
                        $.ligerDialog.closeWaitting();
                        $.ligerDialog.error('操作失败！');
                    }
                });

            }
        }
        function f_save(item, dialog) {
            var issave = dialog.frame.f_save();
            if (issave) {
                dialog.close();
                $.ligerDialog.waitting('数据保存中,请稍候...');
                $.ajax({
                    url: "Product.save.xhd", type: "POST",
                    data: issave,
                    dataType: 'json',
                    success: function (result) {
                        $.ligerDialog.closeWaitting();

                        var obj = eval(result);

                        if (obj.isSuccess) {
                            f_load();
                        }
                        else {
                            $.ligerDialog.error(obj.Message);
                        }
                        //f_load();     
                    },
                    error: function () {
                        $.ligerDialog.closeWaitting();
                        $.ligerDialog.error('操作失败！');
                    }
                });

            }
        }
        function f_load() {
            var manager = $("#maingrid4").ligerGetGridManager();
            manager.loadData(true);
        }

    </script>
</head>
<body style="padding: 0px; overflow: hidden;">
    <div style="padding: 5px 10px 0px 5px;">
        <form id="form1" onsubmit="return false">
            <div id="layout1" style="">
                <div position="left" title="品种">
                    <div id="treediv" style="width: 200px; height: 100%; margin: -1px; float: left; overflow: auto;margin-top:2px;">
                        <ul id="tree1"></ul>
                    </div>
                </div>
                <div position="center">
                    <div id="toolbar" style="margin-top: 10px;"></div>
                    <div id="maingrid4" style="margin: -1px;"></div>

                </div>
            </div>
        </form>

    </div>
</body>
</html>
