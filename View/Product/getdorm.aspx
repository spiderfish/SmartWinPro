<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" />
    <link href="../lib/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" />
    <link href="../CSS/input.css" rel="stylesheet" type="text/css" />

    <script src="../lib/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/ligerui.min.js" type="text/javascript"></script>
    <script src="../lib/json2.js" type="text/javascript"></script>
    <script src="../JS/XHD.js" type="text/javascript"></script>
    <script src="../JS/JsBarcode.all.min.js"></script>
    <script type="text/javascript">
        var baritems = []; //创建一个空数组
        var g1; var SelectTreeData;
        $(function () {
            $("#layout1").ligerLayout({ leftWidth: 200, allowLeftResize: false, allowLeftCollapse: true, space: 2, heightDiff: -5 });
            $("#tree1").ligerTree({
                url: 'Basic_Dorm.tree.xhd?rnd=' + Math.random(),
                onSelect: onSelect,
                idFieldName: 'id',
                //parentIDFieldName: 'pid',
                usericon: 'd_icon',
                checkbox: false,
                itemopen: false,
                onSuccess: function(note) {
                    $(".l-first div:first").click();
                    console.log(note[0])
                    SelectTreeData = note[0];
                }
            });

            treemanager = $("#tree1").ligerGetTreeManager();

            initLayout();
            $(window).resize(function () {
                initLayout();
            });


            $("#maingrid4").ligerGrid({
                columns: [
                    { display: '名称', name: 'corralName', width: 250, align: 'left' },
                    { display: '面积', name: 'area', width: 250, align: 'left' },
                    { display: '备注', name: 'remarks', width: 120 },
                    {
                        display: '图标', name: 'def1', width: 50, render: function (item) {
                            return "<div style='margin-top:8px;'><img class='imgbar' src='../" + item.def1 + "'/></div>";
                        }
                    },

                ],
                dataAction: 'local',
                pageSize: 30,
                pageSizeOptions: [20, 30, 50, 100],
                url: "Basic_Corral.grid.xhd?pid=0&rnd=" + Math.random(),
                width: '100%',
                height: '100%', 
                heightDiff: -10,
                onRClickToSelect: true,

                onContextmenu: function (parm, e) {
                    actionCustomerID = parm.data.id;
                    menu.show({ top: e.pageY, left: e.pageX });
                    return false;
                } 

            });
            initLayout();
            $(window).resize(function () {
                initLayout();
            }); 
        });
      
        function onSelect(note) {
            var manager = $("#maingrid4").ligerGetGridManager(); 
            SelectTreeData = note.data;
            var url = "Basic_Corral.grid.xhd?dornId=" + note.data.id + "&rnd=" + Math.random();
            manager._setUrl(url);
        }
    

        function f_select() {
            var manager = $("#maingrid4").ligerGetGridManager();
            var rows = manager.getSelectedRow();
            data = {
                SelectTree: SelectTreeData,
                SelectRow:rows
            }
            return data;
        }
        function f_reload() {
            var manager = $("#maingrid4").ligerGetGridManager();
            manager.loadData(true);
        };
    </script>
    <style type="text/css">
        .l-leaving { background: #eee; color: #999; }
    </style>

</head>
<body>

        <form id="form1" onsubmit="return false">
            <div id="layout1" style="">
                <div position="left" title="畜舍">
                    <div id="treediv" style="width: 200px; height: 100%; margin: -1px; float: left; overflow: auto;margin-top:2px;">
                        <ul id="tree1"></ul>
                    </div>
                </div>
                <div position="center">
                    <div id="toolbar" style="margin-top: 10px;"></div>
                    <div id="maingrid4" style="margin: -1px;"></div>

                </div>
            </div>
        </form>


</body>
</html>
