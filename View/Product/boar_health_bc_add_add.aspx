<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" />
    <link href="../lib/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" />

    <script src="../lib/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/ligerui.min.js" type="text/javascript"></script>

    <script src="../lib/jquery-validation/jquery.validate.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/messages_cn.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/common.js" type="text/javascript"></script>
    <script src="../lib/jquery.form.js" type="text/javascript"></script>
     <script src="../JS/JsBarcode.all.min.js"></script>
    <script src="../JS/XHD.js" type="text/javascript"></script>
    <script type="text/javascript">
        //图标
        var jiconlist, winicons, currentComboBox;
        $(function () {
            $.metadata.setType("attr", "validate");
            XHD.validate($(form1));

            //$("#T_Contract_name").focus();
            $("form").ligerForm();
            $("#T_monitorPerson").ligerComboBox({
                width: 180,
                onBeforeOpen: f_selectEmp
            })
             
        });
        function f_selectEmp() {
            $.ligerDialog.open({
                zindex: 9005, title: '选择员工', width: 650, height: 300, url: '../hr/getemp_auth.aspx?auth=3', buttons: [
                    { text: '确定', onclick: f_selectEmpOK },
                    { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
                ]
            });
            return false;
        }

        function f_selectEmpOK(item, dialog) {
            var data = dialog.frame.f_select();
            if (!data) {
                alert('请选择行!');
                return;
            }

            $("#T_monitorPerson").val(data.name);
            $("#T_monitorPerson_val").val(data.id);
            dialog.close();
        }

        function f_save() {
            if ($(form1).valid()) {
                var sendtxt = "&corralId=" + getparastr("cid") + "&dornId=" + getparastr("bid");
                return $("form :input").fieldSerialize() + sendtxt;
            }
        }
 

    </script>
    <style type="text/css">
        .iconlist { width: 360px; padding: 3px; }
            .iconlist li { border: 1px solid #FFFFFF; float: left; display: block; padding: 2px; cursor: pointer; }
                .iconlist li.over { border: 1px solid #516B9F; }
                .iconlist li img { height: 16px; height: 16px; }
    </style>

</head>
<body style="padding: 0px">
    <form id="form1" onsubmit="return false">
        <table border="0" cellpadding="3" cellspacing="1" style="background: #fff; width: 400px;" class="aztable">
 
            <tr>
                <td height="23" colspan="2">

                    <div align="left" style="width: 62px">温度(℃)：</div>
                </td>
                <td height="23">
                   <input type="text" id="T_temperature" name="T_temperature" ltype="float" ligerui="{width:180}" validate="{required:true}" />
                
                </td>
            </tr>
                  <tr>
                <td height="23" colspan="2">

                    <div align="left" style="width: 62px">湿度(%)：</div>
                </td>
                <td height="23">
                   <input type="text" id="T_humidity" name="T_humidity" ltype="float" ligerui="{width:180}" validate="{required:true}"  />
                
                </td>
            </tr>
                 <tr>
                <td height="23" colspan="2">

                    <div align="left" style="width: 62px">监控人：</div>
                </td>
                <td height="23">
                   <input type="text" id="T_monitorPerson" name="T_monitorPerson"   />
                
                </td>
            </tr>
         
            
        </table>
    </form>
</body>
</html>
