<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" />
    <link href="../lib/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" />
    <link href="../CSS/input.css" rel="stylesheet" type="text/css" />
    
    <script src="../lib/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/ligerui.min.js" type="text/javascript"></script>
        <script src="../lib/jquery.form.js" type="text/javascript"></script>

    <script src="../JS/XHD.js" type="text/javascript"></script>
    <script type="text/javascript">

        var manager = ""; var nodeid = "0";
        var treemanager;
        $(function () {
            $("#layout1").ligerLayout({ leftWidth: 200, allowLeftResize: false, allowLeftCollapse: true, space: 2, heightDiff: -5 });
            $("#tree1").ligerTree({
                //url: 'Sys_Param.tree.xhd?type=poultry_type&rnd=' + Math.random(),
                url: 'Product.tree.xhd?rnd=' + Math.random(),
                onSelect: onSelect,
                idFieldName: 'id',
                //parentIDFieldName: 'pid',
                //usericon: 'd_icon',
                checkbox: false,
                itemopen: false,
                onSuccess: function () {
                    $(".l-first div:first").click();
                }
            });

            treemanager = $("#tree1").ligerGetTreeManager();

            initLayout();
            $(window).resize(function () {
                initLayout();
            });

            $("#maingrid4").ligerGrid({
                columns: [
                    //{ display: '序号', width: 50, render: function (item,i) { return item.n; } },
                    { display: 'ID', name: 'barcode', width: 120 },
                { display: '分配状态', name: 'status', width: 60, render: function (item) {

                                var html;
                                if (item.status == "Y") {
                                    html = "<div style='color:#339900'>";
                                    html += "可用";
                                    html += "</div>";
                                }
                                else if (item.status == "N") {
                                    html = "<div style='color:#FF0000'>";
                                    html += "使用中";
                                    html += "</div>";
                                }
                               
                               
                                else {
                                    html = item.status;
                                }
                                return html;
                            }
                    },
                { display: '采集状态', name: 'CollectionStatus', width: 60, render: function (item) {

                                var html;
                                if (item.CollectionStatus == "1") {
                                    html = "<div style='color:#339900'>";
                                    html += "可采";
                                    html += "</div>";
                                }
                                else if (item.CollectionStatus == "3") {
                                    html = "<div style='color:#FF0000'>";
                                    html += "禁采";
                                    html += "</div>";
                                }
                               
                               
                                else {
                                    html = item.CollectionStatus;
                                }
                                return html;
                            }
                    },
                {
                        display: '上次采集', name: 'LastCollecTime', width: 90, render: function (item) {
                            return formatTimebytype(item.LastCollecTime, 'yyyy-MM-dd');
                        }
                    },
                {
                        display: '年龄', name: 'LastCollecTime', width: 90, render: function (item) {
                            return formatTimebytype(item.LastCollecTime, 'yyyy-MM-dd');
                        }
                    },
                    { display: '种猪名称', name: 'product_name', width: 120 },
                    { display: '种猪品种', name: 'category_name', width: 120 },
                    { display: '品种类型', name: 'specifications', width: 120 },
                    //{
                    //    display: '成本价（￥）', name: 'cost', width: 120, align: 'right', render: function (item) {
                    //        return toMoney(item.cost);
                    //    }
                    //},
                    //{
                    //    display: '报价（￥）', name: 'price', width: 120, align: 'right', render: function (item) {
                    //        return toMoney(item.price);
                    //    }
                    //},
                    //{
                    //    display: '销售价（￥）', name: 'agio', width: 120, align: 'right', render: function (item) {
                    //        return toMoney(item.agio);
                    //    }
                    //},
                    { display: '单位', name: 'unit', width: 120 },
                    { display: '备注', name: 'remarks', width: 120 }

                ],
                dataAction: 'server',
                url: "Basic_boar.grid.xhd&categoryid=0rnd=" + Math.random(),
                pageSize: 30,
                pageSizeOptions: [20, 30, 50, 100],
                width: '100%',
                height: '100%',
                heightDiff: -8,

                onContextmenu: function (parm, e) {
                    actionCustomerID = parm.data.id;
                    menu.show({ top: e.pageY, left: e.pageX });
                    return false;
                }
            });

            var items = [];
            items.push({ type: 'textbox', id: 'stext', text: '' });
            items.push({ type: 'button', text: '搜索', icon: '../images/search.gif', disable: true, click: function () { doserch() } });
            $("#toolbar").ligerToolBar({
                items: items

            });
            $("#stext").ligerTextBox({ width: 200 });
            $("#maingrid4").ligerGetGridManager()._onResize();
            
        });
        //查询
        function doserch() {
            var manager = $("#maingrid4").ligerGetGridManager(); 
            var sendtxt = "&rnd=" + Math.random();
            var serchtxt = $("#form1 :input").fieldSerialize() + sendtxt; 
            manager._setUrl("Basic_boar.grid.xhd?categoryid=" + nodeid+"&" + serchtxt);
        }

        function onSelect(note) {
            var manager = $("#maingrid4").ligerGetGridManager();
            nodeid = note.data.id;
            var url = "Basic_boar.grid.xhd?categoryid=" + note.data.id + "&rnd=" + Math.random();
            manager._setUrl(url);
        }


         function f_select() {
            var manager = $("#maingrid4").ligerGetGridManager();
            var rows = manager.getSelectedRow(); 
            return rows;
        }
    </script>
</head>
<body style="padding: 0px;overflow:hidden;">
    <form id="form1" onsubmit="return false">
        <div id="layout1" style="margin: -1px">
            <div position="left" title="种禽类别">
                <div id="treediv" style="width: 250px; height: 100%; margin: -1px; float: left; border: 1px solid #ccc; overflow: auto;">
                    <ul id="tree1"></ul>
                </div>
            </div>
            <div position="center">
                <div id="toolbar"></div>
                <div id="maingrid4" style="margin: -1px;"></div>

            </div>
        </div>


        <!--<a class="l-button" onclick="getChecked()" style="float:left;margin-right:10px;">获取选择(复选框)</a> -->
        <div style="display: none">
            <!--  数据统计代码 -->
        </div>
    </form>
</body>
</html>
