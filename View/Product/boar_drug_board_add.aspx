<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" />
    <link href="../lib/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" />

    <script src="../lib/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/ligerui.min.js" type="text/javascript"></script>

    <script src="../lib/jquery-validation/jquery.validate.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/messages_cn.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/common.js" type="text/javascript"></script>
    <script src="../lib/jquery.form.js" type="text/javascript"></script>
     <%--<script src="../JS/JsBarcode.all.min.js"></script>--%>
    <script src="../JS/XHD.js" type="text/javascript"></script>
    <script type="text/javascript">
        //图标
        var jiconlist, winicons, currentComboBox;
        $(function () {
            $.metadata.setType("attr", "validate");
            XHD.validate($(form1));

            //$("#T_Contract_name").focus();
          // $("form").ligerForm();  

          //  loadForm(getparastr("boarId")); 
            //$("#T_drugName").ligerComboBox({
            //    width: 200,
            //    onBeforeOpen: f_selectorder
            //})  
            $("#maingrid4").ligerGrid({
                columns: [
                    //{ display: '公司', name: 'name', align: 'left', width: 150 },
                    { display: '药品', name: 'drugName', width: 100 },
                    { display: '单位', name: 'unit', width: 100 },
                    { display: '用药周期', name: 'drugcycle', width: 100 },
                    //{ display: '入职日期', name: 'IncomeDay', type: 'date', format: 'yyyy年MM月dd', width: 100, editor: { type: 'date' } },
                    {
                        display: '剂量', name: 'Dosage', width: 180, type: 'float',
                        editor: { type: 'float' }

                    }, {
                        display: '用药时间', name: 'drugTime', width: 180, type: 'date',
                        editor: { type: 'date' }

                    } 

                ],
                enabledEdit: true,
                onAfterEdit: f_onAfterEdit,
                usePager: false, 
                url: "Basic_Boar_Drug.grid.xhd?id=0",
                width: "100%", height: "100%",
                heightDiff: 0
            });
            toolbar();
        });
        function f_onAfterEdit(e) {
        
           // var manager = $("#maingrid4").ligerGetGridManager(); 
           // console.log(e)
          //  manager.updateCell('Dosage', e.record.Dosage); 
        }   
         
        function f_getpost(item, dialog) { 
           var rows= dialog.frame.f_select()
            if (!rows) {
                alert('请选择药品!');
                return;
            }
            else { 
                //过滤重复
                var manager = $("#maingrid4").ligerGetGridManager();
                var data = manager.getData();

                for (var i = 0; i < rows.length; i++) {
                    rows[i].IDCode = rows[i].IDCode;
                    var add = 1;
                    for (var j = 0; j < data.length; j++) {
                        if (rows[i].IDCode == data[j].IDCode) {
                            add = 0;
                        }
                    }
                    if (add == 1) { 
                        rows[i].id = rows[i].IDCode;
                        rows[i].Dosage = 1;
                        rows[i]["boarId"] = getparastr("boarId");
                        rows[i]["drugName"] = rows[i].drugName;
                        rows[i]["drugcycle"] = rows[i].drugcycle;
                        rows[i]["drugId"] = rows[i].IDCode;
                        rows[i]["unit"] = rows[i].unit;  
                        rows[i]["drugTime"] = getNowFormatDate();  
                        manager.addRow(rows[i]);
                    }
                }
                dialog.close();
            } 
        }

        function toolbar() {

            var items = [];
          
            items.push({ type: 'button', text: '选择药品', icon: '../images/add.gif', disable: true, click: function () { f_selectorder() } });


            $("#toolbar").ligerToolBar({
                items: items

            });

        }
        function f_selectorder() {
            $.ligerDialog.open({
                zindex: 9005, title: '选择药品', width: 750, height: 400, url: 'GetMulbasic_drug.aspx', buttons: [
                    { text: '确定', onclick: f_getpost },
                    { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
                ]
            });
            return false;
        }
        function f_selectorderOK(item, dialog) {
            var data = dialog.frame.f_select();
            if (!data) {
                alert('请选择行!');
                return;
            }
            //alert(JSON.stringify(data.order_id))
            $("#T_drugName").val(data.drugName); 
            $("#T_drugName_val").val(data.IDCode);//订单明细编号（产品编号）
            $("#T_unit").val(data.unit);
          //drugcycle

            dialog.close();
        }
        function f_save() {
            var manager = $("#maingrid4").ligerGetGridManager();
            var data = manager.getData();
            //console.log(data)
            //datas = {
            //    bid: getparastr("boarId") ,
            //    listData: data
            //}
            var sendtxt = "bid=" + getparastr("boarId");
            sendtxt += "&PostData=" + JSON.stringify(data);
            return sendtxt;
            
            //var sendtxt = "&bid=" + getparastr("boarId") + "&id=" + getparastr("id");
            //    return $("form :input").fieldSerialize() + sendtxt;
           
        }

        function loadForm(oaid) {
            $.ajax({
                type: "GET",
                url: "Basic_Boar_sys.form.xhd", /* 注意后面的名字对应CS的方法名称 */
                data: { id: oaid, rnd: Math.random() }, /* 注意参数的格式和名称 */
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    var obj = eval(result);
                    for (var n in obj) { 
                    }  
                    $("#T_drugName").val(obj.drugName);
                    $("#T_unit").val(obj.unit);
                   
                }
            });
        }
   
        
    </script>
 

</head>
<body style="padding: 0px">

       <div id="toolbar"></div>
    <form id="form1" onsubmit="return false">
        <%--<table border="0" cellpadding="3" cellspacing="1" style="background: #fff; width: 400px;" class="aztable">

            <tr>
                <td height="23" style="width: 85px"  >

                    <div align="left" style="width: 62px">药品：</div>
                </td>
                <td height="23">

                    <input type="text" id="T_drugName" name="T_drugName" />

                </td>
                <td height="23" style="width: 85px"  >

                    <div align="left" style="width: 62px"> 单位：</div>
                </td>
                <td height="23">

                    <input type="text" id="T_unit" name="T_unit" ltype="float" ligerui="{width:180}" validate="{required:true}" />

                </td>
            </tr>
            <tr>
                <td height="23"  >

                    <div align="left" style="width: 62px"> 剂量：</div>
                </td>
                <td height="23">
                    <input type="text" id="T_Dosage" name="T_Dosage" ltype="float" ligerui="{width:180}" validate="{required:true}" />
                </td>
                <td height="23"  >

                    <div align="left" style="width: 62px"> 备注：</div>
                </td>
                <td height="23">
                    <input type="text" id="T_remark" name="T_remark" ltype="int" ligerui="{width:180}" validate="{required:true}" />
                </td>
            </tr>
           
            
        </table>--%>

           <div id="maingrid4" style=""></div>
    </form>
</body>
</html>
