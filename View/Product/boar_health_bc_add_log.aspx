<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" />
    <link href="../lib/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" />
    <link href="../CSS/input.css" rel="stylesheet" type="text/css" />

    <script src="../lib/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/ligerui.min.js" type="text/javascript"></script>
    <script src="../lib/json2.js" type="text/javascript"></script>
    <script src="../JS/XHD.js" type="text/javascript"></script>
    <script src="../JS/JsBarcode.all.min.js"></script>
    <script type="text/javascript">
        var baritems = []; //创建一个空数组
        $(function () {
            $("#maingrid4").ligerGrid({
                columns: [ 
                    { display: '温度℃', name: 'temperature', width: 80, align: 'left' },
                    { display: '湿度', name: 'humidity', width:80, align: 'left' },
                    { display: '监控人员', name: 'monitorPerson', width: 150, align: 'left' },
                    {
                        display: '时间', name: 'createTime', width: 190, render: function (item) {
                            return formatTimebytype(item.createTime, 'yyyy-MM-dd');
                        }
                    }
                ],
                dataAction: 'local',
                pageSize: 30,
                pageSizeOptions: [20, 30, 50, 100],
                url: "Basic_Corral.gridLog.xhd?cid=" + getparastr("cid")+ "&bid=" + getparastr("bid"),
                width: '100%',
                height: '100%', 
                heightDiff: -10,
                onRClickToSelect: true,
              
                onContextmenu: function (parm, e) {
                    actionCustomerID = parm.data.id;
                    menu.show({ top: e.pageY, left: e.pageX });
                    return false;
                } 

            });
            initLayout();
            $(window).resize(function () {
                initLayout();
            });
           
        });
     
        function f_reload() {
            var manager = $("#maingrid4").ligerGetGridManager();
            manager.loadData(true);
        };
    </script>
    <style type="text/css">
        .l-leaving { background: #eee; color: #999; }
    </style>

</head>
<body>

    <form id="form1" onsubmit="return false">
        <div style="padding:10px;">
            <div id="toolbar"></div>
            
            <div id="maingrid4" style="margin: -1px;"></div>
            <img id="bar" />
        </div>
    </form>


</body>
</html>
