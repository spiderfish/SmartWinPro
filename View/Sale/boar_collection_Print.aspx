﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><span id="T_id" name="T_id"></title>
     <meta charset="utf-8" />
    
    <style type="text/css">
        .auto-style1 {
            width: 80mm;
            height:120mm
        }
        .auto-style7 {
            font-family: 微软雅黑;
            font-size: 20px;
            text-align: left;
        }
        .auto-style8 {
            width: 117px;
            text-align: right;
            font-family: 微软雅黑;
            font-size: x-small;
            height: 17px;
        }
        .auto-style9 {
            text-align: left;
            font-family: 微软雅黑;
            font-size: x-small;
            height: 17px;
        }
        .auto-style10 {
            text-align: left;
            font-size: x-small;
            height: 8px;
        }
        .auto-style11 {
            font-size: x-small;
            height: 8px;
            font-weight: bold;
            text-align: right;
        }
        .auto-style13 {
            text-align: left;
            height: 2px;
			
        }
        .auto-style14 {
            text-align: left;
            font-size: x-small;
            height: 2px;
        }
        .auto-style17 {
            height: 2px;
            font-size: x-small;
            font-weight: bold;
            text-align: right;
        }
        .auto-style23 {
            text-align: center;
            height: 25px;
            font-size: small;
            font-family: 微软雅黑;
        }
        .auto-style25 {
            text-align: left;
            font-size: x-small;
            height: 18px;
            font-family: 微软雅黑;
        }
        .auto-style26 {
            text-align: right;
            height: 18px;
            font-size: x-small;
            font-weight: bold;
            width: 117px;
            font-family: 微软雅黑;
        }
        .auto-style27 {
            text-align: left;
            height: 20px;
        }
        .auto-style29 {
            text-align: center;
            height: 3px;
            width: 117px;
            font-family: 微软雅黑;
        }
        .auto-style30 {
            text-align: center;
            font-size: 28px;
            height: 40px;
            width: 117px;
        }
        .auto-style31 {
            font-size: x-small;
            height: 8px;
            font-weight: bold;
            text-align: right;
            width: 117px;
        }
        .auto-style32 {
            height: 2px;
            font-size: x-small;
            font-weight: bold;
            text-align: right;
            width: 117px;
        }
		.auto-style322 {
            height: 2px;
            font-size: 20px;
            font-weight: bold;
            text-align: center;
            width: 117px;
        }
        .auto-style33 {
            text-align: left;
            width: 117px;
            height: 17px;
            font-family: 微软雅黑;
        }
        .auto-style34 {
            text-align: right;
            height: 17px;
            font-size: x-small;
            font-family: 微软雅黑;
        }
        .auto-style35 {
            height: 3px;
        }
        .auto-style36 {
            font-family: 微软雅黑;
        }
        .auto-style37 {
            text-align: right;
        }
        .auto-style38 {
            text-align: right;
            font-size: x-small;
            height: 18px;
            font-family: 微软雅黑;
        }
        .auto-style39 {
            text-align: left;
            height: 11px;
        }
        .auto-style40 {
            text-align: left;
            font-size: 20px;
        }
        .auto-style41 {
            text-align: left;
            font-size: 20px;
            height: 10px;
        }
    </style>
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" />
    <link href="../lib/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" />

    <script src="../lib/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/ligerui.min.js" type="text/javascript"></script>

    <script src="../lib/jquery-validation/jquery.validate.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/messages_cn.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/common.js" type="text/javascript"></script>
        <script src="../JS/JsBarcode.all.min.js"></script>
    <script src="../JS/printTable/jquery.jqprint-0.3.js"></script> 
    <script src="../lib/jquery.form.js" type="text/javascript"></script>
    <script src="../JS/XHD.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            $.metadata.setType("attr", "validate");
            XHD.validate($(form1));
           
                
           
            loadForm(getparastr("id"));
            var items = [];
            items.push({ type: 'button', text: '打印', icon: '../images/search.gif', disable: true, click: function () { print() } });
            $("#toolbar").ligerToolBar({
                items: items

            });
            getsysinfo();
        });

        function getsysinfo() {
            $.ajax({
                type: "GET",
                url: "Sys_info.grid.xhd", /* 注意后面的名字对应CS的方法名称 */
                data: { rnd: Math.random() }, /* 注意参数的格式和名称 */
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    var obj = eval(result);
                    var rows = obj.Rows;

                    var sysinfo = {};
                    for (var i = 0; i < rows.length; i++) {
                        if (rows[i].sys_value == "null" || rows[i].sys_value == null) {
                            rows[i].sys_value = " ";
                        }
                        sysinfo[rows[i].sys_key] = rows[i].sys_value;
                    }

                    document.title = sysinfo["sys_name"] + "-WINPROCRM";
                    var dz = "../" + sysinfo["sys_logo"];
                    $("#logo").attr("src", dz);
                }
            });
        }
        function loadForm(oaid) {
            $.ajax({
                type: "get",
                url: "boar_collection.form.xhd", /* 注意后面的名字对应CS的方法名称 */
                data: { id: oaid, rnd: Math.random() }, /* 注意参数的格式和名称 */
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    var obj = eval(result);
                    for (var n in obj) {
                        if (obj[n] == "null" || obj[n] == null)
                            obj[n] = "";
                    }
                    var rows = [];

                // alert(JSON.stringify(obj))
                   
                   
                    //$("#form1").ligerAutoForm({
                    //    labelWidth: 80, inputWidth: 180, space: 20,
                    //    fields: [
                    //        {
                    //            display: '订单', type: 'group', icon: '',
                    //            rows:rows
                                
                    //        }
                    //    ]
                    //});
              
                    var day = Date.parse(new Date());;
                    $("#T_compyname").html(obj.companyName);
                    $("#T_id").html(obj.id);
                    $("#T_product").html(obj.product_name); 
                    $("#T_sum").html(obj.quantity); 
                    $("#T_unit").html(obj.unit); 
                    $("#T_gg").html(obj.specifications); 
                    $("#T_weight").html(obj.weights); 
                    $("#hdorderid").val(obj.id);
                    $("#T_orderid").html(obj.cus_name);
                    $("#T_boar").html(obj.BoarName);
                    $("#T_device").html(obj.DeviceName);
                    $("#T_date").html(formatTimebytype(obj.collectDate, "yyyy-MM-dd"));
                    $("#T_emp").html(obj.collector);
                    $("#s_dyrq").html(formatTimebytype(day, "yyyy-MM-dd"));
                    $("#T_collectStation").html(obj.collectStationName); 
                    $("#T_WorkstationNo").html(obj.WorkstationName);  
                    $("#T_ddpc").html(obj.Serialnumber);  
                     $("#T_ddpc").html(obj.Serialnumber);  
                    JsBarcode("#barcode", obj.id, {
                        width: 2,
                        height: 100,
                        displayValue: true
                    });

                }
            });
        }
         
        function print() {
            $.ajax({
                url: "boar_collection.addprinttime.xhd", type: "POST",
                data: { id: $("#hdorderid").val(), rnd: Math.random() },
                dataType: 'json',
                success: function (result) {
                    $.ligerDialog.closeWaitting();

                    var obj = eval(result);

                    if (obj.isSuccess) {

                    }
                    else {
                        $.ligerDialog.error(obj.Message);
                    }

                },
                error: function () {
                    
                }
            });
            $("#pt").jqprint({
                debug: false,
                importCSS: true,
                printContainer: true,
                operaSupport: false
            });
           
        }
      

    </script>

</head>
<body style="overflow: hidden;">
     <div id="toolbar" style="margin-top: 10px;"></div>
     <input id="hdorderid" name="hdorderid" type="hidden" />
    <form id="form1" onsubmit="return false">
      <%--   <table>
             <tr>
                 <td colspan="4">--作业单</td>
             </tr>
             <tr>
                 <td>客户名称：</td>
                   <td>&nbsp;</td>
             </tr>
               <tr>
                    <td>成品名称：</td>
                   <td></td>
                 <td>数量：</td>
                   <td></td>
             </tr>
                <tr>
                    <td>单位：</td>
                   <td></td>
                 <td>规格：</td>
                   <td></td>
             </tr>
             <tr>
                    <td>设备名称：</td>
                   <td><span id="T_device" name="T_device"></span></td>
                 <td>种禽名称：</td>
                   <td></td>
             </tr>
              <tr>
                    <td>采精站点：</td>
                   <td></td>
                 <td>采精工位：</td>
                   <td></td>
             </tr>
                <tr>
                    <td>采精人员：</td>
                   <td></td>
                 <td>采精日期：</td>
                   <td></td>
             </tr>
              <tr>
                    <td>采精重量(g)：</td>
                   <td><span id="T_weight" name="T_weight"></span></td>
             </tr>
               <tr>
                 <td colspan="4">订单批次：</td>
             </tr>
              <tr>
                    <td>采精单号：</td>
                   <td colspan="3"></td>
             </tr>
              <tr>
                    <td>WinPro </td>
                   <td>打印日期：<span id="s_dyrq" name="s_dyrq"></span></td>
             </tr>
         </table>--%>
    </form>
    <div style="padding: 5px 4px 5px 2px;">
 
    </div>
     <table id="pt" class="auto-style1">
        <tr>
            <td width="80" class="auto-style30"><img id="logo"  alt="" style="height: 42px; margin-left: 5px; margin-top: 2px;" /></td>
            <td colspan="3" align="center" valign="middle" class="auto-style40"><strong><span class="auto-style7"><span id="T_compyname" name="T_compyname"></span></span></strong></br>采精作业单</td>
        </tr>
        <tr>
            <td colspan="4"  class="auto-style322"><span   id="T_boar" name="T_boar"></span></td>
        </tr>
        <tr>
            <td class="auto-style32">采精站点：</td>
            <td width="88" class="auto-style14"><span id="T_collectStation" name="T_collectStation"></span></td>
            <td width="91" class="auto-style32">采精工位：</td>
            <td width="87" class="auto-style14"><span id="T_WorkstationNo" name="T_WorkstationNo"></span></td>
        </tr>
        <tr>
            <td class="auto-style32">采精人员：</td>
            <td class="auto-style14"><span id="T_emp" name="T_emp"></span></td>
            <td class="auto-style17">采精日期：</td>
            <td class="auto-style14"><span id="T_date" name="T_date"></span></td>
        </tr>
        <tr>
            <td class="auto-style39" colspan="4"><hr /></td>
        </tr>
        <tr>
          <td colspan="4" align="left" class="auto-style23"><span>客户：</span><span id="T_orderid" name="T_orderid"></span></td>
        </tr>
        <tr>
            <td class="auto-style23" colspan="4">订单批号：<span id="T_ddpc" name="T_ddpc"></span></td>
        </tr>
        <tr>
            <td class="auto-style26">成品名称：</td>
            <td class="auto-style25"><span id="T_product" name="T_product"></span></td>
            <td class="auto-style26">型号规格：</td>
            <td class="auto-style25"><span id="T_gg" name="T_gg"></span></td>
        </tr>
        <tr>
            <td class="auto-style26">订单数量：</td>
            <td class="auto-style25"><span id="T_sum" name="T_sum"></span></td>
            <td class="auto-style26"><span >单位：</span></td>
            <td class="auto-style25"><span id="T_unit" name="T_unit"></span></td>
        </tr>
        <tr>
            <td class="auto-style27" colspan="4"><hr style="height: -12px" /></td>
        </tr>
        <tr>
            <td class="auto-style29"><strong style="text-align: center">采</strong><span style="text-align: center"><br class="auto-style36" />
                </span><strong style="text-align: center">精<span style="text-align: center"><br class="auto-style36" />
                </span>单<span style="text-align: center"><br class="auto-style36" />
                <span class="auto-style36">号</span></span></strong><span style="text-align: center"> </span></td>
           
            <td class="auto-style35" colspan="3"><img width="209" height="63"  id="barcode"/></td>
        </tr>
        <tr>
            <td class="auto-style33">WinPro</td>
            <td class="auto-style34" colspan="3">打印日期：<span id="s_dyrq" name="s_dyrq"></span></td>
        </tr>
</table>
</body>
</html>
