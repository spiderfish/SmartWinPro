<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" />
    <link href="../lib/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" />

    <script src="../lib/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/ligerui.min.js" type="text/javascript"></script>

    <script src="../lib/jquery-validation/jquery.validate.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/messages_cn.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/common.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerTip.js"></script>
    <script src="../lib/jquery.form.js" type="text/javascript"></script>
    <script src="../JS/XHD.js" type="text/javascript"></script>

    <script type="text/javascript">
        var existNumber = 0;
        var lx = "";
        $(function () {
            $.metadata.setType("attr", "validate");
            XHD.validate($(form1));

            loadForm(getparastr("id"));

        });

        function f_save() {

            var m5 = $("#maingrid5").ligerGetGridManager();

            var data = JSON.stringify(m5.getData());
            var fa = '';
            var xqo = eval('(' + data + ')');
            for (var i in xqo) {
                fa = (fa == '' ? xqo[i].id : (fa+','+xqo[i].id));
            }
            if (fa == '') {
                alert('没有已分配种源！');
                return;
            }
            $('#fa').val(fa);


            if ($(form1).valid()) {
                var sendtxt = "&id=" + getparastr("id");  
                sendtxt += "&customer_id=" + getparastr("customer_id");
                return $("form :input").fieldSerialize() + sendtxt;
            }
        }
        function getselect()
        {
            return  Number($("#T_mrp").val()) + ";" +Number($("#T_yscsl").val());
        }

        function loadForm(oaid) {
            $.ajax({
                type: "get",
                url: "boar_collection.form.xhd", /* 注意后面的名字对应CS的方法名称 */
                data: { id: oaid, rnd: Math.random() }, /* 注意参数的格式和名称 */
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    var obj = eval(result);
                    for (var n in obj) {
                        if (obj[n] == "null" || obj[n] == null)
                            obj[n] = "";
                    }
                    
                    var rows = [], rows1 = [];
                // alert(JSON.stringify(obj))
                 
                   rows.push([{ display: "选择订单", name: "T_orderid", validate: "{required:true}", options: "{width:150}" },
                        { display: "订单号", name: "T_ddh", type: "text", options: "{width:150,disabled:true}" },
                        { display: "种源信息", name: "T_cpmc", type: "text",options: "{width:150,disabled:true}" },
                    ]
                       ,
                       [{ display: "种源信息", name: "T_cplx", type: "text",options: "{width:150,disabled:true}" },],

                       [
                      
                        
                              
                        
                                   { display: "订单数量(份）", name: "T_sum", type: "int", options: "{width:150,disabled:true}" },
                               { display: "所需种源(头)", name: "T_mrp", type: "int", options: "{width:150,disabled:true}"  },
                                { display: "已分配(头)", name: "T_yscsl", type: "int", options: "{width:150,disabled:true}" },
                               
                                   //{ display: "单位", name: "T_unit", type: "text", options: "{width:150,disabled:true}"  },

                        ],
                        [    { display: "单头产量(份)", name: "T_dailyoutput", type: "int", options: "{width:150,disabled:true}" },
                                { display: "标准规格(ML)", name: "T_price", type: "int", options: "{width:150,disabled:true}"  },
                                { display: "采集周期(天)", name: "T_pickcycle", type: "int", options: "{width:150,disabled:true}"  }, 
                                  
                                
                               ]
                    )

                    rows1.push(
                            [
                            { display: "采集时间", name: "T_date", type: "date", options: "{width:150}", validate: "{required:true}", initValue: formatTimebytype(obj.collectDate, "yyyy-MM-dd") }
                            //,
                            //    { display: "ID", name: "T_boar", validate: "{required:true}", width: 150 },
                            //    { display: "种源名称", name: "T_boar_name", type: "text",options: "{width:150,disabled:true}" },
                              
                            ],
                            //[
                            //    { display: "站点", name: "T_collectStation", type: "select", options: "{width:180,url:'Sys_Param.combo.xhd?type=work_site',value:'" + obj.collectStation + "'}",  initValue: obj.collectStation  },
                            //    { display: "工位号", name: "T_WorkstationNo", type: "select", options: "{width:180,url:'Sys_Param.combo.xhd?type=work_loction',value:'" + obj.WorkstationNo + "'}", initValue: obj.WorkstationNo },
                            //],
                            [
                              
                        ]
                        //,
                        //    [
                        //        { display: "备注", name: "T_remarks", type: "textarea", cols: 140, rows: 10, width: 860, cssClass: "l-textarea", initValue: obj.remarks }
                        //    ]
                        );
                   
                   
                    $("#form1").ligerAutoForm({
                        labelWidth: 120, inputWidth: 160, space: 20,
                        fields: [
                            {
                                display: '订单信息', type: 'group', icon: '',
                                rows:rows
                                
                            },
                              {
                                display: '采精信息', type: 'group', icon: '',
                                rows:rows1
                                
                            }
                        ]
                    });
                    var orderid = obj.orderid || getparastr("orderid");
                 
                    if (orderid)
                    {
                        $("#T_orderid").attr("disabled", true);
                      
                    } 
                    
                    $("#T_orderid").ligerComboBox({
                        width: 400,
                       
                        onBeforeOpen: f_selectorder
                    })
                    $("#T_boar").ligerComboBox({
                        width: 150,
                        onBeforeOpen: f_selectzq
                    })

                    $("#T_device").ligerComboBox({
                        width: 150,
                        onBeforeOpen: f_selectsb
                    })

                    $("#T_orderid").val(obj.cus_name);
                    $("#hdorderid").val(obj.orderid);  //订单编号 
                    $("#T_orderid_val").val(obj.ordermxid);//订单明细编号（产品编号）
                    $("#T_ddh").val(obj.Serialnumber);
                    $("#T_cpmc").val(obj.product_name);
                    $("#T_cplx").val(obj.product_category);
              
                    $("#T_sum").val(obj.quantity);
                    $("#T_unit").val(obj.unit);
                    $("#T_mrp").val(obj.mrp);
                    $("#T_zhtms").val(obj.zhtms);
                    $("#T_yscsl").val(obj.yscsl);
                    $("#T_dailyoutput").val(obj.dailyoutput);
                    $("#T_pickcycle").val(obj.pickcycle);
                    $("#T_price").val(obj.price);
                    $("#T_capacity").val(obj.capacity);



                    $("#T_device").val(obj.DeviceName);
                    $("#T_device_val").val(obj.deviceid);
                    $("#T_boar").val(obj.barcode);
                    $("#T_boar_val").val(obj.boarid);
                    $("#T_boar_name").val(obj.BoarName);
                       $("#T_boar_short").val(obj.shortcode);
                    //$("#hdorderid").value = obj.orderid

                    $("#T_emp").ligerComboBox({
                        width: 180,
                        onBeforeOpen: f_selectEmp
                    })

                    $("#T_emp").val(obj.collector);
                    //$("#T_emp_val").val(obj.emp_id);
                }
            });
        }
        function f_selectorder() {
            $.ligerDialog.open({
                zindex: 9005, title: '选择订单', width: 1000, height: 500, url: '../Sale/GetOrderDetail.aspx', buttons: [
                     { text: '确定', onclick: f_selectorderOK },
                     { text: '取消', onclick: function (item, dialog) { dialog.close();} }
                ]
            });
            return false;
        }
        //选择种禽
        function f_selectzq() {
            $.ligerDialog.open({
                zindex: 9005, title: '选择种禽', width: 800, height: 400, url: '../Product/GetBasic_boar.aspx', buttons: [
                    { text: '确定', onclick: f_selectzqOK },
                    { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
                ]
            });
            return false;
        }
        //选择设备
        function f_selectsb() {
            $.ligerDialog.open({
                zindex: 9005, title: '选择设备', width: 750, height: 400, url: '../Product/GetBasic_device.aspx', buttons: [
                    { text: '确定', onclick: f_selectsbOK },
                    { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
                ]
            });
            return false;
        }
        function f_selectzqOK(item, dialog) {
            var data = dialog.frame.f_select();
            if (!data) {
                alert('请选择行!');
                return;
            }
           // alert(JSON.stringify(data));
            $("#T_boar").val(data.barcode);
            $("#T_boar_short").val(data.shortcode);
            $("#T_boar_val").val(data.id);
            $("#T_boar_name").val(data.product_name);
            dialog.close();
        }
        function f_selectsbOK(item, dialog) {
            var data = dialog.frame.f_select();
            if (!data) {
                alert('请选择行!');
                return;
            }
            //alert(JSON.stringify(data))
            $("#T_device").val(data.product_name);
            $("#T_device_val").val(data.id);
            dialog.close();
        }
        function f_selectorderOK(item, dialog) {
            var data = dialog.frame.f_select();
            if (!data) {
                alert('请选择行!');
                return;
            }
           existNumber = 0;  
           
           // alert(JSON.stringify(data))
            $("#T_orderid").val(data.cus_name);
            $("#hdorderid").val(data.order_id);  //订单编号
            $("#T_orderid_val").val(data.product_id);//订单明细编号（产品编号）
            $("#T_ddh").val(data.Serialnumber);
            $("#T_cpmc").val(data.product_name);
            $("#T_cplx").val(data.product_category);
            $("#T_sum").val(data.quantity);
            $("#T_unit").val(data.unit);
             $("#T_zhtms").val(data.zhtms);
            $("#T_mrp").val(data.mrp);
            $("#T_yscsl").val(data.yscsl);
            $("#T_dailyoutput").val(data.dailyoutput);
            $("#T_pickcycle").val(data.pickcycle);
            $("#T_price").val(data.price);
            $("#T_capacity").val(data.capacity);
            lx = data.Pid;
            dialog.close();
            
            $("#maingrid4").ligerGrid({
                columns: [
                    {
                        display: 'ID', name: 'barcode', width: 150 ,render: function (item) {
                            var html = "<div class='abc'>";
                   
                                  html += item.barcode;
                                 
                                html += " </div>";
                                return html;
                            }
                    },
                 {
                    display: '短号', name: 'shortcode', width: 80
                    
                },
                {
                        display: '上次采集', name: 'LastCollecTime', width: 100, render: function (item) {
                            return formatTimebytype(item.LastCollecTime, 'yyyy-MM-dd');
                        }
                    },
                {
                    display: '年龄', name: 'age', width: 80
                    
                },
                    { display: '种猪名称', name: 'product_name', width: 120 }

                ],
                dataAction: 'server',//类型（），产品名称
                url: "boar_collection.boars.xhd?IsUse=Y&cplx="+data.Pid+"&rnd=" + Math.random(),
                pageSize: 1000,
                pageSizeOptions: [1000],
                width: '500',
                height: '100%',
                onAfterShowData: function (currentData) {
                   
                    $(".abc").hover(function (e) {
                       //alert( $(this).text());
                        loadfjf($(this).text());
                     $(this).ligerTip({ content: $("#tipfjje").html(), width: 300, distanceX: event.clientX - $(this).offset().left - $(this).width() + 15,distanceY: -150  });
                        //$(this).ligerTip({ content: $(this).text(), width: 300, distanceX: event.clientX - $(this).offset().left - $(this).width() + 15 });
                    }, function (e) {
                           $(this).ligerHideTip();
                        //$(this).ligerHideTip(e);
                    });
                    //$(".l-grid-row").hover(function () {
                    //    $(this).find("span").show();
                    //}, function () {
                    //    $(this).find("span").hide();
                    //});
                },
                heightDiff: -8
            });

            $("#maingrid5").ligerGrid({
                columns: [
                    {
                        display: 'ID', name: 'barcode', width: 150 ,render: function (item) {
                            var html = "<div class='abc'>";
                   
                                  html += item.barcode;
                                 
                                html += " </div>";
                                return html;
                            }
                    },
                 {
                        display: '短号', name: 'shortcode', width: 80
                        
                    },
                {
                        display: '上次采集', name: 'LastCollecTime', width: 100, render: function (item) {
                            return formatTimebytype(item.LastCollecTime, 'yyyy-MM-dd');
                        }
                    },
                {
                        display: '年龄', name: 'age', width: 80
                        
                    },
                    { display: '种猪名称', name: 'product_name', width: 120 }
                ],
                //dataAction: 'server',
                data: {},
                //url: "boar_collection.boars.xhd?IsUse=Yrnd=" + Math.random(),
                pageSize: 1000,
                pageSizeOptions: [1000],
                //where : f_getWhere(),
                //data: $.extend(true,{},CustomersData), 
                width: '500',
                height: '100%',
                heightDiff: -8
            });
        }

        function loadfjf(oaid) {
              $.ajax({
                type: "get",
                url: "boar_collection.GetBarList.xhd", /* 注意后面的名字对应CS的方法名称 */
                data: { barcode: oaid, rnd: Math.random() }, /* 注意参数的格式和名称 */
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    var obj = eval(result.Rows);
                  //  alert(JSON.stringify(obj));
            //$.ajax({
            //    type: "get",
            //    url: "boar_collection.GetBarList.xhd", /* 注意后面的名字对应CS的方法名称 */
            //    data: { barcode: oaid,  rnd: Math.random() }, /* 注意参数的格式和名称 */
            //    contentType: "application/json; charset=utf-8",
            //    dataType: "json",
            //    success: function (result) {
            //        var obj = result;
                    var item;
                    $('.abcde').empty();
                var a= 
                        '<tr>' +
                        ' <th width="30%" align="center">采集日期</th>' +
                        '  <th width="20%" align="center">指数</th>' +
                        ' <th width="20%" align="center">前次</th>' +
                     ' <th width="20%" align="center">差异</th>' +
                        ' <th width="20%" align="center"></th>' +
                       
                        ' </tr>';
                     $('.abcde').append(a);
                    $.each(obj, function (i, data) {

                        item = "<tr><td>" + formatTimebytype(data['CreateTime'], 'yyyy-MM-dd') + "</td> "
                            + " <td align='right'>" + data['usemml'] + "</td> <td align='right'>" + data['t_usemml'] + "</td>"
                        + " <td align='right'>" + data['cy_usemml'] + "</td> <td align='center'><img src='../Images/Icon/"+data['ico_usemml']+"' /></td>"
                            + " </tr>";
                        $('.abcde').append(item);

                    });

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus);
                }
            })
        }
        function left_right() {
            
            if (Number($("#T_mrp").val()) <= existNumber + Number($("#T_yscsl").val()))
            {
                alert("所需数量已经小于分配数量，不能再添加了！"); return;
             }
            var m4 = $("#maingrid4").ligerGetGridManager();
            //var m5 = $("#maingrid5").ligerGetGridManager();

            var row = m4.getSelectedRow();
            if (!row) { alert('请选择左侧行'); return; }

            var d = row.barcode;
            existNumber++;
            m4.remove(row);
            commol_left(d);
            commol_right(d);
            
        }

        function right_left() {
            existNumber--;
            var m5 = $("#maingrid5").ligerGetGridManager();
            //var m4 = $("#maingrid4").ligerGetGridManager();
            var row = m5.getSelectedRow();
            if (!row) { alert('请选择右侧行'); return; }
            var d = row.barcode;

            m5.remove(row);
            commol_right('');
            commol_left('');
        }

        function query_left() {
            commol_left('');
        }
        function query_right() {

        }

        function commol_left(sel) {
            var m5 = $("#maingrid5").ligerGetGridManager();

            var data = JSON.stringify(m5.getData());
            var fa = '';
            var xqo = eval('(' + data + ')');
            for (var i in xqo) {
                fa = (fa == '' ? xqo[i].barcode : (fa+','+xqo[i].barcode));
            }
            if (sel != "") {
                fa = (fa == '' ? sel : (sel+','+fa));
            }

            $("#maingrid4").ligerGrid({
                columns: [
                   {
                        display: 'ID', name: 'barcode', width: 150 ,render: function (item) {
                            var html = "<div class='abc'>";
                   
                                  html += item.barcode;
                                 
                                html += " </div>";
                                return html;
                            }
                    },
                 {
                    display: '短号', name: 'shortcode', width: 80
                },
                {
                        display: '上次采集', name: 'LastCollecTime', width: 100, render: function (item) {
                            return formatTimebytype(item.LastCollecTime, 'yyyy-MM-dd');
                        }
                    },
                {
                    display: '年龄', name: 'age', width: 80
                },
                    { display: '种猪名称', name: 'product_name', width: 120 }

                ],
                dataAction: 'server',
                url: "boar_collection.boars.xhd?IsUse=Y&cplx="+lx+"&barcode=" + $('#boarnumber_left').val() + "&fa=" + fa + "&rnd=" + Math.random(),
                pageSize: 1000,
                pageSizeOptions: [1000],
                width: '500',
                height: '100%',
                heightDiff: -8
            });
        }

        function commol_right(sel) {
            var m5 = $("#maingrid5").ligerGetGridManager();

            var data = JSON.stringify(m5.getData());
            var fa = '';
            var xqo = eval('(' + data + ')');
            for (var i in xqo) {
                fa = (fa == '' ? xqo[i].barcode : (fa+','+xqo[i].barcode));
            }
            if (sel != "") {
                fa = (fa == '' ? sel : (sel+','+fa));
            }
            if (fa == '') {
                fa = '0';
            }

            $("#maingrid5").ligerGrid({
                columns: [
                   {
                        display: 'ID', name: 'barcode', width: 150 ,render: function (item) {
                            var html = "<div class='abc'>";
                   
                                  html += item.barcode;
                                 
                                html += " </div>";
                                return html;
                            }
                    },
                  {
                    display: '短号', name: 'shortcode', width: 80
                },
                {
                        display: '上次采集', name: 'LastCollecTime', width: 100, render: function (item) {
                            return formatTimebytype(item.LastCollecTime, 'yyyy-MM-dd');
                        }
                    },
                {
                    display: '年龄', name: 'age', width: 80
                },
                    { display: '种猪名称', name: 'product_name', width: 120 }

                ],
                dataAction: 'server',
                url: "boar_collection.boars.xhd?IsUse=Y&barcode=&in=" + fa + "&rnd=" + Math.random(),
                pageSize: 1000,
                pageSizeOptions: [1000],
                width: '500',
                height: '100%',
                heightDiff: -8
            });

        }

        function f_selectEmp()
        {
            $.ligerDialog.open({
                zindex: 9005, title: '选择员工', width: 650, height: 300, url: '../hr/getemp_auth.aspx?auth=3', buttons: [
                     { text: '确定', onclick: f_selectEmpOK },
                     { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
                ]
            });
            return false;
        }

        function f_selectEmpOK(item, dialog) {
            var data = dialog.frame.f_select();
            if (!data) {
                alert('请选择行!');
                return;
            }

            $("#T_emp").val(data.name);
            $("#T_emp_val").val(data.id);
            dialog.close();
        }

       
   
      

    </script>

</head>
<body style="overflow-y: hidden;">
    <style type="text/css">
table.imagetable {
	font-family: verdana,arial,sans-serif;
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #999999;
	border-collapse: collapse;
}
table.imagetable th {
  
	background:#b5cfd2 url('cell-blue.jpg');
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #999999;
}
table.imagetable td {
	background:#dcddc0 url('cell-grey.jpg');
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #999999;
}
</style>

<%--     <input type="hidden" id="hdorderid" value="" />--%>
    <form id="form1" onsubmit="return false">
          <input id="hdorderid" name="hdorderid" type="hidden" />
        <input id="fa" name="fa" type="hidden" />
    </form>
    <div style="padding: 5px 4px 5px 2px;">
 
    </div>
    <table>
        <tr>
            <td width="80" align="left" style="padding-left:20px" >
                ID:  
            </td>

            <td width="160" height="30" align="left" >
                <input type="text" value="" id="boarnumber_left" name="boarnumber_left" class="liger-textbox" width="150px" /> 
            </td>
            <td align="left">
                <button id="left_query" name="left_query" onclick="query_left()">查询</button>
            </td>
            <td>

            </td>
            <td colspan="3" align="center">
                <h3>已分配种源</h3>
            </td>
            


        </tr>
        <tr>
            <td colspan="3" style="height:200px;width:500px;">
                    <div id="maingrid4" style="margin: -1px;height:100%;width:500px;"></div>

               <%-- overflow-y:scroll;overflow-x:scroll--%>
            </td>
            <td style="width:50px;text-align:center">
 <%--               <input name="" type="button" style=" width:10px; height:10px; border:0;   background:url('') no-repeat left top"  onclick="left_right()" /> --%>
                <button style="height:55px;width:55px;margin-bottom:0px;background:url(../Images/icons/function_icon_set/add_48.png)" onclick="left_right()"></button>
              </br>
                </br>
                </br>
                </br>

                 <button style="height:55px;width:55px;margin-bottom:0px;background:url(../Images/icons/function_icon_set/cancel_48.png)" onclick="right_left()"></button>
                   </br>
                </br>
                </br>
                </br>   </br>
                </br>
                </br>
                </br>   </br>
                </br>
                </br>
                </br>   </br>
                </br>
                </br>
                </br>
            </td>
            <td colspan="3" style="height:200px;width:500px;">
                    <div id="maingrid5" style="margin: -1px;height:100%;width:500px;"></div>
            </td>
        </tr>
    </table>
    <div id="tipfjje" class="abcd" style="width:-30px;height:15px">
                             <table border="1" class="abcde imagetable" >
                             
                              <tr>
                               
                                       <th width="30%" align="center">采集日期</th>
                                        <th width="20%" align="center">指数</th>
                                        <th width="20%" align="center">前次</th>
                                  <th width="20%" align="center">差异</th>
                                  <th width="20%" align="center">上次</th>
                         
                                   </tr>

                             </table>
                         </div>
    
</body>
</html>
