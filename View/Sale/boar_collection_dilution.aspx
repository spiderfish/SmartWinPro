<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <meta http-equiv="X-UA-Compatible" content="ie=edge chrome=1" />
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" />
    <link href="../lib/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" />
    <link href="../CSS/input.css" rel="stylesheet" />

    <script src="../lib/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/ligerui.min.js" type="text/javascript"></script>
    <script src="../lib/jquery.form.js" type="text/javascript"></script>
    <script src="../JS/XHD.js" type="text/javascript"></script>
    <script type="text/javascript">
        var type = "";
        $(function () {
            type = getparastr("type");
            initLayout();
            $(window).resize(function () {
                initLayout();
               
            });
            $("#maingrid4").ligerGrid({
                columns: [
                    //{ display: '序号', width: 50, render: function (item, i) { return item.n; } },
                {
                        display: '作业单号', name: 'id', width: 180, render: function (item) {
                            var html = "<a href='javascript:void(0)' onclick=view('collection','" + item.id + "')>" + item.id + "</a>";
                            return html;
                        }
                    },
                    {
                        display: '订单编号', name: 'Serialnumber', width: 180, render: function (item) {
                            var html = "<a href='javascript:void(0)' onclick=view('order','" + item.orderid + "')>" + item.Serialnumber + "</a>";
                            return html;
                        }
                    },
                    {
                        display: '客户', name: 'Customer_id', width: 200,  render: function (item) {
                            var html = "<a href='javascript:void(0)' onclick=view('customer','" + item.Customer_id + "')>";
                            if (item.cus_name)
                                html += item.cus_name;
                            html += "</a>";
                            return html;
                        }
                    },
                      { display: 'ID', name: 'barcode', width: 120 },
                    //{ display: '设备', name: 'DeviceName', width: 80 },
                    { display: '种源', name: 'BoarName', width: 80 },
                    //{ display: '工位号', name: 'WorkstationName', width: 80 },
                    //{ display: '站点', name: 'collectStationName', width: 80 },
                    { display: '种源类型', name: 'product_name', width: 120 },
                    { display: '原精重量(g)', name: 'ejaculatedvolume', width: 80, type: 'int' },
                      
                    { display: '生产份数', name: 'produceddosis', width: 150, type: 'int' },
                    { display: '稀释剂', name: 'diluentvolume', width: 150, type: 'int' },
                    { display: '稀释比率', name: 'totalfinalvolume', width: 150, type: 'int' },
                    { display: '每份总精', name: 'dilutionratio', width: 150, type: 'int' },
                    { display: '总量', name: 'totalspermsperdosis', width: 150, type: 'int' },
                    //{ display: '活力值', name: 'usemml', width: 80, type: 'int' },
                    //{
                    //    display: '单价', name: 'agio', width: 80, type: 'float', align: 'right', render: function (item) {
                    //        return toMoney(item.agio);
                    //    }
                    //},
                    //{ display: '数量(份）', name: 'quantity', width: 60, type: 'int' },

                        { display: '稀释人员', name: 'UserName', width: 80 },
                     {
                        display: '稀释时间', name: 'EventDate', width: 120, render: function (item) {
                            return formatTimebytype(item.EventDate, 'yyyy-MM-dd hh:mm');
                        }
                    },
                  
                   // { display: '单位', name: 'dotime', width: 40 },
                    //{ display: '规格', name: 'specifications', width: 120 },
                    //{
                    //    display: '采集时间', name: 'collectDate', width: 90, render: function (item) {
                    //        return formatTimebytype(item.collectDate, 'yyyy-MM-dd');
                    //    }
                    //},

                    {
                        display: '状态', name: 'StatusName', width: 90, render: function (item) {
                            return item.StatusName ;
                        }
                    }
                    , { display: '备注', name: 'remarks', width: 150 }
                ],
                //groupColumnName: 'Customer_name', groupColumnDisplay: '客户',
                //groupRender: function (column, display, data) {
                //    return display + ":" + column + "  （共" + data.length + "条记录。）";
                //},
                //defaultCloseGroup: true,
                dataAction: 'server', pageSize: 30, pageSizeOptions: [20, 30, 50, 100],
                url: "boar_collection.grid.xhd?type=" + getparastr("type")+"&rnd=" + Math.random(),
                width: '100%', height: '100%',
                heightDiff: -10,

                onRClickToSelect: true,
                onContextmenu: function (parm, e) {
                    actionCustomerID = parm.data.id;
                    menu.show({ top: e.pageY, left: e.pageX });
                    return false;
                }
            });

            $("#grid").height(document.documentElement.clientHeight - $(".toolbar").height());
            $('#serchform').ligerForm();
            toolbar();

          
        });

        function toolbar() {
            $.get("toolbar.GetSys.xhd?mid=boar_collection_dilution&rnd=" + Math.random(), function (data, textStatus) {
                var data = eval('(' + data + ')');
                //alert(data);
                var items = [];
                var arr = data.Items;
                for (var i = 0; i < arr.length; i++) {
                    arr[i].icon = "../" + arr[i].icon;
                    items.push(arr[i]);
                }
                //items.push({ type: 'button', text: '分组展开/关闭', icon: '../images/folder-open.gif', disable: true, click: function () { expand(); } });
                //items.push({
                //    type: 'serchbtn',
                //    text: '高级搜索',
                //    icon: '../images/search.gif',
                //    disable: true,
                //    click: function () {
                //        serchpanel();
                //    }
                //});
                $("#toolbar").ligerToolBar({
                    items: items
                });
                menu = $.ligerMenu({
                    width: 120, items: getMenuItems(data)
                });

                $("#maingrid4").ligerGetGridManager()._onResize();
            });
        }
      
        $(document).keydown(function (e) {
            if (e.keyCode == 13 && e.target.applyligerui) {
                doserch();
            }
        });


        function add() {
            if (type == "packing")
                f_openWindow("sale/boar_collection_dilution_add.aspx?type=" + type, "流程打包", 800, 600, f_save);
            else 
            f_openWindow("sale/boar_collection_dilution_add.aspx?type="+type, "稀释处理", 800, 600, f_save);
        }

        function edit() {
            var manager = $("#maingrid4").ligerGetGridManager();
            var row = manager.getSelectedRow();
            if (row) {
               // if ((row.IsStatus == "20" || row.IsStatus == "10" || row.IsStatus == "30") && type =="weights")
               // {
               //     f_openWindow('sale/boar_collection_cz_add.aspx?type=' + type + '&id=' + row.id, "修改种禽采集流程--称重", 770, 500, "f_save&保存|f_th&退回");
               // }
               // else if ((row.IsStatus == "40" || row.IsStatus == "50") && type =="density")
               //{
               //     f_openWindow('sale/boar_collection_cz_add.aspx?type=' + type + '&id=' + row.id, "修改种禽采集流程--密度分析", 770, 500, "f_save&保存|f_th&退回");

                if (type == "packing")
                    f_openWindow('sale/boar_collection_dilution_add.aspx?type=' + type + '&id=' + row.id, "流程打包", 800, 600, "f_save&保存|f_th&退回");

                else
                    f_openWindow('sale/boar_collection_dilution_add.aspx?type=' + type + '&id=' + row.id, "稀释处理", 800, 600, "f_save&保存|f_th&退回");

                //}else {
                //    $.ligerDialog.warn('单据状态已经改变，无法修改变更！！');
                //    }

            }
            else {
                $.ligerDialog.warn('请选择行！');
            }
        }
        function print() {
            var manager = $("#maingrid4").ligerGetGridManager();
            var row = manager.getSelectedRow();
            if (row) {
               
                f_openWindow('sale/boar_collection_Print.aspx?id=' + row.id, "打印种禽采集标签", 770, 500 );
            }
            else {
                $.ligerDialog.warn('请选择行！');
            }
        } 
        function f_th(item, dialog)
        {
            var manager = $("#maingrid4").ligerGetGridManager();
            var row = manager.getSelectedRow();
            if (row) {
                //if (row.IsStatus != "30") {
                //    top.$.ligerDialog.error('状态已经改变，无法提交！');
                //    return;
                //}
                dialog.close();
                $.ligerDialog.confirm("是否确定退回！？", function (yes) {
                    if (yes) {
                        $.ajax({
                            url: "boar_collection.UpdateBoar_CollectionStatus.xhd", type: "POST",
                            data: { id: row.id, IsStatus:"0", rnd: Math.random() },
                            dataType: 'json',
                            success: function (result) {
                                $.ligerDialog.closeWaitting();

                                var obj = eval(result);

                                if (obj.isSuccess) {
                                    f_reload();
                                }
                                else {
                                    $.ligerDialog.error(obj.Message);
                                }

                            },
                            error: function () {
                                top.$.ligerDialog.error('生效失败！');
                            }
                        });
                    }
                })
            }
            else {
                $.ligerDialog.warn("请选择数据");
            }
        }
        function submit() {
            var manager = $("#maingrid4").ligerGetGridManager();
            var row = manager.getSelectedRow();
            if (row) {
                //if (row.IsStatus != "30")
                //{
                //    top.$.ligerDialog.error('状态已经改变，无法提交！');
                //    return;
                //}
                $.ligerDialog.confirm("是否确定提交！？", function (yes) {
                    if (yes) {
                        $.ajax({
                            url: "boar_collection.UpdateBoar_CollectionStatus.xhd", type: "POST",
                            data: { id: row.id, type: getparastr("type"), rnd: Math.random() },
                            dataType: 'json',
                            success: function (result) {
                                $.ligerDialog.closeWaitting();

                                var obj = eval(result);

                                if (obj.isSuccess) {
                                    f_reload();
                                }
                                else {
                                    $.ligerDialog.error(obj.Message);
                                }

                            },
                            error: function () {
                                top.$.ligerDialog.error('生效失败！');
                            }
                        });
                    }
                })
            }
            else {
                $.ligerDialog.warn("请选择数据");
            }
        } 
        function del() {
            var manager = $("#maingrid4").ligerGetGridManager();
            var row = manager.getSelectedRow();
            if (row) {
                $.ligerDialog.confirm("单据是否报废无法恢复，确定报废？", function (yes) {
                    if (yes) {
                        $.ajax({
                            url: "boar_collection.del.xhd", type: "POST",
                            data: { id: row.id, orderid: row.orderid, ordermxid:row.ordermxid, rnd: Math.random() },
                            dataType: 'json',
                            success: function (result) {
                                $.ligerDialog.closeWaitting();

                                var obj = eval(result);

                                if (obj.isSuccess) {
                                    f_reload();
                                }
                                else {
                                    $.ligerDialog.error(obj.Message);
                                }

                            },
                            error: function () {
                                top.$.ligerDialog.error('删除失败！');
                            }
                        });
                    }
                })
            }
            else {
                $.ligerDialog.warn("请选择数据");
            }
        }

        function f_check(item, dialog) {

            setTimeout(function (item, dialog) { f_save(item, dialog) }, 100);
        }
        function f_save(item, dialog) {
            var weights = dialog.frame.f_weights();
           // alert(JSON.stringify(weights));
   
            if (!weights) {
                return;
            }

            dialog.close();
            $.ligerDialog.waitting('数据保存中,请稍候...');
            $.ajax({
                url: "boar_collection.UpdateParas.xhd", type: "POST",
                data: { type:weights.type,
                    id: weights.id, weights: weights.weights, density: weights.density, VIT: weights.VIT, shape: weights.shape,
                    dilution_quantity: weights.dilution_quantity,
                    dilution_result: weights.dilution_result,
                    dilution_weights: weights.dilution_weights,
                    //dilution_Diff_weights,
                    In_quantity: weights.In_quantity,
                    In_Warehouse: weights.In_Warehouse,
                    In_Warehouse_kw: weights.In_Warehouse_kw,
                    Out_quantity: weights.Out_quantity,
                    Out_Warehouse: weights.Out_Warehouse,
                    Out_Warehouse_kw: weights.Out_Warehouse_kw,
                    Out_logistics: weights.Out_logistics,rnd: Math.random()
                },
                dataType: 'json',
                success: function (result) {
                    $.ligerDialog.closeWaitting();

                    var obj = eval(result);

                    if (obj.isSuccess) {
                        f_reload();
                    }
                    else {
                        $.ligerDialog.error(obj.Message);
                    }

                },
                error: function () {
                    $.ligerDialog.closeWaitting();
                    top.$.ligerDialog.error('更新失败！');
                }
            });
        }
        function f_reload() {
            var manager = $("#maingrid4").ligerGetGridManager();
            manager.loadData(true);
        };

    </script>
</head>
<body>
    <form id="form1" onsubmit="return false">
        <div style="padding: 10px;">
            <div id="toolbar"></div>

            <div id="grid">
                <div id="maingrid4" style="margin: -1px; min-width: 800px;"></div>
            </div>
        </div>

    </form>
    <div class="az">
   </div>
</body>
</html>
