<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" />
    <link href="../lib/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" />

    <script src="../lib/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/ligerui.min.js" type="text/javascript"></script>

    <script src="../lib/jquery-validation/jquery.validate.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/messages_cn.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/common.js" type="text/javascript"></script>

    <script src="../lib/jquery.form.js" type="text/javascript"></script>
    <script src="../JS/XHD.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            $.metadata.setType("attr", "validate");
            XHD.validate($(form1));

            loadForm(getparastr("id"));
            $("#hdid").val(getparastr("id"));
            var items = [];
            items.push({ type: 'button', text: '作业单：'+getparastr("id")+' 点击查询追溯', icon: '../images/search.gif', disable: true, click: function () { trace() } });
            $("#toolbar").ligerToolBar({
                items: items

            });
      
        });

        
        function loadForm(oaid) {
            $.ajax({
                type: "get",
                url: "boar_collection.form.xhd", /* 注意后面的名字对应CS的方法名称 */
                data: { id: oaid, rnd: Math.random() }, /* 注意参数的格式和名称 */
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    var obj = eval(result);
                    for (var n in obj) {
                        if (obj[n] == "null" || obj[n] == null)
                            obj[n] = "";
                    }
                    var rows = [], rows1 = [], rows2 = [], rows3 = [], rows4 = [], rows5 = [], rows6 = [];
                   // alert(JSON.stringify(obj))
                    //[{ display: "订单", name: "T_orderid", type: "text", options: "{width:465,required:true}", onkeypress: "CheckInfo" }]
                    
                    rows.push(

                        [{ display: "客户", name: "T_orderid", type: "text", options: "{width:150,disabled:true}" },
                            { display: "订单号", name: "T_ddh", type: "text", options: "{width:150,disabled:true}" },
                                { display: "下单时间", name: "T_dddate", type: "datetime", options: "{width:150,disabled:true}", initValue: formatTimebytype(obj.create_time, "yyyy-MM-dd hh:mm") },],
                    [
                            { display: "产品名称", name: "T_cpmc", type: "text", options: "{width:150,disabled:true}" },
                                { display: "订单数量", name: "T_sum", type: "text", options: "{width:150,disabled:true}" },
                               // { display: "单位", name: "T_unit", type: "text", options: "{width:150,disabled:true}" },
                                  { display: "每份容量", name: "T_capacity", type: "int", options: "{width:150,disabled:true}", initValue: obj.capacity },
                               
                            ]
                    );
                    rows1.push(

                        [

                            //{ display: "采集时间", name: "T_date", type: "date", options: "{width:150,disabled:true}", initValue: formatTimebytype(obj.collectDate, "yyyy-MM-dd") },
                         { display: "下单时间", name: "T_date", type: "datetime", options: "{width:150,disabled:true}", initValue: formatTimebytype(obj.dotime, "yyyy-MM-dd hh:mm") },

                       
                            { display: "下单人员", name: "T_doperson", type: "text", options: "{width:150,disabled:true}" },
                            { display: "ID", name: "T_boar_barcode", type: "text", options: "{width:150,disabled:true}" },
                            { display: "种源名称", name: "T_boar", type: "text", options: "{width:150,disabled:true}" },
                            
                        ],
                        //    { display: "站点", name: "T_collectStation", type: "text", options: "{width:150,disabled:true}", initValue: obj.collectStationName },                        ],
                        [

                            //{ display: "工位号", name: "T_WorkstationNo", type: "select", type: "text", options: "{width:150,disabled:true}", initValue: obj.WorkstationName },
                       
                            //{ display: "采集人员", name: "T_emp", type: "text", options: "{width:150,disabled:true}" }
                        ]
                        //[
                        //    { display: "备注", name: "T_remarks", type: "textarea", cols: 90, rows: 2, width: 800, cssClass: "l-textarea", initValue: obj.remarks }
                        //]
                    );
                          rows2.push(
                            [
                                //{ display: "首量(g)", name: "T_wights", type: "float",options: "{width:150,disabled:true}" },
       
                              { display: "采集时间", name: "T_cjdate", type: "datetime", options: "{width:150,disabled:true}", initValue: formatTimebytype(obj.cjddate, "yyyy-MM-dd hh:mm") },
                              
         
                              { display: "采集人员", name: "T_cjusername", type: "text", options: "{width:150,disabled:true}", initValue: obj.cjUserName },
                              //{ display: "形态", name: "T_shape", type: "text", options: "{width:150,disabled:true}", initValue: obj.shape },
                          ]
                          );
                         // alert(obj.dilution_Diff_weights)
                     var rows4 = [];
                    rows4.push([
                          
                        { display: "分析时间", name: "T_fxdate", type: "datetime", options: "{width:150,disabled:true}", initValue: formatTimebytype(obj.fxddate, "yyyy-MM-dd hh:mm") },
                    { display: "分析人员", name: "T_fxusername", type: "text", options: "{width:150,disabled:true}", initValue: obj.fxUserName },
                        { display: "源精重(g)", name: "T_ejaculatedvolume", type: "int", options: "{width:150,disabled:true}", initValue: obj.ejaculatedvolume },
                        { display: "报告编号", name: "casereference", type: "text", options: "{width:150,disabled:true}", initValue: obj.casereference },

                              ],
                        [
                            { display: "生产份数", name: "produceddosis", type: "int", options: "{width:150,disabled:true}", initValue: obj.produceddosis },
                            { display: "稀释剂", name: "diluentvolume", type: "int", options: "{width:150,disabled:true}", initValue: obj.diluentvolume },
                           
                            { display: "稀释比率", name: "dilutionratio", type: "int", options: "{width:150,disabled:true}", initValue: obj.dilutionratio },
                            { display: "活力指数", name: "usemml", type: "float", options: "{width:150,disabled:true}", initValue: obj.usemml },
                        ],
                        [
                               { display: "每份总精", name: "totalspermsperdosis", type: "int", options: "{width:150,disabled:true}", initValue: obj.totalspermsperdosis },
                                { display: "总量", name: "totalfinalvolume", type: "int", options: "{width:150,disabled:true}", initValue: obj.totalfinalvolume },
                            { type: "html", width: 70, html: "<div id='btn_alldata'></div>" },
                            { type: "html", width: 70, html: "<div id='btn_allimg'></div>" }
                        ]
                          );
                          var rows5 = [];
                          rows5.push([
                              { display: "稀释时间", name: "xsddate", type: "datetime", options: "{width:150,disabled:true}", initValue: formatTimebytype(obj.xsddate, "yyyy-MM-dd hh:mm") },
                              { display: "稀释人员", name: "xsUserName", type: "text", options: "{width:150,disabled:true}", initValue: obj.xsUserName },
                              { display: "稀释总量", name: "totalfinalvolume", type: "int", options: "{width:150,disabled:true}", initValue: obj.totalfinalvolume },
                              ]
                    );
                     var rows6 = [];
                          rows6.push([
                              { display: "灌装时间", name: "xsddate", type: "datetime", options: "{width:150,disabled:true}", initValue: formatTimebytype(obj.gzddate, "yyyy-MM-dd hh:mm") },
                              { display: "灌装人员", name: "gzUserName", type: "text", options: "{width:150,disabled:true}", initValue: obj.xsUserName },
                              { display: "完工数量", name: "PrintCount", type: "int", options: "{width:150,disabled:true}", initValue: obj.PrintCount },  { type: "html", width: 70, html: "<div id='btn_barcodelist'></div>" }
                              ]
                    );

         //                 var rows6 = [];
         //                 rows6.push([
         //                     { display: "入库仓库", name: "T_In_Warehouse", type: "text", options: "{width:150,disabled:true}", initValue: obj.In_Warehouse},
         //                     { display: "入库库位", name: "T_In_Warehouse_kw", type: "text",options: "{width:150,disabled:true}", initValue: obj.In_Warehouse_kw},
                         

         //                         { display: "入库数量", name: "T_In_quantity", type: "int",  options: "{width:150,disabled:true}", initValue: obj.In_quantity },
         //                     ],
							  //[

         //                     { display: "出库仓库", name: "T_Out_Warehouse", type: "text", options: "{width:150,disabled:true}", initValue: obj.Out_Warehouse },
         //                     { display: "出库库位", name: "T_Out_Warehouse_kw", type: "text",options: "{width:150,disabled:true}", initValue: obj.Out_Warehouse_kw },
                       

         //                         { display: "出库数量", name: "T_Out_quantity", type: "int", options: "{width:150,disabled:true}", initValue: obj.Out_quantity },
								 //  { display: "库存数量", name: "T_KC_quantity", type: "int", options: "{width:150,disabled:true}", initValue: (obj.In_quantity-obj.Out_quantity) },
         //                     ]
         //                     , [


         //                         { display: "物流信息", name: "T_Out_logistics", type: "textarea", cols: 80, rows: 2, width: 600, cssClass: "l-textarea", initValue: obj.Out_logistics }
         //                     ]
         //                 );
                    
                    
                    var IsStatus = obj.IsStatus;
                    var ddxx = '<span style="color: red">(未完成)</span>';//订单信息
                    var cjxx = '<span style="color:red">(未完成)</span>';//采精信息
                    var sccz = '<span style="color:red">(未完成)</span>';//首次称重
                    var mdfx = '<span style="color:red">(未完成)</span>';//密度分析
                    var hlfx = '<span style="color:red">(未完成)</span>';//活力分析
                    var xsfx = '<span style="color:red">(未完成)</span>';//稀释分析
                    var rkxx = '<span style="color:red">(未完成)</span>';//物流信息
                    var wlxx = '<span style="color:red">(未完成)</span>';//物流信息
                    if (IsStatus == "0")//保存
                    { ddxx = '<span style="color: green">(已完成)</span>';
					cjxx = '<span style="color:#996600">(进行中)</span>'
					 $("#zyd").addClass("active1"); 
					 }
                    else if (IsStatus == "10")//作业单提交
                    {
                        ddxx = '<span style="color: green">(已完成)</span>';
						cjxx = '<span style="color:green">(已完成)</span>'
						
                        $("#zyd").addClass("active"); 
					 
                    }
					 else if (IsStatus == "15")//采精中
                    {
                        ddxx = '<span style="color: green">(已完成)</span>';
						cjxx = '<span style="color:green">(已完成)</span>'
						
                        $("#zyd").addClass("active"); 
						$("#caijin").addClass("active1");
                    }
			 else if (IsStatus == "20")//采集完成
                    {
                        ddxx = '<span style="color: green">(已完成)</span>';
						cjxx = '<span style="color:green">(已完成)</span>'
                        sccz = '<span style="color: green">(已完成)</span>';//首次称重

                        $("#zyd").addClass("active"); 
						$("#caijin").addClass("active");
                    }
                    else if (IsStatus == "30" )//称重保存
                    {
                        ddxx = '<span style="color: green">(已完成)</span>';
                        cjxx = '<span style="color: green">(已完成)</span>'
                        hlfx = '<span style="color: #996600">(称重进行中)</span>';//首次称重
                        $("#zyd").addClass("active");
						$("#caijin").addClass("active");
                        $("#cz").addClass("active1"); 
                     
                    }
					 else if (IsStatus == "40" )//称重提交
                    {
                        ddxx = '<span style="color: green">(已完成)</span>';
                        cjxx = '<span style="color: green">(已完成)</span>'
                        hlfx = '<span style="color: #996600">(称重已完成)</span>';//首次称重
                        $("#zyd").addClass("active");
						$("#caijin").addClass("active");
                        $("#cz").addClass("active"); 
                     
                    }
					else if (IsStatus == "50" )//密度保存
                    {
                        ddxx = '<span style="color: green">(已完成)</span>';
                        cjxx = '<span style="color: green">(已完成)</span>'
                        hlfx = '<span style="color: #996600">(密度分析进行中)</span>';//首次称重
                        $("#zyd").addClass("active");
						$("#caijin").addClass("active");
                        $("#cz").addClass("active"); 
						 $("#md").addClass("active1"); 
                     
                    }
                    else if (IsStatus == "60" )//密度提交
                    {
                        ddxx = '<span style="color: green">(已完成)</span>';
                        cjxx = '<span style="color: green">(已完成)</span>';
                       
                        hlfx = '<span style="color: #996600">(密度分已完成)</span>';//首次称重
						// mdfx = '<span style="color: green">(已完成)</span>';
                        $("#zyd").addClass("active");
						$("#caijin").addClass("active");
                        $("#cz").addClass("active"); 
                        $("#md").addClass("active"); 
                    }
					 else if (IsStatus == "70" )//活力保存
                    {
                        ddxx = '<span style="color: green">(已完成)</span>';
                        cjxx = '<span style="color: green">(已完成)</span>';
                        hlfx = '<span style="color: #996600">(活力分析进行中)</span>';//首次称重
                        $("#zyd").addClass("active");
						$("#caijin").addClass("active");
                        $("#cz").addClass("active"); 
                        $("#md").addClass("active"); 
						$("#hl").addClass("active1"); 
                    }
                    else if (IsStatus == "80")//分析完成
                    {
                        ddxx = '<span style="color: green">(已完成)</span>';
                        cjxx = '<span style="color: green">(已完成)</span>';
                        sccz = '<span style="color: green">(已完成)</span>';//首次称重
                      //  mdfx = '<span style="color: green">(已完成)</span>';
                        hlfx = '<span style="color: green">(已完成)</span>';
                        $("#zyd").addClass("active");
						$("#caijin").addClass("active");
                        $("#cz").addClass("active");
                        $("#md").addClass("active"); 
                        $("#hl").addClass("active"); 
                      
  
                    }
                    else if ( IsStatus == "95")//稀释完成
                    {
                        ddxx = '<span style="color: green">(已完成)</span>';
                        cjxx = '<span style="color: green">(已完成)</span>';
                        sccz = '<span style="color: green">(已完成)</span>';//首次称重
                      //  mdfx = '<span style="color: green">(已完成)</span>';
                        hlfx = '<span style="color: green">(已完成)</span>';
						xsfx = '<span style="color: green">(已完成)</span>';
                        $("#zyd").addClass("active");
						$("#caijin").addClass("active");
                        $("#cz").addClass("active");
                        $("#md").addClass("active"); 
                        $("#hl").addClass("active"); 
						$("#xs").addClass("active"); 
                      
  
                    }
                    else if (IsStatus == "A0" )//灌装完成
                    {
                        ddxx = '<span style="color: green">(已完成)</span>';
                        cjxx = '<span style="color: green">(已完成)</span>';
                        sccz = '<span style="color: green">(已完成)</span>';//首次称重
                      //  mdfx = '<span style="color: green">(已完成)</span>';
                        hlfx = '<span style="color: green">(已完成)</span>';
                        xsfx = '<span style="color:  green"">(已完成)</span>';
                          rkxx = '<span style="color: green">(已完成)</span>';
						
                        $("#zyd").addClass("active");
						$("#caijin").addClass("active");
                        $("#cz").addClass("active");
                        $("#md").addClass("active"); 
                        $("#hl").addClass("active"); 
						 $("#xs").addClass("active"); 
                         $("#db").addClass("active"); 
  
                    }
                    
                    else if ( IsStatus == "B0")//入库提交
                    {
                        ddxx = '<span style="color: green">(已完成)</span>';
                        cjxx = '<span style="color: green">(已完成)</span>';
                        sccz = '<span style="color: green">(已完成)</span>';//首次称重
                        mdfx = '<span style="color: green">(已完成)</span>';
                        hlfx = '<span style="color: green">(已完成)</span>';
                        xsfx = '<span style="color: green">(已完成)</span>';
                        rkxx = '<span style="color: #996600">(入库已完成)</span>';
                        $("#zyd").addClass("active");
						$("#caijin").addClass("active");
                        $("#cz").addClass("active");
                        $("#hl").addClass("active");
                        $("#md").addClass("active");
                        $("#xs").addClass("active");
                        $("#rk").addClass("active");
                        $("#db").addClass("active"); 

                    }
                   
                    else if (IsStatus == "NN")//作废
                    {
                          ddxx = '<span style="color: red">(已作废)</span>';//订单信息
                          cjxx = '<span style="color:red">(已作废)</span>';//采精信息
                          sccz = '<span style="color:red">(已作废)</span>';//首次称重
                          mdfx = '<span style="color:red">(已作废)</span>';//密度分析
                          hlfx = '<span style="color:red">(已作废)</span>';//稀释分析
                          xsfx = '<span style="color:red">(已作废)</span>';//稀释分析
                          wlxx = '<span style="color:red">(已作废)</span>';//物流信息
                          rkxx = '<span style="color:red">(已作废)</span>';
                          $("#xd").removeClass("active");
                    }
                    else if (IsStatus == "YY")//生效
                    {
                        ddxx = '<span style="color: green">(已完成)</span>';//订单信息
                        cjxx = '<span style="color: green">(已完成)</span>';//采精信息
                        sccz = '<span style="color: green">(已完成)</span>';//首次称重
                        mdfx = '<span style="color: green">(已完成)</span>';//密度分析
                        hlfx = '<span style="color: green">(已完成)</span>';//活力分析
                        xsfx = '<span style="color: green">(已完成)</span>';//稀释分析
                        wlxx = '<span style="color: green">(已完成)</span>';//物流信息
                        rkxx = '<span style="color: green">(已完成)</span>';
                        $("#zyd").addClass("active");
						$("#caijin").addClass("active");
                        $("#cz").addClass("active");
                        $("#hl").addClass("active");
                        $("#md").addClass("active");
                        $("#xs").addClass("active"); 
                        $("#rk").addClass("active"); 
                        $("#ck").addClass("active"); 
                        $("#db").addClass("active"); 
                    }
                        $("#form1").ligerAutoForm({
                            labelWidth: 65, inputWidth: 155, space: 20, readonly:true,
                            fields: [
                                {
                                    display: '订单信息'+ddxx+'', type: 'group', icon: '',
                                    rows: rows

                                },
                                 {
                                     display: '作业单' + cjxx +'', type: 'group', icon: '',
                                    rows: rows1

                                },
                                 {
                                     display: '源精采集' + sccz +'', type: 'group', icon: '',
                                    rows: rows2

                                },
                                 //{
//                                     display: '密度分析' + mdfx +'', type: 'group', icon: '',
//                                    rows: rows3
//
//                                },
                                 {
                                     display: '活力分析' + hlfx +'', type: 'group', icon: '',
                                    rows: rows4

                                 },
                                 {
                                     display: '稀释' + xsfx + '', type: 'group', icon: '',
                                     rows: rows5

                                 }
                                ,
                                 {
                                     display: '灌装' + rkxx + '', type: 'group', icon: '',
                                     rows: rows6

                                 }
                                //,
//                                 {
//                                     display: '物流信息' + wlxx + '', type: 'group', icon: '',
//                                    rows: rows7
//
//                                }
                            ]
                        });
                  
                    var orderid = obj.orderid || getparastr("orderid");
                 
                    if (orderid)
                    {
                       // $("#T_orderid").attr("disabled", true);
                      
                    } 
                    ////else
                    //$("#T_orderid").ligerComboBox({
                    //    width: 465,
                    //    onBeforeOpen: f_selectorder
                    //})
                    $("#T_boar").ligerComboBox({
                        width: 150,
                        onBeforeOpen: f_selectzq
                    })
                    $("#T_device").ligerComboBox({
                        width: 150,
                        onBeforeOpen: f_selectsb
                    })
                    
                    $("#T_id").val(obj.id);
                    $("#T_orderid").val(obj.cus_name);
                    $("#hdorderid").val(obj.orderid);  //订单编号 
                    $("#T_orderid_val").val(obj.ordermxid);//订单明细编号（产品编号）
                    $("#T_ddh").val(obj.Serialnumber);
                    $("#T_cpmc").val(obj.product_name);
                    $("#T_sum").val(obj.quantity);
                    $("#T_unit").val(obj.unit);



                    $("#T_device").val(obj.DeviceName);
                    $("#T_device_val").val(obj.deviceid);
                    $("#T_boar").val(obj.BoarName);
                    $("#T_boar_val").val(obj.boarid);
                    $("#T_boar_barcode").val(obj.barcode);
                    $("#T_doperson").val(obj.doperson);
                     $("#T_c_operator").val(obj.c_operator);
                    
                    //$("#hdorderid").value = obj.orderid

                    $("#T_emp").ligerComboBox({
                        width: 150,
                        onBeforeOpen: f_selectEmp
                    })

                    $("#T_emp").val(obj.collector);
                    //$("#T_emp_val").val(obj.emp_id);
                    $("#T_wights").val(obj.weights);
                    $("#T_wights").val(obj.weights); 
                    $("#T_dilution_Diff_weights").val(obj.dilution_Diff_weights);
                    $("#T_KC_quantity").val((obj.In_quantity - obj.Out_quantity)); 
                    $("#T_orderid").focus();
                    $("#btn_barcodelist").ligerButton({ text: "生产清单", width: 60, click: barcodelist })
                    $("#btn_alldata").ligerButton({ text: "全部数据", width: 60, click: alldata })
                    $("#btn_allimg").ligerButton({ text: "查看图片", width: 60, click: allimgs })
                }
            });
        }
        function alldata() {
            var url = "boar_collection_mx_view_allData.aspx?id=" + getparastr("id");
            $.ligerDialog.open({
                zindex: 9905, title: '全部数据', width: 1050, height: 600, url: url, buttons: [
                    { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
                ]
            });
            return false;
            //  f_openWindow(url, "aaa", 800, 500, savemap, 9003);
        }
        function allimgs() {
            var url = "../../System/BatchUpload/BatchView.aspx?id=" + getparastr("id");
            $.ligerDialog.open({
                zindex: 9905, title: '查看图片', width: 750, height: 400, url: url, buttons: [
                    { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
                ]
            });
            return false;
            //  f_openWindow(url, "aaa", 800, 500, savemap, 9003);
        }
         function barcodelist() {
             var url = "boar_collection_list.aspx?id=" + getparastr("id");
             $.ligerDialog.open({
                 zindex: 9905, title: '追溯查询', width: 750, height: 400, url: url, buttons: [
                     { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
                 ]
             });
             return false;
          //  f_openWindow(url, "aaa", 800, 500, savemap, 9003);
        }
        function trace()
        {
            $.ligerDialog.open({
                zindex: 9905, title: '追溯查询', width: 750, height: 400, url: 'boar_collection_log.aspx?id=' + getparastr("id"), buttons: [
                    { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
                ]
            });
            return false;
        }

   function map() {
            var url = "crm/customer/map_mark.aspx?type=mark&xy=" + $("#T_id").val();
            f_openWindow(url, "生产明细清单【" + $("#T_id").val() + "】", 800, 500, savemap, 9003);
        }
        function f_selectorder() {
            $.ligerDialog.open({
                zindex: 9005, title: '选择订单', width: 750, height: 400, url: '../Sale/GetOrderDetail.aspx', buttons: [
                     { text: '确定', onclick: f_selectorderOK },
                     { text: '取消', onclick: function (item, dialog) { dialog.close();} }
                ]
            });
            return false;
        }
        //选择种禽
        function f_selectzq() {
            $.ligerDialog.open({
                zindex: 9005, title: '选择种禽', width: 750, height: 400, url: '../Product/GetBasic_boar.aspx', buttons: [
                    { text: '确定', onclick: f_selectzqOK },
                    { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
                ]
            });
            return false;
        }
        //选择设备
        function f_selectsb() {
            $.ligerDialog.open({
                zindex: 9005, title: '选择设备', width: 750, height: 400, url: '../Product/GetBasic_device.aspx', buttons: [
                    { text: '确定', onclick: f_selectsbOK },
                    { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
                ]
            });
            return false;
        }
        function f_selectzqOK(item, dialog) {
            var data = dialog.frame.f_select();
            if (!data) {
                alert('请选择行!');
                return;
            }
           // alert(JSON.stringify(data));
            $("#T_boar").val(data.product_name);
            $("#T_boar_val").val(data.id);
            dialog.close();
        }
        function f_selectsbOK(item, dialog) {
            var data = dialog.frame.f_select();
            if (!data) {
                alert('请选择行!');
                return;
            }
            //alert(JSON.stringify(data))
            $("#T_device").val(data.product_name);
            $("#T_device_val").val(data.id);
            dialog.close();
        }
        function f_selectorderOK(item, dialog) {
            var data = dialog.frame.f_select();
            if (!data) {
                alert('请选择行!');
                return;
            }
            //alert(JSON.stringify(data.order_id))
            $("#T_orderid").val(data.cus_name);
            $("#hdorderid").val(data.order_id);  //订单编号
            $("#T_orderid_val").val(data.product_id);//订单明细编号（产品编号）
            $("#T_ddh").val(data.Serialnumber);
            $("#T_cpmc").val(data.product_name);
            $("#T_sum").val(data.quantity);
            $("#T_unit").val(data.unit);

            dialog.close();
        }

        function f_selectEmp()
        {
            $.ligerDialog.open({
                zindex: 9005, title: '选择员工', width: 650, height: 300, url: '../hr/getemp_auth.aspx?auth=3', buttons: [
                     { text: '确定', onclick: f_selectEmpOK },
                     { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
                ]
            });
            return false;
        }

        function f_selectEmpOK(item, dialog) {
            var data = dialog.frame.f_select();
            if (!data) {
                alert('请选择行!');
                return;
            }

            $("#T_emp").val(data.name);
            $("#T_emp_val").val(data.id);
            dialog.close();
        }

        function f_weights() {

            var wights = $("#T_wights").val();
            if (wights > 0)
                return { "id": $("#hdid").val(), "weights": wights};
            else {
                alert("重量不能为空！");
            }

        }


    </script>
    
<style type="text/css">

*, *:before, *:after {
	margin: 0;
	padding: 0;
	box-sizing: border-box;
}

html, body {
	height: 100%;
}

body {
	font: 12px/1 'Roboto', sans-serif;
	color: #555;
	background-color: #fff;
	-webkit-font-smoothing: antialiased;
	-moz-osx-font-smoothing: grayscale;
}

ul {list-style: none;}

.cf:before, .cf:after {
	content: ' ';
	display: table;
}
.cf:after {
	clear: both;
}

.title {
	padding: 50px 0;
	font: 24px 'Open Sans', sans-serif;
	text-align: center;
}



.inner {
	max-width:950px;
	margin: 0 auto;
}
.breadcrumbs {
	border-top: 1px solid #ddd;
	border-bottom: 1px solid #ddd;
	background-color: #f5f5f5;
}
.breadcrumbs ul {
	border-left: 1px solid #ddd;
	border-right: 1px solid #ddd;
}
.breadcrumbs li {
	float: left;
	width: 10%;
}
.breadcrumbs a {
	position: relative;
	display: block;
	padding: 20px;
	padding-right: 0 !important;
	/* important overrides media queries */
	font-size: 13px;
	font-weight: bold;
	text-align: center;
	color: #aaa;
	cursor: pointer;
}

.breadcrumbs a:hover {
	background: #eee;
}
.breadcrumbs a.active {
	color: #eee;
	background-color: #009900;
}
.breadcrumbs a.active1 {
	color: #eee;
	background-color: #996600;
}
.breadcrumbs a span:first-child {
	display: inline-block;
	width: 22px;
	height: 22px;
	padding: 2px;
	margin-right: 5px;
	border: 2px solid #aaa;
	border-radius: 50%;
	background-color: #fff;
}

.breadcrumbs a.active span:first-child {
	color: #fff;
	border-color: #777;
	background-color: #777;
}
.breadcrumbs a.active1 span:first-child {
	color: #fff;
	border-color: #777;
	background-color: #777;
}

.breadcrumbs a:before,
.breadcrumbs a:after {
	content: '';
	position: absolute;
	top: 0;
	left: 100%;
	z-index: 1;
	display: block;
	width: 0;
	height: 0;
	border-top: 32px solid transparent;
	border-bottom: 32px solid transparent;
	border-left: 16px solid transparent;
}

.breadcrumbs a:before {
	margin-left: 1px;
	border-left-color: #d5d5d5;
}
.breadcrumbs a:after {
	border-left-color: #f5f5f5;
}
.breadcrumbs a:hover:after {
	border-left-color: #eee;
}
.breadcrumbs a.active:after {
	border-left-color: #009900;
}
.breadcrumbs a.active1:after {
	border-left-color: #996600;
}
.breadcrumbs li:last-child a:before,
.breadcrumbs li:last-child a:after {
	display: none;
}

@media (max-width: 720px) {
	.breadcrumbs a {
		padding: 15px;
	}

	.breadcrumbs a:before,
	.breadcrumbs a:after {
		border-top-width: 26px;
		border-bottom-width: 26px;
		border-left-width: 13px;
	}
}
@media (max-width: 620px) {
	.breadcrumbs a {
		padding: 10px;
		font-size: 12px;
	}

	.breadcrumbs a:before,
	.breadcrumbs a:after {
		border-top-width: 22px;
		border-bottom-width: 22px;
		border-left-width: 11px;
	}
}
@media (max-width: 520px) {
	.breadcrumbs a {
		padding: 5px;
	}
	.breadcrumbs a:before,
	.breadcrumbs a:after {
		border-top-width: 16px;
		border-bottom-width: 16px;
		border-left-width: 8px;
	}
	.breadcrumbs li a span:first-child {
		display: block;
		margin: 0 auto;
	}
	.breadcrumbs li a span:last-child {
		display: none;
	}
}
</style>
</head>
<body style="overflow: scroll;">
  
     <input type="hidden" id="hdid" value="" />    <div class="breadcrumbs">
	<div class="inner">
		<ul class="cf">
			<%--<li><a id="xd" class="active"><span>1</span><span>下单</span></a></li>--%>
			<li><a id="zyd"  ><span>1</span><span>作业单</span></a></li>
			<li><a id="caijin"  ><span>2</span><span>采精</span></a></li>
		<%--	<li><a id="cz"   ><span>3</span><span>称重</span></a></li>
			<li><a id="md" ><span>4</span><span>密度</span></a></li>--%>
			<li><a id="hl"  ><span>3</span><span>分析</span></a></li>
            <li><a id="xs" ><span>4</span><span>稀释</span></a></li>
            <li><a id="db" ><span>5</span><span>灌装</span></a></li>
			<%--<li><a id="rk"  ><span>8</span><span>入库</span></a></li>
			<li><a id="ck" ><span>9</span><span>出库</span></a></li>--%>
			
			
		</ul>
	</div>
</div>
   <div id="toolbar" style="margin-top: 10px;"></div>
    
 
    <form id="form1"  onsubmit="return false">
          <input id="hdorderid" name="hdorderid" type="hidden" />
    </form>
       
    <div style="padding: 5px 4px 5px 2px;">
 
    </div>
        
</body>
</html>
