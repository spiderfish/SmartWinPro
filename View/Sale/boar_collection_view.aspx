<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <meta http-equiv="X-UA-Compatible" content="ie=edge chrome=1" />
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" />
    <link href="../lib/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" />
    <link href="../CSS/input.css" rel="stylesheet" />

    <script src="../lib/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/ligerui.min.js" type="text/javascript"></script>
    <script src="../lib/jquery.form.js" type="text/javascript"></script>
    <script src="../JS/XHD.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            initLayout();
            $(window).resize(function () {
                initLayout();
            });
            $("#maingrid4").ligerGrid({
                columns: [
                    //{ display: '序号', width: 50, render: function (item, i) { return item.n; } },
                        {
                        display: '作业单号', name: 'id', width: 130, render: function (item) {
                            var html = "<a href='javascript:void(0)' onclick=view('collection','" + item.id + "')>" + item.id + "</a>";
                            return html;
                        }
                    },
                    {
                        display: '订单编号', name: 'Serialnumber', width: 130, render: function (item) {
                            var html = "<a href='javascript:void(0)' onclick=view('order','" + item.orderid + "')>" + item.Serialnumber + "</a>";
                            return html;
                        }
                    },
                      { display: '种源名称', name: 'BoarName', width: 80 },
                    {
                        display: '客户', name: 'Customer_id', width: 120,  render: function (item) {
                            var html = "<a href='javascript:void(0)' onclick=view('customer','" + item.Customer_id + "')>";
                            if (item.cus_name)
                                html += item.cus_name;
                            html += "</a>";
                            return html;
                        }
                    },
                    //{ display: '设备', name: 'DeviceName', width: 80 },
                  
                    { display: 'ID', name: 'barcode', width: 150 },
                    //{ display: '站点', name: 'collectStationName', width: 80 },
                    { display: '种源类型', name: 'product_name', width: 80 },
                    //{
                    //    display: '单价', name: 'agio', width: 80, type: 'float', align: 'right', render: function (item) {
                    //        return toMoney(item.agio);
                    //    }
                    //},
                    { display: '订单数量', name: 'quantity', width: 40, type: 'int' },
                    { display: '源精重量(g)', name: 'ejaculatedvolume', width: 80 },
                    { display: '有效精子(M/ml)', name: 'usemml', width: 100 },
                    { display: '分析产出(份)', name: 'produceddosis', width: 80 },
                       { display: '实际产出(份)', name: 'PrintCount', width: 80 },
                      
                    { display: '单位', name: 'unit', width: 40 },
                    //{ display: '规格', name: 'specifications', width: 120 },
                    {
                        display: '采集时间', name: 'collectDate', width: 120, render: function (item) {
                            return formatTimebytype(item.collectDate, 'yyyy-MM-dd');
                        }
                    },

                    {
                        display: '状态', name: 'StatusName', width: 90, render: function (item) {
                            return item.StatusName ;
                        }
                    }
                    , { display: '备注', name: 'remarks', width: 80 },
                ],
                //groupColumnName: 'Customer_name', groupColumnDisplay: '客户',
                //groupRender: function (column, display, data) {
                //    return display + ":" + column + "  （共" + data.length + "条记录。）";
                //},
                //defaultCloseGroup: true,
                dataAction: 'server', pageSize: 30, pageSizeOptions: [20, 30, 50, 100],
                url: "boar_collection.grid.xhd?type=view&rnd=" + Math.random(),
                width: '100%', height: '100%',
                heightDiff: -10,

                onRClickToSelect: true,
                onContextmenu: function (parm, e) {
                    actionCustomerID = parm.data.id;
                    menu.show({ top: e.pageY, left: e.pageX });
                    return false;
                }
            });

            $("#grid").height(document.documentElement.clientHeight - $(".toolbar").height());
            $('#serchform').ligerForm();
            toolbar();

          
        });

        function toolbar() {
            $.get("toolbar.GetSys.xhd?mid=boar_collection_view&rnd=" + Math.random(), function (data, textStatus) {
                var data = eval('(' + data + ')');
                //alert(data);
                var items = [];
                var arr = data.Items;
                for (var i = 0; i < arr.length; i++) {
                    arr[i].icon = "../" + arr[i].icon;
                    items.push(arr[i]);
                }
                //items.push({
                //    type: 'textbox',
                //    id: 'keyword1',
                //    name: 'keyword1',
                //    text: ''
                //});
                items.push({ type: 'textbox', id: 'stext', text: '作业/订单：' });
                 items.push({ type: 'textbox', id: 'sboarid', text: '耳标：' });
                items.push({ type: 'textbox', id: 'T_sectype', text: '状态' });
                items.push({ type: 'textbox', id: 'T_kssj', text: '时间' });
                items.push({ type: 'textbox', id: 'T_jssj', text: '~' });
                items.push({ type: 'button', text: '搜索', icon: '../../images/search.gif', disable: true, click: function () { doserch() } });
                items.push({ type: 'button', text: '重置', icon: '../../images/edit.gif', disable: true, click: function () { doclear() } });
 
                $("#toolbar").ligerToolBar({
                    items: items
                });
                $('#T_sectype').ligerComboBox({ width: 96, emptyText: '（空）', url: "boar_collection.combo.xhd?type=status&rnd=" + Math.random() });
                //boar_collection grid 要放开 【新加的状态】后面注释
                menu = $.ligerMenu({
                    width: 120, items: getMenuItems(data)
                });

                $("#maingrid4").ligerGetGridManager()._onResize();
                $("#stext").ligerTextBox({ width: 150, nullText: "" });
                $("#sboarid").ligerTextBox({ width: 140, nullText: "" });
                var day2 = new Date();
                 day2.setTime(day2.getTime());
                 var s2 = day2.getFullYear() + "-" + (day2.getMonth() + 1) + "-" + day2.getDate();

                // $('#T_kssj').ligerDateEditor({ width: 120,   initValue: s2 });
                //$('#T_jssj').ligerDateEditor({ width: 120, initValue: s2 });

                   $('#T_kssj').ligerDateEditor({ width: 120 });
                 $('#T_jssj').ligerDateEditor({ width: 120 });
            });
        }
      
        $(document).keydown(function (e) {
            if (e.keyCode == 13 && e.target.applyligerui) {
                doserch();
            }
        });

        //查询
        function doserch() {
            var sendtxt = "&rnd=" + Math.random();
            var serchtxt = $("#form1 :input").fieldSerialize() + sendtxt;
            //alert(serchtxt);           
            var manager = $("#maingrid4").ligerGetGridManager();

            manager._setUrl("boar_collection.grid.xhd?type=view&" + serchtxt);
        }

        //重置
        function doclear() {
            //var serchtxt = $("#serchform :input").reset();
            $("#form1").each(function () {
                this.reset();
            });
        }
        function query() {
            var manager = $("#maingrid4").ligerGetGridManager();
            var row = manager.getSelectedRow();
            if (row) {
                f_openWindow('sale/boar_collection_mx_view.aspx?id=' + row.id, "查询详情", 1010, 700);
            }
            else {
                $.ligerDialog.warn('请选择行！');
            }
        }
        function print() {
            var manager = $("#maingrid4").ligerGetGridManager();
            var row = manager.getSelectedRow();
            if (row) {
               
                f_openWindow('sale/boar_collection_Print.aspx?id=' + row.id, "打印作业单标签", 770, 500 );
            }
            else {
                $.ligerDialog.warn('请选择行！');
            }
        } 
        function submit() {
            var manager = $("#maingrid4").ligerGetGridManager();
            var row = manager.getSelectedRow();
            if (row) {
                $.ligerDialog.confirm("是否确定生效！？", function (yes) {
                    if (yes) {
                        $.ajax({
                            url: "boar_collection.UpdateBoar_CollectionStatus.xhd", type: "POST",
                            data: { id: row.id,   type:"weights", rnd: Math.random() },
                            dataType: 'json',
                            success: function (result) {
                                $.ligerDialog.closeWaitting();

                                var obj = eval(result);

                                if (obj.isSuccess) {
                                    f_reload();
                                }
                                else {
                                    $.ligerDialog.error(obj.Message);
                                }

                            },
                            error: function () {
                                top.$.ligerDialog.error('生效失败！');
                            }
                        });
                    }
                })
            }
            else {
                $.ligerDialog.warn("请选择数据");
            }
        } 
        function del() {
            var manager = $("#maingrid4").ligerGetGridManager();
            var row = manager.getSelectedRow();
            if (row) {
                $.ligerDialog.confirm("单据是否报废无法恢复，确定报废？", function (yes) {
                    if (yes) {
                        $.ajax({
                            url: "boar_collection.del.xhd", type: "POST",
                            data: { id: row.id, orderid: row.orderid, ordermxid:row.ordermxid, rnd: Math.random() },
                            dataType: 'json',
                            success: function (result) {
                                $.ligerDialog.closeWaitting();

                                var obj = eval(result);

                                if (obj.isSuccess) {
                                    f_reload();
                                }
                                else {
                                    $.ligerDialog.error(obj.Message);
                                }

                            },
                            error: function () {
                                top.$.ligerDialog.error('删除失败！');
                            }
                        });
                    }
                })
            }
            else {
                $.ligerDialog.warn("请选择数据");
            }
        }

        function f_check(item, dialog) {

            setTimeout(function (item, dialog) { f_save(item, dialog) }, 100);
        }
        function f_save(item, dialog) {
            var weights = dialog.frame.f_weights();
           // alert(JSON.stringify(weights));
   
            if (!weights) {
                return;
            }

            dialog.close();
            $.ligerDialog.waitting('数据保存中,请稍候...');
            $.ajax({
                url: "boar_collection.UpdateWeights.xhd", type: "POST",
                data: { id: weights.id, weights: weights.weights, rnd: Math.random() },
                dataType: 'json',
                success: function (result) {
                    $.ligerDialog.closeWaitting();

                    var obj = eval(result);

                    if (obj.isSuccess) {
                        f_reload();
                    }
                    else {
                        $.ligerDialog.error(obj.Message);
                    }

                },
                error: function () {
                    top.$.ligerDialog.error('重量更新失败！');
                }
            });
        }
        function f_reload() {
            var manager = $("#maingrid4").ligerGetGridManager();
            manager.loadData(true);
        };

    </script>
    
</head>
<body>

    <form id="form1" onsubmit="return false">
        <div style="padding: 10px;">
            <div id="toolbar"></div>

            <div id="grid">
                <div id="maingrid4" style="margin: -1px; min-width: 800px;"></div>
            </div>
        </div>

    </form>
    <div class="az">
   </div>
</body>
</html>
