<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" />
    <link href="../lib/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" />

    <script src="../lib/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/ligerui.min.js" type="text/javascript"></script>

    <script src="../lib/jquery-validation/jquery.validate.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/messages_cn.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/common.js" type="text/javascript"></script>

    <script src="../lib/jquery.form.js" type="text/javascript"></script>
    <script src="../JS/XHD.js" type="text/javascript"></script>

    <script type="text/javascript">
        var type = "";
        $(function () {
            type = getparastr("type");
            $.metadata.setType("attr", "validate");
            XHD.validate($(form1));

            loadForm(getparastr("id"),false);
            $("#hdid").val(getparastr("id"));
         
        });

        //function f_save() {
    
        //    if ($(form1).valid()) {
        //        var sendtxt = "&id=" + getparastr("id");  
        //        sendtxt += "&customer_id=" + getparastr("customer_id");
        //        return $("form :input").fieldSerialize() + sendtxt;
        //    }
        //}
        $(document).keydown(function (e) {
          //&& e.target.applyligerui
            if (e.keyCode == 13) {
                if ($("#T_orderid").val().length < 14)
                {
                  var manager = $.ligerDialog.error('单号不正确!!(3秒自动关闭)');
                        setTimeout(function () {
                            manager.close();
                        }, 3000); 
                }
                $("#hdid").val($("#T_orderid").val());
              loadForm($("#T_orderid").val(),true) ;
            }
        });

        function loadForm(oaid,flag) {
            $.ajax({
                type: "get",
                url: "boar_collection.form.xhd", /* 注意后面的名字对应CS的方法名称 */
                data: { id: oaid, flag: flag, type: type, rnd: Math.random() }, /* 注意参数的格式和名称 */
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    var obj = eval(result);
                    for (var n in obj) {
                        if (obj[n] == "null" || obj[n] == null)
                            obj[n] = "";
                    }
                    var rows = []; var rows1 = []; var rows2 = []; var rowsbarcode = []; var rows3 = []; var rows4 = [];
                    //alert(JSON.stringify(obj))
                    //alert(obj.weights)
                    //[{ display: "订单", name: "T_orderid", type: "text", options: "{width:465,required:true}", onkeypress: "CheckInfo" }]
                    if (!flag) {
                        rowsbarcode.push([{ display: "作业单号", name: "T_orderid", type: "text", validate: "{required:true}", width: 200 },
                            //{ type: "html", width: 200, html: "<span style='color:red'>请扫描容器上的条形码</span>" }
                        ]);
                        rows.push([
                             { display: "客户", name: "T_cus_name", type: "text", options: "{width:180,disabled:true}"  }
                                 ,
                              { display: "订单号", name: "T_ddh", type: "text", options: "{width:180,disabled:true}" }
                         ],
                        [
                                { display: "产品名称", name: "T_cpmc", type: "text", options: "{width:180,disabled:true}" },

                   
                                { display: "订单数量", name: "T_sum", type: "text", options: "{width:180,disabled:true}" },
                                { display: "单位", name: "T_unit", type: "text", options: "{width:180,disabled:true}" },

                            ])
                        rows1.push(
                           
                            [
                                { display: "采精对象", name: "T_boar", type: "text", options: "{width:180,disabled:true}" },
                                { display: "采集时间", name: "T_date", type: "date", options: "{width:180,disabled:true}", initValue: formatTimebytype(obj.collectDate, "yyyy-MM-dd") },
                                { display: "设备", name: "T_device", type: "text", options: "{width:180,disabled:true}" },

                            ],
                            [
                                { display: "站点", name: "T_collectStation", type: "text", options: "{width:180,disabled:true}", initValue: obj.collectStationName },
                                { display: "工位号", name: "T_WorkstationNo", type: "text", type: "text", options: "{width:180,disabled:true}", initValue: obj.WorkstationName },
       
                                { display: "采集人员", name: "T_emp", type: "text", options: "{width:180,disabled:true}" }
                            ],
                            [
                                { display: "备注", name: "T_remarks", type: "textarea", cols: 73, rows: 4, width: 465, cssClass: "l-textarea", initValue: obj.remarks }
                            ]
                           
                        );

                        var field = [];
                        field.push({
                            display: '<span style="color:red">请扫描容器上的条形码</span>', type: 'group', icon: '',
                            rows: rowsbarcode

                        },
                        {
                            display: '订单信息', type: 'group', icon: '',
                            rows: rows

                        },
                        {
                            display: '采精信息', type: 'group', icon: '',
                            rows: rows1

                        });
               

                            rows2.push([
                                { display: "称重(g)", name: "T_weights", type: "float", validate: "{required:true}", width: 180, initValue: obj.weights },
                                { type: "html", width: 200, html: "<span style='color:red'>请填写重量值，单位为克</span>" },
                            ]
                            );
                            field.push({
                                display: '称重信息', type: 'group', icon: '',
                                rows: rows2

                            })  
                   
                        if (type == "density")//密度
                        {
                            rows3.push([
                                { display: "密度值", name: "T_density", type: "float", validate: "{required:true}", width: 180, initValue: obj.density},
                                { type: "html", width: 200, html: "<span style='color:red'>请填写密度值</span>" },
                            ]
                            );
                            field.push({
                                display: '密度分析', type: 'group', icon: '',
                                rows: rows3

                            }) 
                        }
                        if (type == "VIT")//活力
                        {
                            rows4.push([
                                { display: "活力值", name: "T_VIT", type: "float", validate: "{required:true}", width: 180, initValue: obj.VIT },
                                { display: "形态", name: "T_shape", type: "text", validate: "{required:true}", width: 180, initValue: obj.shape },
                            ]
                            );
                            field.push({
                                display: '活力分析', type: 'group', icon: '',
                                rows: rows4

                            }) 
                        }
                        if (type == "dilution")//稀释
                        {
                            var rows5 = [];
                            rows5.push([
                                { display: "重量", name: "T_dilution_weights", type: "float", validate: "{required:true}", width: 180, initValue: obj.dilution_weights },
                                { display: "差异", name: "T_dilution_Diff_weights", type: "text", options: "{width:180,disabled:true}", width: 180, initValue: obj.dilution_Diff_weights },
                            ],
                            [
                                { display: "结果", name: "T_dilution_result", type: "text", validate: "{required:true}", width: 180, initValue: obj.dilution_result },
                                { display: "成品数量", name: "T_dilution_quantity", type: "int", type: "text", validate: "{required:true}", width: 180, initValue: obj.dilution_quantity },
                                ] 
                            );
                            field.push({
                                display: '稀释分析', type: 'group', icon: '',
                                rows: rows5

                            })
                        }

                        if (type == "In")//入库
                        {
                            var rows6 = [];
                            rows6.push([
                                { display: "仓库", name: "T_In_Warehouse", validate: "{required:true}", width: 180 },
                                { display: "库位", name: "T_In_Warehouse_kw", validate: "{required:true}", width: 180 },
                            ],[
                                
                                    { display: "入库数量", name: "T_In_quantity", type: "int", type: "text", validate: "{required:true}", initValue: obj.In_quantity },
                            ]
                            );
                            field.push({
                                display: '入库', type: 'group', icon: '',
                                rows: rows6

                            })
                        }
                        if (type == "Out")//出库
                        {
                            var rows7 = [];
                            rows7.push([
                                { display: "仓库", name: "T_Out_Warehouse", validate: "{required:true}", width: 180 },
                                { display: "库位", name: "T_Out_Warehouse_kw", validate: "{required:true}", width: 180 },
                            ], [

                                    { display: "入库数量", name: "T_Out_quantity", type: "float", validate: "{required:true}", initValue: obj.Out_quantity },
                                ]
                                , [

                                
                                    { display: "物流信息", name: "T_Out_logistics", type: "textarea", cols: 73, rows: 4, width: 465, cssClass: "l-textarea", initValue: obj.Out_logistics }
                                                              ]
                            );
                            field.push({
                                display: '出库', type: 'group', icon: '',
                                rows: rows7

                            })
                        }
                   
                     
                       
                       
                        $("#form1").ligerAutoForm({
                            labelWidth: 65, inputWidth: 155, space: 20,
                            fields:field
                        });
                    }
                    else {
                        $("#T_date").val(formatTimebytype(obj.collectDate, "yyyy-MM-dd"));
                        $("#T_collectStation").val(obj.collectStationName);
                        $("#T_WorkstationNo").val(obj.WorkstationName);
                    }
                    var orderid = obj.orderid || getparastr("orderid");
                 
                    if (orderid)
                    {
                       // $("#T_orderid").attr("disabled", true);
                      
                    } 

                    if (type == "Out")//入库
                    {
                        $("#T_Out_Warehouse").ligerComboBox({
                            width: 180,
                            onBeforeOpen: f_selectwh
                        })
                        $("#T_Out_Warehouse_kw").ligerComboBox({
                            width: 180,
                            onBeforeOpen: f_selectwh_kw
                        })
                        $("#T_Out_Warehouse").val(obj.Out_Warehouse);
                        $("#T_Out_Warehouse_kw").val(obj.Out_Warehouse_kw);
                    }
                    if (type == "In")//入库
                    {
                        $("#T_In_Warehouse").ligerComboBox({
                            width: 180,
                            onBeforeOpen: f_selectwh
                        })
                        $("#T_In_Warehouse_kw").ligerComboBox({
                            width: 180,
                            onBeforeOpen: f_selectwh_kw
                        })
                        $("#T_In_Warehouse").val(obj.In_Warehouse);
                        $("#T_In_Warehouse_kw").val(obj.In_Warehouse_kw);
                    } 
                    ////else
                    //$("#T_orderid").ligerComboBox({
                    //    width: 465,
                    //    onBeforeOpen: f_selectorder
                    //})
                    $("#T_boar").ligerComboBox({
                        width: 180,
                        onBeforeOpen: f_selectzq
                    })
                    $("#T_device").ligerComboBox({
                        width: 180,
                        onBeforeOpen: f_selectsb
                    })
                    
                    if (flag & (obj.cus_name == "" || obj.cus_name ==undefined)) {
                       var manager = $.ligerDialog.error('单号不正确!!(3秒自动关闭)');
                        setTimeout(function () {
                            manager.close();
                        }, 3000); 
                    }
                    $("#T_orderid").val(obj.id);
                    $("#hdorderid").val(obj.orderid);  //订单编号 
                    $("#T_orderid_val").val(obj.ordermxid);//订单明细编号（产品编号）
                    $("#T_ddh").val(obj.Serialnumber);
                    $("#T_cpmc").val(obj.product_name);
                    $("#T_sum").val(obj.quantity);
                    $("#T_unit").val(obj.unit);
                    $("#T_id").val(obj.id);
                     $("#T_cus_name").val(obj.cus_name);



                    $("#T_device").val(obj.DeviceName);
                    $("#T_device_val").val(obj.deviceid);
                    $("#T_boar").val(obj.BoarName);
                    $("#T_boar_val").val(obj.boarid);
                    //$("#hdorderid").value = obj.orderid

                    $("#T_emp").ligerComboBox({
                        width: 180,
                        onBeforeOpen: f_selectEmp
                    })

                    $("#T_emp").val(obj.collector);
                    //$("#T_emp_val").val(obj.emp_id);
                  
                    $("#T_orderid").focus();
                    $("#T_weights").val(obj.weights)
                    $("#T_remarks").val(obj.remarks)
                }
            });


          
        }

        
        //选择仓库
        function f_selectwh() {
            $.ligerDialog.open({
                zindex: 9005, title: '选择仓库', width: 750, height: 400,  url: '../CRM/BasicData/Get_BD_Warehouse.aspx', buttons: [
                    { text: '确定', onclick: f_selectwhOK },
                    { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
                ]
            });
            return false;
        }
        //选择库位
        function f_selectwh_kw() {
            $.ligerDialog.open({
                zindex: 9005, title: '选择库位', width: 750, height: 400, url: '../CRM/BasicData/Get_BD_Warehouse_KW.aspx', buttons: [
                    { text: '确定', onclick: f_selectwhkwOK },
                    { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
                ]
            });
            return false;
        }
        function f_selectorder() {
            $.ligerDialog.open({
                zindex: 9005, title: '选择订单', width: 750, height: 400, url: '../Sale/GetOrderDetail.aspx', buttons: [
                     { text: '确定', onclick: f_selectorderOK },
                     { text: '取消', onclick: function (item, dialog) { dialog.close();} }
                ]
            });
            return false;
        }
        //选择种禽
        function f_selectzq() {
            $.ligerDialog.open({
                zindex: 9005, title: '选择种禽', width: 750, height: 400, url: '../Product/GetBasic_boar.aspx', buttons: [
                    { text: '确定', onclick: f_selectzqOK },
                    { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
                ]
            });
            return false;
        }
        //选择设备
        function f_selectsb() {
            $.ligerDialog.open({
                zindex: 9005, title: '选择设备', width: 750, height: 400, url: '../Product/GetBasic_device.aspx', buttons: [
                    { text: '确定', onclick: f_selectsbOK },
                    { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
                ]
            });
            return false;
        }
        function f_selectwhOK(item, dialog) {
            var data = dialog.frame.f_select();
            if (!data) {
                alert('请选择行!');
                return;
            }
            // alert(JSON.stringify(data));
            if (type == "Out")//出库
            {
                $("#T_Out_Warehouse").val(data.Name);
                $("#T_Out_Warehouse_val").val(data.ID);
            }
            if (type == "In")//出库
            {
                $("#T_In_Warehouse").val(data.Name);
                $("#T_In_Warehouse_val").val(data.ID);
            }
            dialog.close();
        }
        function f_selectwhkwOK(item, dialog) {
            var data = dialog.frame.f_select();
            if (!data) {
                alert('请选择行!');
                return;
            }
            // alert(JSON.stringify(data));
            if (type == "Out")//出库
            {
                $("#T_Out_Warehouse_kw").val(data.Name);
                $("#T_Out_Warehouse_kw_val").val(data.KWID);
            }
            if (type == "In")//出库
            {
                $("#T_In_Warehouse_kw").val(data.Name);
                $("#T_In_Warehouse_kw_val").val(data.KWID);
            }
            dialog.close();
        }
        function f_selectzqOK(item, dialog) {
            var data = dialog.frame.f_select();
            if (!data) {
                alert('请选择行!');
                return;
            }
           // alert(JSON.stringify(data));
            $("#T_boar").val(data.product_name);
            $("#T_boar_val").val(data.id);
            dialog.close();
        }
        function f_selectsbOK(item, dialog) {
            var data = dialog.frame.f_select();
            if (!data) {
                alert('请选择行!');
                return;
            }
            //alert(JSON.stringify(data))
            $("#T_device").val(data.product_name);
            $("#T_device_val").val(data.id);
            dialog.close();
        }
        function f_selectorderOK(item, dialog) {
            var data = dialog.frame.f_select();
            if (!data) {
                alert('请选择行!');
                return;
            }
            //alert(JSON.stringify(data.order_id))
            $("#T_orderid").val(data.cus_name);
            $("#T_cus_name").val(data.cus_name);
            $("#hdorderid").val(data.order_id);  //订单编号
            $("#T_orderid_val").val(data.product_id);//订单明细编号（产品编号）
            $("#T_ddh").val(data.Serialnumber);
            $("#T_cpmc").val(data.product_name);
            $("#T_sum").val(data.quantity);
            $("#T_unit").val(data.unit);

            dialog.close();
        }

        function f_selectEmp()
        {
            $.ligerDialog.open({
                zindex: 9005, title: '选择员工', width: 650, height: 300, url: '../hr/getemp_auth.aspx?auth=3', buttons: [
                     { text: '确定', onclick: f_selectEmpOK },
                     { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
                ]
            });
            return false;
        }

        function f_selectEmpOK(item, dialog) {
            var data = dialog.frame.f_select();
            if (!data) {
                alert('请选择行!');
                return;
            }

            $("#T_emp").val(data.name);
            $("#T_emp_val").val(data.id);
            dialog.close();
        }

        function f_weights() {

            var weights = $("#T_weights").val() == null ? "" : $("#T_weights").val();
            var density = $("#T_density").val() == null ? "" : $("#T_density").val();
            var VIT = $("#T_VIT").val() == null ? "" : $("#T_VIT").val();
            var shape = $("#T_shape").val() == null ? "" : $("#T_shape").val();
            var dilution_quantity = $("#T_dilution_quantity").val() == null ? "" : $("#T_dilution_quantity").val();
            var dilution_result = $("#T_dilution_result").val() == null ? "" : $("#T_dilution_result").val();
            var dilution_weights = $("#T_dilution_weights").val() == null ? "" : $("#T_dilution_weights").val();
            var In_quantity = $("#T_In_quantity").val() == null ? "" : $("#T_In_quantity").val();
            var In_Warehouse = $("#T_In_Warehouse").val() == null ? "" : $("#T_In_Warehouse").val();
            var In_Warehouse_kw = $("#T_In_Warehouse_kw").val() == null ? "" : $("#T_In_Warehouse_kw").val();
            var Out_quantity = $("#T_Out_quantity").val() == null ? "" : $("#T_Out_quantity").val();
            var Out_Warehouse = $("#T_Out_Warehouse").val() == null ? "" : $("#T_Out_Warehouse").val();
            var Out_Warehouse_kw = $("#T_Out_Warehouse_kw").val() == null ? "" : $("#T_Out_Warehouse_kw").val();
            var Out_logistics = $("#T_Out_logistics").val() == null ? "" : $("#T_Out_logistics").val(); 
            if (weights <= 0 && type == "weights")//密度)
            {
                $.ligerDialog.error('重量不能为空');
                return;
            }
            if (density <= 0 && type == "density")//密度)
            {
                $.ligerDialog.error('密度值不能为空');
                return;
            }
            if (VIT <= 0 && type == "VIT")//hu)
            {
                $.ligerDialog.error('活力值不能为空');
                return;
            }
            var a = {
                "type": type,
                "id": $("#hdid").val(), "weights": weights, "density": density, "shape": shape, "VIT": VIT
                , "dilution_quantity": dilution_quantity,
                "dilution_result": dilution_result,
                "dilution_weights": dilution_weights,
                //dilution_Diff_weights,
                "In_quantity": In_quantity,
                "In_Warehouse": In_Warehouse,
                "In_Warehouse_kw": In_Warehouse_kw,
                "Out_quantity": Out_quantity,
                "Out_Warehouse": Out_Warehouse,
                "Out_Warehouse_kw": Out_Warehouse_kw,
                "Out_logistics": Out_logistics
            };
            return a;
            

        }


    </script>

</head>
<body style="overflow-y: scroll;">
    
     <input type="hidden" id="hdid" value="" />
    <form id="form1" onsubmit="return false">
          <input id="hdorderid" name="hdorderid" type="hidden" />
    </form>
    <div style="padding: 5px 4px 5px 2px;">
 
    </div>
</body>
</html>
