<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <meta http-equiv="X-UA-Compatible" content="ie=edge chrome=1" />
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" />
    <link href="../lib/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" />
    <link href="../CSS/input.css" rel="stylesheet" />

    <script src="../lib/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/ligerui.min.js" type="text/javascript"></script>
    <script src="../lib/jquery.form.js" type="text/javascript"></script>
    <script src="../JS/XHD.js" type="text/javascript"></script>
    <script type="text/javascript">
        var type = "input";
        $(function () {
            type = getparastr("type");
            initLayout();
            $(window).resize(function () {
                initLayout();
            });
            $("#maingrid4").ligerGrid({
                columns: [
                    //{ display: '序号', width: 50, render: function (item, i) { return item.n; } },
                   
                {
                        display: '作业单号', name: 'id', width: 160, render: function (item) {
                            var html = "<a href='javascript:void(0)' onclick=view('collection','" + item.id + "')>" + item.id + "</a>";
                            return html;
                        }
                    },
                    {
                        display: '订单编号', name: 'Serialnumber', width: 160, render: function (item) {
                            var html = "<a href='javascript:void(0)' onclick=view('order','" + item.orderid + "')>" + item.Serialnumber + "</a>";
                            return html;
                        }
                    },
                    {
                        display: '客户', name: 'Customer_id', width: 100,  render: function (item) {
                            var html = "<a href='javascript:void(0)' onclick=view('customer','" + item.Customer_id + "')>";
                            if (item.cus_name)
                                html += item.cus_name;
                            html += "</a>";
                            return html;
                        }
                    },
                      { display: 'ID', name: 'barcode', width: 150 },
                    //{ display: '设备', name: 'DeviceName', width: 80 },
                    { display: '种源', name: 'BoarName', width: 80 },
                    //{ display: '工位号', name: 'WorkstationName', width: 80 },
                    //{ display: '站点', name: 'collectStationName', width: 80 },
                    { display: '种源类型', name: 'product_name', width: 140 },
                    //{
                    //    display: '单价', name: 'agio', width: 80, type: 'float', align: 'right', render: function (item) {
                    //        return toMoney(item.agio);
                    //    }
                    //},
                    //{ display: '数量(份）', name: 'quantity', width: 60, type: 'int' },
                     {
                        display: '下单时间', name: 'dotime', width: 120, render: function (item) {
                            return formatTimebytype(item.dotime, 'yyyy-MM-dd hh:mm');
                        }
                    },
                    { display: '下单人员', name: 'doperson', width: 60 },
                    { display: '规格', name: 'specifications', width: 120 },
                    //{
                    //    display: '采集时间', name: 'collectDate', width: 90, render: function (item) {
                    //        return formatTimebytype(item.collectDate, 'yyyy-MM-dd');
                    //    }
                    //},

                    {
                        display: '状态', name: 'StatusName', width: 90, render: function (item) {
                            return item.StatusName ;
                        }
                    }
                    , { display: '备注', name: 'remarks', width: 150 },
                ],
                //groupColumnName: 'Customer_name', groupColumnDisplay: '客户',
                //groupRender: function (column, display, data) {
                //    return display + ":" + column + "  （共" + data.length + "条记录。）";
                //},
                //defaultCloseGroup: true,
                dataAction: 'server', pageSize: 30, pageSizeOptions: [20, 30, 50, 100],
                url: "boar_collection.grid.xhd?type=" + type+"&rnd=" + Math.random(),
                width: '100%', height: '100%',
                heightDiff: -10,
                detail: {
                    height: 'auto',
                    onShowDetail: function (r, p) {
                        for (var n in r) {
                            if (r[n] == null) r[n] = "";
                        }
                        var grid = document.createElement('div');
                        $(p).append(grid);
                        $(grid).css('margin', 3).ligerGrid({
                            columns: [
                                //{ display: '序号', width: 30, render: function (item, i) { return i + 1; } },
                                //{ display: '种禽号', name: 'boarid', width: 180 },
                                {
                                    display: '站点', name: 'collectStation', width: 80,  align: 'right'
                                },
                                { display: '工位号', name: 'WorkstationNo', width: 80 },
                                {
                                    display: '采集日期', name: 'collectDate', width: 80, width: 120, render: function (item) {
                                        return formatTimebytype(item.collectDate, 'yyyy-MM-dd hh:mm');
                                    }
                                },
                                {
                                    display: '采集员', name: 'collector', width: 100
                                }

                            ],
                            //selectRowButtonOnly: true,
                            usePager: false,
                            checkbox: false,
                            url: "boar_collection.gridmx.xhd?id=" + r.id,
                            width: '99%', height: '180',
                            heightDiff: 0
                        })

                    }
                },
                onRClickToSelect: true,
                onContextmenu: function (parm, e) {
                    actionCustomerID = parm.data.id;
                    menu.show({ top: e.pageY, left: e.pageX });
                    return false;
                }
            });

            $("#grid").height(document.documentElement.clientHeight - $(".toolbar").height());
            $('#serchform').ligerForm();
            toolbar();

          
        });

        function toolbar() {
            var mid = "boar_collection";
            if (type == "collection") mid = "boar_collection_collection";
            $.get("toolbar.GetSys.xhd?mid=" + mid+"&rnd=" + Math.random(), function (data, textStatus) {
                var data = eval('(' + data + ')');
                //alert(data);
                var items = [];
                var arr = data.Items;
                for (var i = 0; i < arr.length; i++) {
                    arr[i].icon = "../" + arr[i].icon;
                    items.push(arr[i]);
                }
                //items.push({ type: 'button', text: '分组展开/关闭', icon: '../images/folder-open.gif', disable: true, click: function () { expand(); } });
                //items.push({
                //    type: 'serchbtn',
                //    text: '高级搜索',
                //    icon: '../images/search.gif',
                //    disable: true,
                //    click: function () {
                //        serchpanel();
                //    }
                //});
                $("#toolbar").ligerToolBar({
                    items: items
                });
                menu = $.ligerMenu({
                    width: 120, items: getMenuItems(data)
                });

                $("#maingrid4").ligerGetGridManager()._onResize();
            });
        }
      
        $(document).keydown(function (e) {
            if (e.keyCode == 13 && e.target.applyligerui) {
                doserch();
            }
        });


        function add() {
            f_openWindow("sale/boar_collection_add.aspx", "新增作业单", 900, 600, f_save);
        }
        function addp() {
            f_openWindow("sale/boar_collection_add_patch.aspx", "批量新增作业单", 1080, 700, f_save_patch);

        }

        function edit() {
            var manager = $("#maingrid4").ligerGetGridManager();
            var row = manager.getSelectedRow();
            if (row) {
                if (row.IsStatus == "0")
                f_openWindow('sale/boar_collection_add.aspx?id=' + row.id, "修改作业单", 900, 600, f_save);
                else {
                    $.ligerDialog.warn('单据状态已经改变，无法修改变更！！');
                }
            }
            else {
                $.ligerDialog.warn('请选择行！');
            }
        }
        function print() {
            var manager = $("#maingrid4").ligerGetGridManager();
            var row = manager.getSelectedRow();
            if (row) {
                f_openWindow('sale/boar_collection_Print.aspx?id=' + row.id, "打印种禽采集标签", 400, 650 );
            }
            else {
                $.ligerDialog.warn('请选择行！');
            }
        } 
        function submit() {
            var manager = $("#maingrid4").ligerGetGridManager();
            var row = manager.getSelectedRow();
            if (row) {
                $.ligerDialog.confirm("是否确定进入下一个流程，称量流程？", function (yes) {
                    if (yes) {
                        $.ajax({
                            url: "boar_collection.UpdateBoar_CollectionStatus.xhd", type: "POST",
                            data: { id: row.id, type: getparastr("type"), rnd: Math.random() },
                            dataType: 'json',
                            success: function (result) {
                                $.ligerDialog.closeWaitting();

                                var obj = eval(result);

                                if (obj.isSuccess) {
                                    top.$.ligerDialog.success('提交成功！！');
                                    f_reload();
                                }
                                else {
                                    $.ligerDialog.error(obj.Message);
                                }

                            },
                            error: function () {
                                top.$.ligerDialog.error('提交失败！');
                            }
                        });
                    }
                })
            }
            else {
                $.ligerDialog.warn("请选择数据");
            }
        } 
        function del() {
            var manager = $("#maingrid4").ligerGetGridManager();
            var row = manager.getSelectedRow();
            if (row) {
                $.ligerDialog.confirm("单据是否报废无法恢复，确定报废？", function (yes) {
                    if (yes) {
                        $.ajax({
                            url: "boar_collection.del.xhd", type: "POST",
                            data: { id: row.id, orderid: row.orderid, ordermxid:row.ordermxid, rnd: Math.random() },
                            dataType: 'json',
                            success: function (result) {
                                $.ligerDialog.closeWaitting();

                                var obj = eval(result);

                                if (obj.isSuccess) {
                                    f_reload();
                                }
                                else {
                                    $.ligerDialog.error(obj.Message);
                                }

                            },
                            error: function () {
                                top.$.ligerDialog.error('删除失败！');
                            }
                        });
                    }
                })
            }
            else {
                $.ligerDialog.warn("请选择数据");
            }
        }
         function back() {
            var manager = $("#maingrid4").ligerGetGridManager();
            var row = manager.getSelectedRow();
            if (row) {
                $.ligerDialog.confirm("单据确认退回录入状态吗？", function (yes) {
                    if (yes) {
                        $.ajax({
                            url: "boar_collection.back.xhd", type: "POST",
                            data: { id: row.id, orderid: row.orderid, ordermxid:row.ordermxid, rnd: Math.random() },
                            dataType: 'json',
                            success: function (result) {
                                $.ligerDialog.closeWaitting();

                                var obj = eval(result);

                                if (obj.isSuccess) {
                                    f_reload();
                                }
                                else {
                                    $.ligerDialog.error(obj.Message);
                                }

                            },
                            error: function () {
                                top.$.ligerDialog.error('删除失败！');
                            }
                        });
                    }
                })
            }
            else {
                $.ligerDialog.warn("请选择数据");
            }
        }

        function f_check(item, dialog) {

            setTimeout(function (item, dialog) { f_save(item, dialog) }, 100);
        }
        function f_save(item, dialog) {
            var issave = dialog.frame.f_save();
            if (!issave) {
                return;
            }

            dialog.close();
            $.ligerDialog.waitting('数据保存中,请稍候...');
            $.ajax({
                url: "boar_collection.save.xhd", type: "POST",
                data: issave,
                dataType: 'json',
                success: function (result) {
                    $.ligerDialog.closeWaitting();

                    var obj = eval(result);

                    if (obj.isSuccess) {
                        f_reload();
                    }
                    else {
                        $.ligerDialog.error(obj.Message);
                    }
                },
                error: function () {
                    $.ligerDialog.closeWaitting();
                    $.ligerDialog.error('操作失败！');
                }
            });
        }

        function f_save_patch(item, dialog) {
            var issave = dialog.frame.f_save();
            var isselect = dialog.frame.getselect();//关闭之前获取
            if (!issave) {
                return;
            }
            dialog.close();
            $.ligerDialog.waitting('数据保存中,请稍候...');
            $.ajax({
                url: "boar_collection.savepatch.xhd", type: "POST",
                data: issave,
                dataType: 'json',
                success: function (result) {
                    $.ligerDialog.closeWaitting();

                    var obj = eval(result);

                    if (obj.isSuccess) {
                        var msg = obj.Message;
                        var num = msg.split(',').length;
                        //如果保存成功，总数；已分配数

                        var num_select = isselect.split(';');
                        if (num > 0) {
                            //$.ligerDialog.confirm('成功生成' + num + '张作业单，订单是否分配完成？',
                            //function (yes) {
                            //    if (yes) {
                            if (Number(num_select[0]) <= Number(num_select[1]) + num) {
                                //再更新订单明细状态为1
                                $.ajax({
                                    url: "boar_collection.saveddmx.xhd", type: "POST",
                                    data: { fa: msg },
                                    dataType: 'json',
                                    success: function (result) {
                                        if (obj.isSuccess) {
                                            f_reload();
                                            //如果总数量<=已分配数量+正在分配数量

                                            $.ligerDialog.success('此订单已分配完成，订单结案！');
                                        }
                                    },
                                    error: function () {
                                        $.ligerDialog.closeWaitting();
                                        $.ligerDialog.error('操作失败1！');
                                    }
                                });

                            } else {
                                //生成作业单的状态直接10
                                $.ajax({
                                    url: "boar_collection.savestatus.xhd", type: "POST",
                                    data: { fa: msg },
                                    dataType: 'json',
                                    success: function (result) {
                                        if (obj.isSuccess) {
                                            f_reload();
                                        }
                                    },
                                    error: function () {
                                        $.ligerDialog.closeWaitting();
                                        $.ligerDialog.error('操作失败2！');
                                    }
                                });
                            }
                        //});
        
                        }                      
                        //f_reload();
                    }
                    else {
                        $.ligerDialog.error(obj.Message);
                    }
                },
                error: function () {
                    $.ligerDialog.closeWaitting();
                    $.ligerDialog.error('操作失败！');
                }
            });
        }

        function f_reload() {
            var manager = $("#maingrid4").ligerGetGridManager();
            manager.loadData(true);
        };

    </script>
</head>
<body>
    <form id="form1" onsubmit="return false">
        <div style="padding: 10px;">
            <div id="toolbar"></div>

            <div id="grid">
                <div id="maingrid4" style="margin: -1px; min-width: 800px;"></div>
            </div>
        </div>

    </form>
    <div class="az">
   </div>
</body>
</html>
