<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" />
    <link href="../lib/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" />

    <script src="../lib/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/ligerui.min.js" type="text/javascript"></script>

    <script src="../lib/jquery-validation/jquery.validate.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/messages_cn.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/common.js" type="text/javascript"></script>

    <script src="../lib/jquery.form.js" type="text/javascript"></script>
    <script src="../JS/XHD.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            $.metadata.setType("attr", "validate");
            XHD.validate($(form1));

            loadForm(getparastr("id"));

        });

        function f_save() {
    
            if ($(form1).valid()) {
                var sendtxt = "&id=" + getparastr("id");  
                sendtxt += "&customer_id=" + getparastr("customer_id");
                return $("form :input").fieldSerialize() + sendtxt;
            }
        }

        function loadForm(oaid) {
            $.ajax({
                type: "get",
                url: "boar_collection.form.xhd", /* 注意后面的名字对应CS的方法名称 */
                data: { id: oaid, rnd: Math.random() }, /* 注意参数的格式和名称 */
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    var obj = eval(result);
                    for (var n in obj) {
                        if (obj[n] == "null" || obj[n] == null)
                            obj[n] = "";
                    }
                    var rows = [],rows1=[];
                // alert(JSON.stringify(obj))
                 
                    rows.push([{ display: "选择订单", name: "T_orderid", validate: "{required:true}", width: 405 },
                    { display: "订单号", name: "T_ddh", type: "text", options: "{width:150,disabled:true}" }]
                               ,[
                      
                        { display: "产品名称", name: "T_cpmc", type: "text",options: "{width:150,disabled:true}" },
                              
                        
                                   { display: "订单数量", name: "T_sum", type: "text", options: "{width:150,disabled:true}"  },
                                   { display: "单位", name: "T_unit", type: "text", options: "{width:150,disabled:true}"  },

                        ]
                    )

                    rows1.push(
                            [
                            { display: "采集时间", name: "T_date", type: "date", options: "{width:150}", validate: "{required:true}", initValue: formatTimebytype(obj.collectDate, "yyyy-MM-dd") },
                            //   { display: "采集人员", name: "T_emp" } ,   
                            //],
                            //[
                                { display: "ID", name: "T_boar", validate: "{required:true}", width: 150 },
                                { display: "种源名称", name: "T_boar_name", type: "text",options: "{width:150,disabled:true}" },
                              
                            ],
                            //[
                            //    { display: "站点", name: "T_collectStation", type: "select", options: "{width:180,url:'Sys_Param.combo.xhd?type=work_site',value:'" + obj.collectStation + "'}",  initValue: obj.collectStation  },
                            //    { display: "工位号", name: "T_WorkstationNo", type: "select", options: "{width:180,url:'Sys_Param.combo.xhd?type=work_loction',value:'" + obj.WorkstationNo + "'}", initValue: obj.WorkstationNo },
                            //],
                            [
                              
                            ],
                            [
                                { display: "备注", name: "T_remarks", type: "textarea", cols: 140, rows: 10, width: 860, cssClass: "l-textarea", initValue: obj.remarks }
                            ]
                        );
                   
                   
                    $("#form1").ligerAutoForm({
                        labelWidth: 80, inputWidth: 150, space: 20,
                        fields: [
                            {
                                display: '订单信息', type: 'group', icon: '',
                                rows:rows
                                
                            },
                              {
                                display: '采精信息', type: 'group', icon: '',
                                rows:rows1
                                
                            }
                        ]
                    });
                    var orderid = obj.orderid || getparastr("orderid");
                 
                    if (orderid)
                    {
                        $("#T_orderid").attr("disabled", true);
                      
                    } 
                    
                    $("#T_orderid").ligerComboBox({
                        width: 400,
                        onBeforeOpen: f_selectorder
                    })
                    $("#T_boar").ligerComboBox({
                        width: 150,
                        onBeforeOpen: f_selectzq
                    })

                    $("#T_device").ligerComboBox({
                        width: 150,
                        onBeforeOpen: f_selectsb
                    })

                    $("#T_orderid").val(obj.cus_name);
                    $("#hdorderid").val(obj.orderid);  //订单编号 
                    $("#T_orderid_val").val(obj.ordermxid);//订单明细编号（产品编号）
                    $("#T_ddh").val(obj.Serialnumber);
                    $("#T_cpmc").val(obj.product_name);
                    $("#T_sum").val(obj.quantity);
                    $("#T_unit").val(obj.unit);



                    $("#T_device").val(obj.DeviceName);
                    $("#T_device_val").val(obj.deviceid);
                    $("#T_boar").val(obj.barcode);
                    $("#T_boar_val").val(obj.boarid);
                    $("#T_boar_name").val(obj.BoarName);
                    //$("#hdorderid").value = obj.orderid

                    $("#T_emp").ligerComboBox({
                        width: 180,
                        onBeforeOpen: f_selectEmp
                    })

                    $("#T_emp").val(obj.collector);
                    //$("#T_emp_val").val(obj.emp_id);
                }
            });
        }
        function f_selectorder() {
            $.ligerDialog.open({
                zindex: 9005, title: '选择订单', width: 750, height: 400, url: '../Sale/GetOrderDetail.aspx', buttons: [
                     { text: '确定', onclick: f_selectorderOK },
                     { text: '取消', onclick: function (item, dialog) { dialog.close();} }
                ]
            });
            return false;
        }
        //选择种禽
        function f_selectzq() {
            $.ligerDialog.open({
                zindex: 9005, title: '选择种禽', width: 800, height: 400, url: '../Product/GetBasic_boar.aspx', buttons: [
                    { text: '确定', onclick: f_selectzqOK },
                    { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
                ]
            });
            return false;
        }
        //选择设备
        function f_selectsb() {
            $.ligerDialog.open({
                zindex: 9005, title: '选择设备', width: 750, height: 400, url: '../Product/GetBasic_device.aspx', buttons: [
                    { text: '确定', onclick: f_selectsbOK },
                    { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
                ]
            });
            return false;
        }
        function f_selectzqOK(item, dialog) {
            var data = dialog.frame.f_select();
            if (!data) {
                alert('请选择行!');
                return;
            }
           // alert(JSON.stringify(data));
            $("#T_boar").val(data.barcode);
            $("#T_boar_val").val(data.id);
            $("#T_boar_name").val(data.product_name);
            dialog.close();
        }
        function f_selectsbOK(item, dialog) {
            var data = dialog.frame.f_select();
            if (!data) {
                alert('请选择行!');
                return;
            }
            //alert(JSON.stringify(data))
            $("#T_device").val(data.product_name);
            $("#T_device_val").val(data.id);
            dialog.close();
        }
        function f_selectorderOK(item, dialog) {
            var data = dialog.frame.f_select();
            if (!data) {
                alert('请选择行!');
                return;
            }
            //alert(JSON.stringify(data.order_id))
            $("#T_orderid").val(data.cus_name);
            $("#hdorderid").val(data.order_id);  //订单编号
            $("#T_orderid_val").val(data.product_id);//订单明细编号（产品编号）
            $("#T_ddh").val(data.Serialnumber);
            $("#T_cpmc").val(data.product_name);
            $("#T_sum").val(data.quantity);
            $("#T_unit").val(data.unit);
            dialog.close();
        }

        function f_selectEmp()
        {
            $.ligerDialog.open({
                zindex: 9005, title: '选择员工', width: 650, height: 300, url: '../hr/getemp_auth.aspx?auth=3', buttons: [
                     { text: '确定', onclick: f_selectEmpOK },
                     { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
                ]
            });
            return false;
        }

        function f_selectEmpOK(item, dialog) {
            var data = dialog.frame.f_select();
            if (!data) {
                alert('请选择行!');
                return;
            }

            $("#T_emp").val(data.name);
            $("#T_emp_val").val(data.id);
            dialog.close();
        }

       
   
      

    </script>

</head>
<body style="overflow-y: hidden;">
    
<%--     <input type="hidden" id="hdorderid" value="" />--%>
    <form id="form1" onsubmit="return false">
          <input id="hdorderid" name="hdorderid" type="hidden" />
    </form>
    <div style="padding: 5px 4px 5px 2px;">
 
    </div>
</body>
</html>
