<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" />
    <link href="../lib/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" />

    <script src="../lib/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/ligerui.min.js" type="text/javascript"></script>

    <script src="../lib/jquery-validation/jquery.validate.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/messages_cn.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/common.js" type="text/javascript"></script>

    <script src="../lib/jquery.form.js" type="text/javascript"></script>
    <script src="../JS/XHD.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            $.metadata.setType("attr", "validate");
            XHD.validate($(form1));

            loadForm(getparastr("id"));

        });

        function f_save() {
            var manager = $("#maingrid4").ligerGetGridManager();
            f_check();
            
            //if (f_postnum() == 0)
            //{
            //    $.ligerDialog.warn("请添加产品！");
            //    return;
            //}
          
            if ($(form1).valid()) {
                var postData = JSON.stringify(manager.getChanges());
                console.log(postData)
                var sendtxt = "&id=" + getparastr("id");
                sendtxt += "&PostData=" + postData;
                sendtxt += "&customer_id=" + getparastr("customer_id");
                return $("form :input").fieldSerialize() + sendtxt ;
            }
        }

        function loadForm(oaid) {
            $.ajax({
                type: "get",
                url: "Sale_orderOnline.form.xhd", /* 注意后面的名字对应CS的方法名称 */
                data: { id: oaid, rnd: Math.random() }, /* 注意参数的格式和名称 */
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) { 
                    var obj = eval(result);
                    for (var n in obj) {
                        if (obj[n] == "null" || obj[n] == null)
                            obj[n] = "";
                    }
                    var rows = [];
                  // alert(JSON.stringify(result));
                  //  var customer_id = obj.Customer_id || getparastr("customer_id");
                   // if (!customer_id||customer_id=="临时客户")
                     //   rows.push([{ display: "客户", name: "T_customer", validate: "{required:true}", width: 465 }])
                   // console.log(obj.pay_type_id)
                    var paytype = obj.pay_type_id;
                    if (paytype === "" || paytype == undefined) paytype = obj.payType;
                    rows.push(
                            [
                                { display: "交货日期", name: "T_date", type: "date", options: "{width:180}", validate: "{required:true}", initValue: formatTimebytype(obj.Order_date, "yyyy-MM-dd") },
                            { display: "支付方式", name: "T_payType", type: "select", options: "{width:180,url:'Sys_Param.combo.xhd?type=pay_type',value:'" + paytype + "'}", validate: "{required:true}", initValue: formatTimebytype(obj.import_time, "yyyy-MM-dd") },
								  { display: "订单状态", name: "T_status", type: "select", options: "{width:180,url:'Sys_Param.combo.xhd?type=order_status',value:'" + obj.Order_status_id + "'}", validate: "{required:true}" }
                              
                            ],
                            [
							   { display: "订单金额", name: "T_amount", type: "text", options: "{width:180,disabled:true,onChangeValue:function(){ getAmount(); }}", validate: "{required:true}", initValue: toMoney(obj.Order_amount) },
                                { display: "订单周期", name: "T_payCycle", type: "text", options: "{width:180,disabled:true}",  initValue: obj.payCycle  },
                                
								    { display: "金额总计", name: "T_total", type: "text", options: "{width:180,disabled:true}", validate: "{required:true}", initValue: toMoney( obj.total_amount) }
                            ],
                            [
                                { display: "优惠金额", name: "T_disc", type: "text", validate: "{required:true}", options: "{width:180,disabled:true,onChangeValue:function(){ getAmount(); }}",initValue: toMoney(obj.OnlineDiscount_amount) },
                                { display: "折扣%", name: "T_zk", type: "text", validate: "{required:true}", options: "{width:180,disabled:true,onChangeValue:function(){ getAmount(); }}",initValue: toMoney(obj.OnlieDiscount_rate) },

                             
                            ], 
                            [
                                { display: "备注", name: "T_details", type: "textarea", cols: 100, rows: 4, width: 800, cssClass: "l-textarea", initValue: obj.Order_details }
                            ]
                        );
                   
                    if (!obj.discount_amount)
                        obj.discount_amount = 0;

                    $("#form1").ligerAutoForm({
                        labelWidth: 80, inputWidth: 180, space: 20,
                        fields: [
                            {
                                display: '订单', type: 'group', icon: '',
                                rows:rows
                                
                            }
                        ]
                    });
                    f_grid();
                    $("#T_payType").ligerGetComboBoxManager().setDisabled();  
                    //$("#T_customer").ligerComboBox({
                    //    width: 465,
                    //    onBeforeOpen: f_selectCustomer
                    //})

                    //$("#T_customer").val(obj.cus_name);
                    //$("#T_customer_val").val(obj.customer_id);


                    //$("#T_emp").ligerComboBox({
                    //    width: 180,
                    //    onBeforeOpen: f_selectEmp
                    //})

                    //$("#T_emp").val(obj.emp_name);
                    //$("#T_emp_val").val(obj.emp_id);
                }
            });
        }
        function f_selectCustomer() {
            $.ligerDialog.open({
                zindex: 9005, title: '选择客户', width: 500, height: 500, url: '../crm/customer/getCustomer.aspx', buttons: [
                     { text: '确定', onclick: f_selectCustomerOK },
                     { text: '取消', onclick: function (item, dialog) { dialog.close();} }
                ]
            });
            return false;
        }

        function f_selectCustomerOK(item, dialog) {
            var data = dialog.frame.f_select();
            if (!data) {
                alert('请选择行!');
                return;
            }
            
            $("#T_customer").val(data.cus_name);
            $("#T_customer_val").val(data.id);
            dialog.close();
        }

        //function f_selectEmp()
        //{
        //    $.ligerDialog.open({
        //        zindex: 9005, title: '选择员工', width: 750, height: 500, url: '../hr/getemp_auth.aspx?auth=3', buttons: [
        //             { text: '确定', onclick: f_selectEmpOK },
        //             { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
        //        ]
        //    });
        //    return false;
        //}

        //function f_selectEmpOK(item, dialog) {
        //    var data = dialog.frame.f_select();
        //    if (!data) {
        //        alert('请选择行!');
        //        return;
        //    }

        //    $("#T_emp").val(data.name);
        //    $("#T_emp_val").val(data.id);
        //    dialog.close();
        //}

        function getAmount()
        {
            var manager = $("#maingrid4").ligerGetGridManager();

            var t = manager.getColumnDateByType('amount', 'sum') * 1.0;
            var tt = manager.getColumnDateByType('amount_agio', 'sum') * 1.0;
            console.log(tt)
            console.log(t)
            $("#T_amount").val(toMoney(t));
            $("#T_disc").val(toMoney(parseFloat(tt) - parseFloat(t)));
            if (t === 0) $("#T_zk").val(0)
            else
            $("#T_zk").val(toMoney(parseFloat(tt) / parseFloat(t)));

            var T_amount = $("#T_amount").val();
            //var T_discount = $("#T_discount").val();
            //var T_total = $("#T_total").val();
      
            //$("#T_discount").val(toMoney(T_discount));
            //- parseFloat(T_discount.replace(/\$|\,/g, ''))
            $("#T_total").val(toMoney(parseFloat(T_amount.replace(/\$|\,/g, '')) ));
        }
        function getAmount2() {
            //var T_disc = $("#T_disc").val();
            //var T_zk = $("#T_zk").val();
            //var T_total = $("#T_total").val();

            ////$("#T_discount").val(toMoney(T_discount));
            //$("#T_total").val(toMoney(parseFloat(T_amount.replace(/\$|\,/g, '')) - parseFloat(T_discount.replace(/\$|\,/g, ''))));
        }
            //第1列：品种名称；
            //第2列：种源类型，
            //第3列：订单规格(毫升) 、
            //第4列：订单数量(份）；
            //第5列 ：有效精子（百万）；
            //第6列 ：标准规格（ML）；
            //第7列 ：标准数量（份）；
            //第8列 ：所需种源（头）；

        function f_grid() {
            $("#maingrid4").ligerGrid({
                columns: [

                    {
                        display: '品种名称', name: 'product_name', width: 120
                    },
                {
                        display: '种源类型', name: 'product_category', width: 100
                    },
                  {
                        display: '规格(毫升)', name: 'capacity', width: 80, type: 'int', align: 'right', editor: { type: 'int', isNegative: false }
                        
                       
                    },
                   {
                        display: '数量(份)', name: 'quantity', width: 80, type: 'int',
                        editor: { type: 'int', isNegative: false }
                    },
                    {
                          display: '精子(百万)', name: 'uspd', width: 80, type: 'int',
                        editor: { type: 'int', isNegative: false }
                       
                    },
                    //{
                    //    display: '规格', name: 'specifications', width: 100
                    //},
                    {
                        display: '每份剂量(ML)', name: 'price', width: 100, type: 'int', align: 'right'
                        
                       
                    },
                  
                    {
                        display: '产出份数', name: 'dailyoutput', width: 100, type: 'int', align: 'right'
                        
                       
                    },
                    {
                        display: '所需种源(头)', name: 'mrp', width: 100, type: 'int', align: 'right'
                        }
                    ,
                    {
                        display: '销售价', name: 'CusPrice', width: 80, type: 'float', align: 'right', render: function (item) {
                            return toMoney(item.CusPrice);
                        }
                      /*  , editor: { type: 'float' }*/
                    },
                   
                    {
                        display: '总价', name: 'amount', width: 100, type: 'float', align: 'right', render: function (item) {
                            return toMoney(item.amount);
                        }
                    },
                     
                    {
                        display: '状态', name: 'detail_status', width: 90, render: function (item) {
                            return item.detail_status;
                        }
                    }
                ],
                allowHideColumn:false,
                onAfterEdit: f_onAfterEdit,
                title: '产品明细',
                usePager: false,
                enabledEdit: true,
                url: "Sale_order_detailsOnline.grid.xhd?orderid=" + getparastr("id"),
                width: '100%',
                height: 350,
                heightDiff: -1,
                onLoaded: f_loaded
            });

        }

        function f_loaded() {
            if ($("#btn_add").length > 0)
                return;

            $(".l-panel-header").append("<div style='width:150px;float:right'><div id = 'btn_add' style='margin-top:2px;'></div><div id = 'btn_del' style='margin-top:2px;'></div></div>");
            $(".l-grid-loading").fadeOut();
            $("#btn_add").ligerButton({
                width: 60,
                text: "添加",
                icon: '../../images/icon/11.png',
                click: add
            })

            $("#btn_del").ligerButton({
                width: 60,
                text: "删除",
                icon: '../../images/icon/12.png',
                click: pro_remove
            })
            $("#maingrid4").ligerGetGridManager()._onResize();
        }        

        function f_onAfterEdit(e) {
            var manager = $("#maingrid4").ligerGetGridManager();
          //alert(JSON.stringify(e)); return;
            manager.updateCell('amount', e.record.CusPrice * e.record.quantity, e.record);
            manager.updateCell('amount_agio', e.record.agio * e.record.quantity, e.record);
            manager.updateCell('mrp', Math.ceil(e.record.quantity / e.record.dailyoutput * (e.record.capacity / e.record.price)), e.record);
            console.log(manager)
            getAmount();
        }        

        function add() {
            f_openWindow("product/GetProductOnline.aspx", "选择产品", 800, 400, f_getpost, 9003);
        }
        function pro_remove() {
            var manager = $("#maingrid4").ligerGetGridManager();
            manager.deleteSelectedRow();
            setTimeout(function () { 
                getAmount();
            }, 50)
           
        }
        function f_getpost(item, dialog) {
            var rows = null;
            if (!dialog.frame.f_select()) {
                alert('请选择产品!');
                return;
            }
            else {
                rows = dialog.frame.f_select();

                //过滤重复
                var manager = $("#maingrid4").ligerGetGridManager();
                var data = manager.getData();

                for (var i = 0; i < rows.length; i++) {
                    rows[i].product_id = rows[i].id;
                    var add = 1;
                    for (var j = 0; j < data.length; j++) {
                        if (rows[i].product_id == data[j].product_id) {
                            add = 0;
                        }
                    }
                    if (add == 1) {
                        //price
                        rows[i].richText = "";
                        rows[i].quantity = 1;
                        rows[i].CusPrice = rows[i].CusPrice;
                        rows[i]["amount"] = rows[i].CusPrice * rows[i].quantity;
                        rows[i]["capacity"] = rows[i].price;
                        rows[i]["mrp"] = Math.ceil(rows[i].quantity / rows[i].dailyoutput * (rows[i].capacity / rows[i].price));
                        rows[i]["amount_agio"] = rows[i].agio * rows[i].quantity;
                        rows[i]["uspd"] = 3000;
                        rows[i]['product_category'] = rows[i].category_name;
                        manager.addRow(rows[i]);
                    }
                }
                dialog.close();
            }
           
            getAmount();
        }

        function f_postnum() {
            var manager = $("#maingrid4").ligerGetGridManager();
            return manager.getColumnDateByType('amount', 'count') * 1.0;            
        }
        
        function f_check() {
            var g = $("#maingrid4").ligerGetGridManager().endEdit(true);
        }
        function remote() {
            var url = "PB_BicycleType.Exist.xhd?id=" + getparastr("id") + "&rnd=" + Math.random();
            return url;
        }

    </script>

</head>
<body style="overflow: hidden;">
    <form id="form1" onsubmit="return false">
    </form>
    <div style="padding: 5px 4px 5px 2px;">

        <div id="maingrid4">
        </div>
    </div>
</body>
</html>
