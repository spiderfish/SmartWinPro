<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <meta http-equiv="X-UA-Compatible" content="ie=edge chrome=1" />
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" />
    <link href="../lib/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" />
    <link href="../CSS/input.css" rel="stylesheet" />

    <script src="../lib/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/ligerui.min.js" type="text/javascript"></script>
    <script src="../lib/jquery.form.js" type="text/javascript"></script>
    <script src="../JS/XHD.js" type="text/javascript"></script>
    <script type="text/javascript">
        var type = "input";
        $(function () {
            type = getparastr("type");
            initLayout();
            $(window).resize(function () {
                initLayout();
            });
            $("#maingrid4").ligerGrid({
                columns: [
                    //{ display: '序号', width: 50, render: function (item, i) { return item.n; } },
                   
                //{
                //        display: '作业单号', name: 'id', width: 180, render: function (item) {
                //            var html = item.id;// "<a href='javascript:void(0)' onclick=view('collection','" + item.id + "')>" + item.id + "</a>";
                //            return html;
                //        }
                //    },
                 
                   
                    { display: '种禽名称', name: 'BoarName', width: 120 },
                    { display: '识别码', name: 'barcode', width: 300 },
                    { display: '成品名称', name: 'product_name', width: 120 },
                 
                    { display: '需求数量', name: 'Quantity', width: 60, type: 'int' },
                    
                    { display: '分配数量', name: 'Dailyoutput', width: 60 },
                    { display: '剩余数量', name: 'NowQuantity', width: 60, type: 'int' },
                  //  { display: '采精种禽', name: 'specifications', width: 120 },
                   // { display: '工位号', name: 'WorkstationName', width: 80 },
                    //{ display: '站点', name: 'collectStationName', width: 80 },
                    {
                        display: '采集时间', name: 'cjrq', width: 120, render: function (item) {
                            return formatTimebytype(item.cjrq, 'yyyy-MM-dd');
                        }
                    } 

                     
                ],
                //groupColumnName: 'Customer_name', groupColumnDisplay: '客户',
                //groupRender: function (column, display, data) {
                //    return display + ":" + column + "  （共" + data.length + "条记录。）";
                //},
                //defaultCloseGroup: true,
                dataAction: 'server', pageSize: 30, pageSizeOptions: [20, 30, 50, 100],
                url: "boar_collection.gridauto.xhd?rnd=" + Math.random(),
                width: '100%', height: '100%',
                heightDiff: -10,

                onRClickToSelect: true,
                onContextmenu: function (parm, e) {
                    actionCustomerID = parm.data.id;
                    menu.show({ top: e.pageY, left: e.pageX });
                    return false;
                }
            });

            $("#grid").height(document.documentElement.clientHeight - $(".toolbar").height());
            $('#serchform').ligerForm();
            toolbar();

          
        });

        function toolbar() {
           
            $.get("toolbar.GetSys.xhd?mid=boar_collection_auto&rnd=" + Math.random(), function (data, textStatus) {
                var data = eval('(' + data + ')');
                //alert(data);
                var items = [];
                var arr = data.Items;
              
                //items.push({ type: 'button', text: '分组展开/关闭', icon: '../images/folder-open.gif', disable: true, click: function () { expand(); } });
                items.push({
                    type: 'textbox',
                    id: 'T_orderid',
                    name: 'T_orderid',
                    text: '选择订单'
                });

                items.push({
                    type: 'textbox',
                    id: 'begintime',
                    name: 'begintime',
                    text: '开始时间'
                });

                items.push({
                    type: 'textbox',
                    id: 'endtime',
                    name: 'endtime',
                    text: '结束时间',
                });
                items.push({ type: 'line' });
                for (var i = 0; i < arr.length; i++) {
                    arr[i].icon = "../" + arr[i].icon;
                    items.push(arr[i]);
                }
                $("#toolbar").ligerToolBar({
                    items: items
                });
                $("#T_orderid").ligerComboBox({
                    width: 165,
                    onBeforeOpen: f_selectorder
                })
                menu = $.ligerMenu({
                    width: 120, items: getMenuItems(data)
                });
                $("#begintime").ligerDateEditor({ width: 100 })
                $("#endtime").ligerDateEditor({ width: 100 })
                $("#maingrid4").ligerGetGridManager()._onResize();
            });
        }
        function f_selectorder() {
            $.ligerDialog.open({
                zindex: 9005, title: '选择订单', width: 750, height: 400, url: '../Sale/GetOrder.aspx', buttons: [
                    { text: '确定', onclick: f_selectorderOK },
                    { text: '取消', onclick: function (item, dialog) { dialog.close(); } }
                ]
            });
            return false;
        }
        function f_selectorderOK(item, dialog) {
            var data = dialog.frame.f_select();
            if (!data) {
                alert('请选择行!');
                return;
            }
            //alert(JSON.stringify(data.order_id))
            $("#T_orderid").val(data.Serialnumber); 
            $("#T_orderid_val").val(data.id);//订单 
            dialog.close();
        }

        $(document).keydown(function (e) {
            if (e.keyCode == 13 && e.target.applyligerui) {
                doserch();
            }
        });

        //计算
        function computer() {
            if ($("#T_orderid").val() == "")
            {
                $.ligerDialog.warn('必须选择一个有效单据！！');
                return;
            } 
            if ($("#begintime").val() == "" || $("#endtime").val()=="" ) {
                $.ligerDialog.warn('时间段必须选择！！');
                return;
            }

            
                var sendtxt = "&rnd=" + Math.random();
                var serchtxt = $("#form1 :input").fieldSerialize() + sendtxt;

                var manager = $("#maingrid4").ligerGetGridManager();
                manager._setUrl("boar_collection.gridauto.xhd?" + serchtxt);
            
          
        }
        //自动生成
        function autoinsert() {
            $.ajax({
                url: "boar_collection.CreateAutoDoc.xhd", type: "POST",
                data: { orderid: $("#T_orderid_val").val() , rnd: Math.random() },
                dataType: 'json',
                success: function (result) {
                    $.ligerDialog.closeWaitting();

                    var obj = eval(result);

                    if (obj.isSuccess) {
                        top.$.ligerDialog.error('生成成功！！');
                    }
                    else {
                        $.ligerDialog.error(obj.Message);
                    }

                },
                error: function () {
                    top.$.ligerDialog.error('提交失败！');
                }
            });

        }
   
        function f_reload() {
            var manager = $("#maingrid4").ligerGetGridManager();
            manager.loadData(true);
        };

    </script>
</head>
<body>
    <form id="form1" onsubmit="return false">
        <div style="padding: 10px;">
            <div id="toolbar"></div>

            <div id="grid">
                <div id="maingrid4" style="margin: -1px; min-width: 800px;"></div>
            </div>
        </div>

    </form>
    <div class="az">
   </div>
</body>
</html>
