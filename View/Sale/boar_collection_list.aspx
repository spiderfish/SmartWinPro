<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <meta http-equiv="X-UA-Compatible" content="ie=edge chrome=1" />
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" />
    <link href="../lib/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" />
    <link href="../CSS/input.css" rel="stylesheet" />

    <script src="../lib/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/ligerui.min.js" type="text/javascript"></script>
    <script src="../lib/jquery.form.js" type="text/javascript"></script>
    <script src="../JS/XHD.js" type="text/javascript"></script>
    <script type="text/javascript">
        var type = "input";
        $(function () {
            type = getparastr("type");
            initLayout();
            $(window).resize(function () {
                initLayout();
            });
            $("#maingrid4").ligerGrid({
                columns: [
                
                { display: '流水号', name: 'barcode', width: 160 , render: function (item) {
                            return item.barcode.substr(15);
                        }}, 
                    //{ display: '是否打印', name: 'IsPrint', width: 60 },
                {
                        display: '操作时间', name: 'dotime', width: 150, render: function (item) {
                            return formatTimebytype(item.dotime, 'yyyy-MM-dd hh:mm:ss');
                        }
                    },
                    //, { display: '备注', name: 'remarks', width: 150 },
                    
                    {
                        display: '备注', name: 'remarks', width: 90, render: function (item) {

                            var html;
                            if (item.remarks == "自动") {
                                html = "<div style='color:#339900'>自动</div>";
                            }

                            else if (item.remarks == "手动") {
                                html = "<div style='color:#FF0000'>手动</div>";
                            }


                            else {
                                html = "<div style='color:#5D5D5D'>未知</div>";
                            }
                            return html;
                        }
                    }
                ],
                
                dataAction: 'server', //pageSize: 100, pageSizeOptions: [20, 30, 50, 100],
                url: "boar_collection.gridlist.xhd?id=" + getparastr("id")+"&rnd=" + Math.random(),
                width: '100%', height: '100%',
                heightDiff: -10,
                //onRClickToSelect: true,
                //onContextmenu: function (parm, e) {
                //    actionCustomerID = parm.data.id;
                //    //menu.show({ top: e.pageY, left: e.pageX });
                //    return false;
                //}
            });

            //$("#grid").height(document.documentElement.clientHeight - $(".toolbar").height());
            //$('#serchform').ligerForm();
            //toolbar();

          
        });

        
        $(document).keydown(function (e) {
            if (e.keyCode == 13 && e.target.applyligerui) {
                doserch();
            }
        });

 

        
      
        function f_reload() {
            var manager = $("#maingrid4").ligerGetGridManager();
            manager.loadData(true);
        };

    </script>
</head>
<body>
    <form id="form1" onsubmit="return false">
        <div style="padding: 10px;">
           <%-- <div id="toolbar"></div>--%>

            <div id="grid">
                <div id="maingrid4" style="margin: -1px; min-width: 400px;"></div>
            </div>
        </div>

    </form>
    <div class="az">
   </div>
</body>
</html>
