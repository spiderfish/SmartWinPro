<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" />
    <link href="../lib/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" />

    <script src="../lib/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/ligerui.min.js" type="text/javascript"></script>

    <script src="../lib/jquery-validation/jquery.validate.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/messages_cn.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/common.js" type="text/javascript"></script>

    <script src="../lib/jquery.form.js" type="text/javascript"></script>
    <script src="../JS/XHD.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            $.metadata.setType("attr", "validate");
            XHD.validate($(form1));

            loadForm(getparastr("id")); 
            var items = [];
              $("#toolbar").ligerToolBar({
                items: items

            });
      
        });

        
        function loadForm(oaid) {
            $.ajax({
                type: "get",
                url: "boar_collection.formAllData.xhd", /* 注意后面的名字对应CS的方法名称 */
                data: { id: oaid, rnd: Math.random() }, /* 注意参数的格式和名称 */
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    var obj = eval(result);
                    for (var n in obj) {
                        if (obj[n] == "null" || obj[n] == null)
                            obj[n] = "";
                    }
                    var rows = [], rows1 = [], rows2 = [], rows3 = [], rows4 = [], rows5 = [], rows6 = [];
                     
                    rows.push(

                        [{ display: "单号", name: "casereference", type: "text", options: "{width:150,disabled:true}", initValue: obj.casereference },
                            { display: "采集人", name: "collector", type: "text", options: "{width:150,disabled:true}", initValue: obj.collector},
                            { display: "操作员", name: "operator", type: "text", options: "{width:150,disabled:true}", initValue: obj.operator},
                            { display: "dosisvolume", name: "dosisvolume", type: "text", options: "{width:150,disabled:true}", initValue: obj.dosisvolume},
                               
                            ]
                    );
                    rows1.push(

                        [

                            { display: "usefulspermperdosis", name: "usefulspermperdosis", type: "text", options: "{width:150,disabled:true}", initValue: obj.usefulspermperdosis},
                            { display: "ejaculatedvolume", name: "ejaculatedvolume", type: "text", options: "{width:150,disabled:true}", initValue: obj.ejaculatedvolume},
                            { display: "sementemp", name: "sementemp", type: "text", options: "{width:150,disabled:true}", initValue: obj.sementemp},
                            
                        ],
                           [

                               { display: "时间", name: "date", type: "text", options: "{width:150,disabled:true}", initValue: obj.date},      
                               { display: "totnumber", name: "totnumber", type: "text", options: "{width:150,disabled:true}", initValue: obj.totnumber},
                        ]
                       
                    );
                          rows2.push(
                            [
                                //{ display: "首量(g)", name: "T_wights", type: "float",options: "{width:150,disabled:true}" },
                                  { display: "单号", name: "casereference", type: "text", options: "{width:150,disabled:true}", initValue: obj.casereference},
                                  { display: "单号", name: "casereference", type: "text", options: "{width:150,disabled:true}", initValue: obj.casereference},
                          ]
                          );
                         // alert(obj.dilution_Diff_weights)
                     var rows4 = [];
                    rows4.push([
                          
                        { display: "单号", name: "casereference", type: "text", options: "{width:150,disabled:true}", initValue: obj.casereference},
                        { display: "单号", name: "casereference", type: "text", options: "{width:150,disabled:true}", initValue: obj.casereference},
                        ],
                        [
                            { display: "单号", name: "casereference", type: "text", options: "{width:150,disabled:true}", initValue: obj.casereference},
                            { display: "单号", name: "casereference", type: "text", options: "{width:150,disabled:true}", initValue: obj.casereference},
                        ]
                          );
                          var rows5 = [];
                          rows5.push([
                              { display: "单号", name: "casereference", type: "text", options: "{width:150,disabled:true}", initValue: obj.casereference },
                              { display: "单号", name: "casereference", type: "text", options: "{width:150,disabled:true}", initValue: obj.casereference },
                              ]
                    );
                     var rows6 = [];
                          rows6.push([
                              { display: "单号", name: "casereference", type: "text", options: "{width:150,disabled:true}", initValue: obj.casereference},
                              { display: "单号", name: "casereference", type: "text", options: "{width:150,disabled:true}", initValue: obj.casereference},
                          ]
                    );

          
                        $("#form1").ligerAutoForm({
                            labelWidth: 65, inputWidth: 155, space: 20, readonly:true,
                            fields: [
                                {
                                    display: '基本信息', type: 'group', icon: '',
                                    rows: rows

                                },
                                 {
                                     display: '信息1', type: 'group', icon: '',
                                    rows: rows1

                                },
                                 {
                                     display: '信息2', type: 'group', icon: '',
                                    rows: rows2

                                },
                                 //{
//                                     display: '密度分析' + mdfx +'', type: 'group', icon: '',
//                                    rows: rows3
//
//                                },
                                 {
                                     display: '信息3', type: 'group', icon: '',
                                    rows: rows4

                                 },
                                 {
                                     display: '信息4', type: 'group', icon: '',
                                     rows: rows5

                                 }
                                ,
                                 {
                                     display: '信息5', type: 'group', icon: '',
                                     rows: rows6

                                 }
 
                            ]
                        });
                  
                  
                    
                    $("#T_id").val(obj.casereference);
                    $("#T_orderid").val(obj.casereference);
               
                }
            });
        }
        

        

    </script>
    
<style type="text/css">

*, *:before, *:after {
	margin: 0;
	padding: 0;
	box-sizing: border-box;
}

html, body {
	height: 100%;
}

body {
	font: 12px/1 'Roboto', sans-serif;
	color: #555;
	background-color: #fff;
	-webkit-font-smoothing: antialiased;
	-moz-osx-font-smoothing: grayscale;
}

ul {list-style: none;}

.cf:before, .cf:after {
	content: ' ';
	display: table;
}
.cf:after {
	clear: both;
}

.title {
	padding: 50px 0;
	font: 24px 'Open Sans', sans-serif;
	text-align: center;
}



.inner {
	max-width:950px;
	margin: 0 auto;
}
.breadcrumbs {
	border-top: 1px solid #ddd;
	border-bottom: 1px solid #ddd;
	background-color: #f5f5f5;
}
.breadcrumbs ul {
	border-left: 1px solid #ddd;
	border-right: 1px solid #ddd;
}
.breadcrumbs li {
	float: left;
	width: 10%;
}
.breadcrumbs a {
	position: relative;
	display: block;
	padding: 20px;
	padding-right: 0 !important;
	/* important overrides media queries */
	font-size: 13px;
	font-weight: bold;
	text-align: center;
	color: #aaa;
	cursor: pointer;
}

.breadcrumbs a:hover {
	background: #eee;
}
.breadcrumbs a.active {
	color: #eee;
	background-color: #009900;
}
.breadcrumbs a.active1 {
	color: #eee;
	background-color: #996600;
}
.breadcrumbs a span:first-child {
	display: inline-block;
	width: 22px;
	height: 22px;
	padding: 2px;
	margin-right: 5px;
	border: 2px solid #aaa;
	border-radius: 50%;
	background-color: #fff;
}

.breadcrumbs a.active span:first-child {
	color: #fff;
	border-color: #777;
	background-color: #777;
}
.breadcrumbs a.active1 span:first-child {
	color: #fff;
	border-color: #777;
	background-color: #777;
}

.breadcrumbs a:before,
.breadcrumbs a:after {
	content: '';
	position: absolute;
	top: 0;
	left: 100%;
	z-index: 1;
	display: block;
	width: 0;
	height: 0;
	border-top: 32px solid transparent;
	border-bottom: 32px solid transparent;
	border-left: 16px solid transparent;
}

.breadcrumbs a:before {
	margin-left: 1px;
	border-left-color: #d5d5d5;
}
.breadcrumbs a:after {
	border-left-color: #f5f5f5;
}
.breadcrumbs a:hover:after {
	border-left-color: #eee;
}
.breadcrumbs a.active:after {
	border-left-color: #009900;
}
.breadcrumbs a.active1:after {
	border-left-color: #996600;
}
.breadcrumbs li:last-child a:before,
.breadcrumbs li:last-child a:after {
	display: none;
}

@media (max-width: 720px) {
	.breadcrumbs a {
		padding: 15px;
	}

	.breadcrumbs a:before,
	.breadcrumbs a:after {
		border-top-width: 26px;
		border-bottom-width: 26px;
		border-left-width: 13px;
	}
}
@media (max-width: 620px) {
	.breadcrumbs a {
		padding: 10px;
		font-size: 12px;
	}

	.breadcrumbs a:before,
	.breadcrumbs a:after {
		border-top-width: 22px;
		border-bottom-width: 22px;
		border-left-width: 11px;
	}
}
@media (max-width: 520px) {
	.breadcrumbs a {
		padding: 5px;
	}
	.breadcrumbs a:before,
	.breadcrumbs a:after {
		border-top-width: 16px;
		border-bottom-width: 16px;
		border-left-width: 8px;
	}
	.breadcrumbs li a span:first-child {
		display: block;
		margin: 0 auto;
	}
	.breadcrumbs li a span:last-child {
		display: none;
	}
}
</style>
</head>
<body style="overflow: scroll;">
  
 
   <div id="toolbar" style="margin-top: 10px;"></div>
    
 
    <form id="form1"  onsubmit="return false">
          <input id="hdorderid" name="hdorderid" type="hidden" />
    </form>
       
    <div style="padding: 5px 4px 5px 2px;">
 
    </div>
        
</body>
</html>
