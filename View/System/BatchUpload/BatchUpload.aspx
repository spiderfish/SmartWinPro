 <%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head> 
<title>批量的文件上传预览</title>
<!-- 引用控制层插件样式 -->
<link rel="stylesheet" href="control/css/zyUpload.css" type="text/css"> 
    <script src="control/js/jquery-1.7.2.js"></script>
    <!-- 引用核心层插件 --> 
<script src="core/zyFile.js"></script>
<!-- 引用控制层插件 -->
<script src="control/js/zyUpload.js"></script>
    <script src="../../JS/XHD.js"></script>
<!-- 引用初始化JS --> 
     <script type="text/javascript">
         var url = window.location.href;
         var IsSuccess = false;
         // 初始化插件
         $(function () {
             var appName = getparastr("appName");
             var funName = getparastr("funName");
             var djbh = getparastr("djbh");
             if (appName == "" || funName == "") {
                 alert("参数传入不正确，请与管理员联系!")
                 return;
             }
             $("#demo").zyUpload({
             width: "650px",                 // 宽度
             height: "400px",                 // 宽度
             itemWidth: "120px",                 // 文件项的宽度
             itemHeight: "100px",                 // 文件项的高度
                 url: "../../../Data/uploadOSSBatch.ashx",  // 上传文件的路径
             multiple: true,                    // 是否可以多个文件上传
             dragDrop: true,                    // 是否可以拖动上传文件
                 data: { Action: 'uploadOss',djbh:djbh, appName: appName, funName: funName, appbucket: url, rnd: Math.random() },
             del: true,                    // 是否可以删除文件 
             finishDel: false,  				  // 是否在上传文件完成后删除预览
             /* 外部获得的回调接口 */
             onSelect: function (files, allFiles) {                    // 选择文件的回调方法
                 console.info("当前选择了以下文件：");
                 console.info(files);
                 console.info("之前没上传的文件：");
                 console.info(allFiles);
             },
             onDelete: function (file, surplusFiles) {                     // 删除一个文件的回调方法
                 console.info("当前删除了此文件：");
                 console.info(file);
                 console.info("当前剩余的文件：");
                 console.info(surplusFiles);
             },
                 onSuccess: function (file) {                    // 文件上传成功的回调方法
                     alert(IsSuccess)
                 IsSuccess = true;
                 console.info("此文件上传成功：");
                 console.info(file);
             },
             onFailure: function (file) {                    // 文件上传失败的回调方法
                 console.info("此文件上传失败：");
                 console.info(file);
             },
             onComplete: function (responseInfo) {           // 上传完成的回调方法
                 console.info("文件上传完成");
                 IsSuccess = true;
                 console.info(responseInfo);
             }
             });
         });

         function f_Success() {
             if ($("#uploadInf").text().indexOf("成功") > 0)
                 IsSuccess = true;
             return IsSuccess;
         }

     </script>
</head>
<body>
<h1 style="text-align:center;">批量上传</h1>
<div id="demo" class="demo"></div>
</body>
</html>