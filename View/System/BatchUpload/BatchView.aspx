 <%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head> 
<title>批量的文件预览</title>
<!-- 引用控制层插件样式 -->
<link rel="stylesheet" href="control/css/zyUpload.css" type="text/css"> 
    <script src="control/js/jquery-1.7.2.js"></script> 
    <script src="../../JS/XHD.js"></script>
    <script src="control/js/jqueryrotate.js"></script>
<!-- 引用初始化JS --> 
     <script type="text/javascript">
 
         var url = window.location.href; 
         // 初始化插件
         $(function () { 
             var djbh = getparastr("id"); 
             if (djbh == ""  ) {
                 alert("参数传入不正确，请与管理员联系!")
                 return;
             }
             $.ajax({ 
                 type: "GET",
                 url: "boar_collection.GetSAS_InfoImgDataList.xhd", /* 注意后面的名字对应CS的方法名称 */
                 data: {  id: djbh , rnd: Math.random() }, /* 注意参数的格式和名称 */
                 contentType: "application/json; charset=utf-8",
                 dataType: "json",

                 success: function (result) {
                     // alert(JSON.stringify(result))
                     var obj = eval(result);
                     for (var n in obj.Rows) {
                         if (obj.Rows[n] == "null" || obj.Rows[n] == null)
                             obj.Rows[n] = "";
                       //  console.log(obj.Rows[n])
                         var html = Preview("../../uploadedFiles/"+obj.Rows[n].img_Name, n, obj.Rows[n].orgin_Name);
                         $("#preview").append(html);
                     }

                    
                
                     
                 }
             });
            
         });
         var operimg_id;
         var zoom_rate = 100;
         var zoom_timeout;
         function rotateimg() {
             var smallImg = $("#" + operimg_id);
             var num = smallImg.attr('curr_rotate');
             if (num == null) {
                 num = 0;
             }
             num = parseInt(num);
             num += 1;
             smallImg.attr('curr_rotate', num);

             $("#show_img").rotate({ angle: 90 * num });
             smallImg.rotate({ angle: 90 * num });
         }

         function createOpenBox() {
             if ($('#operimg_box').length == 0) {
                 var html = '    <div class="operimg_box" id="operimg_box">';
                 html += '       <img id="show_img" src="" onclick="get_imgsize()" />';
                 html += '       <span class="set_img percent_img" id="zoom_show" >percent</span>';
                 html += '       <span class="set_img zoomin" onclick="zoomIn()"></span>';
                 html += '       <span class="set_img zoomout" onclick="zoomOut()"></span>';
                 html += '       <span class="set_img ratateimg" id="btn_rotateimg" onclick="rotateimg()"></span>';
                 html += '       <span class="set_img close_img" id="delimg"></span>';
                 html += '       <span class="set_img arrowleft" onclick="prevImg()"></span>';
                 html += '       <span class="set_img arrowright" onclick="nextImg()"></span>';
                 html += '       <span class="set_img operarea_box"></span>';
                 html += '   </div><div class="clearboth"></div><div id="mask_bg"></div>';
                 $('body').append(html);
             }
         }
         function get_imgsize() {
             var img_size = $("#show_img").width();
             //alert(img_size);
         }

         function setNewIndex(isNext) {
             var imgs = $('#preview').find('img[class="upload_image"]');
             var imgCount = imgs.length;
             var currIndex = 0;
             for (var i = 0; i < imgCount; i++) {
                 if ($(imgs[i]).attr('id') == operimg_id) {
                     currIndex = i;
                     break;
                 }
             }

             if (isNext) {
                 currIndex += 1;
                 if (currIndex >= imgCount) {
                     currIndex = 0;
                 }
             } else {
                 currIndex -= 1;
                 if (currIndex < 0) {
                     currIndex = imgCount - 1;
                 }
             }

             return $(imgs[currIndex]).attr('id').replace('uploadImage_', '');
         }
         function nextImg() {
             var index = setNewIndex(true);
             setimgbox(index);
         }
         function prevImg() {
             var index = setNewIndex(false);
             setimgbox(index);
         }
         function zoomIn() {
             zoom_rate += 10;
             doZoom(zoom_rate);
             setoperimgbox();
         }
         function zoomOut() {
             zoom_rate -= 10;
             doZoom(zoom_rate);
             setoperimgbox();
         }

         function doZoom(zoom_rate) {
             //$("#show_img").css('width', zoom_rate + '%').css('height', zoom_rate + '%');	
             var naturalWidth = $("#show_img")[0].naturalWidth;
             $("#show_img").css('width', naturalWidth * zoom_rate * 0.01 + 'px');

             clearTimeout(zoom_timeout);
             zoom_rate = parseInt(zoom_rate);
             $("#zoom_show").show();
             $("#zoom_show").html(zoom_rate + '%');
             zoom_timeout = setTimeout(function () {
                 $("#zoom_show").hide('fast');
             }, 1000);
         }
         function setoperimgbox() {
             var obImage = $("#operimg_box");
             var ob_width = obImage.width();
             var ob_height = obImage.height();
             //alert(ob_width +":"+ob_height);
             var ob_left = (window.innerWidth - ob_width) / 2;
             var ob_top = (window.innerHeight - ob_height) / 2;
             console.log(ob_left + "   " + ob_top + " " + ob_width + " " + ob_height + " " + window.innerWidth + " " + window.innerHeight);
             $("#operimg_box").css("left", ob_left).css("top", ob_top);
             $("#mask_bg").show();
             $("#operimg_box").show();
         }


         /*鍒犻櫎*/
         function delimg(index) {
             //var imgboxid=$(x).parent().parent().attr("id");
             var smallimgbox = 'uploadList_' + index;
             ZYFILE.funDeleteFile(parseInt(index), true)
             $("#" + smallimgbox).remove();

         }

         function setimgbox(index) {
             createOpenBox();
             zoom_rate = 100;
             operimg_id = 'uploadImage_' + index;

             var smallImg = $("#" + operimg_id);

             // alert(smallImg[0].naturalWidth);

             var naturalWidth = smallImg[0].naturalWidth;
             var naturalHeight = smallImg[0].naturalHeight;
             zoom_rate = 400 / Math.max(naturalWidth, naturalHeight) * 100;

             $("#show_img").attr("src", smallImg.attr('src'));
             $("#mask_bg").show();
             $("#operimg_box").show();
             //$("#show_img").css('width', zoom_rate + '%').css('height', zoom_rate + '%');
             $("#show_img").css('width', naturalWidth * zoom_rate * 0.01 + 'px');

             var num = $("#" + operimg_id).attr('curr_rotate');
             $("#show_img").rotate({ angle: 90 * num });

             $("#delimg").click(function () {
                 $("#show_img").attr("src", "");
                 $("#operimg_box").hide();
                 $("#mask_bg").hide();
             });

             setoperimgbox();
         }
         function download(index) {
             operimg_id = 'uploadImage_' + index;

             var smallImg = $("#" + operimg_id);
             window.open(smallImg.attr('src'));
         }


         function Preview(file, index, name) {
             console.log(name)
             var html = "";
             var imgWidth = 200;
             var htmlname=""
             // 处理不同类型文件代表的图标
             var fileImgSrc = "control/images/fileType/";
             if (name.indexOf("rar") > 0) {
                 fileImgSrc = fileImgSrc + "rar.png";
             } else if (name.indexOf("zip") > 0) {
                 fileImgSrc = fileImgSrc + "zip.png";
             } else if (name.indexOf("text") > 0) {
                 fileImgSrc = fileImgSrc + "txt.png";
             } else {
                 fileImgSrc = fileImgSrc + "file.png";
             }

             // 图片上传的是图片还是其他类型文件
             if (file.indexOf("image") == 0) {
                 //if (str.substring(name.length-15)= "Chart_Class.bmp")
                 //    htmlname = "上位图"
                 //else
                 //     htmlname = "未定义"

                 htmlname == name.substring(name.length-4)
                 html += '<div id="uploadList_' + index + '" class="upload_append_list">';
                 html += '	<div class="file_bar">';
                 html += '		<div style="padding:5px;">';
                 html += '			<p class="file_name">' + name + '</p>';

                 html += '		</div>';
                 html += '	</div>';
                 html += '	<a style="height:200px;width:200px;" href="#" class="imgBox">';
                 html += '		<div class="uploadImg" style="width:200px">';
                 html += '			<img id="uploadImage_' + index + '" class="upload_image" src="' + file + '" style="width:expression(this.width > ' + imgWidth + ' ? ' + imgWidth + 'px : this.width)" />';
                 html += '		</div>';
                 html += '	</a>';
                 html += '		<span style="padding-left:15px;padding-right:30px;margin-top:6px;display:inline-block;" onclick="download(' + index + ')" id="img' + index + '">下载</span>&nbsp;';
                 html += '</div>';

             } else {
                 html += '<div id="uploadList_' + index + '" class="upload_append_list">';
                 html += '	<div class="file_bar">';
                 html += '		<div style="padding:5px;">';
                 html += '			<p class="file_name">' + name + '</p>';

                 html += '		</div>';
                 html += '	</div>';
                 html += '	<a style="height:200px;width:200px;" href="#" class="imgBox">';
                 html += '		<div class="uploadImg" style="width:' + imgWidth + 'px">';
                 html += '			<img id="uploadImage_' + index + '" class="upload_image" src="' + file + '" style="width:expression(this.width > ' + imgWidth + ' ? ' + imgWidth + 'px : this.width)" />';
                 html += '		</div>';
                 html += '	</a>';
                 //html += '		<span style="padding-left:15px;padding-right:30px;margin-top:6px;display:inline-block;" onclick="setimgbox(' + index + ')" id="img' + index + '">放大</span>&nbsp;';
                 html += '		<span style="padding-left:15px;padding-right:30px;margin-top:6px;display:inline-block;" onclick="download(' + index + ')" id="img' + index + '">' + htmlname + '查看原图</span>&nbsp;';
                 html += '</div>';
             }

             return html;
         }

     </script>
</head>
<body> 
<div id="demo" class="demo">
    <div id="preview" class="upload_preview"></div>
</div>
</body>
</html>