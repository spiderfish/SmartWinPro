﻿/*
   　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;
using System.Data;
using System.Text;
using System.Web;
using XHD.Common;
using XHD.Controller;

namespace XHD.Server
{
    public class Basic_Boar_Heath
    {
        public static BLL.Basic_Boar_Heath category = new BLL.Basic_Boar_Heath();
        public static Model.Basic_Boar_Heath model = new Model.Basic_Boar_Heath();

        public HttpContext Context;
        public string emp_id;
        public string emp_name;
        public Model.hr_employee employee;
        public HttpRequest request;
        public string uid;
        

        public Basic_Boar_Heath()
        {
        }

        public Basic_Boar_Heath(HttpContext context)
        {
            Context = context;
            request = context.Request;

            var userinfo = new User_info();
            employee = userinfo.GetCurrentEmpInfo(context);

            emp_id = employee.id;
            emp_name = PageValidate.InputText(employee.name, 50);
            uid = PageValidate.InputText(employee.uid, 50);
            
        }

        public string save()
        {
            model.weight = StringPlus.str2dec(PageValidate.InputText(request["T_weight"], 50));
            model.backfat = StringPlus.str2dec(PageValidate.InputText(request["T_rbackfat"], 250));
            model.boarId = (PageValidate.InputText(request["bid"], 250));
            model.drinkWater = PageValidate.InputText(request["T_drinkWater_val"],  50);
            model.eyeArea = StringPlus.str2dec(PageValidate.InputText(request["T_eyeArea"], 250));
            model.Feces = PageValidate.InputText(request["T_Feces_val"], 20);
            model.feed = PageValidate.InputText(request["T_feed_val"], 250);
            model.others = PageValidate.InputText(request["T_others"], 20);
            model.pigHeath = PageValidate.InputText(request["T_pigHeath_val"], 20);
            model.dormHeath = PageValidate.InputText(request["T_dormHeath_val"], 20); 
            model.remarks = PageValidate.InputText(request["T_remarks"], 250);
            model.saliva = PageValidate.InputText(request["T_saliva_val"], 20);
            model.semen = PageValidate.InputText(request["T_semen_val"], 20);
            model.serum = PageValidate.InputText(request["T_serum_val"], 20);
            model.temperature = StringPlus.str2dec( PageValidate.InputText(request["T_temperature"], 50));
            
          model.monitorDate = StringPlus.str2date(PageValidate.InputText(request["T_time"], 50));
            model.id = Guid.NewGuid().ToString();
                model.createPerson = emp_name;
                model.createDate = DateTime.Now;
             //  model.monitorDate = DateTime.Now;
            model.monitorPerson = emp_name;
                category.Add(model);

          
            return XhdResult.Success().ToString();
        }

        public string grid()
        {
            int PageIndex = int.Parse(request["page"] == null ? "1" : request["page"]);
            int PageSize = int.Parse(request["pagesize"] == null ? "30" : request["pagesize"]);
            string sortname = request["sortname"];
            string sortorder = request["sortorder"];

            if (string.IsNullOrEmpty(sortname))
                sortname = " id";
            if (string.IsNullOrEmpty(sortorder))
                sortorder = "asc";

            string sorttext = " " + sortname + " " + sortorder;

            string Total;
            string serchtxt = $" 1=1 ";
            if (!string.IsNullOrEmpty(request["id"]))
                serchtxt += $" and boarId='{PageValidate.InputText(request["id"], 50)}'";

            //if (!string.IsNullOrEmpty(request["stext"]))
            //    serchtxt += $" and product_name like N'%{ PageValidate.InputText(request["stext"], 255) }%'";

            //权限
            DataSet ds = category.GetList(PageSize, PageIndex, serchtxt, sorttext, out Total);

            string dt = GetGridJSON.DataTableToJSON1(ds.Tables[0], Total);
            return (dt);

        }

        public string tree()
        {
            DataSet ds = category.GetList($"1=1");
            var str = new StringBuilder();
            str.Append("[");
            str.Append(GetTreeString("root", ds.Tables[0]));
            str.Replace(",", "", str.Length - 1, 1);
            str.Append("]");
            return str.ToString();
        }

        public string combo(string id)
        {
            DataSet ds = category.GetList($"1=1 and ProductId='" +id+"'");
            var str = new StringBuilder();
            str.Append("[");
            //str.Append("{\"id\":\"root\",\"text\":\"无\",\"d_icon\":\"\"},");
            str.Append(GetTreeString(id, ds.Tables[0]));
            str.Replace(",", "", str.Length - 1, 1);
            str.Append("]");
            return str.ToString();
        }

        public string form(string id)
        {
            if (!PageValidate.checkID(id)) return "{}";
            id = PageValidate.InputText(id, 50);
            DataSet ds = category.GetList($" id='{id}' ");
            return DataToJson.DataToJSON(ds);

        }
        public string formFull(string id)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(" SELECT   ");
            sb.AppendLine("  drom.dormName,cor.corralName,cat.product_name ,ser.SeriesName,lev.LevelName,pcat.product_category  ");
            sb.AppendLine(" ,A.*    ");
            sb.AppendLine(" ,B.weight   ");
            sb.AppendLine("      ,B.birthPlace   ");
            sb.AppendLine("      ,B.leftNip   ");
            sb.AppendLine("      ,B.rightNip   ");
            sb.AppendLine("      ,B.stopMilkDate   ");
            sb.AppendLine("      ,B.stopMilkWeight   ");
            sb.AppendLine("      ,B.sameNestsNumber   ");
            sb.AppendLine("      ,B.bithsNumber   ");
            sb.AppendLine("      ,B.outDate   ");
            sb.AppendLine("      ,B.levels   ");
            sb.AppendLine("      ,B.description   ");
            sb.AppendLine("      ,B.license  "); 
            sb.AppendLine("  FROM	dbo.Basic_boar A  ");
            sb.AppendLine(" left JOIN  dbo.Basic_Boar_sys B ON	 A.id=B.BoarId  ");
            sb.AppendLine("  LEFT JOIN  dbo.Basic_dorm drom ON	A.dormId =drom.id    ");
            sb.AppendLine("   LEFT JOIN  dbo.Basic_corral cor ON	A.corralId =cor.id    ");
            sb.AppendLine("   LEFT JOIN dbo.Product cat ON	 a.category_id=cat.id  ");
            sb.AppendLine("   LEFT JOIN dbo.Basic_Product_Series ser ON a.Pro_SeriesId=ser.id  ");
            sb.AppendLine("   LEFT JOIN  dbo.Basic_Product_Series_Level lev ON lev.id=a.Pro_Series_LevelId  ");
            sb.AppendLine("   LEFT JOIN dbo.Product_category pcat ON	pcat.id=cat.category_id  ");
            sb.AppendLine(" WHERE 1=1 ");
            if (!PageValidate.checkID(id)) return "{}";
            id = PageValidate.InputText(id, 50);
            string sql = sb.ToString() + " and a.id='"+id+"'";
            DataSet ds = DBUtility.DbHelperSQL.Query(sql);
            return DataToJson.DataToJSON(ds);

        }
        
        //del
        public string del(string id)
        {
            if (!PageValidate.checkID(id)) return "false";
            id = PageValidate.InputText(id, 50);
          

            bool isdel = category.Delete(id);
            if (!isdel) return XhdResult.Error("系统错误！").ToString();

            //日志
            string EventType = "产品类别删除";

            string UserID = emp_id;
            string UserName = emp_name;
            string IPStreet = request.UserHostAddress;
            string EventID = id;
            string EventTitle = "";

            Syslog.Add_log(UserID, UserName, IPStreet, EventTitle, EventType, EventID, null);

            return XhdResult.Success().ToString();

        }


        private static string GetTreeString(string Id, DataTable table)
        {
            DataRow[] rows = table.Select($"ProductId='{Id}'");

            if (rows.Length == 0) return string.Empty;
            ;
            var str = new StringBuilder();

            foreach (DataRow row in rows)
            {
                str.Append("{\"id\":\"" + (string)row["id"] + "\",\"text\":\"" + (string)row["SeriesName"] + "\",\"d_icon\":\"../../" + (string)row["def1"] + "\"");

                //if (GetTreeString((string)row["id"], table).Length > 0)
                //{
                //    str.Append(",\"children\":[");
                //    str.Append(GetTreeString((string)row["id"], table));
                //    str.Append("]},");
                //}
                //else
                {
                    str.Append("},");
                }
            }
            return str[str.Length - 1] == ',' ? str.ToString(0, str.Length - 1) : str.ToString();
        }
    }
}
