﻿/*
* CRM_order_details.cs
*
* 功 能： N/A
* 类 名： CRM_order_details
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:38:21    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;
using System.Data;
using System.Web;
using XHD.Common;
using XHD.Controller;

namespace XHD.Server
{
    public class Sale_order_details
    {
        public static BLL.Sale_order_details cod = new BLL.Sale_order_details();
        public static Model.Sale_order_details model = new Model.Sale_order_details();

        public HttpContext Context;
        public string emp_id;
        public string emp_name;
        public Model.hr_employee employee;
        public HttpRequest request;
        public string uid;
        

        public Sale_order_details()
        {
        }

        public Sale_order_details(HttpContext context)
        {
            Context = context;
            request = context.Request;

            var userinfo = new User_info();
            employee = userinfo.GetCurrentEmpInfo(context);

            emp_id = employee.id;
            emp_name = PageValidate.InputText(employee.name, 50);
            uid = PageValidate.InputText(employee.uid, 50);
            
        }

        public string grid(string orderid)
        {
            if (!PageValidate.checkID(orderid)) return "{\"Rows\":[],\"Total\":0}";

            DataSet ds = cod.GetList($" order_id = '{orderid}'");
            return GetGridJSON.DataTableToJSON(ds.Tables[0]);
        }
        public string gridboar(string orderid)
        {
            if (!PageValidate.checkID(orderid)) return "{\"Rows\":[],\"Total\":0}";

            DataSet ds = cod.GetListboar($" order_id = '{orderid}'");
            return GetGridJSON.DataTableToJSON(ds.Tables[0]);
        }


        public string griddetail()
        {
            int PageIndex = int.Parse(request["page"] == null ? "1" : request["page"]);
            int PageSize = int.Parse(request["pagesize"] == null ? "30" : request["pagesize"]);
            string sortname = request["sortname"];
            string sortorder = request["sortorder"];

            if (string.IsNullOrEmpty(sortname))
                sortname = "  create_time";
            if (string.IsNullOrEmpty(sortorder))
                sortorder = "desc";

            string sorttext = $" { sortname } { sortorder}";

            string Total;
            string serchtxt = $" 1=1 ";

            //string issar = request["issarr"];
            //if (issar == "1")
            //{
            //    serchtxt += " and isnull( arrears_money,0)>0";
            //}

            if (PageValidate.checkID(request["customerid"]))
                serchtxt += $" and  Customer_id = '{PageValidate.InputText(request["customerid"], 50)}' ";

            if (!string.IsNullOrEmpty(request["T_cus"]))
                serchtxt += $" and  cus_name like N'%{ PageValidate.InputText(request["T_cus"], 255)}%'";

            if (PageValidate.checkID(request["T_Status_val"]))
                serchtxt += $" and Order_status_id = '{PageValidate.InputText(request["T_Status_val"], 50)}'";

            if (PageValidate.checkID(request["employee_val"]))
                serchtxt += $" and  emp_id = '{PageValidate.InputText(request["employee_val"], 50)}'";
            else if (PageValidate.checkID(request["department_val"]))
                serchtxt += $" and hr_department.id = '{PageValidate.InputText(request["department_val"], 50)}' ";

            if (!string.IsNullOrEmpty(request["startdate"]))
                serchtxt += $" and Order_date >= '{ PageValidate.InputText(request["startdate"], 255) }'";

            if (!string.IsNullOrEmpty(request["enddate"]))
            {
                DateTime enddate = DateTime.Parse(request["enddate"]).AddHours(23).AddMinutes(59).AddSeconds(59);
                serchtxt += $" and Order_date <= '{request["enddate"] }'";
            }

            //权限 
            serchtxt += DataAuth();
            DataSet ds = cod.GetList(PageSize, PageIndex, serchtxt, sorttext, out Total);

            string dt = GetGridJSON.DataTableToJSON1(ds.Tables[0], Total);
            return (dt);
        }

        public string griddetailselect()
        {
            int PageIndex = int.Parse(request["page"] == null ? "1" : request["page"]);
            int PageSize = int.Parse(request["pagesize"] == null ? "30" : request["pagesize"]);
            string sortname = request["sortname"];
            string sortorder = request["sortorder"];

            if (string.IsNullOrEmpty(sortname))
                sortname = "  create_time";
            if (string.IsNullOrEmpty(sortorder))
                sortorder = "desc";

            string sorttext = $" { sortname } { sortorder}";

            string Total;
            string serchtxt = $" detail_status=0 ";

            //string issar = request["issarr"];
            //if (issar == "1")
            //{
            //    serchtxt += " and isnull( arrears_money,0)>0";
            //}

            if (PageValidate.checkID(request["customerid"]))
                serchtxt += $" and  Customer_id = '{PageValidate.InputText(request["customerid"], 50)}' ";

            if (!string.IsNullOrEmpty(request["T_cus"]))
                serchtxt += $" and  cus_name like N'%{ PageValidate.InputText(request["T_cus"], 255)}%'";

            if (PageValidate.checkID(request["T_Status_val"]))
                serchtxt += $" and Order_status_id = '{PageValidate.InputText(request["T_Status_val"], 50)}'";

            if (PageValidate.checkID(request["employee_val"]))
                serchtxt += $" and  emp_id = '{PageValidate.InputText(request["employee_val"], 50)}'";
            else if (PageValidate.checkID(request["department_val"]))
                serchtxt += $" and hr_department.id = '{PageValidate.InputText(request["department_val"], 50)}' ";

            if (!string.IsNullOrEmpty(request["startdate"]))
                serchtxt += $" and Order_date >= '{ PageValidate.InputText(request["startdate"], 255) }'";

            if (!string.IsNullOrEmpty(request["enddate"]))
            {
                DateTime enddate = DateTime.Parse(request["enddate"]).AddHours(23).AddMinutes(59).AddSeconds(59);
                serchtxt += $" and Order_date <= '{request["enddate"] }'";
            }

            //权限 
            serchtxt += DataAuth();
            DataSet ds = cod.GetList(PageSize, PageIndex, serchtxt, sorttext, out Total);

            string dt = GetGridJSON.DataTableToJSON1(ds.Tables[0], Total);
            return (dt);
        }

        private string DataAuth()
        {
            GetDataAuth dataauth = new GetDataAuth();
            DataAuth auth = dataauth.getAuth(emp_id);

            switch (auth.authtype)
            {
                case 0: return " 1=2 ";
                case 1:
                case 2:
                case 3:
                case 4:
                    return $" and Sale_order.emp_id in ({auth.authtext})";
                case 5: return "";
            }

            return auth.authtype + ":" + auth.authtext;
        }

    }
}