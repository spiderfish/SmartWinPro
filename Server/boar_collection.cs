﻿/*
* CRM_order.cs
*
* 功 能： N/A
* 类 名： CRM_order
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:38:21    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using XHD.Common;
using XHD.Controller;
using XHD.DBUtility;

namespace XHD.Server
{
    public class boar_collection
    {
        public static BLL.boar_collection order = new BLL.boar_collection();
        public static Model.boar_collection model = new Model.boar_collection();
        public static BLL.Sys_Param sp = new BLL.Sys_Param();
        public static BLL.Sys_info info = new BLL.Sys_info();
        public HttpContext Context;
        public string emp_id;
        public string emp_name;
        public Model.hr_employee employee;
        public HttpRequest request;
        public string uid;


        public boar_collection()
        {
        }

        public boar_collection(HttpContext context)
        {
            Context = context;
            request = context.Request;

            var userinfo = new User_info();
            employee = userinfo.GetCurrentEmpInfo(context);

            emp_id = employee.id;
            emp_name = PageValidate.InputText(employee.name, 50);
            uid = PageValidate.InputText(employee.uid, 50);

        }

        public string save()
        {


            model.collector = PageValidate.InputText(request["T_emp"], 50);
            model.collectDate = DateTime.Parse(PageValidate.InputText(request["T_date"], 50));
            model.deviceid = PageValidate.InputText(request["T_device_val"], 50);
            model.boarid = PageValidate.InputText(request["T_boar_val"], 50);
            string orderid = PageValidate.InputText(request["hdorderid"], 50);
            string ordermxid = PageValidate.InputText(request["T_orderid_val"], 50);
            model.ordermxid = ordermxid;
            model.collectStation = PageValidate.InputText(request["T_collectStation_val"], 50);
            model.WorkstationNo = PageValidate.InputText(request["T_WorkstationNo_val"], 50);

            model.orderid = orderid;
            model.remarks = PageValidate.InputText(request["T_remarks"], int.MaxValue);

            string id = PageValidate.InputText(request["id"], 50);

            string logcontent = "", EventType = "";
            if (PageValidate.checkID(id, false))
            {
                model.id = id;

                DataSet ds = order.GetList("A.id = '" + id + "'  ");

                order.Update(model);
                //context.Response.Write(model.id );

                try
                {


                    if (ds.Tables[0].Rows.Count == 0)
                        return XhdResult.Error("参数不正确，更新失败！").ToString();

                    DataRow dr = ds.Tables[0].Rows[0];


                    logcontent += Syslog.get_log_content(dr["collector"].ToString(), request["T_emp"], "采精人员", dr["collector"].ToString(), request["T_emp"]);
                    logcontent += Syslog.get_log_content(dr["remarks"].ToString(), request["T_remarks"], "备注", dr["remarks"].ToString(), request["T_remarks"]);
                    if (dr["collectDate"].ToString().Length > 8)
                        logcontent += Syslog.get_log_content(DateTime.Parse(dr["collectDate"].ToString()).ToString("yyy-MM-dd"), request["T_date"], "采精日期", DateTime.Parse(dr["collectDate"].ToString()).ToString("yyy-MM-dd"), request["T_date"]);
                    logcontent += Syslog.get_log_content(dr["WorkstationNo"].ToString(), request["T_WorkstationNo_val"], "工位", dr["WorkstationNo"].ToString(), request["T_WorkstationNo_val"]);
                    logcontent += Syslog.get_log_content(dr["deviceid"].ToString(), request["T_device_val"], "设备", dr["deviceid"].ToString(), request["T_device_val"]);
                    logcontent += Syslog.get_log_content(dr["collectStation"].ToString(), request["T_collectStation_val"], "站点", dr["collectStation"].ToString(), request["T_collectStation_val"]);
                    logcontent += Syslog.get_log_content(dr["boarid"].ToString(), request["T_boar_val"], "站点", dr["boarid"].ToString(), request["T_boar_val"]);
                    EventType = "修改种禽流程";



                }
                catch (Exception e) {
                    string UserID = emp_id;
                    string UserName = emp_name;
                    string IPStreet = request.UserHostAddress;
                    string EventTitle = id;
                    EventType = "种禽流程操作错误";
                    string EventID = id;

                    Syslog.Add_log(UserID, UserName, IPStreet, EventTitle, EventType, EventID, e.Message);
                }

            }
            else
            {

                //  id = Guid.NewGuid().ToString();
                id = sp.GetBarCode("WORK");
                model.id = id;// sp.GetBarCode("WORK") ;
                model.dotime = DateTime.Now;
                model.doperson = emp_name;
                model.IsStatus = "10";//保存为0
                                     // order.UpdateOrderStatus(orderid,ordermxid, "1");//去除一个订单明细只能取一头猪的限制，后续改为手工结案，更新订单状态
                                     //model.arrears_invoice = decimal.Parse(request["T_amount"]);
                order.Add(model);
                EventType = "新增种禽流程";

                //一旦保存，在途
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("   UPDATE B SET	B.status='N' ");//更新为在途状态
                sb.AppendLine("  FROM dbo.boar_collection A ");
                sb.AppendLine("  INNER JOIN  dbo.Basic_boar B ON  A.boarid = B.id");
                sb.AppendLine(" WHERE A.id='" + id + "'  ");
                SqlParameter[] parameters = { };
                DbHelperSQL.ExecuteSql(sb.ToString(), parameters);


            }

            if (!string.IsNullOrEmpty(logcontent))
            {
                //日志

                string UserID = emp_id;
                string UserName = emp_name;
                string IPStreet = request.UserHostAddress;
                string EventTitle = id;
                string EventID = id;

                Syslog.Add_log(UserID, UserName, IPStreet, EventTitle, EventType, EventID, logcontent);
            }

            return XhdResult.Success().ToString();
        }

        public string savepatch()
        {
            model.collector = PageValidate.InputText(request["T_emp"], 50);
            model.collectDate = DateTime.Parse(PageValidate.InputText(request["T_date"], 50));
            model.deviceid = PageValidate.InputText(request["T_device_val"], 50);
            //model.boarid = PageValidate.InputText(request["T_boar_val"], 50);
            string orderid = PageValidate.InputText(request["hdorderid"], 50);
            string ordermxid = PageValidate.InputText(request["T_orderid_val"], 50);
            model.ordermxid = ordermxid;
            model.collectStation = PageValidate.InputText(request["T_collectStation_val"], 50);
            model.WorkstationNo = PageValidate.InputText(request["T_WorkstationNo_val"], 50);

            model.orderid = orderid;
            model.remarks = PageValidate.InputText(request["T_remarks"], int.MaxValue);

            //string id = PageValidate.InputText(request["id"], 50);
            string fa = request["fa"];

            string ids = "";
            string[] ss = fa.Split(',');
            for(int i=0; i < ss.Length; i++)
            {
                string logcontent = "", EventType = "";

                string id = sp.GetBarCode("WORK");
                model.id = id;// sp.GetBarCode("WORK") ;
                model.dotime = DateTime.Now;
                model.doperson = emp_name;
                model.IsStatus = "10";//保存为0
                model.boarid = ss[i].ToString();
                order.Add(model);
                EventType = "新增种禽流程";

                //一旦保存，在途
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("   UPDATE B SET	B.status='N' ");//更新为在途状态
                sb.AppendLine("  FROM dbo.boar_collection A ");
                sb.AppendLine("  INNER JOIN  dbo.Basic_boar B ON  A.boarid = B.id");
                sb.AppendLine(" WHERE A.id='" + id + "'  ");
                SqlParameter[] parameters = { };
                DbHelperSQL.ExecuteSql(sb.ToString(), parameters);

                ids = (ids == "" ? model.id : (ids + "," + model.id));

                if (!string.IsNullOrEmpty(logcontent))
                {
                    //日志
                    string UserID = emp_id;
                    string UserName = emp_name;
                    string IPStreet = request.UserHostAddress;
                    string EventTitle = id;
                    string EventID = id;

                    Syslog.Add_log(UserID, UserName, IPStreet, EventTitle, EventType, EventID, logcontent);
                }
            }
            return XhdResult.Success(ids).ToString();
        }

        public string saveddmx()
        {
            string fa = request["fa"];
            fa =  fa.Replace(",", "','");
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("   UPDATE B SET	B.detail_status='1' ");
            sb.AppendLine("  FROM dbo.boar_collection A ");
            sb.AppendLine("  INNER JOIN  dbo.Sale_order_details B ON  A.orderid = B.order_id AND a.ordermxid=b.product_id ");
            sb.AppendLine(" WHERE a.id in( '" + fa + "')  ");
            SqlParameter[] parameters = { };
            DbHelperSQL.ExecuteSql(sb.ToString(), parameters);

            return XhdResult.Success().ToString();

        }

        public string savestatus()
        {
            string fa = request["fa"];
            fa = fa.Replace(",", "','");
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("   UPDATE a SET	a.IsStatus='10' ");
            sb.AppendLine("  FROM dbo.boar_collection A ");
            sb.AppendLine(" WHERE a.id in( '" + fa + "')  ");
            SqlParameter[] parameters = { };
            DbHelperSQL.ExecuteSql(sb.ToString(), parameters);

            return XhdResult.Success().ToString();
        }

        public string grid()
        {
            int PageIndex = int.Parse(request["page"] == null ? "1" : request["page"]);
            int PageSize = int.Parse(request["pagesize"] == null ? "30" : request["pagesize"]);
            string sortname = request["sortname"];
            string sortorder = request["sortorder"];

            if (string.IsNullOrEmpty(sortname))
                sortname = "  dotime ";
            if (string.IsNullOrEmpty(sortorder))
                sortorder = " desc ";

            string sorttext = $" { sortname } { sortorder}";

            string Total;
            string serchtxt = $" 1=1 ";

            string type = request["type"];

            if (type == "input")//新增编辑
            {
                serchtxt += " and ISNULL(IsStatus,'0') IN('0','10') ";
            }
            if (type == "collection")//采集
            {
                serchtxt += " and ISNULL(IsStatus,'0') IN( '10','20') ";
            }
            else if (type == "weights")//称重
            {
                serchtxt += " and ISNULL(IsStatus,'0') IN('30', '20','10') ";
            }
            else if (type == "density")//密度
            {
                serchtxt += " and ISNULL(IsStatus,'0') IN( '40','50'  ) ";
            }
            else if (type == "VIT")//活力
            {
                serchtxt += " and ISNULL(IsStatus,'0') IN('80'  ) ";
            }
            else if (type == "dilution")//稀释
            { serchtxt += " and ISNULL(IsStatus,'0') IN( '95'  ) "; }
            else if (type == "packing")//打包
            { serchtxt += " and ISNULL(IsStatus,'0') IN( 'A0'  ) "; }
            else if (type == "In")//入库
            { serchtxt += " and ISNULL(IsStatus,'0') IN( 'A0' ) "; }
            else if (type == "Out")//出库
            { serchtxt += " and ISNULL(IsStatus,'0') IN( 'B0'  ) "; }

            if (!string.IsNullOrEmpty(request["stext"]))
            {
                if (request["stext"] != "输入关键词搜索")
                    serchtxt += " and ( barcode like N'%" + PageValidate.InputText(request["stext"], 50) + "%' OR id like N'%" + PageValidate.InputText(request["stext"], 50) + "%' OR Serialnumber like N'%" + PageValidate.InputText(request["stext"], 50) + "%')";
            }

            if (!string.IsNullOrEmpty(request["sboarid"]))
            {
                if (request["sboarid"] != "输入关键词搜索")
                    serchtxt += " and ( barcode like N'%" + PageValidate.InputText(request["sboarid"], 50) + "%')";
            }
            //新加的状态
            if (!string.IsNullOrEmpty(request["T_sectype_val"]))
            {

                serchtxt += " and  IsStatus='" + request["T_sectype_val"] + "'";
            }
            if (!string.IsNullOrEmpty(request["T_kssj"]))
            {

                serchtxt += " and   dotime>='" + request["T_kssj"] + "'";
            }
            if (!string.IsNullOrEmpty(request["T_jssj"]))
            {

                serchtxt += " and   dotime<='" + request["T_jssj"] + "'";
            }
            DataSet ds = order.GetList(PageSize, PageIndex, serchtxt, sorttext, out Total);

            string dt = GetGridJSON.DataTableToJSON1(ds.Tables[0], Total);
            return (dt);
        }
        //明细
        public string gridmx(string id)
        {
            //if (!PageValidate.checkID(id)) return "{\"Rows\":[],\"Total\":0}";

            DataSet ds = order.GetListCollection($" id = '{id}'");
            return GetGridJSON.DataTableToJSON(ds.Tables[0]);
        }

        public string boars()
        {
            string sortname = request["sortname"];
            string sortorder = request["sortorder"];
            string fa = request["fa"];
            string infa = request["in"];
            if (string.IsNullOrEmpty(sortname))
                sortname = "  A.birthday ";
            if (string.IsNullOrEmpty(sortorder))
                sortorder = "desc";
            string barcode = (null == request["barcode"] ? "" : request["barcode"]);

            if (!string.IsNullOrEmpty(fa))
            {
                fa = fa.Replace(",", "','");
            }
            if (!string.IsNullOrEmpty(infa))
            {
                infa = infa.Replace(",", "','");
            }

         
            string sql = "";
            //if (null == fa || string.IsNullOrEmpty(request["fa"]) || fa == "")
            //{
            //    sql = $" status='Y' and CollectionStatus='1' and barcode like '%" + barcode + "%' "; 
            //}
            //else
            //{
            //    sql = $" status='Y' and CollectionStatus='1' and barcode like '%" + barcode + "%' and barcode not in( '" + fa + "') ";
            //}
            sql = $" A.status='Y' and A.CollectionStatus='1' and A.barcode like '%" + barcode + "%' ";

            if (!string.IsNullOrEmpty(request["fa"]))
            {
                sql += " and A.barcode not in( '" + fa + "') ";
            }
            if (!string.IsNullOrEmpty(request["in"]))
            {
                sql += " and A.barcode  in( '" + infa + "') ";
            }
            else if(request["in"] == "0")
            {
                sql += " and 1=2  ";
            }
            string cplx = PageValidate.InputText(request["cplx"], 50);

            if (!string.IsNullOrEmpty(cplx))//产品表的名称
            {
                sql += " AND B.id='"+cplx+"' ";
            }
                //if(!string.IsNullOrEmpty(request["in"]) && infa != "")
                //{
                //    sql += " and barcode  in( '" + infa + "')  ";
                //}
                //else if(string.IsNullOrEmpty(request["fa"]))
                //{
                //    sql += " and 1=2  ";
                //}

                sql += " order by  " + sortname + " " + sortorder;
            //if()

            DataSet ds = order.GetListBoars(sql);
            //DataSet ds = order.GetListBoars($" status='Y' and CollectionStatus='1' ");
            return GetGridJSON.DataTableToJSON(ds.Tables[0]);
        }

        public string gridlist()
        {
            string sortname = request["sortname"];
            string sortorder = request["sortorder"];
            string id = request["id"];
           
            if (string.IsNullOrEmpty(sortname))
                sortname = "  dotime ";
            if (string.IsNullOrEmpty(sortorder))
                sortorder = "asc";
            string sql = "";
            sql += " where bc_id='" + id + "' ";
            sql += " order by  " + sortname + " " + sortorder;
            DataSet ds = order.GetListLog(sql);
            string dt = GetGridJSON.DataTableToJSON1(ds.Tables[0], "999");
            return (dt);
        }


        //自动生成
        public string gridauto()
        {

            string orderid = "",bt="",et="";
            if (!string.IsNullOrEmpty(request["T_orderid_val"]))
            {
                orderid = request["T_orderid_val"];
            }
            if (!string.IsNullOrEmpty(request["begintime"]))
            {
                bt =   PageValidate.InputText(request["begintime"], 50) ;
            }
            if (!string.IsNullOrEmpty(request["endtime"]))
            {
                et  = PageValidate.InputText(request["endtime"], 50)  ;
            }
            DataSet ds = order.GetListAuto(orderid,bt,et);

            string dt = GetGridJSON.DataTableToJSON1(ds.Tables[0], ds.Tables[0].Rows.Count.ToString());
            return (dt);
        }


        public string form(string id)
        {
            if (!PageValidate.checkID(id, false))
                return "{}";
            id = PageValidate.InputText(id, 50);
            string IsStatus = " ";
            string flag = request["flag"];
            string type = request["type"];
            if (type == "collection")//采集
            {

                IsStatus = " and ISNULL(A.IsStatus,'0') IN('10','15','20')";// 

            }
            if (type == "weights")
            {
                if (flag == "true") IsStatus = " and ISNULL(A.IsStatus,'0') IN('20')";//扫描只能20状态
                else
                    IsStatus = " and ISNULL(A.IsStatus,'0') IN('20','30')";//手工修改可以10，30状态

            }
           
            else if (type == "density")//密度
            {
                IsStatus = " and ISNULL(A.IsStatus,'0') IN( '40','50' ) ";
            }
            else if (type == "VIT")//活力
            {
                IsStatus = " and ISNULL(A.IsStatus,'0') IN( '60','70','80' ) ";
            }
            else if (type == "dilution")//活力
            { IsStatus = " and ISNULL(A.IsStatus,'0') IN( '80','90','95' ) "; }
            else if (type == "packing")//打包
            { IsStatus  = " and ISNULL(A.IsStatus,'0') IN( '95','96','A0'  ) "; }
            else if (type == "In")//活力
            { IsStatus = " and ISNULL(A.IsStatus,'0') IN( 'A0' ) "; }
            else if (type == "Out")//活力
            { IsStatus = " and ISNULL(A.IsStatus,'0') IN( 'B0'  ) "; }
            DataSet ds = order.GetList( "A.id = '"+id+"'  "+IsStatus);
            if (ds.Tables[0].Rows.Count<=0) return "{}";
            else
            return DataToJson.DataToJSON(ds);

        }

        public string addprinttime(string id)
        {
            if (!PageValidate.checkID(id, false))
                return XhdResult.Error().ToString(); ;
            id = PageValidate.InputText(id, 50);

            var r = order.AddPrintTimes(id);
            if(!r) XhdResult.Error().ToString();

            return XhdResult.Success().ToString();
           
        }

        public string CreateAutoDoc(string id)
        {
             
            string orderid = PageValidate.InputText(request["orderid"], 50); 
            string Msg = order.CreateAutoDoc(orderid); //order.Delete(id);

            if (Msg!="") return XhdResult.Error(Msg).ToString();
             

            return XhdResult.Success().ToString();


        }


        public string combo()
        {
            string id = PageValidate.InputText(request["type"], 50);
            if (!PageValidate.checkID(id, false)) return "{}";
            string sql= "SELECT* FROM dbo.DocStatus_Name";
            DataSet ds = DbHelperSQL.Query(sql);
            var str = new StringBuilder();

            str.Append("[");
            //str.Append("{id:0,text:'无'},");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                str.Append($"{{\"id\":\"{ ds.Tables[0].Rows[i]["IsStatusCode"]  }\",\"text\":\" { ds.Tables[0].Rows[i]["StatusName"] } \"}},");
            }
            str.Replace(",", "", str.Length - 1, 1);
            str.Append("]");

            return str.ToString();
        }


        public string del(string id)
        {
            if (!PageValidate.checkID(id, false)) return ("delfalse");

            id = PageValidate.InputText(id, 50);
            string orderid = PageValidate.InputText(request["orderid"], 50);
            string ordermxid = PageValidate.InputText(request["ordermxid"], 50);
            bool isdel = order.UpdateBoar_CollectionStatus(id, "NN"); //order.Delete(id);
            
            if (!isdel) return XhdResult.Error().ToString();

            order.UpdateOrderStatus(orderid, ordermxid, "0");//更新订单状态，可以重新选择再做

            return XhdResult.Success().ToString();


        }
        public string back(string id)
        {
            if (!PageValidate.checkID(id, false)) return ("delfalse");

            id = PageValidate.InputText(id, 50);
            string orderid = PageValidate.InputText(request["orderid"], 50);
            string ordermxid = PageValidate.InputText(request["ordermxid"], 50);
            bool isdel = order.UpdateBoar_CollectionStatus(id, "10"); //order.Delete(id);

            if (!isdel) return XhdResult.Error().ToString();

            order.UpdateOrderStatus(orderid, ordermxid, "0");//更新订单状态，可以重新选择再做

            return XhdResult.Success().ToString();


        }

        /// <summary>
        /// 更新状态
        /// </summary>
        /// <returns></returns>
        public string UpdateBoar_CollectionStatus()
        {
            string id = PageValidate.InputText(request["id"], 50);
            string IsStatus = "10";//作业单提交，默认
            // PageValidate.InputText(request["IsStatus"], 50);
            string status = "作业单提交";
            string type = request["type"];
           
            if (type == "collection")//采集
            {
                IsStatus = "20";
                status = "采集精液";
            }
                if (type == "weights")//称重
            {
                status = "称重";
              // string sys_mdfx = "";//是否密度分析  
              DataSet ds=  info.GetList(" sys_key='sys_mdfx' ");
                if(ds==null) IsStatus = "50";
                if (ds.Tables[0].Rows.Count<0) IsStatus = "50";
                if(ds.Tables[0].Rows[0]["sys_value"].ToString()=="Y")
                IsStatus = "40";//需作密度分析
                else IsStatus = "50";

            }
            if (type == "density")//密度
            { IsStatus = "60"; status = "密度分析"; }
            if (type == "VIT")//活力
            { IsStatus = "80"; status = "活力分析"; }
            if (type == "dilution")//活力
            { IsStatus = "95"; status = "稀释分析"; }
            else if (type == "packing")//打包
            { IsStatus = "A0"; status = "流程打包"; }
            if (type == "In")//活力
            { IsStatus = "B0"; status = "入库"; }
            if (type == "Out")//活力
            { IsStatus = "YY"; status = "出库"; }
            if (!PageValidate.checkID(id, false))
                return XhdResult.Error().ToString();  
            var r = order.UpdateBoar_CollectionStatus(id,IsStatus);
            if (!r) XhdResult.Error().ToString();


           
            string logcontent = "";
            logcontent += Syslog.get_log_content(IsStatus, status, "状态修改", IsStatus, status);

            if (!string.IsNullOrEmpty(logcontent))
            {
                //日志

                string UserID = emp_id;
                string UserName = emp_name;
                string IPStreet = request.UserHostAddress;
                string EventTitle =  id;
                string EventType = "种禽流程状态修改";
                string EventID = id;

                Syslog.Add_log(UserID, UserName, IPStreet, EventTitle, EventType, EventID, logcontent);
            }

            return XhdResult.Success().ToString();

        }
        /// <summary>
        /// 更新状态
        /// </summary>
        /// <returns></returns>
        public string UpdateParas()
        {
            string type = PageValidate.InputText(request["type"], 50);
            string id = PageValidate.InputText(request["id"], 50);
            string weights = PageValidate.InputText(request["weights"], 50);
            string density = PageValidate.InputText(request["density"], 50);//密度
            string shape = PageValidate.InputText(request["shape"], 50);//形态
            string VIT = PageValidate.InputText(request["VIT"], 50);//活力
            //出库
            string Out_quantity = PageValidate.InputText(request["Out_quantity"], 50);
            string Out_Warehouse = PageValidate.InputText(request["Out_Warehouse"], 50);
            string Out_Warehouse_kw= PageValidate.InputText(request["Out_Warehouse_kw"], 50);
            string  Out_logistics = PageValidate.InputText(request["Out_logistics"], 50);
            //入库
            string In_quantity = PageValidate.InputText(request["In_quantity"], 50);
            string In_Warehouse = PageValidate.InputText(request["In_Warehouse"], 50);
            string In_Warehouse_kw= PageValidate.InputText(request["In_Warehouse_kw"], 50);
            //稀释
           
            string dilution_result = PageValidate.InputText(request["dilution_result"], 50);
            string dilution_weights = PageValidate.InputText(request["dilution_weights"], 50);
            //打包
            string dilution_quantity = PageValidate.InputText(request["dilution_quantity"], 50);
            if (!PageValidate.checkID(id, false))
                return XhdResult.Error().ToString();
            string EventType = "";
            string logcontent = "";
            if (type == "collection")//采集
            {
                var r = order.UpdateParas(type, id, "", "", "", "", "", "", "", "", "", "", "", "", "", "");
                if (!r) XhdResult.Error().ToString();
                EventType = "种禽流程采精";
                logcontent += Syslog.get_log_content("10", "15", "采集修改", "", weights);
            }
            if (type=="weights") 
            {
                var r = order.UpdateParas(type,id, weights,density,shape,VIT,"","","","","","","","","","");
                if (!r) XhdResult.Error().ToString();
                 EventType = "种禽流程称重";
                     logcontent += Syslog.get_log_content("", weights, "重量修改", "", weights);

            }
            if (type == "density")
            {
                var r = order.UpdateParas(type, id, weights, density, shape, VIT, "", "", "", "", "", "", "", "", "", "");
                if (!r) XhdResult.Error().ToString();
                EventType = "种禽流程密度分析";
                logcontent += Syslog.get_log_content("", density, "密度分析", "", density);
            }
            if (type == "VIT")
            {
                var r = order.UpdateParas(type, id, weights, density, shape, VIT, "", "", "", "", "", "", "", "", "", "");
                if (!r) XhdResult.Error().ToString();
                EventType = "种禽流程活力分析";
                logcontent += Syslog.get_log_content(shape, VIT, "活力分析", shape, VIT);
            }
            if (type == "dilution")
            {
                var r = order.UpdateParas(type, id, "", "", "", "", "", "", "", "", "", "","", "",dilution_result,dilution_weights);
                if (!r) XhdResult.Error().ToString();
                EventType = "种禽流程稀释分析";
                logcontent += Syslog.get_log_content(dilution_quantity, dilution_result, "稀释分析", dilution_weights, dilution_quantity);
            }
            if (type == "packing")//打包
            {
                var r = order.UpdateParas(type, id, "", "", "", "", "", "", "", "", "", "", "", dilution_quantity, "", "");
                if (!r) XhdResult.Error().ToString();
                EventType = "种禽流程打包";
                logcontent += Syslog.get_log_content(dilution_quantity, "", "流程打包", dilution_quantity, "");
            }
            if (type == "In")
            {
                var r = order.UpdateParas(type, id, "", "", "", "", "", "", "", "", In_quantity,In_Warehouse, In_Warehouse_kw, "", "", "");
                if (!r) XhdResult.Error().ToString();
                EventType = "种禽流程入库";
                logcontent += Syslog.get_log_content(In_quantity, In_Warehouse, "入库", In_Warehouse_kw, In_Warehouse_kw);
            }
            if (type == "Out")
            {
                var r = order.UpdateParas(type, id, "", "", "", "",   Out_quantity,Out_Warehouse,Out_Warehouse_kw,Out_logistics, "", "", "", "", "", "");
                if (!r) XhdResult.Error().ToString();
                EventType = "种禽流程出库";
                logcontent += Syslog.get_log_content(Out_quantity, Out_Warehouse, "出库", Out_logistics, Out_Warehouse_kw);
            }
            if (!string.IsNullOrEmpty(logcontent))
            {
                //日志

                string UserID = emp_id;
                string UserName = emp_name;
                string IPStreet = request.UserHostAddress;
                string EventTitle = id; 
                string EventID = id;

                Syslog.Add_log(UserID, UserName, IPStreet, EventTitle, EventType, EventID, logcontent);
            }
            return XhdResult.Success().ToString();

        }

        public string GetBarList(string barcode)
        {
             
            DataSet ds = order.GetBarList(barcode);
            if (ds.Tables[0].Rows.Count <= 0) return "{}";
            else
                return GetGridJSON.DataTableToJSON1(ds.Tables[0], "999");

        }

        public string formAllData(string id)
        {
            
            id = PageValidate.InputText(id, 50);
          
            DataSet ds = order.GetSAS_InfoDataList(" casereference = '" + id + "'  " );
            if (ds.Tables[0].Rows.Count <= 0) return "{}";
            else
                return DataToJson.DataToJSON(ds);

        }
        public string GetSAS_InfoImgDataList(string id)
        {

            DataSet ds = order.GetSAS_InfoImgDataList(" casereference = '" + id + "' ");
            if (ds.Tables[0].Rows.Count <= 0) return "{}";
            else
                return GetGridJSON.DataTableToJSON1(ds.Tables[0], "999");

        }

    }
}
