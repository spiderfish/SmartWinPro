﻿/*
   　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using XHD.Common;
using XHD.Controller;

namespace XHD.Server
{
    public class Basic_Boar_Drug
    {
        public static BLL.Basic_Boar_Drug category = new BLL.Basic_Boar_Drug();
        public static Model.Basic_Boar_Drug model = new Model.Basic_Boar_Drug();

        public HttpContext Context;
        public string emp_id;
        public string emp_name;
        public Model.hr_employee employee;
        public HttpRequest request;
        public string uid;
        

        public Basic_Boar_Drug()
        {
        }

        public Basic_Boar_Drug(HttpContext context)
        {
            Context = context;
            request = context.Request;

            var userinfo = new User_info();
            employee = userinfo.GetCurrentEmpInfo(context);

            emp_id = employee.id;
            emp_name = PageValidate.InputText(employee.name, 50);
            uid = PageValidate.InputText(employee.uid, 50);
            
        }

        public string save()
        {
            model.drugId = PageValidate.InputText(request["T_drugName_val"], 50);
            model.remarks = PageValidate.InputText(request["T_remarks"], 250);
            model.boarId = (PageValidate.InputText(request["bid"], 250));
            model.Dosage = StringPlus.str2dec( PageValidate.InputText(request["T_Dosage"], 50));

            string id = PageValidate.InputText(request["bid"], 50); 
            //if (PageValidate.checkID(id))
            //{
            //     model.boarId = id; 
            //    category.Update(model); 
            //    string UserID = emp_id; 
            //} 
            //else
            {
                
                model.id = Guid.NewGuid().ToString();
                model.createId = emp_name;
                model.createDate = DateTime.Now;
                category.Add(model);

            }
            return XhdResult.Success().ToString();
        }

        public string savebatch(string bid,string PostData)
        {
            string json = PostData.ToLower();
            var js = new JavaScriptSerializer();

            Model.Basic_Boar_Drug[] postdata;
            postdata = js.Deserialize<Model.Basic_Boar_Drug[]>(json);
            //List<Model.Basic_Boar_Drug> list = List<Model.Basic_Boar_Drug>
            for (int i = 0; i < postdata.Length; i++)
            {
                model.drugId= postdata[i].drugId;
                model.Dosage = postdata[i].Dosage;
                model.boarId = bid;
                model.remarks = postdata[i].remarks;
                model.drugTime = postdata[i].drugTime;
                model.id = Guid.NewGuid().ToString();
                model.createId = emp_name;
                model.createDate = DateTime.Now;
                category.Add(model);
            }
          
            return XhdResult.Success().ToString();
        }

        public string grid()
        {
            int PageIndex = int.Parse(request["page"] == null ? "1" : request["page"]);
            int PageSize = int.Parse(request["pagesize"] == null ? "30" : request["pagesize"]);
            string sortname = request["sortname"];
            string sortorder = request["sortorder"];

            if (string.IsNullOrEmpty(sortname))
                sortname = " id";
            if (string.IsNullOrEmpty(sortorder))
                sortorder = "asc";

            string sorttext = " " + sortname + " " + sortorder;

            string Total;
            string serchtxt = $" 1=1 ";
            if (!string.IsNullOrEmpty(request["id"]))
                serchtxt += $" and boarId='{PageValidate.InputText(request["id"], 50)}'";

            //if (!string.IsNullOrEmpty(request["stext"]))
            //    serchtxt += $" and product_name like N'%{ PageValidate.InputText(request["stext"], 255) }%'";

            //权限
            DataSet ds = category.GetList(PageSize, PageIndex, serchtxt, sorttext, out Total);

            string dt = GetGridJSON.DataTableToJSON1(ds.Tables[0], Total);
            return (dt);

        }

        public string tree()
        {
            DataSet ds = category.GetList($"1=1");
            var str = new StringBuilder();
            str.Append("[");
            str.Append(GetTreeString("root", ds.Tables[0]));
            str.Replace(",", "", str.Length - 1, 1);
            str.Append("]");
            return str.ToString();
        }

        public string combo(string id)
        {
            DataSet ds = category.GetList($"1=1 and ProductId='" +id+"'");
            var str = new StringBuilder();
            str.Append("[");
            //str.Append("{\"id\":\"root\",\"text\":\"无\",\"d_icon\":\"\"},");
            str.Append(GetTreeString(id, ds.Tables[0]));
            str.Replace(",", "", str.Length - 1, 1);
            str.Append("]");
            return str.ToString();
        }

        public string form(string id)
        {
            if (!PageValidate.checkID(id)) return "{}";
            id = PageValidate.InputText(id, 50);
            DataSet ds = category.GetList($" id='{id}' ");
            return DataToJson.DataToJSON(ds);

        }

        //del
        public string del(string id)
        {
            if (!PageValidate.checkID(id)) return "false";
            id = PageValidate.InputText(id, 50);
          

            bool isdel = category.Delete(id);
            if (!isdel) return XhdResult.Error("系统错误！").ToString();

            //日志
            string EventType = "产品类别删除";

            string UserID = emp_id;
            string UserName = emp_name;
            string IPStreet = request.UserHostAddress;
            string EventID = id;
            string EventTitle = "";

            Syslog.Add_log(UserID, UserName, IPStreet, EventTitle, EventType, EventID, null);

            return XhdResult.Success().ToString();

        }


        public string gridView()
        {
            int PageIndex = int.Parse(request["page"] == null ? "1" : request["page"]);
            int PageSize = int.Parse(request["pagesize"] == null ? "30" : request["pagesize"]);
            string sortname = request["sortname"];
            string sortorder = request["sortorder"];

            if (string.IsNullOrEmpty(sortname))
                sortname = " id";
            if (string.IsNullOrEmpty(sortorder))
                sortorder = "asc";

            string sorttext = " " + sortname + " " + sortorder;

            string Total="999";
            string serchtxt = $" 1=1 ";
            if (!string.IsNullOrEmpty(request["id"]))
                serchtxt += $" and boarId='{PageValidate.InputText(request["id"], 50)}'";



            //权限
            DataSet ds = DBUtility.DbHelperSQL.Query("EXEC Usp_Drug_View '" + request["id"] + "'");

            if (ds == null||ds.Tables.Count<1) return "{\"Rows\":[],\"Total\":\"0\"}";
            else
            {
                string dt = GetGridJSON.DataTableToJSON1(ds.Tables[0], Total);
                return (dt);
            }

        }



        private static string GetTreeString(string Id, DataTable table)
        {
            DataRow[] rows = table.Select($"ProductId='{Id}'");

            if (rows.Length == 0) return string.Empty;
            ;
            var str = new StringBuilder();

            foreach (DataRow row in rows)
            {
                str.Append("{\"id\":\"" + (string)row["id"] + "\",\"text\":\"" + (string)row["SeriesName"] + "\",\"d_icon\":\"../../" + (string)row["def1"] + "\"");

                //if (GetTreeString((string)row["id"], table).Length > 0)
                //{
                //    str.Append(",\"children\":[");
                //    str.Append(GetTreeString((string)row["id"], table));
                //    str.Append("]},");
                //}
                //else
                {
                    str.Append("},");
                }
            }
            return str[str.Length - 1] == ',' ? str.ToString(0, str.Length - 1) : str.ToString();
        }
    }
}
