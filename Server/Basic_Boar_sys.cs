﻿/*
   　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;
using System.Data;
using System.Text;
using System.Web;
using XHD.Common;
using XHD.Controller;

namespace XHD.Server
{
    public class Basic_Boar_sys
    {
        public static BLL.Basic_Boar_sys category = new BLL.Basic_Boar_sys();
        public static Model.Basic_Boar_sys model = new Model.Basic_Boar_sys();

        public HttpContext Context;
        public string emp_id;
        public string emp_name;
        public Model.hr_employee employee;
        public HttpRequest request;
        public string uid;
        

        public Basic_Boar_sys()
        {
        }

        public Basic_Boar_sys(HttpContext context)
        {
            Context = context;
            request = context.Request;

            var userinfo = new User_info();
            employee = userinfo.GetCurrentEmpInfo(context);

            emp_id = employee.id;
            emp_name = PageValidate.InputText(employee.name, 50);
            uid = PageValidate.InputText(employee.uid, 50);
            
        }

        public string save()
        {
            model.weight = StringPlus.str2dec( PageValidate.InputText(request["T_weight"], 50));
            model.birthPlace = PageValidate.InputText(request["T_bithPlace"], 250);
            //model.BoarId = (PageValidate.InputText(request["bid"], 50));
            model.leftNip = StringPlus.str2int(PageValidate.InputText(request["T_leftNip"], 50));
            model.rightNip = StringPlus.str2int(PageValidate.InputText(request["T_rightNip"], 50));
            model.stopMilkDate = StringPlus.str2date(PageValidate.InputText(request["T_stopmilkdate"], 50));
            model.stopMilkWeight = StringPlus.str2dec(PageValidate.InputText(request["T_stopmilkweight"], 50));
            model.sameNestsNumber = StringPlus.str2int(PageValidate.InputText(request["T_sameNestsNumber"], 50));
            model.bithsNumber = StringPlus.str2int(PageValidate.InputText(request["T_bithsNumber"], 50));
            model.outDate = StringPlus.str2date(PageValidate.InputText(request["T_outDate"], 50));
            model.levels =  (PageValidate.InputText(request["T_level"], 50));
            model.description = (PageValidate.InputText(request["T_description"], 50));
            model.license = (PageValidate.InputText(request["T_license"], 50));

            string BoarId = PageValidate.InputText(request["bid"], 50);
            if (category.Exists(BoarId))
            //if (PageValidate.checkID(BoarId))
            {
                 model.BoarId = BoarId; 
           
                category.Update(model); 
            }

            else
            {
                
                model.id = Guid.NewGuid().ToString();
                model.BoarId = BoarId;
                //model.cr = emp_name;
                //model.create_time = DateTime.Now;
                category.Add(model);

            }
            return XhdResult.Success().ToString();
        }

        public string grid()
        {
            int PageIndex = int.Parse(request["page"] == null ? "1" : request["page"]);
            int PageSize = int.Parse(request["pagesize"] == null ? "30" : request["pagesize"]);
            string sortname = request["sortname"];
            string sortorder = request["sortorder"];

            if (string.IsNullOrEmpty(sortname))
                sortname = " id";
            if (string.IsNullOrEmpty(sortorder))
                sortorder = "asc";

            string sorttext = " " + sortname + " " + sortorder;

            string Total;
            string serchtxt = $" 1=1 ";
            //if (!string.IsNullOrEmpty(request["categoryid"]))
            //    serchtxt += $" and category_id='{PageValidate.InputText(request["categoryid"], 50)}'";

            //if (!string.IsNullOrEmpty(request["stext"]))
            //    serchtxt += $" and product_name like N'%{ PageValidate.InputText(request["stext"], 255) }%'";

            //权限
            DataSet ds = category.GetList(PageSize, PageIndex, serchtxt, sorttext, out Total);

            string dt = GetGridJSON.DataTableToJSON1(ds.Tables[0], Total);
            return (dt);

        }

        public string tree()
        {
            DataSet ds = category.GetList($"1=1");
            var str = new StringBuilder();
            str.Append("[");
            str.Append(GetTreeString("root", ds.Tables[0]));
            str.Replace(",", "", str.Length - 1, 1);
            str.Append("]");
            return str.ToString();
        }

        public string combo(string id)
        {
            DataSet ds = category.GetList($"1=1 and ProductId='" +id+"'");
            var str = new StringBuilder();
            str.Append("[");
            //str.Append("{\"id\":\"root\",\"text\":\"无\",\"d_icon\":\"\"},");
            str.Append(GetTreeString(id, ds.Tables[0]));
            str.Replace(",", "", str.Length - 1, 1);
            str.Append("]");
            return str.ToString();
        }

        public string form(string id)
        {
            if (!PageValidate.checkID(id)) return "{}";
            id = PageValidate.InputText(id, 50);
            DataSet ds = category.GetList($" BoarId='{id}' ");
            return DataToJson.DataToJSON(ds);

        }

        //del
        public string del(string id)
        {
            if (!PageValidate.checkID(id)) return "false";
            id = PageValidate.InputText(id, 50);
          

            bool isdel = category.Delete(id);
            if (!isdel) return XhdResult.Error("系统错误！").ToString();

            //日志
            string EventType = "产品类别删除";

            string UserID = emp_id;
            string UserName = emp_name;
            string IPStreet = request.UserHostAddress;
            string EventID = id;
            string EventTitle = "";

            Syslog.Add_log(UserID, UserName, IPStreet, EventTitle, EventType, EventID, null);

            return XhdResult.Success().ToString();

        }


        private static string GetTreeString(string Id, DataTable table)
        {
            DataRow[] rows = table.Select($"ProductId='{Id}'");

            if (rows.Length == 0) return string.Empty;
            ;
            var str = new StringBuilder();

            foreach (DataRow row in rows)
            {
                str.Append("{\"id\":\"" + (string)row["id"] + "\",\"text\":\"" + (string)row["SeriesName"] + "\",\"d_icon\":\"../../" + (string)row["def1"] + "\"");

                //if (GetTreeString((string)row["id"], table).Length > 0)
                //{
                //    str.Append(",\"children\":[");
                //    str.Append(GetTreeString((string)row["id"], table));
                //    str.Append("]},");
                //}
                //else
                {
                    str.Append("},");
                }
            }
            return str[str.Length - 1] == ',' ? str.ToString(0, str.Length - 1) : str.ToString();
        }
    }
}
