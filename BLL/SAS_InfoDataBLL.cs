﻿using System;
using System.Data;
using System.Collections.Generic;
using XHD.Common;
using XHD.Model;
using XHD.DAL;

namespace XHD.BLL
{
    public class SAS_InfoDataBll
    {
    private readonly SAS_InfoDataDao dal=new SAS_InfoDataDao();
		public SAS_InfoDataBll()
		{}
		
     
        
        #region 方法
        ///<summary>
        ///是否存在该记录
        ///</summary>
        public bool Exists(string casereference)
        {
            return dal.Exists(casereference);
        }
        
        ///<summary>
        ///根据条件查询是否存在记录
        ///</summary>
        public bool ExistsByWhere(string strWhere)
        {
            return dal.ExistsByWhere(strWhere);
        }
    
        ///<summary>
        ///增加一条数据
        ///</summary>
        public bool Insert( SAS_InfoDataEntity Model)
        {
            return dal.Insert(Model);
        }
        
        ///<summary>
        ///更新一条数据
        ///</summary>
        public bool Update(SAS_InfoDataEntity Model)
        {
            return dal.Update(Model);
        }
        
        ///<summary>
        ///删除一条数据
        ///</summary>
        public bool Delete(string casereference)
        {
             return dal.Delete(casereference);
        }
          ///<summary>
        ///删除一条数据
        ///</summary>
        public bool DeleteList(string ids)
        {
             return dal.DeleteList(ids);
        }
        ///<summary>
        ///根据条件删除记录
        ///</summary>
        public bool DeleteByWhere(string strWhere)
        {
            return dal.DeleteByWhere(strWhere);
        }
        
      
        ///<summary>
        ///获取数据列表
        ///</summary>
        public DataSet GetList(string strWhere)
        {
             return dal.GetList(strWhere);
        }
        
        ///<summary>
        ///获取数据列表(List)
        ///</summary>
        public List<SAS_InfoDataEntity> GetModelList(string strWhere)
        {
            DataSet ds = GetList(strWhere);
            return ds.Tables[0].ToList<SAS_InfoDataEntity>();
        }
        
        ///<summary>
        ///获取前几行数据
        ///</summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex,out int count)
        {
            return dal.GetListByPage(strWhere,orderby, startIndex, endIndex,out count);
        }
        
        //<summary>
        ///获取前几行数据(List)
        ///</summary>
        public List<SAS_InfoDataEntity> GetModelListByPage(string strWhere, string orderby, int startIndex, int endIndex,out int count)
        {
           
           DataSet ds = GetListByPage(strWhere,orderby, startIndex, endIndex,out count);
           return ds.Tables[0].ToList<SAS_InfoDataEntity>();
        }
        #endregion
    }
}


