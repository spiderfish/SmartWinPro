﻿using System;
using System.Data;
using System.Collections.Generic;
using XHD.Common;
using XHD.Model;
namespace XHD.BLL
{
	/// <summary>
	/// boar_collection
	/// </summary>
	public partial class boar_collection
	{
		private readonly XHD.DAL.boar_collection dal=new XHD.DAL.boar_collection();
		public boar_collection()
		{}
		#region  BasicMethod
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string id)
		{
			return dal.Exists(id);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(XHD.Model.boar_collection model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XHD.Model.boar_collection model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string id)
		{
			
			return dal.Delete(id);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string idlist )
		{
			return dal.DeleteList(idlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XHD.Model.boar_collection GetModel(string id)
		{
			
			return dal.GetModel(id);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public XHD.Model.boar_collection GetModelByCache(string id)
		{
			
			string CacheKey = "boar_collectionModel-" + id;
			object objModel = XHD.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(id);
					if (objModel != null)
					{
						int ModelCache = XHD.Common.ConfigHelper.GetConfigInt("ModelCache");
						XHD.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (XHD.Model.boar_collection)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<XHD.Model.boar_collection> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<XHD.Model.boar_collection> DataTableToList(DataTable dt)
		{
			List<XHD.Model.boar_collection> modelList = new List<XHD.Model.boar_collection>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				XHD.Model.boar_collection model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = dal.DataRowToModel(dt.Rows[n]);
					if (model != null)
					{
						modelList.Add(model);
					}
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //return dal.GetList(PageSize,PageIndex,strWhere);
        //}

        #endregion  BasicMethod
        #region  ExtensionMethod
        /// <summary>
        /// /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, out string Total)
        {
            return dal.GetList(PageSize, PageIndex, strWhere, filedOrder, out Total);
        }

        public DataSet GetListCollection(string strWhere)
        {
            return dal.GetListCollection(strWhere);
        }

        public DataSet GetListBoars(string strWhere)
        {
            return dal.GetListBoars(strWhere);
        }

        /// <summary>
        /// 更新订单明细状态
        /// </summary>
        public bool UpdateOrderStatus(string Orderid,string ordermxid, string dstatus)
        {
            return dal.UpdateOrderStatus(Orderid,ordermxid,dstatus);
        }
        /// <summary>
        /// 增加一次打印记录
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool AddPrintTimes(string id)
        {
            return dal.AddPrintTimes(id);
        }
        /// <summary>
        /// 更新状态
        /// </summary>
        /// <param name="id"></param>
        /// <param name="IsStatus"></param>
        /// <returns></returns>
        public bool UpdateBoar_CollectionStatus(string id, string IsStatus)
        {
            return dal.UpdateBoar_CollectionStatus(id,IsStatus);
        }
        /// <summary>
        /// /更新重量
        /// </summary>
        /// <param name="id"></param>
        /// <param name="weights"></param>
        /// <returns></returns>
        public bool UpdateWeights(string id, decimal weights)
        {
            return dal.UpdateWeights(id,weights);
        }

        public bool UpdateParas(string type,string id, string weights , string density , string shape , string VIT ,
            string Out_quantity , string Out_Warehouse , string Out_Warehouse_kw , string Out_logistics  
            , string In_quantity , string In_Warehouse , string In_Warehouse_kw 
            , string dilution_quantity , string dilution_result , string dilution_weights )
        {
            return dal.UpdateParas(type,id,   weights,   density,   shape,   VIT 
                 ,  Out_quantity,  Out_Warehouse,  Out_Warehouse_kw, Out_logistics
            ,  In_quantity,  In_Warehouse,  In_Warehouse_kw
            ,  dilution_quantity,  dilution_result,  dilution_weights);
        }

        public DataSet GetListAuto(string  SaleOrderID,string bt,string et)
        {
            return dal.GetListAuto(SaleOrderID,bt,et);
        }
        public string CreateAutoDoc(string orderid)
        {
            return dal.CreateAutoDoc(orderid);
        }
        public DataSet GetListLog(string orderid)
        {
            return dal.GetListLog(orderid);
        }
        public DataSet GetBarList(string barcode)
        {
            return dal.GetBarList(barcode);
        }

		public DataSet GetSAS_InfoDataList(string strWhere)
		{
			return dal.GetSAS_InfoDataList(strWhere);
		}

		public DataSet GetSAS_InfoImgDataList(string strWhere)
		{
			return dal.GetSAS_InfoImgDataList(strWhere);
		}

		#endregion  ExtensionMethod
	}
    }

