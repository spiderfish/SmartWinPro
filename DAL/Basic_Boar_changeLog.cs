﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using XHD.DBUtility;//Please add references
namespace XHD.DAL
{
	/// <summary>
	/// 数据访问类:Basic_Boar_changeLog
	/// </summary>
	public partial class Basic_Boar_changeLog
	{
		public Basic_Boar_changeLog()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Basic_Boar_changeLog");
			strSql.Append(" where id=@id ");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.VarChar,50)			};
			parameters[0].Value = id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(XHD.Model.Basic_Boar_changeLog model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Basic_Boar_changeLog(");
			strSql.Append("id,BoarId,InDormId,InCorralId,InTime,OutDormId,OutCorralId,OutTime,accpetPerson,remarks)");
			strSql.Append(" values (");
			strSql.Append("@id,@BoarId,@InDormId,@InCorralId,@InTime,@OutDormId,@OutCorralId,@OutTime,@accpetPerson,@remarks)");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.VarChar,50),
					new SqlParameter("@BoarId", SqlDbType.VarChar,50),
					new SqlParameter("@InDormId", SqlDbType.VarChar,50),
					new SqlParameter("@InCorralId", SqlDbType.VarChar,50),
					new SqlParameter("@InTime", SqlDbType.DateTime),
					new SqlParameter("@OutDormId", SqlDbType.VarChar,50),
					new SqlParameter("@OutCorralId", SqlDbType.VarChar,50),
					new SqlParameter("@OutTime", SqlDbType.DateTime),
					new SqlParameter("@accpetPerson", SqlDbType.VarChar,50),
					new SqlParameter("@remarks", SqlDbType.VarChar,50)};
			parameters[0].Value = model.id;
			parameters[1].Value = model.BoarId;
			parameters[2].Value = model.InDormId;
			parameters[3].Value = model.InCorralId;
			parameters[4].Value = model.InTime;
			parameters[5].Value = model.OutDormId;
			parameters[6].Value = model.OutCorralId;
			parameters[7].Value = model.OutTime;
			parameters[8].Value = model.accpetPerson;
			parameters[9].Value = model.remarks;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XHD.Model.Basic_Boar_changeLog model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Basic_Boar_changeLog set "); 
			strSql.Append("InDormId=@InDormId,");
			strSql.Append("InCorralId=@InCorralId,");
			strSql.Append("InTime=@InTime,");
			strSql.Append("OutDormId=@OutDormId,");
			strSql.Append("OutCorralId=@OutCorralId,");
			strSql.Append("OutTime=@OutTime,");
			strSql.Append("accpetPerson=@accpetPerson,");
			strSql.Append("remarks=@remarks");
			strSql.Append(" where BoarId=@BoarId ");
			SqlParameter[] parameters = {
					new SqlParameter("@InDormId", SqlDbType.VarChar,50),
					new SqlParameter("@InCorralId", SqlDbType.VarChar,50),
					new SqlParameter("@InTime", SqlDbType.DateTime),
					new SqlParameter("@OutDormId", SqlDbType.VarChar,50),
					new SqlParameter("@OutCorralId", SqlDbType.VarChar,50),
					new SqlParameter("@OutTime", SqlDbType.DateTime),
					new SqlParameter("@accpetPerson", SqlDbType.VarChar,50),
					new SqlParameter("@remarks", SqlDbType.VarChar,50), 
					new SqlParameter("@BoarId", SqlDbType.VarChar,50)};
	 
			parameters[0].Value = model.InDormId;
			parameters[1].Value = model.InCorralId;
			parameters[2].Value = model.InTime;
			parameters[3].Value = model.OutDormId;
			parameters[4].Value = model.OutCorralId;
			parameters[5].Value = model.OutTime;
			parameters[6].Value = model.accpetPerson;
			parameters[7].Value = model.remarks;
			parameters[8].Value = model.BoarId;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Basic_Boar_changeLog ");
			strSql.Append(" where id=@id ");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.VarChar,50)			};
			parameters[0].Value = id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Basic_Boar_changeLog ");
			strSql.Append(" where id in ("+idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XHD.Model.Basic_Boar_changeLog GetModel(string id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 id,BoarId,InDormId,InCorralId,InTime,OutDormId,OutCorralId,OutTime,accpetPerson,remarks from Basic_Boar_changeLog ");
			strSql.Append(" where id=@id ");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.VarChar,50)			};
			parameters[0].Value = id;

			XHD.Model.Basic_Boar_changeLog model=new XHD.Model.Basic_Boar_changeLog();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XHD.Model.Basic_Boar_changeLog DataRowToModel(DataRow row)
		{
			XHD.Model.Basic_Boar_changeLog model=new XHD.Model.Basic_Boar_changeLog();
			if (row != null)
			{
				if(row["id"]!=null)
				{
					model.id=row["id"].ToString();
				}
				if(row["BoarId"]!=null)
				{
					model.BoarId=row["BoarId"].ToString();
				}
				if(row["InDormId"]!=null)
				{
					model.InDormId=row["InDormId"].ToString();
				}
				if(row["InCorralId"]!=null)
				{
					model.InCorralId=row["InCorralId"].ToString();
				}
				if(row["InTime"]!=null && row["InTime"].ToString()!="")
				{
					model.InTime=DateTime.Parse(row["InTime"].ToString());
				}
				if(row["OutDormId"]!=null)
				{
					model.OutDormId=row["OutDormId"].ToString();
				}
				if(row["OutCorralId"]!=null)
				{
					model.OutCorralId=row["OutCorralId"].ToString();
				}
				if(row["OutTime"]!=null && row["OutTime"].ToString()!="")
				{
					model.OutTime=DateTime.Parse(row["OutTime"].ToString());
				}
				if(row["accpetPerson"]!=null)
				{
					model.accpetPerson=row["accpetPerson"].ToString();
				}
				if(row["remarks"]!=null)
				{
					model.remarks=row["remarks"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select id,BoarId,InDormId,InCorralId,InTime,OutDormId,OutCorralId,OutTime,accpetPerson,remarks ");
			strSql.Append(" FROM Basic_Boar_changeLog ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" id,BoarId,InDormId,InCorralId,InTime,OutDormId,OutCorralId,OutTime,accpetPerson,remarks ");
			strSql.Append(" FROM Basic_Boar_changeLog ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Basic_Boar_changeLog ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.id desc");
			}
			strSql.Append(")AS Row, T.*  from Basic_Boar_changeLog T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Basic_Boar_changeLog";
			parameters[1].Value = "id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod
		public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, out string Total)
		{
			StringBuilder strSql_grid = new StringBuilder();
			StringBuilder strSql_total = new StringBuilder();
			StringBuilder sb = new StringBuilder();
			sb.AppendLine(" SELECT A.*,  ");
			sb.AppendLine(" B.dormName AS InBorm,C.corralName AS InCorral,D.dormName AS OutBorm,E.corralName AS OutCorral  ");
			sb.AppendLine("  FROM  dbo.Basic_Boar_changeLog A  ");
			sb.AppendLine(" LEFT JOIN  dbo.Basic_dorm B ON	A.InDormId =B.id  ");
			sb.AppendLine("  LEFT JOIN  dbo.Basic_corral C ON	A.InCorralId =C.id  ");
			sb.AppendLine("  LEFT JOIN  dbo.Basic_dorm D ON	A.OutDormId =D.id  ");
			sb.AppendLine("  LEFT JOIN  dbo.Basic_corral E ON	A.OutCorralId =E.id  ");

			strSql_total.Append(" SELECT COUNT(id) FROM ("+sb.ToString()+") AA ");
			strSql_grid.Append("SELECT ");
			strSql_grid.Append("      n,* ");
			strSql_grid.Append(" FROM ( SELECT *, ROW_NUMBER() OVER( Order by " + filedOrder + " ) AS n from  (" + sb.ToString() + ") AA");
			if (strWhere.Trim() != "")
			{
				strSql_grid.Append(" WHERE " + strWhere);
				strSql_total.Append(" WHERE " + strWhere);
			}
			strSql_grid.Append("  ) as w1  ");
			strSql_grid.Append("WHERE n BETWEEN " + (PageSize * (PageIndex - 1) + 1) + " AND " + PageSize * PageIndex);
			strSql_grid.Append(" ORDER BY " + filedOrder);
			Total = DbHelperSQL.Query(strSql_total.ToString()).Tables[0].Rows[0][0].ToString();
			return DbHelperSQL.Query(strSql_grid.ToString());
		}
		#endregion  ExtensionMethod
	}
}

