﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using XHD.DBUtility;//Please add references
namespace XHD.DAL
{
	/// <summary>
	/// 数据访问类:SmallAPP_Order_Head
	/// </summary>
	public partial class SmallAPP_Order_Head
	{
		public SmallAPP_Order_Head()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string OrderId)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from SmallAPP_Order_Head");
			strSql.Append(" where OrderId=@OrderId ");
			SqlParameter[] parameters = {
					new SqlParameter("@OrderId", SqlDbType.VarChar,50)			};
			parameters[0].Value = OrderId;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(XHD.Model.SmallAPP_Order_Head model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into SmallAPP_Order_Head(");
			strSql.Append("OrderId,CreateTime,CreateName,WX_ID,CusId,orderStatus,orderStatusName,remarks,payRemarks,addressId)");
			strSql.Append(" values (");
			strSql.Append("@OrderId,@CreateTime,@CreateName,@WX_ID,@CusId,@orderStatus,@orderStatusName,@remarks,@payRemarks,@addressId)");
			SqlParameter[] parameters = {
					new SqlParameter("@OrderId", SqlDbType.VarChar,50),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@CreateName", SqlDbType.VarChar,20),
					new SqlParameter("@WX_ID", SqlDbType.VarChar,50),
					new SqlParameter("@CusId", SqlDbType.VarChar,50),
					new SqlParameter("@orderStatus", SqlDbType.Char,1),
					new SqlParameter("@orderStatusName", SqlDbType.VarChar,50),
				new SqlParameter("@remarks", SqlDbType.VarChar,50),
				new SqlParameter("@payRemarks", SqlDbType.VarChar,50),
			new SqlParameter("@addressId", SqlDbType.VarChar,50)};
			parameters[0].Value = model.OrderId;
			parameters[1].Value = model.CreateTime;
			parameters[2].Value = model.CreateName;
			parameters[3].Value = model.WX_ID;
			parameters[4].Value = model.CusId;
			parameters[5].Value = model.orderStatus;
			parameters[6].Value = model.orderStatusName;
			parameters[7].Value = model.Remarks;
			parameters[8].Value = model.PayRemarks;
			parameters[9].Value = model.AddressId;
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XHD.Model.SmallAPP_Order_Head model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update SmallAPP_Order_Head set ");
			strSql.Append("CreateTime=@CreateTime,");
			strSql.Append("CreateName=@CreateName,");
			strSql.Append("WX_ID=@WX_ID,");
			strSql.Append("CusId=@CusId,");
			strSql.Append("orderStatus=@orderStatus,");
			strSql.Append("Remarks=@Remarks,");
			strSql.Append("PayRemarks=@PayRemarks,");
			strSql.Append("AddressId=@AddressId,");
			strSql.Append("orderStatusName=@orderStatusName");
			strSql.Append(" where OrderId=@OrderId ");
			SqlParameter[] parameters = {
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@CreateName", SqlDbType.VarChar,20),
					new SqlParameter("@WX_ID", SqlDbType.VarChar,50),
					new SqlParameter("@CusId", SqlDbType.VarChar,50),
					new SqlParameter("@orderStatus", SqlDbType.Char,1),
					new SqlParameter("@Remarks", SqlDbType.VarChar,50),
					new SqlParameter("@PayRemarks", SqlDbType.VarChar,50),
					new SqlParameter("@AddressId", SqlDbType.VarChar,50),
					new SqlParameter("@orderStatusName", SqlDbType.VarChar,50),
					new SqlParameter("@OrderId", SqlDbType.VarChar,50)};
			parameters[0].Value = model.CreateTime;
			parameters[1].Value = model.CreateName;
			parameters[2].Value = model.WX_ID;
			parameters[3].Value = model.CusId;
			parameters[4].Value = model.orderStatus;
			parameters[5].Value = model.Remarks;
			parameters[6].Value = model.PayRemarks;
			parameters[7].Value = model.AddressId;
			parameters[8].Value = model.orderStatusName;
			parameters[9].Value = model.OrderId;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string OrderId)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from SmallAPP_Order_Head ");
			strSql.Append(" where OrderId=@OrderId ");
			SqlParameter[] parameters = {
					new SqlParameter("@OrderId", SqlDbType.VarChar,50)			};
			parameters[0].Value = OrderId;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string OrderIdlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from SmallAPP_Order_Head ");
			strSql.Append(" where OrderId in ("+OrderIdlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XHD.Model.SmallAPP_Order_Head GetModel(string OrderId)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 OrderId,CreateTime,CreateName,WX_ID,CusId,orderStatus,orderStatusName,remarks,payRemarks,AddressId from SmallAPP_Order_Head ");
			strSql.Append(" where OrderId=@OrderId ");
			SqlParameter[] parameters = {
					new SqlParameter("@OrderId", SqlDbType.VarChar,50)			};
			parameters[0].Value = OrderId;

			XHD.Model.SmallAPP_Order_Head model=new XHD.Model.SmallAPP_Order_Head();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XHD.Model.SmallAPP_Order_Head DataRowToModel(DataRow row)
		{
			XHD.Model.SmallAPP_Order_Head model=new XHD.Model.SmallAPP_Order_Head();
			if (row != null)
			{
				if(row["OrderId"]!=null)
				{
					model.OrderId=row["OrderId"].ToString();
				}
				if(row["CreateTime"]!=null && row["CreateTime"].ToString()!="")
				{
					model.CreateTime=DateTime.Parse(row["CreateTime"].ToString());
				}
				if(row["CreateName"]!=null)
				{
					model.CreateName=row["CreateName"].ToString();
				}
				if(row["WX_ID"]!=null)
				{
					model.WX_ID=row["WX_ID"].ToString();
				}
				if(row["CusId"]!=null)
				{
					model.CusId=row["CusId"].ToString();
				}
				if(row["orderStatus"]!=null)
				{
					model.orderStatus=row["orderStatus"].ToString();
				}
				if(row["orderStatusName"]!=null)
				{
					model.orderStatusName=row["orderStatusName"].ToString();
				}
				if (row["remarks"] != null)
				{
					model.Remarks = row["remarks"].ToString();
				}
				if (row["payRemarks"] != null)
				{
					model.PayRemarks = row["payRemarks"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select OrderId,CreateTime,CreateName,WX_ID,CusId,orderStatus,orderStatusName,remarks,payRemarks,AddressId ");
			strSql.Append(" FROM SmallAPP_Order_Head ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" OrderId,CreateTime,CreateName,WX_ID,CusId,orderStatus,orderStatusName,remarks,payRemarks,AddressId ");
			strSql.Append(" FROM SmallAPP_Order_Head ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM SmallAPP_Order_Head ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.OrderId desc");
			}
			strSql.Append(")AS Row, T.*  from SmallAPP_Order_Head T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "SmallAPP_Order_Head";
			parameters[1].Value = "OrderId";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

