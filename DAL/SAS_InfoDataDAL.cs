﻿using System;
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using XHD.DBUtility;//Please add references
using XHD.Model;

namespace XHD.DAL
{
    public  partial class SAS_InfoDataDao 
    {
        ///<summary>
        ///是否存在该记录
        ///</summary>
        public bool Exists(string casereference)
        {
            StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from SAS_InfoData ");
            strSql.Append(" where casereference = @casereference ");
		    SqlParameter[] parameters = {
            new SqlParameter("@casereference", SqlDbType.VarChar,50)
			
		    };
            parameters[0].Value = casereference;
			
			return DbHelperSQL.Exists(strSql.ToString(),parameters);
        }
        
        ///<summary>
        ///根据条件查询是否存在记录
        ///</summary>
        public bool ExistsByWhere(string strWhere)
        {
            StringBuilder strSql=new StringBuilder();	
            strSql.Append("select count(1) from SAS_InfoData ");
            strSql.Append(" where " + strWhere);
		    
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
        }
    
        ///<summary>
        ///增加一条数据
        ///</summary>
        public bool Insert(SAS_InfoDataEntity Model)
        {     
            StringBuilder strSql=new StringBuilder();	
            strSql.Append("insert into  SAS_InfoData(");
            strSql.Append("casereference ,sperm ,breed ,strain ,donorname ,subject ,additionalnumber ,barn ,lastcollection ,state ,geneticline ,batchnumber ,spermnumber ,initialdilutorvolume ,analysisdilution ,collector ,operator ,dosisvolume ,usefulspermperdosis ,dilutor ,ejaculatedvolume ,sementemp ,spermsdsispayunits ,produceddosis ,diluentvolume ,dilutionratio ,totalspermsperdosis ,totalfinalvolume ,date ,totnumber ,totpercen ,totmillions ,totmml ,motnumber ,motpercen ,motmillions ,motmml ,nornumber ,norpercen ,normillions ,normml ,mnonumber ,mnopercen ,mnomillions ,mnomml ,usenumber ,usepercen ,usemillions ,usemml ,pronumber ,propercen ,promillions ,promml ,rapnumber ,rappercen ,rapmillions ,rapmml ,slonumber ,slopercen ,slomillions ,slomml ,cirnumber ,cirpercen ,cirmillions ,cirmml ,locnumber ,locpercen ,locmillions ,locmml ,immnumber ,immpercen ,immmillions ,immmml ,mornumber ,morpercen ,mormillions ,mormml ,bennumber ,benpercen ,benmillions ,benmml ,coinumber ,coipercen ,coimillions ,coimml ,dmrnumber ,dmrpercen ,dmrmillions ,dmrmml ,disnumber ,dispercen ,dismillions ,dismml, malnumber,malpercen,prognumber,progpercen ,concentration)");
            strSql.Append(" values (");
            strSql.Append("@casereference ,@sperm ,@breed ,@strain ,@donorname ,@subject ,@additionalnumber ,@barn ,@lastcollection ,@state ,@geneticline ,@batchnumber ,@spermnumber ,@initialdilutorvolume ,@analysisdilution ,@collector ,@operator ,@dosisvolume ,@usefulspermperdosis ,@dilutor ,@ejaculatedvolume ,@sementemp ,@spermsdsispayunits ,@produceddosis ,@diluentvolume ,@dilutionratio ,@totalspermsperdosis ,@totalfinalvolume ,@date ,@totnumber ,@totpercen ,@totmillions ,@totmml ,@motnumber ,@motpercen ,@motmillions ,@motmml ,@nornumber ,@norpercen ,@normillions ,@normml ,@mnonumber ,@mnopercen ,@mnomillions ,@mnomml ,@usenumber ,@usepercen ,@usemillions ,@usemml ,@pronumber ,@propercen ,@promillions ,@promml ,@rapnumber ,@rappercen ,@rapmillions ,@rapmml ,@slonumber ,@slopercen ,@slomillions ,@slomml ,@cirnumber ,@cirpercen ,@cirmillions ,@cirmml ,@locnumber ,@locpercen ,@locmillions ,@locmml ,@immnumber ,@immpercen ,@immmillions ,@immmml ,@mornumber ,@morpercen ,@mormillions ,@mormml ,@bennumber ,@benpercen ,@benmillions ,@benmml ,@coinumber ,@coipercen ,@coimillions ,@coimml ,@dmrnumber ,@dmrpercen ,@dmrmillions ,@dmrmml ,@disnumber ,@dispercen ,@dismillions ,@dismml, @malnumber,@malpercen,@prognumber,@progpercen ,@concentration)");
		    SqlParameter[] parameters = {
            new SqlParameter("@casereference", SqlDbType.VarChar,50)
			,new SqlParameter("@sperm", SqlDbType.VarChar,50)
			,new SqlParameter("@breed", SqlDbType.VarChar,50)
			,new SqlParameter("@strain", SqlDbType.VarChar,50)
			,new SqlParameter("@donorname", SqlDbType.VarChar,50)
			,new SqlParameter("@subject", SqlDbType.VarChar,50)
			,new SqlParameter("@additionalnumber", SqlDbType.VarChar,50)
			,new SqlParameter("@barn", SqlDbType.VarChar,50)
			,new SqlParameter("@lastcollection", SqlDbType.VarChar,50)
			,new SqlParameter("@state", SqlDbType.VarChar,50)
			,new SqlParameter("@geneticline", SqlDbType.VarChar,50)
			,new SqlParameter("@batchnumber", SqlDbType.VarChar,50)
			,new SqlParameter("@spermnumber", SqlDbType.VarChar,50)
			,new SqlParameter("@initialdilutorvolume", SqlDbType.Int,4)
			,new SqlParameter("@analysisdilution", SqlDbType.Int,4)
			,new SqlParameter("@collector", SqlDbType.VarChar,50)
			,new SqlParameter("@operator", SqlDbType.VarChar,50)
			,new SqlParameter("@dosisvolume", SqlDbType.Int,4)
			,new SqlParameter("@usefulspermperdosis", SqlDbType.Int,4)
			,new SqlParameter("@dilutor", SqlDbType.VarChar,50)
			,new SqlParameter("@ejaculatedvolume", SqlDbType.Int,4)
			,new SqlParameter("@sementemp", SqlDbType.VarChar,50)
			,new SqlParameter("@spermsdsispayunits", SqlDbType.VarChar,50)
			,new SqlParameter("@produceddosis", SqlDbType.Int,4)
			,new SqlParameter("@diluentvolume", SqlDbType.Int,4)
			,new SqlParameter("@dilutionratio", SqlDbType.Int,4)
			,new SqlParameter("@totalspermsperdosis", SqlDbType.Int,4)
			,new SqlParameter("@totalfinalvolume", SqlDbType.Int,4)
			,new SqlParameter("@date", SqlDbType.VarChar,50)
			,new SqlParameter("@totnumber", SqlDbType.Int,4)
			,new SqlParameter("@totpercen", SqlDbType.Float,8)
			,new SqlParameter("@totmillions", SqlDbType.Int,4)
			,new SqlParameter("@totmml", SqlDbType.Float,8)
			,new SqlParameter("@motnumber", SqlDbType.Int,4)
			,new SqlParameter("@motpercen", SqlDbType.Float,8)
			,new SqlParameter("@motmillions", SqlDbType.Int,4)
			,new SqlParameter("@motmml", SqlDbType.Float,8)
			,new SqlParameter("@nornumber", SqlDbType.Int,4)
			,new SqlParameter("@norpercen", SqlDbType.Float,8)
			,new SqlParameter("@normillions", SqlDbType.Int,4)
			,new SqlParameter("@normml", SqlDbType.Int,4)
			,new SqlParameter("@mnonumber", SqlDbType.Int,4)
			,new SqlParameter("@mnopercen", SqlDbType.Float,8)
			,new SqlParameter("@mnomillions", SqlDbType.Int,4)
			,new SqlParameter("@mnomml", SqlDbType.Float,8)
			,new SqlParameter("@usenumber", SqlDbType.Int,4)
			,new SqlParameter("@usepercen", SqlDbType.Float,8)
			,new SqlParameter("@usemillions", SqlDbType.Int,4)
			,new SqlParameter("@usemml", SqlDbType.Float,8)
			,new SqlParameter("@pronumber", SqlDbType.Int,4)
			,new SqlParameter("@propercen", SqlDbType.Float,8)
			,new SqlParameter("@promillions", SqlDbType.Int,4)
			,new SqlParameter("@promml", SqlDbType.Float,8)
			,new SqlParameter("@rapnumber", SqlDbType.Int,4)
			,new SqlParameter("@rappercen", SqlDbType.Float,8)
			,new SqlParameter("@rapmillions", SqlDbType.Int,4)
			,new SqlParameter("@rapmml", SqlDbType.Float,8)
			,new SqlParameter("@slonumber", SqlDbType.Int,4)
			,new SqlParameter("@slopercen", SqlDbType.Float,8)
			,new SqlParameter("@slomillions", SqlDbType.Int,4)
			,new SqlParameter("@slomml", SqlDbType.Float,8)
			,new SqlParameter("@cirnumber", SqlDbType.Int,4)
			,new SqlParameter("@cirpercen", SqlDbType.Float,8)
			,new SqlParameter("@cirmillions", SqlDbType.Int,4)
			,new SqlParameter("@cirmml", SqlDbType.Float,8)
			,new SqlParameter("@locnumber", SqlDbType.Int,4)
			,new SqlParameter("@locpercen", SqlDbType.Float,8)
			,new SqlParameter("@locmillions", SqlDbType.Int,4)
			,new SqlParameter("@locmml", SqlDbType.Float,8)
			,new SqlParameter("@immnumber", SqlDbType.Int,4)
			,new SqlParameter("@immpercen", SqlDbType.Float,8)
			,new SqlParameter("@immmillions", SqlDbType.Int,4)
			,new SqlParameter("@immmml", SqlDbType.Float,8)
			,new SqlParameter("@mornumber", SqlDbType.Int,4)
			,new SqlParameter("@morpercen", SqlDbType.Float,8)
			,new SqlParameter("@mormillions", SqlDbType.Int,4)
			,new SqlParameter("@mormml", SqlDbType.Float,8)
			,new SqlParameter("@bennumber", SqlDbType.Int,4)
			,new SqlParameter("@benpercen", SqlDbType.Float,8)
			,new SqlParameter("@benmillions", SqlDbType.Int,4)
			,new SqlParameter("@benmml", SqlDbType.Float,8)
			,new SqlParameter("@coinumber", SqlDbType.Int,4)
			,new SqlParameter("@coipercen", SqlDbType.Float,8)
			,new SqlParameter("@coimillions", SqlDbType.Int,4)
			,new SqlParameter("@coimml", SqlDbType.Float,8)
			,new SqlParameter("@dmrnumber", SqlDbType.Int,4)
			,new SqlParameter("@dmrpercen", SqlDbType.Float,8)
			,new SqlParameter("@dmrmillions", SqlDbType.Int,4)
			,new SqlParameter("@dmrmml", SqlDbType.Float,8)
			,new SqlParameter("@disnumber", SqlDbType.Int,4)
			,new SqlParameter("@dispercen", SqlDbType.Float,8)
			,new SqlParameter("@dismillions", SqlDbType.Int,4)
			,new SqlParameter("@dismml", SqlDbType.Float,8)
             ,new SqlParameter("@malnumber", SqlDbType.Int,4)
            ,new SqlParameter("@malpercen", SqlDbType.Float,8)
            ,new SqlParameter("@prognumber", SqlDbType.Int,4)
            ,new SqlParameter("@progpercen", SqlDbType.Float,8)
            ,new SqlParameter("@concentration", SqlDbType.Int,4)
			
		    };
            parameters[0].Value = Model.casereference;
			parameters[1].Value = Model.sperm;
			parameters[2].Value = Model.breed;
			parameters[3].Value = Model.strain;
			parameters[4].Value = Model.donorname;
			parameters[5].Value = Model.subject;
			parameters[6].Value = Model.additionalnumber;
			parameters[7].Value = Model.barn;
			parameters[8].Value = Model.lastcollection;
			parameters[9].Value = Model.state;
			parameters[10].Value = Model.geneticline;
			parameters[11].Value = Model.batchnumber;
			parameters[12].Value = Model.spermnumber;
			parameters[13].Value = Model.initialdilutorvolume;
			parameters[14].Value = Model.analysisdilution;
			parameters[15].Value = Model.collector;
			parameters[16].Value = Model.Operator;
			parameters[17].Value = Model.dosisvolume;
			parameters[18].Value = Model.usefulspermperdosis;
			parameters[19].Value = Model.dilutor;
			parameters[20].Value = Model.ejaculatedvolume;
			parameters[21].Value = Model.sementemp;
			parameters[22].Value = Model.spermsdsispayunits;
			parameters[23].Value = Model.produceddosis;
			parameters[24].Value = Model.diluentvolume;
			parameters[25].Value = Model.dilutionratio;
			parameters[26].Value = Model.totalspermsperdosis;
			parameters[27].Value = Model.totalfinalvolume;
			parameters[28].Value = Model.date;
			parameters[29].Value = Model.totnumber;
			parameters[30].Value = Model.totpercen;
			parameters[31].Value = Model.totmillions;
			parameters[32].Value = Model.totmml;
			parameters[33].Value = Model.motnumber;
			parameters[34].Value = Model.motpercen;
			parameters[35].Value = Model.motmillions;
			parameters[36].Value = Model.motmml;
			parameters[37].Value = Model.nornumber;
			parameters[38].Value = Model.norpercen;
			parameters[39].Value = Model.normillions;
			parameters[40].Value = Model.normml;
			parameters[41].Value = Model.mnonumber;
			parameters[42].Value = Model.mnopercen;
			parameters[43].Value = Model.mnomillions;
			parameters[44].Value = Model.mnomml;
			parameters[45].Value = Model.usenumber;
			parameters[46].Value = Model.usepercen;
			parameters[47].Value = Model.usemillions;
			parameters[48].Value = Model.usemml;
			parameters[49].Value = Model.pronumber;
			parameters[50].Value = Model.propercen;
			parameters[51].Value = Model.promillions;
			parameters[52].Value = Model.promml;
			parameters[53].Value = Model.rapnumber;
			parameters[54].Value = Model.rappercen;
			parameters[55].Value = Model.rapmillions;
			parameters[56].Value = Model.rapmml;
			parameters[57].Value = Model.slonumber;
			parameters[58].Value = Model.slopercen;
			parameters[59].Value = Model.slomillions;
			parameters[60].Value = Model.slomml;
			parameters[61].Value = Model.cirnumber;
			parameters[62].Value = Model.cirpercen;
			parameters[63].Value = Model.cirmillions;
			parameters[64].Value = Model.cirmml;
			parameters[65].Value = Model.locnumber;
			parameters[66].Value = Model.locpercen;
			parameters[67].Value = Model.locmillions;
			parameters[68].Value = Model.locmml;
			parameters[69].Value = Model.immnumber;
			parameters[70].Value = Model.immpercen;
			parameters[71].Value = Model.immmillions;
			parameters[72].Value = Model.immmml;
			parameters[73].Value = Model.mornumber;
			parameters[74].Value = Model.morpercen;
			parameters[75].Value = Model.mormillions;
			parameters[76].Value = Model.mormml;
			parameters[77].Value = Model.bennumber;
			parameters[78].Value = Model.benpercen;
			parameters[79].Value = Model.benmillions;
			parameters[80].Value = Model.benmml;
			parameters[81].Value = Model.coinumber;
			parameters[82].Value = Model.coipercen;
			parameters[83].Value = Model.coimillions;
			parameters[84].Value = Model.coimml;
			parameters[85].Value = Model.dmrnumber;
			parameters[86].Value = Model.dmrpercen;
			parameters[87].Value = Model.dmrmillions;
			parameters[88].Value = Model.dmrmml;
			parameters[89].Value = Model.disnumber;
			parameters[90].Value = Model.dispercen;
			parameters[91].Value = Model.dismillions;
			parameters[92].Value = Model.dismml;
            parameters[93].Value = Model.malnumber;
            parameters[94].Value = Model.malpercen;
            parameters[95].Value = Model.prognumber;
            parameters[96].Value = Model.progpercen;
            parameters[97].Value = Model.concentration;
			
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
        }
        
        ///<summary>
        ///更新一条数据
        ///</summary>
        public bool Update(SAS_InfoDataEntity Model)
        {
        	StringBuilder strSql=new StringBuilder();
             strSql.Append("update SAS_InfoData set ");
            strSql.Append("sperm = @sperm ,breed = @breed ,strain = @strain ,donorname = @donorname ,subject = @subject ,additionalnumber = @additionalnumber ,barn = @barn ,lastcollection = @lastcollection ,state = @state ,geneticline = @geneticline ,batchnumber = @batchnumber ,spermnumber = @spermnumber ,initialdilutorvolume = @initialdilutorvolume ,analysisdilution = @analysisdilution ,collector = @collector ,operator = @operator ,dosisvolume = @dosisvolume ,usefulspermperdosis = @usefulspermperdosis ,dilutor = @dilutor ,ejaculatedvolume = @ejaculatedvolume ,sementemp = @sementemp ,spermsdsispayunits = @spermsdsispayunits ,produceddosis = @produceddosis ,diluentvolume = @diluentvolume ,dilutionratio = @dilutionratio ,totalspermsperdosis = @totalspermsperdosis ,totalfinalvolume = @totalfinalvolume ,date = @date ,totnumber = @totnumber ,totpercen = @totpercen ,totmillions = @totmillions ,totmml = @totmml ,motnumber = @motnumber ,motpercen = @motpercen ,motmillions = @motmillions ,motmml = @motmml ,nornumber = @nornumber ,norpercen = @norpercen ,normillions = @normillions ,normml = @normml ,mnonumber = @mnonumber ,mnopercen = @mnopercen ,mnomillions = @mnomillions ,mnomml = @mnomml ,usenumber = @usenumber ,usepercen = @usepercen ,usemillions = @usemillions ,usemml = @usemml ,pronumber = @pronumber ,propercen = @propercen ,promillions = @promillions ,promml = @promml ,rapnumber = @rapnumber ,rappercen = @rappercen ,rapmillions = @rapmillions ,rapmml = @rapmml ,slonumber = @slonumber ,slopercen = @slopercen ,slomillions = @slomillions ,slomml = @slomml ,cirnumber = @cirnumber ,cirpercen = @cirpercen ,cirmillions = @cirmillions ,cirmml = @cirmml ,locnumber = @locnumber ,locpercen = @locpercen ,locmillions = @locmillions ,locmml = @locmml ,immnumber = @immnumber ,immpercen = @immpercen ,immmillions = @immmillions ,immmml = @immmml ,mornumber = @mornumber ,morpercen = @morpercen ,mormillions = @mormillions ,mormml = @mormml ,bennumber = @bennumber ,benpercen = @benpercen ,benmillions = @benmillions ,benmml = @benmml ,coinumber = @coinumber ,coipercen = @coipercen ,coimillions = @coimillions ,coimml = @coimml ,dmrnumber = @dmrnumber ,dmrpercen = @dmrpercen ,dmrmillions = @dmrmillions ,dmrmml = @dmrmml ,disnumber = @disnumber ,dispercen = @dispercen ,dismillions = @dismillions ,dismml = @dismml , malnumber=@malnumber,malpercen=@malpercen,prognumber=@prognumber,progpercen=@prognumber,concentration = @concentration ");
            strSql.Append("where casereference = @casereference ");
		    SqlParameter[] parameters = {
            new SqlParameter("@casereference", SqlDbType.VarChar,50)
			,new SqlParameter("@sperm", SqlDbType.VarChar,50)
			,new SqlParameter("@breed", SqlDbType.VarChar,50)
			,new SqlParameter("@strain", SqlDbType.VarChar,50)
			,new SqlParameter("@donorname", SqlDbType.VarChar,50)
			,new SqlParameter("@subject", SqlDbType.VarChar,50)
			,new SqlParameter("@additionalnumber", SqlDbType.VarChar,50)
			,new SqlParameter("@barn", SqlDbType.VarChar,50)
			,new SqlParameter("@lastcollection", SqlDbType.VarChar,50)
			,new SqlParameter("@state", SqlDbType.VarChar,50)
			,new SqlParameter("@geneticline", SqlDbType.VarChar,50)
			,new SqlParameter("@batchnumber", SqlDbType.VarChar,50)
			,new SqlParameter("@spermnumber", SqlDbType.VarChar,50)
			,new SqlParameter("@initialdilutorvolume", SqlDbType.Int,4)
			,new SqlParameter("@analysisdilution", SqlDbType.Int,4)
			,new SqlParameter("@collector", SqlDbType.VarChar,50)
			,new SqlParameter("@operator", SqlDbType.VarChar,50)
			,new SqlParameter("@dosisvolume", SqlDbType.Int,4)
			,new SqlParameter("@usefulspermperdosis", SqlDbType.Int,4)
			,new SqlParameter("@dilutor", SqlDbType.VarChar,50)
			,new SqlParameter("@ejaculatedvolume", SqlDbType.Int,4)
			,new SqlParameter("@sementemp", SqlDbType.VarChar,50)
			,new SqlParameter("@spermsdsispayunits", SqlDbType.VarChar,50)
			,new SqlParameter("@produceddosis", SqlDbType.Int,4)
			,new SqlParameter("@diluentvolume", SqlDbType.Int,4)
			,new SqlParameter("@dilutionratio", SqlDbType.Int,4)
			,new SqlParameter("@totalspermsperdosis", SqlDbType.Int,4)
			,new SqlParameter("@totalfinalvolume", SqlDbType.Int,4)
			,new SqlParameter("@date", SqlDbType.VarChar,50)
			,new SqlParameter("@totnumber", SqlDbType.Int,4)
			,new SqlParameter("@totpercen", SqlDbType.Float,8)
			,new SqlParameter("@totmillions", SqlDbType.Int,4)
			,new SqlParameter("@totmml", SqlDbType.Float,8)
			,new SqlParameter("@motnumber", SqlDbType.Int,4)
			,new SqlParameter("@motpercen", SqlDbType.Float,8)
			,new SqlParameter("@motmillions", SqlDbType.Int,4)
			,new SqlParameter("@motmml", SqlDbType.Float,8)
			,new SqlParameter("@nornumber", SqlDbType.Int,4)
			,new SqlParameter("@norpercen", SqlDbType.Float,8)
			,new SqlParameter("@normillions", SqlDbType.Int,4)
			,new SqlParameter("@normml", SqlDbType.Int,4)
			,new SqlParameter("@mnonumber", SqlDbType.Int,4)
			,new SqlParameter("@mnopercen", SqlDbType.Float,8)
			,new SqlParameter("@mnomillions", SqlDbType.Int,4)
			,new SqlParameter("@mnomml", SqlDbType.Float,8)
			,new SqlParameter("@usenumber", SqlDbType.Int,4)
			,new SqlParameter("@usepercen", SqlDbType.Float,8)
			,new SqlParameter("@usemillions", SqlDbType.Int,4)
			,new SqlParameter("@usemml", SqlDbType.Float,8)
			,new SqlParameter("@pronumber", SqlDbType.Int,4)
			,new SqlParameter("@propercen", SqlDbType.Float,8)
			,new SqlParameter("@promillions", SqlDbType.Int,4)
			,new SqlParameter("@promml", SqlDbType.Float,8)
			,new SqlParameter("@rapnumber", SqlDbType.Int,4)
			,new SqlParameter("@rappercen", SqlDbType.Float,8)
			,new SqlParameter("@rapmillions", SqlDbType.Int,4)
			,new SqlParameter("@rapmml", SqlDbType.Float,8)
			,new SqlParameter("@slonumber", SqlDbType.Int,4)
			,new SqlParameter("@slopercen", SqlDbType.Float,8)
			,new SqlParameter("@slomillions", SqlDbType.Int,4)
			,new SqlParameter("@slomml", SqlDbType.Float,8)
			,new SqlParameter("@cirnumber", SqlDbType.Int,4)
			,new SqlParameter("@cirpercen", SqlDbType.Float,8)
			,new SqlParameter("@cirmillions", SqlDbType.Int,4)
			,new SqlParameter("@cirmml", SqlDbType.Float,8)
			,new SqlParameter("@locnumber", SqlDbType.Int,4)
			,new SqlParameter("@locpercen", SqlDbType.Float,8)
			,new SqlParameter("@locmillions", SqlDbType.Int,4)
			,new SqlParameter("@locmml", SqlDbType.Float,8)
			,new SqlParameter("@immnumber", SqlDbType.Int,4)
			,new SqlParameter("@immpercen", SqlDbType.Float,8)
			,new SqlParameter("@immmillions", SqlDbType.Int,4)
			,new SqlParameter("@immmml", SqlDbType.Float,8)
			,new SqlParameter("@mornumber", SqlDbType.Int,4)
			,new SqlParameter("@morpercen", SqlDbType.Float,8)
			,new SqlParameter("@mormillions", SqlDbType.Int,4)
			,new SqlParameter("@mormml", SqlDbType.Float,8)
			,new SqlParameter("@bennumber", SqlDbType.Int,4)
			,new SqlParameter("@benpercen", SqlDbType.Float,8)
			,new SqlParameter("@benmillions", SqlDbType.Int,4)
			,new SqlParameter("@benmml", SqlDbType.Float,8)
			,new SqlParameter("@coinumber", SqlDbType.Int,4)
			,new SqlParameter("@coipercen", SqlDbType.Float,8)
			,new SqlParameter("@coimillions", SqlDbType.Int,4)
			,new SqlParameter("@coimml", SqlDbType.Float,8)
			,new SqlParameter("@dmrnumber", SqlDbType.Int,4)
			,new SqlParameter("@dmrpercen", SqlDbType.Float,8)
			,new SqlParameter("@dmrmillions", SqlDbType.Int,4)
			,new SqlParameter("@dmrmml", SqlDbType.Float,8)
			,new SqlParameter("@disnumber", SqlDbType.Int,4)
			,new SqlParameter("@dispercen", SqlDbType.Float,8)
			,new SqlParameter("@dismillions", SqlDbType.Int,4)
			,new SqlParameter("@dismml", SqlDbType.Float,8)
            ,new SqlParameter("@malnumber", SqlDbType.Int,4)
            ,new SqlParameter("@malpercen", SqlDbType.Float,8)
            ,new SqlParameter("@prognumber", SqlDbType.Int,4)
            ,new SqlParameter("@progpercen", SqlDbType.Float,8)
            ,new SqlParameter("@concentration", SqlDbType.Int,4)
			
		    };
            parameters[0].Value = Model.casereference;
			parameters[1].Value = Model.sperm;
			parameters[2].Value = Model.breed;
			parameters[3].Value = Model.strain;
			parameters[4].Value = Model.donorname;
			parameters[5].Value = Model.subject;
			parameters[6].Value = Model.additionalnumber;
			parameters[7].Value = Model.barn;
			parameters[8].Value = Model.lastcollection;
			parameters[9].Value = Model.state;
			parameters[10].Value = Model.geneticline;
			parameters[11].Value = Model.batchnumber;
			parameters[12].Value = Model.spermnumber;
			parameters[13].Value = Model.initialdilutorvolume;
			parameters[14].Value = Model.analysisdilution;
			parameters[15].Value = Model.collector;
			parameters[16].Value = Model.Operator;
			parameters[17].Value = Model.dosisvolume;
			parameters[18].Value = Model.usefulspermperdosis;
			parameters[19].Value = Model.dilutor;
			parameters[20].Value = Model.ejaculatedvolume;
			parameters[21].Value = Model.sementemp;
			parameters[22].Value = Model.spermsdsispayunits;
			parameters[23].Value = Model.produceddosis;
			parameters[24].Value = Model.diluentvolume;
			parameters[25].Value = Model.dilutionratio;
			parameters[26].Value = Model.totalspermsperdosis;
			parameters[27].Value = Model.totalfinalvolume;
			parameters[28].Value = Model.date;
			parameters[29].Value = Model.totnumber;
			parameters[30].Value = Model.totpercen;
			parameters[31].Value = Model.totmillions;
			parameters[32].Value = Model.totmml;
			parameters[33].Value = Model.motnumber;
			parameters[34].Value = Model.motpercen;
			parameters[35].Value = Model.motmillions;
			parameters[36].Value = Model.motmml;
			parameters[37].Value = Model.nornumber;
			parameters[38].Value = Model.norpercen;
			parameters[39].Value = Model.normillions;
			parameters[40].Value = Model.normml;
			parameters[41].Value = Model.mnonumber;
			parameters[42].Value = Model.mnopercen;
			parameters[43].Value = Model.mnomillions;
			parameters[44].Value = Model.mnomml;
			parameters[45].Value = Model.usenumber;
			parameters[46].Value = Model.usepercen;
			parameters[47].Value = Model.usemillions;
			parameters[48].Value = Model.usemml;
			parameters[49].Value = Model.pronumber;
			parameters[50].Value = Model.propercen;
			parameters[51].Value = Model.promillions;
			parameters[52].Value = Model.promml;
			parameters[53].Value = Model.rapnumber;
			parameters[54].Value = Model.rappercen;
			parameters[55].Value = Model.rapmillions;
			parameters[56].Value = Model.rapmml;
			parameters[57].Value = Model.slonumber;
			parameters[58].Value = Model.slopercen;
			parameters[59].Value = Model.slomillions;
			parameters[60].Value = Model.slomml;
			parameters[61].Value = Model.cirnumber;
			parameters[62].Value = Model.cirpercen;
			parameters[63].Value = Model.cirmillions;
			parameters[64].Value = Model.cirmml;
			parameters[65].Value = Model.locnumber;
			parameters[66].Value = Model.locpercen;
			parameters[67].Value = Model.locmillions;
			parameters[68].Value = Model.locmml;
			parameters[69].Value = Model.immnumber;
			parameters[70].Value = Model.immpercen;
			parameters[71].Value = Model.immmillions;
			parameters[72].Value = Model.immmml;
			parameters[73].Value = Model.mornumber;
			parameters[74].Value = Model.morpercen;
			parameters[75].Value = Model.mormillions;
			parameters[76].Value = Model.mormml;
			parameters[77].Value = Model.bennumber;
			parameters[78].Value = Model.benpercen;
			parameters[79].Value = Model.benmillions;
			parameters[80].Value = Model.benmml;
			parameters[81].Value = Model.coinumber;
			parameters[82].Value = Model.coipercen;
			parameters[83].Value = Model.coimillions;
			parameters[84].Value = Model.coimml;
			parameters[85].Value = Model.dmrnumber;
			parameters[86].Value = Model.dmrpercen;
			parameters[87].Value = Model.dmrmillions;
			parameters[88].Value = Model.dmrmml;
			parameters[89].Value = Model.disnumber;
			parameters[90].Value = Model.dispercen;
			parameters[91].Value = Model.dismillions;
			parameters[92].Value = Model.dismml;
            parameters[93].Value = Model.malnumber;
            parameters[94].Value = Model.malpercen;
            parameters[95].Value = Model.prognumber;
            parameters[96].Value = Model.progpercen;
            parameters[97].Value = Model.concentration;
			

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
        }
        
        ///<summary>
        ///删除一条数据
        ///</summary>
        public bool Delete(string casereference)
        {
            StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from SAS_InfoData ");
            strSql.Append("where casereference = @casereference ");
		    SqlParameter[] parameters = {
            new SqlParameter("@casereference", SqlDbType.VarChar,50)
			
		    };
            parameters[0].Value = casereference;
			

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
        }
        
           public bool DeleteList(string IDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from SAS_InfoData ");
            strSql.Append(" where ID in (" + IDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        ///<summary>
        ///根据条件删除记录
        ///</summary>
        public bool DeleteByWhere(string strWhere)
        {
            StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from SAS_InfoData ");
            strSql.Append("where " + strWhere);
		    

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
        }
        
       
         
        
        ///<summary>
        ///获取数据列表
        ///</summary>
        public DataSet GetList(string strWhere)
        {
        	StringBuilder strSql=new StringBuilder();
			strSql.Append("select *  from SAS_InfoData ");
            if(strWhere.Trim()!=string.Empty)
		    {
                strSql.Append(" where "+ strWhere);
		    }
			return DbHelperSQL.Query(strSql.ToString());
        }
        
        ///<summary>
        ///获取前几行数据
        ///</summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex,out int count)
        {
            StringBuilder strCount=new StringBuilder();
			strCount.Append("select count(1) FROM SAS_InfoData ");
			if(strWhere.Trim()!="")
			{
				strCount.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strCount.ToString());
			if (obj == null)
			{
				count = 0;
			}
			else
			{
				count = Convert.ToInt32(obj);
			}
        
        	StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if(!string.IsNullOrEmpty(orderby.Trim()))
		    {
                strSql.Append("order by T." + orderby );
		    }
            else
            {
				strSql.Append("order by  T.casereference desc");
			}
            strSql.Append(")AS Row, T.*  from SAS_InfoData T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            
			return DbHelperSQL.Query(strSql.ToString());
        }        
    }
}



