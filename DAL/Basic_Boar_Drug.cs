﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using XHD.DBUtility;//Please add references
namespace XHD.DAL
{
	/// <summary>
	/// 数据访问类:Basic_Boar_Drug
	/// </summary>
	public partial class Basic_Boar_Drug
	{
		public Basic_Boar_Drug()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Basic_Boar_Drug");
			strSql.Append(" where id=@id ");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.VarChar,50)			};
			parameters[0].Value = id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(XHD.Model.Basic_Boar_Drug model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Basic_Boar_Drug(");
			strSql.Append("id,boarId,Dosage,createId,createDate,drugId,remarks,drugTime)");
			strSql.Append(" values (");
			strSql.Append("@id,@boarId,@Dosage,@createId,@createDate,@drugId,@remarks,@drugTime)");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.VarChar,50),
					new SqlParameter("@boarId", SqlDbType.VarChar,50),
					new SqlParameter("@Dosage", SqlDbType.Float,8),
					new SqlParameter("@createId", SqlDbType.VarChar,50),
					new SqlParameter("@createDate", SqlDbType.DateTime),
			new SqlParameter("@drugId", SqlDbType.VarChar,50),
			new SqlParameter("@remarks", SqlDbType.VarChar,50),
			new SqlParameter("@drugTime", SqlDbType.DateTime)};
			parameters[0].Value = model.id;
			parameters[1].Value = model.boarId;
			parameters[2].Value = model.Dosage;
			parameters[3].Value = model.createId;
			parameters[4].Value = model.createDate;
			parameters[5].Value = model.drugId;
			parameters[6].Value = model.remarks;
			parameters[7].Value = model.drugTime;
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XHD.Model.Basic_Boar_Drug model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Basic_Boar_Drug set "); 
			strSql.Append("Dosage=@Dosage,");
			strSql.Append("createId=@createId,");
			strSql.Append("drugId=@drugId,");
			strSql.Append("remarks=@remarks,");
			strSql.Append("drugTime=@drugTime,");
			strSql.Append("createDate=@createDate");
			strSql.Append(" where boarId=@id ");
			SqlParameter[] parameters = {
					
					new SqlParameter("@Dosage", SqlDbType.Float,8),
					new SqlParameter("@createId", SqlDbType.VarChar,50),
					new SqlParameter("@drugId", SqlDbType.VarChar,50),
					new SqlParameter("@remarks", SqlDbType.VarChar,50),
					new SqlParameter("@drugTime", SqlDbType.DateTime),
					new SqlParameter("@createDate", SqlDbType.DateTime),
					new SqlParameter("@boarId", SqlDbType.VarChar,50)};
		
			parameters[0].Value = model.Dosage;
			parameters[1].Value = model.createId;
			parameters[2].Value = model.createId;
			parameters[3].Value = model.createId;
			parameters[4].Value = model.drugTime;
			parameters[5].Value = model.createDate;
			parameters[6].Value = model.id;
			parameters[7].Value = model.boarId;
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Basic_Boar_Drug ");
			strSql.Append(" where id=@id ");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.VarChar,50)			};
			parameters[0].Value = id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Basic_Boar_Drug ");
			strSql.Append(" where id in ("+idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XHD.Model.Basic_Boar_Drug GetModel(string id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 id,boarId,Dosage,createId,createDate,drugId,remarks,drugTime from Basic_Boar_Drug ");
			strSql.Append(" where id=@id ");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.VarChar,50)			};
			parameters[0].Value = id;

			XHD.Model.Basic_Boar_Drug model=new XHD.Model.Basic_Boar_Drug();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XHD.Model.Basic_Boar_Drug DataRowToModel(DataRow row)
		{
			XHD.Model.Basic_Boar_Drug model=new XHD.Model.Basic_Boar_Drug();
			if (row != null)
			{
				if(row["id"]!=null)
				{
					model.id=row["id"].ToString();
				}
				if(row["boarId"]!=null)
				{
					model.boarId=row["boarId"].ToString();
				}
				if(row["Dosage"]!=null && row["Dosage"].ToString()!="")
				{
					model.Dosage=decimal.Parse(row["Dosage"].ToString());
				}
				if(row["createId"]!=null)
				{
					model.createId=row["createId"].ToString();
				}
				if(row["createDate"]!=null && row["createDate"].ToString()!="")
				{
					model.createDate=DateTime.Parse(row["createDate"].ToString());
				}
				if (row["drugId"] != null)
				{
					model.drugId = row["drugId"].ToString();
				}
				if (row["remarks"] != null)
				{
					model.remarks = row["remarks"].ToString();
				}
				if (row["drugTime"] != null && row["drugTime"].ToString() != "")
				{
					model.drugTime = DateTime.Parse(row["drugTime"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select id,boarId,Dosage,createId,createDate,drugId,remarks,drugTime ");
			strSql.Append(" FROM Basic_Boar_Drug ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" id,boarId,Dosage,createId,createDate ,drugId,remarks,drugTime");
			strSql.Append(" FROM Basic_Boar_Drug ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Basic_Boar_Drug ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.id desc");
			}
			strSql.Append(")AS Row, T.*  from Basic_Boar_Drug T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Basic_Boar_Drug";
			parameters[1].Value = "id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod
		public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, out string Total)
		{
			StringBuilder strSql_grid = new StringBuilder();
			StringBuilder strSql_total = new StringBuilder();
			StringBuilder sb = new StringBuilder();
			sb.AppendLine(" SELECT A.*,B.drugName,B.drugcycle,B.unit,B.unit_id FROM dbo.Basic_Boar_Drug A  ");
			sb.AppendLine(" INNER JOIN Basic_Drug B ON A.drugId=B.IDCode  ");

			strSql_total.Append(" SELECT COUNT(id) FROM ("+sb.ToString()+")AA ");
			strSql_grid.Append("SELECT ");
			strSql_grid.Append("      n,* ");
			strSql_grid.Append(" FROM ( SELECT *, ROW_NUMBER() OVER( Order by " + filedOrder + " ) AS n from (" + sb.ToString() + ")AA ");
			if (strWhere.Trim() != "")
			{
				strSql_grid.Append(" WHERE " + strWhere);
				strSql_total.Append(" WHERE " + strWhere);
			}
			strSql_grid.Append("  ) as w1  ");
			strSql_grid.Append("WHERE n BETWEEN " + (PageSize * (PageIndex - 1) + 1) + " AND " + PageSize * PageIndex);
			strSql_grid.Append(" ORDER BY " + filedOrder);
			Total = DbHelperSQL.Query(strSql_total.ToString()).Tables[0].Rows[0][0].ToString();
			return DbHelperSQL.Query(strSql_grid.ToString());
		}
		#endregion  ExtensionMethod
	}
}

