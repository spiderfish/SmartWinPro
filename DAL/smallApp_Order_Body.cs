﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using XHD.DBUtility;//Please add references
namespace XHD.DAL
{
	/// <summary>
	/// 数据访问类:smallApp_Order_Body
	/// </summary>
	public partial class smallApp_Order_Body
	{
		public smallApp_Order_Body()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string OrderId,string ProductId)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from smallApp_Order_Body");
			strSql.Append(" where OrderId=@OrderId and ProductId=@ProductId ");
			SqlParameter[] parameters = {
					new SqlParameter("@OrderId", SqlDbType.VarChar,50),
					new SqlParameter("@ProductId", SqlDbType.VarChar,50)			};
			parameters[0].Value = OrderId;
			parameters[1].Value = ProductId;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(XHD.Model.smallApp_Order_Body model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into smallApp_Order_Body(");
			strSql.Append("OrderId,ProductId,TotalAmount,price,TotalSum)");
			strSql.Append(" values (");
			strSql.Append("@OrderId,@ProductId,@TotalAmount,@price,@TotalSum)");
			SqlParameter[] parameters = {
					new SqlParameter("@OrderId", SqlDbType.VarChar,50),
					new SqlParameter("@ProductId", SqlDbType.VarChar,50),
					new SqlParameter("@TotalAmount", SqlDbType.Float,8),
					new SqlParameter("@price", SqlDbType.Float,8),
					new SqlParameter("@TotalSum", SqlDbType.Float,8)};
			parameters[0].Value = model.OrderId;
			parameters[1].Value = model.ProductId;
			parameters[2].Value = model.TotalAmount;
			parameters[3].Value = model.price;
			parameters[4].Value = model.TotalSum;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XHD.Model.smallApp_Order_Body model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update smallApp_Order_Body set ");
			strSql.Append("TotalAmount=@TotalAmount,");
			strSql.Append("price=@price,");
			strSql.Append("TotalSum=@TotalSum");
			strSql.Append(" where OrderId=@OrderId and ProductId=@ProductId ");
			SqlParameter[] parameters = {
					new SqlParameter("@TotalAmount", SqlDbType.Float,8),
					new SqlParameter("@price", SqlDbType.Float,8),
					new SqlParameter("@TotalSum", SqlDbType.Float,8),
					new SqlParameter("@OrderId", SqlDbType.VarChar,50),
					new SqlParameter("@ProductId", SqlDbType.VarChar,50)};
			parameters[0].Value = model.TotalAmount;
			parameters[1].Value = model.price;
			parameters[2].Value = model.TotalSum;
			parameters[3].Value = model.OrderId;
			parameters[4].Value = model.ProductId;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string OrderId,string ProductId)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from smallApp_Order_Body ");
			strSql.Append(" where OrderId=@OrderId and ProductId=@ProductId ");
			SqlParameter[] parameters = {
					new SqlParameter("@OrderId", SqlDbType.VarChar,50),
					new SqlParameter("@ProductId", SqlDbType.VarChar,50)			};
			parameters[0].Value = OrderId;
			parameters[1].Value = ProductId;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XHD.Model.smallApp_Order_Body GetModel(string OrderId,string ProductId)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 OrderId,ProductId,TotalAmount,price,TotalSum from smallApp_Order_Body ");
			strSql.Append(" where OrderId=@OrderId and ProductId=@ProductId ");
			SqlParameter[] parameters = {
					new SqlParameter("@OrderId", SqlDbType.VarChar,50),
					new SqlParameter("@ProductId", SqlDbType.VarChar,50)			};
			parameters[0].Value = OrderId;
			parameters[1].Value = ProductId;

			XHD.Model.smallApp_Order_Body model=new XHD.Model.smallApp_Order_Body();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XHD.Model.smallApp_Order_Body DataRowToModel(DataRow row)
		{
			XHD.Model.smallApp_Order_Body model=new XHD.Model.smallApp_Order_Body();
			if (row != null)
			{
				if(row["OrderId"]!=null)
				{
					model.OrderId=row["OrderId"].ToString();
				}
				if(row["ProductId"]!=null)
				{
					model.ProductId=row["ProductId"].ToString();
				}
				if(row["TotalAmount"]!=null && row["TotalAmount"].ToString()!="")
				{
					model.TotalAmount=decimal.Parse(row["TotalAmount"].ToString());
				}
				if(row["price"]!=null && row["price"].ToString()!="")
				{
					model.price=decimal.Parse(row["price"].ToString());
				}
				if(row["TotalSum"]!=null && row["TotalSum"].ToString()!="")
				{
					model.TotalSum=decimal.Parse(row["TotalSum"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select OrderId,ProductId,TotalAmount,price,TotalSum ");
			strSql.Append(" FROM smallApp_Order_Body ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" OrderId,ProductId,TotalAmount,price,TotalSum ");
			strSql.Append(" FROM smallApp_Order_Body ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM smallApp_Order_Body ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ProductId desc");
			}
			strSql.Append(")AS Row, T.*  from smallApp_Order_Body T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "smallApp_Order_Body";
			parameters[1].Value = "ProductId";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

