﻿/*
* Product.cs
*
* 功 能： N/A
* 类 名： Product
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2016-03-06 13:36:04    黄润伟    
*
* Copyright © 2015 www.xhdcrm.com All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：小黄豆                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using XHD.DBUtility;//Please add references
namespace XHD.DAL
{
    /// <summary>
    /// 数据访问类:Product
    /// </summary>
    public partial class Product
    {
        public Product()
        { }
        #region  BasicMethod


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(XHD.Model.Product model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into Product(");
            strSql.Append("id,product_name,category_id,status,unit,cost,price,agio,remarks,specifications,create_id,create_time,barcode,pickcycle,dailyoutput,dosageUnit_id,unit_id,dosageUnit,specifications_id, Series_Id,Series_Name,Level_Id,Level_Name,img,richText,OnlinePrice,waitDay,Dose,EffectiveSperm,Quantity)");
            strSql.Append(" values (");
            strSql.Append("@id,@product_name,@category_id,@status,@unit,@cost,@price,@agio,@remarks,@specifications,@create_id,@create_time,@barcode,@pickcycle,@dailyoutput,@dosageUnit_id,@unit_id,@dosageUnit,@specifications_id, @Series_Id,@Series_Name,@Level_Id,@Level_Name,@img,@richText,@OnlinePrice,@waitDay,@Dose,@EffectiveSperm,@Quantity)");
            SqlParameter[] parameters = {
                    new SqlParameter("@id", SqlDbType.VarChar,50),
                    new SqlParameter("@product_name", SqlDbType.VarChar,250),
                    new SqlParameter("@category_id", SqlDbType.VarChar,50),
                    new SqlParameter("@status", SqlDbType.VarChar,250),
                    new SqlParameter("@unit", SqlDbType.VarChar,250),
                    new SqlParameter("@cost", SqlDbType.Decimal,9),
                    new SqlParameter("@price", SqlDbType.Decimal,9),
                    new SqlParameter("@agio", SqlDbType.Decimal,9),
                    new SqlParameter("@remarks", SqlDbType.VarChar,-1),
                    new SqlParameter("@specifications", SqlDbType.VarChar,250),
                    new SqlParameter("@create_id", SqlDbType.VarChar,50),
                    new SqlParameter("@create_time", SqlDbType.DateTime),
                   new SqlParameter("@barcode", SqlDbType.VarChar,50),
                    new SqlParameter("@pickcycle", SqlDbType.Int,9),
                    new SqlParameter("@dailyoutput", SqlDbType.Int,9),
                    new SqlParameter("@dosageUnit_id", SqlDbType.VarChar,50),
                    new SqlParameter("@unit_id", SqlDbType.VarChar,50),
                    new SqlParameter("@dosageUnit", SqlDbType.VarChar,50),
                   new SqlParameter("@specifications_id", SqlDbType.VarChar,50),
                    new SqlParameter("@Series_Id", SqlDbType.VarChar,50),
                    new SqlParameter("@Series_Name", SqlDbType.VarChar,50),
                    new SqlParameter("@Level_Id", SqlDbType.VarChar,50),
                    new SqlParameter("@Level_Name", SqlDbType.VarChar,50),
                     new SqlParameter("@img", SqlDbType.VarChar,250),
                    new SqlParameter("@OnlinePrice", SqlDbType.Decimal,9),
                    new SqlParameter("@waitDay", SqlDbType.Int,9),
                     new SqlParameter("@Dose", SqlDbType.Decimal,9),
                     new SqlParameter("@EffectiveSperm", SqlDbType.Decimal,9),
                     new SqlParameter("@Quantity", SqlDbType.Decimal,9),
                      new SqlParameter("@RichText", SqlDbType.NVarChar,int.MaxValue),
            }; 
            parameters[0].Value = model.id;
            parameters[1].Value = model.product_name;
            parameters[2].Value = model.category_id;
            parameters[3].Value = model.status;
            parameters[4].Value = model.unit;
            parameters[5].Value = model.cost;
            parameters[6].Value = model.price;
            parameters[7].Value = model.agio;
            parameters[8].Value = model.remarks;
            parameters[9].Value = model.specifications;
            parameters[10].Value = model.create_id;
            parameters[11].Value = model.create_time;
            parameters[12].Value = model.barcode;
            parameters[13].Value = model.pickcycle;
            parameters[14].Value = model.dailyoutput;
            parameters[15].Value = model.dosageUnit_id;
            parameters[16].Value = model.unit_id;
            parameters[17].Value = model.dosageUnit;
            parameters[18].Value = model.specifications_id;
            parameters[19].Value = model.Series_Id;
            parameters[20].Value = model.Series_Name;
            parameters[21].Value = model.Level_Id;
            parameters[22].Value = model.Level_Name;
            parameters[23].Value = model.Img;
            parameters[24].Value = model.OnlinePrice;
            parameters[25].Value = model.WaitDay;
            parameters[26].Value = model.Dose;
            parameters[27].Value = model.EffectiveSperm;
            parameters[28].Value = model.Quantity;
            parameters[29].Value = model.RichText;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(XHD.Model.Product model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update Product set ");
            strSql.Append("product_name=@product_name,");
            strSql.Append("category_id=@category_id,");
            strSql.Append("status=@status,");
            strSql.Append("unit=@unit,");
            strSql.Append("cost=@cost,");
            strSql.Append("price=@price,");
            strSql.Append("agio=@agio,");
            strSql.Append("remarks=@remarks,");
            strSql.Append("barcode=@barcode,");
            strSql.Append("specifications=@specifications,");
            strSql.Append("pickcycle=@pickcycle,");
            strSql.Append("specifications_id=@specifications_id,");
            strSql.Append("dosageUnit_id=@dosageUnit_id,");
            strSql.Append("unit_id=@unit_id,");
            strSql.Append("dosageUnit=@dosageUnit,");
            strSql.Append("Series_Id=@Series_Id,");
            strSql.Append("Series_Name=@Series_Name,");
            strSql.Append("Level_Id=@Level_Id,");
            strSql.Append("Level_Name=@Level_Name,");
            strSql.Append("img=@img,");
            strSql.Append("RichText=@RichText,");
            strSql.Append("OnlinePrice=@OnlinePrice,");
            strSql.Append("dailyoutput=@dailyoutput,");
            strSql.Append("Dose=@Dose,");
            strSql.Append("EffectiveSperm=@EffectiveSperm,");
            strSql.Append("Quantity=@Quantity,"); 
            strSql.Append("waitDay=@waitDay"); 
            //
           
            strSql.Append(" where id=@id ");
            SqlParameter[] parameters = {
                    new SqlParameter("@product_name", SqlDbType.VarChar,250),
                    new SqlParameter("@category_id", SqlDbType.VarChar,50),
                    new SqlParameter("@status", SqlDbType.VarChar,250),
                    new SqlParameter("@unit", SqlDbType.VarChar,250),
                    new SqlParameter("@cost", SqlDbType.Decimal,9),
                    new SqlParameter("@price", SqlDbType.Decimal,9),
                    new SqlParameter("@agio", SqlDbType.Decimal,9),
                    new SqlParameter("@remarks", SqlDbType.VarChar,-1),
                    new SqlParameter("@barcode", SqlDbType.VarChar,50),
                    new SqlParameter("@specifications", SqlDbType.VarChar,250),
                      new SqlParameter("@pickcycle", SqlDbType.Int,10),
                        new SqlParameter("@dailyoutput", SqlDbType.Int,10),
                    new SqlParameter("@id", SqlDbType.VarChar,50),
                     new SqlParameter("@dosageUnit_id", SqlDbType.VarChar,50),
                    new SqlParameter("@unit_id", SqlDbType.VarChar,50),
                    new SqlParameter("@Series_Id", SqlDbType.VarChar,50),
                    new SqlParameter("@Series_Name", SqlDbType.VarChar,50),
                    new SqlParameter("@Level_Id", SqlDbType.VarChar,50),
                    new SqlParameter("@Level_Name", SqlDbType.VarChar,50),
                     new SqlParameter("@img", SqlDbType.VarChar,250),
                     new SqlParameter("@RichText", SqlDbType.NVarChar,int.MaxValue),
                            new SqlParameter("@OnlinePrice", SqlDbType.Decimal,9),
                     new SqlParameter("@waitDay", SqlDbType.Int,9),
                        new SqlParameter("@Dose", SqlDbType.Decimal,9),
                     new SqlParameter("@EffectiveSperm", SqlDbType.Decimal,9),
                     new SqlParameter("@Quantity", SqlDbType.Decimal,9),

                new SqlParameter("@dosageUnit", SqlDbType.VarChar,50),
                   new SqlParameter("@specifications_id", SqlDbType.VarChar,50)};
            parameters[0].Value = model.product_name;
            parameters[1].Value = model.category_id;
            parameters[2].Value = model.status;
            parameters[3].Value = model.unit;
            parameters[4].Value = model.cost;
            parameters[5].Value = model.price;
            parameters[6].Value = model.agio;
            parameters[7].Value = model.remarks;
            parameters[8].Value = model.barcode;
            parameters[9].Value = model.specifications;
            parameters[10].Value = model.pickcycle;
            parameters[11].Value = model.dailyoutput;
            parameters[12].Value = model.id;
            parameters[13].Value = model.dosageUnit_id;
            parameters[14].Value = model.unit_id;
            parameters[15].Value = model.Series_Id;
            parameters[16].Value = model.Series_Name;
            parameters[17].Value = model.Level_Id;
            parameters[18].Value = model.Level_Name;
            parameters[19].Value = model.Img;
            parameters[20].Value = model.RichText;
            parameters[21].Value = model.OnlinePrice;
            parameters[22].Value = model.WaitDay;
            parameters[23].Value = model.Dose;
            parameters[24].Value = model.EffectiveSperm;
            parameters[25].Value = model.Quantity;
            parameters[26].Value = model.dosageUnit;
            parameters[27].Value = model.specifications_id;
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string id)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Product ");
            strSql.Append(" where id=@id ");
            SqlParameter[] parameters = {
                    new SqlParameter("@id", SqlDbType.VarChar,50)           };
            parameters[0].Value = id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string idlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Product ");
            strSql.Append(" where id in (" + idlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }



        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select id,product_name,category_id,status,unit,cost,price,agio,remarks,specifications,create_id,create_time,barcode,pickcycle,dailyoutput,dosageUnit_id,unit_id,dosageUnit,specifications_id, Series_Id,Series_Name,Level_Id,Level_Name,img,richText,OnlinePrice,waitDay,Dose,EffectiveSperm,Quantity  ");
            strSql.Append(",(select product_category from Product_category where id = Product.category_id) as category_name");
            strSql.Append(" FROM Product ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" id,product_name,category_id,status,unit,cost,price,agio,remarks,specifications,create_id,create_time ,barcode,pickcycle,dailyoutput,dosageUnit_id,unit_id,dosageUnit,specifications_id , Series_Id,Series_Name,Level_Id,Level_Name,img ,richText,OnlinePrice,waitDay,Dose,EffectiveSperm,Quantity");
            strSql.Append(",(select product_category from Product_category where id = Product.category_id) as category_name");
            strSql.Append(" FROM Product ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, out string Total)
        {
            StringBuilder strSql_grid = new StringBuilder();
            StringBuilder strSql_total = new StringBuilder();
            strSql_total.Append(" SELECT COUNT(id) FROM Product ");
            strSql_grid.Append("SELECT ");
            strSql_grid.Append("      n,id,product_name,category_id,status,unit,cost,price,agio,remarks,specifications,create_id,create_time ,barcode,pickcycle,dailyoutput,dosageUnit_id,unit_id,dosageUnit,specifications_id , Series_Id,Series_Name,Level_Id,Level_Name,img,richText,OnlinePrice,waitDay,Dose,EffectiveSperm,Quantity ");
            strSql_grid.Append(",(select product_category from Product_category where id = w1.category_id) as category_name");
            strSql_grid.Append(" FROM ( SELECT id,product_name,category_id,status,unit,cost,price,agio,remarks,specifications,create_id,create_time,barcode,pickcycle,dailyoutput,dosageUnit_id,unit_id,dosageUnit,specifications_id, Series_Id,Series_Name,Level_Id,Level_Name,img ,richText,OnlinePrice,waitDay,Dose,EffectiveSperm,Quantity, ROW_NUMBER() OVER( Order by " + filedOrder + " ) AS n from Product");
            if (strWhere.Trim() != "")
            {
                strSql_grid.Append(" WHERE " + strWhere);
                strSql_total.Append(" WHERE " + strWhere);
            }
            strSql_grid.Append("  ) as w1  ");
            strSql_grid.Append("WHERE n BETWEEN " + (PageSize * (PageIndex - 1) + 1) + " AND " + PageSize * PageIndex);
            strSql_grid.Append(" ORDER BY " + filedOrder);
            Total = DbHelperSQL.Query(strSql_total.ToString()).Tables[0].Rows[0][0].ToString();
            return DbHelperSQL.Query(strSql_grid.ToString());
        }

        public DataSet GetListOnline(int PageSize, int PageIndex, string strWhere, string filedOrder,string cid, out string Total)
        {
            StringBuilder strSql_grid = new StringBuilder();
            StringBuilder strSql_total = new StringBuilder();
            strSql_total.Append(" SELECT COUNT(id) FROM Product A");
            strSql_total.Append(" INNER JOIN  dbo.Product_Customer_Price B ON	A.id=B.product_id  AND B.customer_id='"+cid+"'");
            strSql_grid.Append("SELECT ");
            strSql_grid.Append("      n,id,product_name,category_id,status,unit,cost,price,agio,remarks,specifications,create_id,create_time ,barcode,pickcycle,dailyoutput,dosageUnit_id,unit_id,dosageUnit,specifications_id , Series_Id,Series_Name,Level_Id,Level_Name,OnlinePrice,CusPrice,customer_id,waitDay,Dose,EffectiveSperm,Quantity ");
            strSql_grid.Append(",(select product_category from Product_category where id = w1.category_id) as category_name");
            strSql_grid.Append(" FROM ( SELECT id,product_name,category_id,status,unit,cost,A.price,agio,A.remarks,specifications,create_id,create_time,barcode,pickcycle,dailyoutput,dosageUnit_id,unit_id,dosageUnit,specifications_id, Series_Id,Series_Name,Level_Id,Level_Name,OnlinePrice,B.price as CusPrice,B.customer_id, waitDay,Dose,EffectiveSperm,Quantity,  ROW_NUMBER() OVER( Order by " + filedOrder + " ) AS n from Product A");
            strSql_grid.Append(" INNER JOIN  dbo.Product_Customer_Price B ON	A.id=B.product_id AND B.customer_id='" + cid + "' ");
            if (strWhere.Trim() != "")
            {
                strSql_grid.Append(" WHERE " + strWhere);
                strSql_total.Append(" WHERE " + strWhere);
            }
            strSql_grid.Append("  ) as w1  ");
            strSql_grid.Append("WHERE n BETWEEN " + (PageSize * (PageIndex - 1) + 1) + " AND " + PageSize * PageIndex);
            strSql_grid.Append(" ORDER BY " + filedOrder);
            Total = DbHelperSQL.Query(strSql_total.ToString()).Tables[0].Rows[0][0].ToString();
            return DbHelperSQL.Query(strSql_grid.ToString());
        }



        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

