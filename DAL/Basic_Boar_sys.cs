﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using XHD.DBUtility;//Please add references
namespace XHD.DAL
{
	/// <summary>
	/// 数据访问类:Basic_Boar_sys
	/// </summary>
	public partial class Basic_Boar_sys
	{
		public Basic_Boar_sys()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Basic_Boar_sys");
			strSql.Append(" where BoarId=@id ");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.VarChar,50)			};
			parameters[0].Value = id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(XHD.Model.Basic_Boar_sys model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Basic_Boar_sys(");
			strSql.Append("id,BoarId,weight,birthPlace,leftNip,rightNip,stopMilkDate,stopMilkWeight,sameNestsNumber,bithsNumber,outDate,levels,description,license)");
			strSql.Append(" values (");
			strSql.Append("@id,@BoarId,@weight,@birthPlace,@leftNip,@rightNip,@stopMilkDate,@stopMilkWeight,@sameNestsNumber,@bithsNumber,@outDate,@levels,@description,@license)");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.VarChar,50),
					new SqlParameter("@BoarId", SqlDbType.VarChar,50),
					new SqlParameter("@weight", SqlDbType.Float,8),
					new SqlParameter("@birthPlace", SqlDbType.VarChar,50),
					new SqlParameter("@leftNip", SqlDbType.Int,4),
					new SqlParameter("@rightNip", SqlDbType.Int,4),
					new SqlParameter("@stopMilkDate", SqlDbType.DateTime),
					new SqlParameter("@stopMilkWeight", SqlDbType.Float,8),
					new SqlParameter("@sameNestsNumber", SqlDbType.Int,4),
					new SqlParameter("@bithsNumber", SqlDbType.Int,4),
					new SqlParameter("@outDate", SqlDbType.DateTime),
					new SqlParameter("@levels", SqlDbType.VarChar,50),
					new SqlParameter("@description", SqlDbType.VarChar,500),
					new SqlParameter("@license", SqlDbType.VarChar,50)};
			parameters[0].Value = model.id;
			parameters[1].Value = model.BoarId;
			parameters[2].Value = model.weight;
			parameters[3].Value = model.birthPlace;
			parameters[4].Value = model.leftNip;
			parameters[5].Value = model.rightNip;
			parameters[6].Value = model.stopMilkDate;
			parameters[7].Value = model.stopMilkWeight;
			parameters[8].Value = model.sameNestsNumber;
			parameters[9].Value = model.bithsNumber;
			parameters[10].Value = model.outDate;
			parameters[11].Value = model.levels;
			parameters[12].Value = model.description;
			parameters[13].Value = model.license;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XHD.Model.Basic_Boar_sys model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Basic_Boar_sys set ");
			//strSql.Append("BoarId=@BoarId,");
			strSql.Append("weight=@weight,");
			strSql.Append("birthPlace=@birthPlace,");
			strSql.Append("leftNip=@leftNip,");
			strSql.Append("rightNip=@rightNip,");
			strSql.Append("stopMilkDate=@stopMilkDate,");
			strSql.Append("stopMilkWeight=@stopMilkWeight,");
			strSql.Append("sameNestsNumber=@sameNestsNumber,");
			strSql.Append("bithsNumber=@bithsNumber,");
			strSql.Append("outDate=@outDate,");
			strSql.Append("levels=@levels,");
			strSql.Append("description=@description,");
			strSql.Append("license=@license");
			strSql.Append(" where BoarId=@BoarId ");
			SqlParameter[] parameters = {
					new SqlParameter("@BoarId", SqlDbType.VarChar,50),
					new SqlParameter("@weight", SqlDbType.Float,8),
					new SqlParameter("@birthPlace", SqlDbType.VarChar,50),
					new SqlParameter("@leftNip", SqlDbType.Int,4),
					new SqlParameter("@rightNip", SqlDbType.Int,4),
					new SqlParameter("@stopMilkDate", SqlDbType.DateTime),
					new SqlParameter("@stopMilkWeight", SqlDbType.Float,8),
					new SqlParameter("@sameNestsNumber", SqlDbType.Int,4),
					new SqlParameter("@bithsNumber", SqlDbType.Int,4),
					new SqlParameter("@outDate", SqlDbType.DateTime),
					new SqlParameter("@levels", SqlDbType.VarChar,50),
					new SqlParameter("@description", SqlDbType.VarChar,500),
					new SqlParameter("@license", SqlDbType.VarChar,50),
					new SqlParameter("@id", SqlDbType.VarChar,50)};
			parameters[0].Value = model.BoarId;
			parameters[1].Value = model.weight;
			parameters[2].Value = model.birthPlace;
			parameters[3].Value = model.leftNip;
			parameters[4].Value = model.rightNip;
			parameters[5].Value = model.stopMilkDate;
			parameters[6].Value = model.stopMilkWeight;
			parameters[7].Value = model.sameNestsNumber;
			parameters[8].Value = model.bithsNumber;
			parameters[9].Value = model.outDate;
			parameters[10].Value = model.levels;
			parameters[11].Value = model.description;
			parameters[12].Value = model.license;
			parameters[13].Value = model.id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Basic_Boar_sys ");
			strSql.Append(" where id=@id ");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.VarChar,50)			};
			parameters[0].Value = id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Basic_Boar_sys ");
			strSql.Append(" where id in ("+idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XHD.Model.Basic_Boar_sys GetModel(string id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 id,BoarId,weight,birthPlace,leftNip,rightNip,stopMilkDate,stopMilkWeight,sameNestsNumber,bithsNumber,outDate,levels,description,license from Basic_Boar_sys ");
			strSql.Append(" where id=@id ");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.VarChar,50)			};
			parameters[0].Value = id;

			XHD.Model.Basic_Boar_sys model=new XHD.Model.Basic_Boar_sys();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XHD.Model.Basic_Boar_sys DataRowToModel(DataRow row)
		{
			XHD.Model.Basic_Boar_sys model=new XHD.Model.Basic_Boar_sys();
			if (row != null)
			{
				if(row["id"]!=null)
				{
					model.id=row["id"].ToString();
				}
				if(row["BoarId"]!=null)
				{
					model.BoarId=row["BoarId"].ToString();
				}
				if(row["weight"]!=null && row["weight"].ToString()!="")
				{
					model.weight=decimal.Parse(row["weight"].ToString());
				}
				if(row["birthPlace"]!=null)
				{
					model.birthPlace=row["birthPlace"].ToString();
				}
				if(row["leftNip"]!=null && row["leftNip"].ToString()!="")
				{
					model.leftNip=int.Parse(row["leftNip"].ToString());
				}
				if(row["rightNip"]!=null && row["rightNip"].ToString()!="")
				{
					model.rightNip=int.Parse(row["rightNip"].ToString());
				}
				if(row["stopMilkDate"]!=null && row["stopMilkDate"].ToString()!="")
				{
					model.stopMilkDate=DateTime.Parse(row["stopMilkDate"].ToString());
				}
				if(row["stopMilkWeight"]!=null && row["stopMilkWeight"].ToString()!="")
				{
					model.stopMilkWeight=decimal.Parse(row["stopMilkWeight"].ToString());
				}
				if(row["sameNestsNumber"]!=null && row["sameNestsNumber"].ToString()!="")
				{
					model.sameNestsNumber=int.Parse(row["sameNestsNumber"].ToString());
				}
				if(row["bithsNumber"]!=null && row["bithsNumber"].ToString()!="")
				{
					model.bithsNumber=int.Parse(row["bithsNumber"].ToString());
				}
				if(row["outDate"]!=null && row["outDate"].ToString()!="")
				{
					model.outDate=DateTime.Parse(row["outDate"].ToString());
				}
				if(row["levels"]!=null)
				{
					model.levels=row["levels"].ToString();
				}
				if(row["description"]!=null)
				{
					model.description=row["description"].ToString();
				}
				if(row["license"]!=null)
				{
					model.license=row["license"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select id,BoarId,weight,birthPlace,leftNip,rightNip,stopMilkDate,stopMilkWeight,sameNestsNumber,bithsNumber,outDate,levels,description,license ");
			strSql.Append(" FROM Basic_Boar_sys ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" id,BoarId,weight,birthPlace,leftNip,rightNip,stopMilkDate,stopMilkWeight,sameNestsNumber,bithsNumber,outDate,levels,description,license ");
			strSql.Append(" FROM Basic_Boar_sys ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Basic_Boar_sys ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.id desc");
			}
			strSql.Append(")AS Row, T.*  from Basic_Boar_sys T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Basic_Boar_sys";
			parameters[1].Value = "id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod
		public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, out string Total)
		{
			StringBuilder strSql_grid = new StringBuilder();
			StringBuilder strSql_total = new StringBuilder();
			strSql_total.Append(" SELECT COUNT(id) FROM Basic_Boar_sys ");
			strSql_grid.Append("SELECT ");
			strSql_grid.Append("      n,* ");
			strSql_grid.Append(" FROM ( SELECT *, ROW_NUMBER() OVER( Order by " + filedOrder + " ) AS n from Basic_Boar_sys");
			if (strWhere.Trim() != "")
			{
				strSql_grid.Append(" WHERE " + strWhere);
				strSql_total.Append(" WHERE " + strWhere);
			}
			strSql_grid.Append("  ) as w1  ");
			strSql_grid.Append("WHERE n BETWEEN " + (PageSize * (PageIndex - 1) + 1) + " AND " + PageSize * PageIndex);
			strSql_grid.Append(" ORDER BY " + filedOrder);
			Total = DbHelperSQL.Query(strSql_total.ToString()).Tables[0].Rows[0][0].ToString();
			return DbHelperSQL.Query(strSql_grid.ToString());
		}
		#endregion  ExtensionMethod
	}
}

