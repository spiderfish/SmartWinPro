﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using XHD.DBUtility;//Please add references
namespace XHD.DAL
{
	/// <summary>
	/// 数据访问类:CRM_Customer_Address
	/// </summary>
	public partial class CRM_Customer_Address
	{
		public CRM_Customer_Address()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string Id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from CRM_Customer_Address");
			strSql.Append(" where Id=@Id ");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.VarChar,50)			};
			parameters[0].Value = Id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(XHD.Model.CRM_Customer_Address model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into CRM_Customer_Address(");
			strSql.Append("Id,CusId,addName,addTel,addMail,address,IsDefault,CreatTime)");
			strSql.Append(" values (");
			strSql.Append("@Id,@CusId,@addName,@addTel,@addMail,@address,@IsDefault,@CreatTime)");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.VarChar,50),
					new SqlParameter("@CusId", SqlDbType.VarChar,50),
								new SqlParameter("@addName", SqlDbType.VarChar,50),
					new SqlParameter("@addTel", SqlDbType.VarChar,20),
					new SqlParameter("@addMail", SqlDbType.VarChar,50),
					new SqlParameter("@address", SqlDbType.VarChar,250),
					new SqlParameter("@IsDefault", SqlDbType.Char,1),
					new SqlParameter("@CreatTime", SqlDbType.DateTime)};
			parameters[0].Value = model.Id;
			parameters[1].Value = model.CusId;
			parameters[2].Value = model.addName;
			parameters[3].Value = model.addTel;
			parameters[4].Value = model.addMail;
			parameters[5].Value = model.address;
			parameters[6].Value = model.IsDefault;
			parameters[7].Value = model.CreatTime;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XHD.Model.CRM_Customer_Address model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update CRM_Customer_Address set ");
			strSql.Append("CusId=@CusId,");
			strSql.Append("addTel=@addTel,");
			strSql.Append("addMail=@addMail,");
			strSql.Append("address=@address,");
			strSql.Append("IsDefault=@IsDefault,");
			strSql.Append("addName=@addName,");
			strSql.Append("CreatTime=@CreatTime");
			strSql.Append(" where Id=@Id ");
			SqlParameter[] parameters = {
					new SqlParameter("@CusId", SqlDbType.VarChar,50),
					new SqlParameter("@addTel", SqlDbType.VarChar,20),
					new SqlParameter("@addMail", SqlDbType.VarChar,50),
					new SqlParameter("@address", SqlDbType.VarChar,250),
					new SqlParameter("@IsDefault", SqlDbType.Char,1),
								new SqlParameter("@addName", SqlDbType.VarChar,50),
					new SqlParameter("@CreatTime", SqlDbType.DateTime),
					new SqlParameter("@Id", SqlDbType.VarChar,50)};
			parameters[0].Value = model.CusId;
			parameters[1].Value = model.addTel;
			parameters[2].Value = model.addMail;
			parameters[3].Value = model.address;
			parameters[4].Value = model.IsDefault;
			parameters[5].Value = model.addName;
			parameters[6].Value = model.CreatTime;
			parameters[7].Value = model.Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from CRM_Customer_Address ");
			strSql.Append(" where Id=@Id ");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.VarChar,50)			};
			parameters[0].Value = Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from CRM_Customer_Address ");
			strSql.Append(" where Id in ("+Idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XHD.Model.CRM_Customer_Address GetModel(string Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 Id,CusId,addName,addTel,addMail,address,IsDefault,CreatTime from CRM_Customer_Address ");
			strSql.Append(" where Id=@Id ");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.VarChar,50)			};
			parameters[0].Value = Id;

			XHD.Model.CRM_Customer_Address model=new XHD.Model.CRM_Customer_Address();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XHD.Model.CRM_Customer_Address DataRowToModel(DataRow row)
		{
			XHD.Model.CRM_Customer_Address model=new XHD.Model.CRM_Customer_Address();
			if (row != null)
			{
				if(row["Id"]!=null)
				{
					model.Id=row["Id"].ToString();
				}
				if(row["CusId"]!=null)
				{
					model.CusId=row["CusId"].ToString();
				}
				if(row["addTel"]!=null)
				{
					model.addTel=row["addTel"].ToString();
				}
				if(row["addMail"]!=null)
				{
					model.addMail=row["addMail"].ToString();
				}
				if(row["address"]!=null)
				{
					model.address=row["address"].ToString();
				}
				if(row["IsDefault"]!=null)
				{
					model.IsDefault=row["IsDefault"].ToString();
				}
				if(row["CreatTime"]!=null && row["CreatTime"].ToString()!="")
				{
					model.CreatTime=DateTime.Parse(row["CreatTime"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select Id,CusId,addName,addTel,addMail,address,IsDefault,CreatTime ");
			strSql.Append(" FROM CRM_Customer_Address ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" Id,CusId,addName,addTel,addMail,address,IsDefault,CreatTime ");
			strSql.Append(" FROM CRM_Customer_Address ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM CRM_Customer_Address ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.Id desc");
			}
			strSql.Append(")AS Row, T.*  from CRM_Customer_Address T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "CRM_Customer_Address";
			parameters[1].Value = "Id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

