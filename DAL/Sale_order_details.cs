﻿
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using XHD.DBUtility;//Please add references
namespace XHD.DAL
{
    /// <summary>
	/// 数据访问类:Sale_order_details
	/// </summary>
	public partial class Sale_order_details
    {
        public Sale_order_details()
        { }

        #region  BasicMethod
        
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(XHD.Model.Sale_order_details model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into Sale_order_details(");
            strSql.Append("order_id,product_id,agio,quantity,amount,detail_status,capacity,uspd)");
            strSql.Append(" values (");
            strSql.Append("@order_id,@product_id,@agio,@quantity,@amount,@detail_status,@capacity,@uspd)");
            SqlParameter[] parameters = {
                    new SqlParameter("@order_id", SqlDbType.VarChar,250),
                    new SqlParameter("@product_id", SqlDbType.VarChar,250),
                    new SqlParameter("@agio", SqlDbType.Decimal,9),
                    new SqlParameter("@quantity", SqlDbType.Int,4),
                    new SqlParameter("@amount", SqlDbType.Decimal,9),
                    new SqlParameter("@detail_status", SqlDbType.VarChar,10),
                    new SqlParameter("@capacity", SqlDbType.Decimal,9),
                    new SqlParameter("@uspd", SqlDbType.Int, 4)};
            
            parameters[0].Value = model.order_id;
            parameters[1].Value = model.product_id;
            parameters[2].Value = model.agio;
            parameters[3].Value = model.quantity;
            parameters[4].Value = model.amount;
            parameters[5].Value = model.detail_status;
            parameters[6].Value = model.capacity;
            parameters[7].Value = model.uspd;
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XHD.Model.Sale_order_details model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update Sale_order_details set ");
            //strSql.Append("product_id=@product_id,");
            strSql.Append("agio=@agio,");
            strSql.Append("quantity=@quantity,");
            //strSql.Append("detail_status=@detail_status,");
            strSql.Append("capacity=@capacity,");
            strSql.Append("uspd=@uspd ");
            strSql.Append(" where order_id=@order_id and product_id=@product_id  ");
            SqlParameter[] parameters = {
                    new SqlParameter("@order_id", SqlDbType.VarChar,250),
                    new SqlParameter("@product_id", SqlDbType.VarChar,250),
                    new SqlParameter("@agio", SqlDbType.Decimal,9),
                    new SqlParameter("@quantity", SqlDbType.Int,4),
                    new SqlParameter("@amount", SqlDbType.Decimal,9),
                new SqlParameter("@capacity", SqlDbType.Decimal,9),
            new SqlParameter("@uspd", SqlDbType.Int,4)};
            parameters[0].Value = model.order_id;
            parameters[1].Value = model.product_id;
            parameters[2].Value = model.agio;
            parameters[3].Value = model.quantity;
            //parameters[4].Value = model.detail_status;
            parameters[4].Value = model.amount;
            parameters[5].Value = model.capacity;
            parameters[6].Value = model.uspd;
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string whereStr)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Sale_order_details ");
            strSql.Append(" where "+whereStr);
           
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT  ");
            strSql.Append("        Sale_order_details.[order_id] ");
            strSql.Append("      , Sale_order_details.[product_id] ");
            strSql.Append("      , Sale_order_details.[agio] AS CusPrice ");
            strSql.Append("      , Sale_order_details.[quantity] ");
            strSql.Append("      , Sale_order_details.[amount] ");
            strSql.Append("      , Sale_order_details.[capacity] ");
            strSql.Append("      , Sale_order_details.[detail_status] ");
            strSql.Append("      , Sale_order_details.[uspd] ");
            strSql.Append("      , Product.product_name ");
            strSql.Append("      , Product.specifications ");
            strSql.Append("      , Product.unit ");
            strSql.Append("      , Product.price ");
            strSql.Append("      , Product.agio ");
            strSql.Append("      , Product.dailyoutput ");
            strSql.Append("      , Product.pickcycle ");
            strSql.Append("      , Product_category.product_category ");
            strSql.Append("     ,   Sale_order_details.[quantity]*1.0000*Product.agio as amount_agio "); 
            strSql.Append("     , case when Product.dailyoutput=0 then 0 else  ceiling(Sale_order_details.[quantity]*1.0000/Product.dailyoutput*(Sale_order_details.capacity/Product.price)) end as mrp ");
            strSql.Append("FROM[dbo].[Sale_order_details] ");
            strSql.Append("  left JOIN Product ON Product.id = Sale_order_details.product_id ");
            strSql.Append("  left JOIN Product_category ON Product.category_id = Product_category.id ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }
        public DataSet GetListboar(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT  ");
            strSql.Append("        Sale_order_details.[order_id] ");
            strSql.Append("      , Sale_order_details.[product_id] ");
            strSql.Append("      , Sale_order_details.[agio] ");
            strSql.Append("      , Sale_order_details.[quantity] ");
            strSql.Append("      , Sale_order_details.[amount] ");
            strSql.Append("      , Sale_order_details.[capacity] ");
            strSql.Append("      , Sale_order_details.[detail_status] ");
            strSql.Append("      , Sale_order_details.[uspd] ");   
            strSql.Append("      , Product.product_name ");
            strSql.Append("      , Product.specifications ");
            strSql.Append("      , Product.unit ");
            strSql.Append("      , Product.price ");
            strSql.Append("      , Product.dailyoutput ");
            strSql.Append("      , Product.pickcycle ");
            strSql.Append("      , Product_category.product_category ");
            strSql.Append("      , case when Product.dailyoutput=0 then 0 else  ceiling(Sale_order_details.[quantity]*1.0000/Product.dailyoutput*(Sale_order_details.capacity/Product.price)) end as mrp ");
            strSql.Append("      , bc.* ");
            strSql.Append(" FROM [dbo].[Sale_order_details] ");
            strSql.Append("  INNER JOIN Product ON Product.id = Sale_order_details.product_id ");
            strSql.Append("  left  JOIN v_boar_collection bc ON Sale_order_details.product_id = bc.c_ordermxid and Sale_order_details.order_id=bc.c_orderid ");
            strSql.Append("  left JOIN Product_category ON Product.category_id = Product_category.id ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append("        Sale_order_details.[order_id] ");
            strSql.Append("      , Sale_order_details.[product_id] ");
            strSql.Append("      , Sale_order_details.[agio] ");
            strSql.Append("      , Sale_order_details.[quantity] ");
            strSql.Append("      , Sale_order_details.[amount] ");
            strSql.Append("      , Sale_order_details.[capacity] ");
            strSql.Append("      , Sale_order_details.[detail_status], ");
            strSql.Append("      , Sale_order_details.[uspd] ");
            strSql.Append("      , Product.product_name ");
            strSql.Append("      , Product.specifications ");
            strSql.Append("      , Product.unit ");
            strSql.Append("      , Product.price ");
            strSql.Append("      , Product.dailyoutput ");
            strSql.Append("      , Product.pickcycle ");
            strSql.Append("      , Product_category.product_category ");
            strSql.Append("      ,  case when Product.dailyoutput=0 then 0 else  ceiling(Sale_order_details.[quantity]*1.0000/Product.dailyoutput*(Sale_order_details.capacity/Product.price)) end as mrp  ");
            strSql.Append("FROM[dbo].[Sale_order_details] ");

            strSql.Append("  left JOIN Product ON Product.id = Sale_order_details.product_id ");
            strSql.Append("  left JOIN Product_category ON Product.category_id = Product_category.id ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }
        #endregion  BasicMethod
        #region  ExtensionMethod
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList_OrderDetail(int PageSize, int PageIndex, string strWhere, string filedOrder, out string Total)
        {
            StringBuilder strSql_inner = new StringBuilder();
            StringBuilder strSql_grid = new StringBuilder();
            StringBuilder strSql_total = new StringBuilder();

            //联合数据
            strSql_inner.Append("( SELECT * ");
            strSql_inner.Append($"        ,ROW_NUMBER() OVER(Order by {filedOrder}) AS n FROM ");
            strSql_inner.Append("   (");
            strSql_inner.Append("        SELECT  ");
            strSql_inner.Append("            * ");  
            strSql_inner.Append("        FROM(");
            strSql_inner.AppendLine(" SELECT b.*,C.cus_name,a.*,D.product_name,D.unit,D.specifications  ");
            strSql_inner.AppendLine(" ,CASE WHEN e.orderid IS NULL AND b.detail_status='0' THEN '未分配' ");
            strSql_inner.AppendLine("  WHEN e.orderid IS NOT NULL AND b.detail_status='0' THEN '正在分配'");
            strSql_inner.AppendLine("  WHEN b.detail_status =' 1' THEN '分配完成' ELSE '未知' END zhtms,");
            strSql_inner.AppendLine("d.price,d.dailyoutput,d.pickcycle, case when d.dailyoutput=0 then 0 else CASE WHEN d.price=0 THEN 0 ELSE  CEILING(b.[quantity]*1.0000/d.dailyoutput*(b.capacity/d.price)) END END  as mrp ");
            strSql_inner.AppendLine(",isnull(e.yscsl,0) yscsl,dd.product_category,D.id as Pid ");

           strSql_inner.AppendLine("  FROM  dbo.Sale_order a  ");
            strSql_inner.AppendLine(" INNER JOIN dbo.Sale_order_details b ON a.id=b.order_id  ");
            strSql_inner.AppendLine(" left JOIN dbo.CRM_Customer C ON	a.Customer_id=C.id  ");
            strSql_inner.AppendLine(" left JOIN dbo.Product D ON b.product_id = D.id ");
                        strSql_inner.AppendLine(" left JOIN dbo.Product_category Dd ON d.category_id = dD.id ");
            strSql_inner.AppendLine(" left JOIN ( SELECT orderid,ordermxid,COUNT(*) yscsl FROM  boar_collection WHERE IsStatus<>'NN' GROUP BY orderid,ordermxid) e ON e.orderid=a.id and e.ordermxid=b.product_id ");
            //strSql_inner.AppendLine(" WHERE a.IsStatus='Y' AND b.detail_status='0'  ");
            strSql_inner.AppendLine(" WHERE a.IsStatus='Y'  ");
            strSql_inner.AppendLine("  ) as w1  ");

            strSql_inner.Append("   ) w2 ");
            if (strWhere.Trim() != "")
            {
                strSql_inner.Append(" WHERE " + strWhere);
            }
            strSql_inner.Append(") W3");

            //Total数据
            strSql_total.Append(" SELECT COUNT(ID) FROM ");
            strSql_total.Append(strSql_inner.ToString());

            //grid数据
            strSql_grid.Append(" SELECT * FROM ");
            strSql_grid.Append(strSql_inner.ToString());
            strSql_grid.Append(" WHERE n BETWEEN " + PageSize * (PageIndex - 1) + " AND " + PageSize * PageIndex);

            Total = DbHelperSQL.Query(strSql_total.ToString()).Tables[0].Rows[0][0].ToString();
            return DbHelperSQL.Query(strSql_grid.ToString());
        }

        #endregion  ExtensionMethod
    }
}

