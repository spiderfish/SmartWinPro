﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using XHD.DBUtility;//Please add references
namespace XHD.DAL
{
	/// <summary>
	/// 数据访问类:Basic_corral
	/// </summary>
	public partial class Basic_corral
	{
		public Basic_corral()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Basic_corral");
			strSql.Append(" where id=@id ");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.VarChar,50)			};
			parameters[0].Value = id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(XHD.Model.Basic_corral model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Basic_corral(");
			strSql.Append("id,corralName,dornId,area,remarks,create_id,create_time,def1,def2)");
			strSql.Append(" values (");
			strSql.Append("@id,@corralName,@dornId,@area,@remarks,@create_id,@create_time,@def1,@def2)");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.VarChar,50),
					new SqlParameter("@corralName", SqlDbType.VarChar,50),
					new SqlParameter("@dornId", SqlDbType.VarChar,50),
					new SqlParameter("@area", SqlDbType.Float,8),
					new SqlParameter("@remarks", SqlDbType.VarChar,50),
					new SqlParameter("@create_id", SqlDbType.VarChar,50),
					new SqlParameter("@create_time", SqlDbType.DateTime),
					new SqlParameter("@def1", SqlDbType.VarChar,50),
					new SqlParameter("@def2", SqlDbType.VarChar,50)};
			parameters[0].Value = model.id;
			parameters[1].Value = model.corralName;
			parameters[2].Value = model.dornId;
			parameters[3].Value = model.area;
			parameters[4].Value = model.remarks;
			parameters[5].Value = model.create_id;
			parameters[6].Value = model.create_time;
			parameters[7].Value = model.def1;
			parameters[8].Value = model.def2;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XHD.Model.Basic_corral model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Basic_corral set ");
			strSql.Append("corralName=@corralName,");
			strSql.Append("dornId=@dornId,");
			strSql.Append("area=@area,");
			strSql.Append("remarks=@remarks,");
			strSql.Append("create_id=@create_id,");
			strSql.Append("create_time=@create_time,");
			strSql.Append("def1=@def1,");
			strSql.Append("def2=@def2");
			strSql.Append(" where id=@id ");
			SqlParameter[] parameters = {
					new SqlParameter("@corralName", SqlDbType.VarChar,50),
					new SqlParameter("@dornId", SqlDbType.VarChar,50),
					new SqlParameter("@area", SqlDbType.Float,8),
					new SqlParameter("@remarks", SqlDbType.VarChar,50),
					new SqlParameter("@create_id", SqlDbType.VarChar,50),
					new SqlParameter("@create_time", SqlDbType.DateTime),
					new SqlParameter("@def1", SqlDbType.VarChar,50),
					new SqlParameter("@def2", SqlDbType.VarChar,50),
					new SqlParameter("@id", SqlDbType.VarChar,50)};
			parameters[0].Value = model.corralName;
			parameters[1].Value = model.dornId;
			parameters[2].Value = model.area;
			parameters[3].Value = model.remarks;
			parameters[4].Value = model.create_id;
			parameters[5].Value = model.create_time;
			parameters[6].Value = model.def1;
			parameters[7].Value = model.def2;
			parameters[8].Value = model.id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Basic_corral ");
			strSql.Append(" where id=@id ");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.VarChar,50)			};
			parameters[0].Value = id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Basic_corral ");
			strSql.Append(" where id in ("+idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XHD.Model.Basic_corral GetModel(string id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 id,corralName,dornId,area,remarks,create_id,create_time,def1,def2 from Basic_corral ");
			strSql.Append(" where id=@id ");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.VarChar,50)			};
			parameters[0].Value = id;

			XHD.Model.Basic_corral model=new XHD.Model.Basic_corral();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XHD.Model.Basic_corral DataRowToModel(DataRow row)
		{
			XHD.Model.Basic_corral model=new XHD.Model.Basic_corral();
			if (row != null)
			{
				if(row["id"]!=null)
				{
					model.id=row["id"].ToString();
				}
				if(row["corralName"]!=null)
				{
					model.corralName=row["corralName"].ToString();
				}
				if(row["dornId"]!=null)
				{
					model.dornId=row["dornId"].ToString();
				}
				if(row["area"]!=null && row["area"].ToString()!="")
				{
					model.area=decimal.Parse(row["area"].ToString());
				}
				if(row["remarks"]!=null)
				{
					model.remarks=row["remarks"].ToString();
				}
				if(row["create_id"]!=null)
				{
					model.create_id=row["create_id"].ToString();
				}
				if(row["create_time"]!=null && row["create_time"].ToString()!="")
				{
					model.create_time=DateTime.Parse(row["create_time"].ToString());
				}
				if(row["def1"]!=null)
				{
					model.def1=row["def1"].ToString();
				}
				if(row["def2"]!=null)
				{
					model.def2=row["def2"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select id,corralName,dornId,area,remarks,create_id,create_time,def1,def2 ");
			strSql.Append(" FROM Basic_corral ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" id,corralName,dornId,area,remarks,create_id,create_time,def1,def2 ");
			strSql.Append(" FROM Basic_corral ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Basic_corral ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.id desc");
			}
			strSql.Append(")AS Row, T.*  from Basic_corral T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Basic_corral";
			parameters[1].Value = "id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod
		public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, out string Total)
		{
			StringBuilder strSql_grid = new StringBuilder();
			StringBuilder strSql_total = new StringBuilder();
			strSql_total.Append(" SELECT COUNT(id) FROM Basic_corral ");
			strSql_grid.Append("SELECT ");
			strSql_grid.Append("      n,* ");
			strSql_grid.Append(" FROM ( SELECT *, ROW_NUMBER() OVER( Order by " + filedOrder + " ) AS n from Basic_corral");
			if (strWhere.Trim() != "")
			{
				strSql_grid.Append(" WHERE " + strWhere);
				strSql_total.Append(" WHERE " + strWhere);
			}
			strSql_grid.Append("  ) as w1  ");
			strSql_grid.Append("WHERE n BETWEEN " + (PageSize * (PageIndex - 1) + 1) + " AND " + PageSize * PageIndex);
			strSql_grid.Append(" ORDER BY " + filedOrder);
			Total = DbHelperSQL.Query(strSql_total.ToString()).Tables[0].Rows[0][0].ToString();
			return DbHelperSQL.Query(strSql_grid.ToString());
		}

		public DataSet GetListHealth(int PageSize, int PageIndex, string strWhere, string filedOrder, out string Total)
		{
			StringBuilder sb = new StringBuilder();
			sb.AppendLine(" SELECT   A.*,B.id AS BoarId,B.barcode,AA.createTime AS checkData ,BB.temperature,BB.humidity  ");
			sb.AppendLine("  FROM  dbo.Basic_corral A  ");
			sb.AppendLine(" LEFT JOIN dbo.Basic_boar B ON A.dornId=B.dormId AND  B.corralId=A.id  ");
			sb.AppendLine(" LEFT JOIN  (  ");
			sb.AppendLine(" SELECT  MAX(createTime)createTime,dornId,corralId  FROM dbo.Basic_corral_check_log  GROUP BY dornId,corralId  ");
			sb.AppendLine(" )AA  ON	 A.id=AA.corralId AND A.dornId=AA.dornId  ");
			sb.AppendLine(" LEFT JOIN Basic_corral_check_log BB ON	 	 A.id=AA.corralId AND A.dornId=AA.dornId AND AA.createTime=BB.createTime  ");
			sb.AppendLine("    ");

			StringBuilder strSql_grid = new StringBuilder();
			StringBuilder strSql_total = new StringBuilder();
			strSql_total.Append(" SELECT COUNT(id) FROM ("+sb.ToString()+")AAA");
			strSql_grid.Append("SELECT ");
			strSql_grid.Append("      n,* ");
			strSql_grid.Append(" FROM ( SELECT *, ROW_NUMBER() OVER( Order by " + filedOrder + " ) AS n from (" + sb.ToString() + ")AAA");
			if (strWhere.Trim() != "")
			{
				strSql_grid.Append(" WHERE " + strWhere);
				strSql_total.Append(" WHERE " + strWhere);
			}
			strSql_grid.Append("  ) as w1  ");
			strSql_grid.Append("WHERE n BETWEEN " + (PageSize * (PageIndex - 1) + 1) + " AND " + PageSize * PageIndex);
			strSql_grid.Append(" ORDER BY " + filedOrder);
			Total = DbHelperSQL.Query(strSql_total.ToString()).Tables[0].Rows[0][0].ToString();
			return DbHelperSQL.Query(strSql_grid.ToString());
		}


		public DataSet GetListLog(int PageSize, int PageIndex, string strWhere, string filedOrder, out string Total)
		{
			StringBuilder strSql_grid = new StringBuilder();
			StringBuilder strSql_total = new StringBuilder();
			strSql_total.Append(" SELECT COUNT(id) FROM Basic_corral_check_log ");
			strSql_grid.Append("SELECT ");
			strSql_grid.Append("      n,* ");
			strSql_grid.Append(" FROM ( SELECT *, ROW_NUMBER() OVER( Order by " + filedOrder + " ) AS n from Basic_corral_check_log");
			if (strWhere.Trim() != "")
			{
				strSql_grid.Append(" WHERE " + strWhere);
				strSql_total.Append(" WHERE " + strWhere);
			}
			strSql_grid.Append("  ) as w1  ");
			strSql_grid.Append("WHERE n BETWEEN " + (PageSize * (PageIndex - 1) + 1) + " AND " + PageSize * PageIndex);
			strSql_grid.Append(" ORDER BY " + filedOrder);
			Total = DbHelperSQL.Query(strSql_total.ToString()).Tables[0].Rows[0][0].ToString();
			return DbHelperSQL.Query(strSql_grid.ToString());
		}
		#endregion  ExtensionMethod
	}
}

