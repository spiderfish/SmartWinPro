﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using XHD.DBUtility;//Please add references
namespace XHD.DAL
{
	/// <summary>
	/// 数据访问类:Product_Customer_Price
	/// </summary>
	public partial class Product_Customer_Price
	{
		public Product_Customer_Price()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string product_id,string customer_id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Product_Customer_Price");
			strSql.Append(" where product_id=@product_id and customer_id=@customer_id ");
			SqlParameter[] parameters = {
					new SqlParameter("@product_id", SqlDbType.VarChar,50),
					new SqlParameter("@customer_id", SqlDbType.VarChar,50)			};
			parameters[0].Value = product_id;
			parameters[1].Value = customer_id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(XHD.Model.Product_Customer_Price model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Product_Customer_Price(");
			strSql.Append("product_id,customer_id,price,createTime,createName,remarks)");
			strSql.Append(" values (");
			strSql.Append("@product_id,@customer_id,@price,@createTime,@createName,@remarks)");
			SqlParameter[] parameters = {
					new SqlParameter("@product_id", SqlDbType.VarChar,50),
					new SqlParameter("@customer_id", SqlDbType.VarChar,50),
					new SqlParameter("@price", SqlDbType.Decimal,9),
					new SqlParameter("@createTime", SqlDbType.DateTime),
					new SqlParameter("@createName", SqlDbType.VarChar,20),
			new SqlParameter("@remarks", SqlDbType.VarChar,50)};
			parameters[0].Value = model.product_id;
			parameters[1].Value = model.customer_id;
			parameters[2].Value = model.price;
			parameters[3].Value = model.createTime;
			parameters[4].Value = model.createName;
			parameters[4].Value = model.remarks;
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XHD.Model.Product_Customer_Price model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Product_Customer_Price set ");
			strSql.Append("price=@price,");
			strSql.Append("createTime=@createTime,");
			strSql.Append("remarks=@remarks,");
			strSql.Append("createName=@createName");
			strSql.Append(" where product_id=@product_id and customer_id=@customer_id ");
			SqlParameter[] parameters = {
					new SqlParameter("@price", SqlDbType.Decimal,9),
					new SqlParameter("@createTime", SqlDbType.DateTime),
						new SqlParameter("@remarks", SqlDbType.VarChar,50),
					new SqlParameter("@createName", SqlDbType.VarChar,20),
					new SqlParameter("@product_id", SqlDbType.VarChar,50),
					new SqlParameter("@customer_id", SqlDbType.VarChar,50)};
			parameters[0].Value = model.price;
			parameters[1].Value = model.createTime;
			parameters[2].Value = model.remarks;
			parameters[3].Value = model.createName;
			parameters[4].Value = model.product_id;
			parameters[5].Value = model.customer_id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string product_id,string customer_id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Product_Customer_Price ");
			strSql.Append(" where product_id=@product_id and customer_id=@customer_id ");
			SqlParameter[] parameters = {
					new SqlParameter("@product_id", SqlDbType.VarChar,50),
					new SqlParameter("@customer_id", SqlDbType.VarChar,50)			};
			parameters[0].Value = product_id;
			parameters[1].Value = customer_id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XHD.Model.Product_Customer_Price GetModel(string product_id,string customer_id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 product_id,customer_id,price,createTime,createName from Product_Customer_Price ");
			strSql.Append(" where product_id=@product_id and customer_id=@customer_id ");
			SqlParameter[] parameters = {
					new SqlParameter("@product_id", SqlDbType.VarChar,50),
					new SqlParameter("@customer_id", SqlDbType.VarChar,50)			};
			parameters[0].Value = product_id;
			parameters[1].Value = customer_id;

			XHD.Model.Product_Customer_Price model=new XHD.Model.Product_Customer_Price();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XHD.Model.Product_Customer_Price DataRowToModel(DataRow row)
		{
			XHD.Model.Product_Customer_Price model=new XHD.Model.Product_Customer_Price();
			if (row != null)
			{
				if(row["product_id"]!=null)
				{
					model.product_id=row["product_id"].ToString();
				}
				if(row["customer_id"]!=null)
				{
					model.customer_id=row["customer_id"].ToString();
				}
				if(row["price"]!=null && row["price"].ToString()!="")
				{
					model.price=decimal.Parse(row["price"].ToString());
				}
				if(row["createTime"]!=null && row["createTime"].ToString()!="")
				{
					model.createTime=DateTime.Parse(row["createTime"].ToString());
				}
				if(row["createName"]!=null)
				{
					model.createName=row["createName"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select product_id,customer_id,price,createTime,createName,cus_name ");
			strSql.Append(" FROM Product_Customer_Price A");
			strSql.Append(" INNER JOIN dbo.CRM_Customer B ON A.customer_id = B.id ");
			if (strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" product_id,customer_id,price,createTime,createName ");
			strSql.Append(" FROM Product_Customer_Price ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Product_Customer_Price ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.customer_id desc");
			}
			strSql.Append(")AS Row, T.*  from Product_Customer_Price T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Product_Customer_Price";
			parameters[1].Value = "customer_id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, out string Total)
		{
			StringBuilder strSql_inner = new StringBuilder();
			StringBuilder strSql_grid = new StringBuilder();
			StringBuilder strSql_total = new StringBuilder();

			//联合数据
			strSql_inner.Append("( SELECT  product_id,customer_id,price,createTime,remarks,payType, payCycle, payTypeName ");
			strSql_inner.Append("        ,createName,cus_name, product_name ");
			strSql_inner.Append($"        ,ROW_NUMBER() OVER(Order by {filedOrder}) AS n FROM ");
			strSql_inner.Append("   (");
			strSql_inner.Append("       SELECT A.*,B.cus_name,C.product_name,B.payType,B.payCycle,D.params_name as payTypeName   FROM dbo.Product_Customer_Price A ");
			strSql_inner.Append(" INNER JOIN dbo.CRM_Customer B ON A.customer_id = B.id ");
			strSql_inner.Append(" INNER JOIN  dbo.Product C ON     A.product_id = C.id ");
			strSql_inner.Append(" LEFT JOIN  dbo.Sys_Param D ON     B.payType = D.id ");
			strSql_inner.Append("   ) w2 ");
			if (strWhere.Trim() != "")
			{
				strSql_inner.Append(" WHERE " + strWhere);
			}
			strSql_inner.Append(") W3");

			//Total数据
			strSql_total.Append(" SELECT COUNT(product_id) FROM ");
			strSql_total.Append(strSql_inner.ToString());

			//grid数据
			strSql_grid.Append(" SELECT * FROM ");
			strSql_grid.Append(strSql_inner.ToString());
			strSql_grid.Append(" WHERE n BETWEEN " + PageSize * (PageIndex - 1) + " AND " + PageSize * PageIndex);

			Total = DbHelperSQL.Query(strSql_total.ToString()).Tables[0].Rows[0][0].ToString();
			return DbHelperSQL.Query(strSql_grid.ToString());
		}

		#endregion  ExtensionMethod
	}
}

