﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using XHD.DBUtility;//Please add references
namespace XHD.DAL
{
	/// <summary>
	/// 数据访问类:Basic_Drug
	/// </summary>
	public partial class Basic_Drug
	{
		public Basic_Drug()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string IDCode)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Basic_Drug");
			strSql.Append(" where IDCode=@IDCode ");
			SqlParameter[] parameters = {
					new SqlParameter("@IDCode", SqlDbType.VarChar,50)			};
			parameters[0].Value = IDCode;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(XHD.Model.Basic_Drug model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Basic_Drug(");
			strSql.Append("IDCode,drugName,drugcycle,Remarks,unit,unit_id)");
			strSql.Append(" values (");
			strSql.Append("@IDCode,@drugName,@drugcycle,@Remarks,@unit,@unit_id)");
			SqlParameter[] parameters = {
					new SqlParameter("@IDCode", SqlDbType.VarChar,50),
					new SqlParameter("@drugName", SqlDbType.VarChar,150),
					new SqlParameter("@drugcycle", SqlDbType.Float,8),
					new SqlParameter("@Remarks", SqlDbType.VarChar,50),
				new SqlParameter("@unit", SqlDbType.VarChar,50),
				new SqlParameter("@unit_id", SqlDbType.VarChar,50)};
			parameters[0].Value = model.IDCode;
			parameters[1].Value = model.drugName;
			parameters[2].Value = model.drugcycle;
			parameters[3].Value = model.Remarks;
			parameters[4].Value = model.unit;
			parameters[5].Value = model.unit_id;
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XHD.Model.Basic_Drug model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Basic_Drug set ");
			strSql.Append("drugName=@drugName,");
			strSql.Append("drugcycle=@drugcycle,");
			strSql.Append("unit=@unit,");
			strSql.Append("unit_id=@unit_id,");
			strSql.Append("Remarks=@Remarks");
			strSql.Append(" where IDCode=@IDCode ");
			SqlParameter[] parameters = {
					new SqlParameter("@drugName", SqlDbType.VarChar,150),
					new SqlParameter("@drugcycle", SqlDbType.Float,8),
					new SqlParameter("@Remarks", SqlDbType.VarChar,50),
					new SqlParameter("@unit", SqlDbType.VarChar,50),
					new SqlParameter("@unit_id", SqlDbType.VarChar,50),
					new SqlParameter("@IDCode", SqlDbType.VarChar,50)};
			parameters[0].Value = model.drugName;
			parameters[1].Value = model.drugcycle;
			parameters[2].Value = model.Remarks;
			parameters[3].Value = model.IDCode;
			parameters[4].Value = model.unit;
			parameters[5].Value = model.unit_id;
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string IDCode)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Basic_Drug ");
			strSql.Append(" where IDCode=@IDCode ");
			SqlParameter[] parameters = {
					new SqlParameter("@IDCode", SqlDbType.VarChar,50)			};
			parameters[0].Value = IDCode;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDCodelist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Basic_Drug ");
			strSql.Append(" where IDCode in ("+IDCodelist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XHD.Model.Basic_Drug GetModel(string IDCode)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 IDCode,drugName,drugcycle,Remarks,unit,unit_id from Basic_Drug ");
			strSql.Append(" where IDCode=@IDCode ");
			SqlParameter[] parameters = {
					new SqlParameter("@IDCode", SqlDbType.VarChar,50)			};
			parameters[0].Value = IDCode;

			XHD.Model.Basic_Drug model=new XHD.Model.Basic_Drug();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XHD.Model.Basic_Drug DataRowToModel(DataRow row)
		{
			XHD.Model.Basic_Drug model=new XHD.Model.Basic_Drug();
			if (row != null)
			{
				if(row["IDCode"]!=null)
				{
					model.IDCode=row["IDCode"].ToString();
				}
				if(row["drugName"]!=null)
				{
					model.drugName=row["drugName"].ToString();
				}
				if(row["drugcycle"]!=null && row["drugcycle"].ToString()!="")
				{
					model.drugcycle=decimal.Parse(row["drugcycle"].ToString());
				}
				if(row["Remarks"]!=null)
				{
					model.Remarks=row["Remarks"].ToString();
				}
				if (row["unit"] != null)
				{
					model.unit = row["unit"].ToString();
				}
				if (row["unit_id"] != null)
				{
					model.unit_id = row["unit_id"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select IDCode,drugName,drugcycle,Remarks,unit,unit_id ");
			strSql.Append(" FROM Basic_Drug ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" IDCode,drugName,drugcycle,Remarks,unit,unit_id ");
			strSql.Append(" FROM Basic_Drug ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Basic_Drug ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.IDCode desc");
			}
			strSql.Append(")AS Row, T.*  from Basic_Drug T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

        /*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Basic_Drug";
			parameters[1].Value = "IDCode";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

        #endregion  BasicMethod
        #region  ExtensionMethod
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, out string Total)
        {
            StringBuilder strSql_grid = new StringBuilder();
            StringBuilder strSql_total = new StringBuilder();
            strSql_total.Append(" SELECT COUNT(IDCode) FROM Basic_Drug ");
            strSql_grid.Append("SELECT ");
            strSql_grid.Append("      n,IDCode ,drugName,drugcycle, Remarks,unit,unit_id "); 
            strSql_grid.Append(" FROM ( SELECT IDCode ,drugName,drugcycle,unit,unit_id, Remarks, ROW_NUMBER() OVER( Order by " + filedOrder + " ) AS n from Basic_Drug");
            if (strWhere.Trim() != "")
            {
                strSql_grid.Append(" WHERE " + strWhere);
                strSql_total.Append(" WHERE " + strWhere);
            }
            strSql_grid.Append("  ) as w1  ");
            strSql_grid.Append("WHERE n BETWEEN " + (PageSize * (PageIndex - 1) + 1) + " AND " + PageSize * PageIndex);
            strSql_grid.Append(" ORDER BY " + filedOrder);
            Total = DbHelperSQL.Query(strSql_total.ToString()).Tables[0].Rows[0][0].ToString();
            return DbHelperSQL.Query(strSql_grid.ToString());
        }

        #endregion  ExtensionMethod
    }
}

