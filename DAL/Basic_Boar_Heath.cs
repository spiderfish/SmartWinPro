﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using XHD.DBUtility;//Please add references
namespace XHD.DAL
{
	/// <summary>
	/// 数据访问类:Basic_Boar_Heath
	/// </summary>
	public partial class Basic_Boar_Heath
	{
		public Basic_Boar_Heath()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Basic_Boar_Heath");
			strSql.Append(" where id=@id ");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.VarChar,50)			};
			parameters[0].Value = id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(XHD.Model.Basic_Boar_Heath model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Basic_Boar_Heath(");
			strSql.Append("id,boarId,weight,backfat,eyeArea,temperature,serum,semen,drinkWater,feed,Feces,saliva,pigHeath,dormHeath,others,remarks,monitorPerson,monitorDate,createPerson,createDate)");
			strSql.Append(" values (");
			strSql.Append("@id,@boarId,@weight,@backfat,@eyeArea,@temperature,@serum,@semen,@drinkWater,@feed,@Feces,@saliva,@pigHeath,@dormHeath,@others,@remarks,@monitorPerson,@monitorDate,@createPerson,@createDate)");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.VarChar,50),
					new SqlParameter("@boarId", SqlDbType.VarChar,50),
					new SqlParameter("@weight", SqlDbType.Float,8),
					new SqlParameter("@backfat", SqlDbType.Float,8),
					new SqlParameter("@eyeArea", SqlDbType.Float,8),
					new SqlParameter("@temperature", SqlDbType.Float,8),
					new SqlParameter("@serum", SqlDbType.Char,1),
					new SqlParameter("@semen", SqlDbType.Char,1),
					new SqlParameter("@drinkWater", SqlDbType.Char,1),
					new SqlParameter("@feed", SqlDbType.Char,1),
					new SqlParameter("@Feces", SqlDbType.Char,1),
					new SqlParameter("@saliva", SqlDbType.Char,1),
					new SqlParameter("@pigHeath", SqlDbType.Char,1),
					new SqlParameter("@dormHeath", SqlDbType.Char,1),
					new SqlParameter("@others", SqlDbType.VarChar,50),
					new SqlParameter("@remarks", SqlDbType.VarChar,250),
					new SqlParameter("@monitorPerson", SqlDbType.VarChar,20),
					new SqlParameter("@monitorDate", SqlDbType.DateTime),
					new SqlParameter("@createPerson", SqlDbType.VarChar,20),
					new SqlParameter("@createDate", SqlDbType.DateTime)};
			parameters[0].Value = model.id;
			parameters[1].Value = model.boarId;
			parameters[2].Value = model.weight;
			parameters[3].Value = model.backfat;
			parameters[4].Value = model.eyeArea;
			parameters[5].Value = model.temperature;
			parameters[6].Value = model.serum;
			parameters[7].Value = model.semen;
			parameters[8].Value = model.drinkWater;
			parameters[9].Value = model.feed;
			parameters[10].Value = model.Feces;
			parameters[11].Value = model.saliva;
			parameters[12].Value = model.pigHeath;
			parameters[13].Value = model.dormHeath;
			parameters[14].Value = model.others;
			parameters[15].Value = model.remarks;
			parameters[16].Value = model.monitorPerson;
			parameters[17].Value = model.monitorDate;
			parameters[18].Value = model.createPerson;
			parameters[19].Value = model.createDate;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XHD.Model.Basic_Boar_Heath model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Basic_Boar_Heath set ");
			strSql.Append("boarId=@boarId,");
			strSql.Append("weight=@weight,");
			strSql.Append("backfat=@backfat,");
			strSql.Append("eyeArea=@eyeArea,");
			strSql.Append("temperature=@temperature,");
			strSql.Append("serum=@serum,");
			strSql.Append("semen=@semen,");
			strSql.Append("drinkWater=@drinkWater,");
			strSql.Append("feed=@feed,");
			strSql.Append("Feces=@Feces,");
			strSql.Append("saliva=@saliva,");
			strSql.Append("pigHeath=@pigHeath,");
			strSql.Append("dormHeath=@dormHeath,");
			strSql.Append("others=@others,");
			strSql.Append("remarks=@remarks,");
			strSql.Append("monitorPerson=@monitorPerson,");
			strSql.Append("monitorDate=@monitorDate,");
			strSql.Append("createPerson=@createPerson,");
			strSql.Append("createDate=@createDate");
			strSql.Append(" where id=@id ");
			SqlParameter[] parameters = {
					new SqlParameter("@boarId", SqlDbType.VarChar,50),
					new SqlParameter("@weight", SqlDbType.Float,8),
					new SqlParameter("@backfat", SqlDbType.Float,8),
					new SqlParameter("@eyeArea", SqlDbType.Float,8),
					new SqlParameter("@temperature", SqlDbType.Float,8),
					new SqlParameter("@serum", SqlDbType.Char,1),
					new SqlParameter("@semen", SqlDbType.Char,1),
					new SqlParameter("@drinkWater", SqlDbType.Char,1),
					new SqlParameter("@feed", SqlDbType.Char,1),
					new SqlParameter("@Feces", SqlDbType.Char,1),
					new SqlParameter("@saliva", SqlDbType.Char,1),
					new SqlParameter("@pigHeath", SqlDbType.Char,1),
					new SqlParameter("@dormHeath", SqlDbType.Char,1),
					new SqlParameter("@others", SqlDbType.VarChar,50),
					new SqlParameter("@remarks", SqlDbType.VarChar,250),
					new SqlParameter("@monitorPerson", SqlDbType.VarChar,20),
					new SqlParameter("@monitorDate", SqlDbType.DateTime),
					new SqlParameter("@createPerson", SqlDbType.VarChar,20),
					new SqlParameter("@createDate", SqlDbType.DateTime),
					new SqlParameter("@id", SqlDbType.VarChar,50)};
			parameters[0].Value = model.boarId;
			parameters[1].Value = model.weight;
			parameters[2].Value = model.backfat;
			parameters[3].Value = model.eyeArea;
			parameters[4].Value = model.temperature;
			parameters[5].Value = model.serum;
			parameters[6].Value = model.semen;
			parameters[7].Value = model.drinkWater;
			parameters[8].Value = model.feed;
			parameters[9].Value = model.Feces;
			parameters[10].Value = model.saliva;
			parameters[11].Value = model.pigHeath;
			parameters[12].Value = model.dormHeath;
			parameters[13].Value = model.others;
			parameters[14].Value = model.remarks;
			parameters[15].Value = model.monitorPerson;
			parameters[16].Value = model.monitorDate;
			parameters[17].Value = model.createPerson;
			parameters[18].Value = model.createDate;
			parameters[19].Value = model.id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Basic_Boar_Heath ");
			strSql.Append(" where id=@id ");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.VarChar,50)			};
			parameters[0].Value = id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Basic_Boar_Heath ");
			strSql.Append(" where id in ("+idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XHD.Model.Basic_Boar_Heath GetModel(string id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 id,boarId,weight,backfat,eyeArea,temperature,serum,semen,drinkWater,feed,Feces,saliva,pigHeath,dormHeath,others,remarks,monitorPerson,monitorDate,createPerson,createDate from Basic_Boar_Heath ");
			strSql.Append(" where id=@id ");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.VarChar,50)			};
			parameters[0].Value = id;

			XHD.Model.Basic_Boar_Heath model=new XHD.Model.Basic_Boar_Heath();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XHD.Model.Basic_Boar_Heath DataRowToModel(DataRow row)
		{
			XHD.Model.Basic_Boar_Heath model=new XHD.Model.Basic_Boar_Heath();
			if (row != null)
			{
				if(row["id"]!=null)
				{
					model.id=row["id"].ToString();
				}
				if(row["boarId"]!=null)
				{
					model.boarId=row["boarId"].ToString();
				}
				if(row["weight"]!=null && row["weight"].ToString()!="")
				{
					model.weight=decimal.Parse(row["weight"].ToString());
				}
				if(row["backfat"]!=null && row["backfat"].ToString()!="")
				{
					model.backfat=decimal.Parse(row["backfat"].ToString());
				}
				if(row["eyeArea"]!=null && row["eyeArea"].ToString()!="")
				{
					model.eyeArea=decimal.Parse(row["eyeArea"].ToString());
				}
				if(row["temperature"]!=null && row["temperature"].ToString()!="")
				{
					model.temperature=decimal.Parse(row["temperature"].ToString());
				}
				if(row["serum"]!=null)
				{
					model.serum=row["serum"].ToString();
				}
				if(row["semen"]!=null)
				{
					model.semen=row["semen"].ToString();
				}
				if(row["drinkWater"]!=null)
				{
					model.drinkWater=row["drinkWater"].ToString();
				}
				if(row["feed"]!=null)
				{
					model.feed=row["feed"].ToString();
				}
				if(row["Feces"]!=null)
				{
					model.Feces=row["Feces"].ToString();
				}
				if(row["saliva"]!=null)
				{
					model.saliva=row["saliva"].ToString();
				}
				if(row["pigHeath"]!=null)
				{
					model.pigHeath=row["pigHeath"].ToString();
				}
				if(row["dormHeath"]!=null)
				{
					model.dormHeath=row["dormHeath"].ToString();
				}
				if(row["others"]!=null)
				{
					model.others=row["others"].ToString();
				}
				if(row["remarks"]!=null)
				{
					model.remarks=row["remarks"].ToString();
				}
				if(row["monitorPerson"]!=null)
				{
					model.monitorPerson=row["monitorPerson"].ToString();
				}
				if(row["monitorDate"]!=null && row["monitorDate"].ToString()!="")
				{
					model.monitorDate=DateTime.Parse(row["monitorDate"].ToString());
				}
				if(row["createPerson"]!=null)
				{
					model.createPerson=row["createPerson"].ToString();
				}
				if(row["createDate"]!=null && row["createDate"].ToString()!="")
				{
					model.createDate=DateTime.Parse(row["createDate"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select id,boarId,weight,backfat,eyeArea,temperature,serum,semen,drinkWater,feed,Feces,saliva,pigHeath,dormHeath,others,remarks,monitorPerson,monitorDate,createPerson,createDate ");
			strSql.Append(" FROM Basic_Boar_Heath ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" id,boarId,weight,backfat,eyeArea,temperature,serum,semen,drinkWater,feed,Feces,saliva,pigHeath,dormHeath,others,remarks,monitorPerson,monitorDate,createPerson,createDate ");
			strSql.Append(" FROM Basic_Boar_Heath ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Basic_Boar_Heath ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.id desc");
			}
			strSql.Append(")AS Row, T.*  from Basic_Boar_Heath T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Basic_Boar_Heath";
			parameters[1].Value = "id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod
		public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, out string Total)
		{
			StringBuilder strSql_grid = new StringBuilder();
			StringBuilder strSql_total = new StringBuilder();
			strSql_total.Append(" SELECT COUNT(id) FROM Basic_Boar_Heath ");
			strSql_grid.Append("SELECT ");
			strSql_grid.Append("      n,* ");
			strSql_grid.Append(" FROM ( SELECT *, ROW_NUMBER() OVER( Order by " + filedOrder + " ) AS n from Basic_Boar_Heath");
			if (strWhere.Trim() != "")
			{
				strSql_grid.Append(" WHERE " + strWhere);
				strSql_total.Append(" WHERE " + strWhere);
			}
			strSql_grid.Append("  ) as w1  ");
			strSql_grid.Append("WHERE n BETWEEN " + (PageSize * (PageIndex - 1) + 1) + " AND " + PageSize * PageIndex);
			strSql_grid.Append(" ORDER BY " + filedOrder);
			Total = DbHelperSQL.Query(strSql_total.ToString()).Tables[0].Rows[0][0].ToString();
			return DbHelperSQL.Query(strSql_grid.ToString());
		}
		#endregion  ExtensionMethod
	}
}

