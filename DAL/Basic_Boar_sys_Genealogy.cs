﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using XHD.DBUtility;//Please add references
namespace XHD.DAL
{
	/// <summary>
	/// 数据访问类:Basic_Boar_sys_Genealogy
	/// </summary>
	public partial class Basic_Boar_sys_Genealogy
	{
		public Basic_Boar_sys_Genealogy()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Basic_Boar_sys_Genealogy");
			strSql.Append(" where id=@id ");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.VarChar,50)			};
			parameters[0].Value = id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(XHD.Model.Basic_Boar_sys_Genealogy model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Basic_Boar_sys_Genealogy(");
			strSql.Append("id,boarId_origin,boarId,ParFatherBoarId,ParMotherBoarId,ParF_barcode,ParF_name,ParF_remark,ParM_barcode,ParM_name,ParM_remark,levels)");
			strSql.Append(" values (");
			strSql.Append("@id,@boarId_origin,@boarId,@ParFatherBoarId,@ParMotherBoarId,@ParF_barcode,@ParF_name,@ParF_remark,@ParM_barcode,@ParM_name,@ParM_remark,@levels)");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.VarChar,50),
					new SqlParameter("@boarId_origin", SqlDbType.VarChar,50),
					new SqlParameter("@boarId", SqlDbType.VarChar,50),
					new SqlParameter("@ParFatherBoarId", SqlDbType.VarChar,50),
					new SqlParameter("@ParMotherBoarId", SqlDbType.VarChar,50),
					new SqlParameter("@ParF_barcode", SqlDbType.VarChar,50),
					new SqlParameter("@ParF_name", SqlDbType.VarChar,50),
					new SqlParameter("@ParF_remark", SqlDbType.VarChar,50),
					new SqlParameter("@ParM_barcode", SqlDbType.VarChar,50),
					new SqlParameter("@ParM_name", SqlDbType.VarChar,50),
					new SqlParameter("@ParM_remark", SqlDbType.VarChar,50),
					new SqlParameter("@levels", SqlDbType.Int,4)};
			parameters[0].Value = model.id;
			parameters[1].Value = model.boarId_origin;
			parameters[2].Value = model.boarId;
			parameters[3].Value = model.ParFatherBoarId;
			parameters[4].Value = model.ParMotherBoarId;
			parameters[5].Value = model.ParF_barcode;
			parameters[6].Value = model.ParF_name;
			parameters[7].Value = model.ParF_remark;
			parameters[8].Value = model.ParM_barcode;
			parameters[9].Value = model.ParM_name;
			parameters[10].Value = model.ParM_remark;
			parameters[11].Value = model.levels;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XHD.Model.Basic_Boar_sys_Genealogy model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Basic_Boar_sys_Genealogy set ");
			strSql.Append("boarId_origin=@boarId_origin,");
			strSql.Append("boarId=@boarId,");
			strSql.Append("ParFatherBoarId=@ParFatherBoarId,");
			strSql.Append("ParMotherBoarId=@ParMotherBoarId,");
			strSql.Append("ParF_barcode=@ParF_barcode,");
			strSql.Append("ParF_name=@ParF_name,");
			strSql.Append("ParF_remark=@ParF_remark,");
			strSql.Append("ParM_barcode=@ParM_barcode,");
			strSql.Append("ParM_name=@ParM_name,");
			strSql.Append("ParM_remark=@ParM_remark,");
			strSql.Append("levels=@levels");
			strSql.Append(" where id=@id ");
			SqlParameter[] parameters = {
					new SqlParameter("@boarId_origin", SqlDbType.VarChar,50),
					new SqlParameter("@boarId", SqlDbType.VarChar,50),
					new SqlParameter("@ParFatherBoarId", SqlDbType.VarChar,50),
					new SqlParameter("@ParMotherBoarId", SqlDbType.VarChar,50),
					new SqlParameter("@ParF_barcode", SqlDbType.VarChar,50),
					new SqlParameter("@ParF_name", SqlDbType.VarChar,50),
					new SqlParameter("@ParF_remark", SqlDbType.VarChar,50),
					new SqlParameter("@ParM_barcode", SqlDbType.VarChar,50),
					new SqlParameter("@ParM_name", SqlDbType.VarChar,50),
					new SqlParameter("@ParM_remark", SqlDbType.VarChar,50),
					new SqlParameter("@levels", SqlDbType.Int,4),
					new SqlParameter("@id", SqlDbType.VarChar,50)};
			parameters[0].Value = model.boarId_origin;
			parameters[1].Value = model.boarId;
			parameters[2].Value = model.ParFatherBoarId;
			parameters[3].Value = model.ParMotherBoarId;
			parameters[4].Value = model.ParF_barcode;
			parameters[5].Value = model.ParF_name;
			parameters[6].Value = model.ParF_remark;
			parameters[7].Value = model.ParM_barcode;
			parameters[8].Value = model.ParM_name;
			parameters[9].Value = model.ParM_remark;
			parameters[10].Value = model.levels;
			parameters[11].Value = model.id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Basic_Boar_sys_Genealogy ");
			strSql.Append(" where id=@id ");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.VarChar,50)			};
			parameters[0].Value = id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Basic_Boar_sys_Genealogy ");
			strSql.Append(" where id in ("+idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XHD.Model.Basic_Boar_sys_Genealogy GetModel(string id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 id,boarId_origin,boarId,ParFatherBoarId,ParMotherBoarId,ParF_barcode,ParF_name,ParF_remark,ParM_barcode,ParM_name,ParM_remark,levels from Basic_Boar_sys_Genealogy ");
			strSql.Append(" where id=@id ");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.VarChar,50)			};
			parameters[0].Value = id;

			XHD.Model.Basic_Boar_sys_Genealogy model=new XHD.Model.Basic_Boar_sys_Genealogy();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XHD.Model.Basic_Boar_sys_Genealogy DataRowToModel(DataRow row)
		{
			XHD.Model.Basic_Boar_sys_Genealogy model=new XHD.Model.Basic_Boar_sys_Genealogy();
			if (row != null)
			{
				if(row["id"]!=null)
				{
					model.id=row["id"].ToString();
				}
				if(row["boarId_origin"]!=null)
				{
					model.boarId_origin=row["boarId_origin"].ToString();
				}
				if(row["boarId"]!=null)
				{
					model.boarId=row["boarId"].ToString();
				}
				if(row["ParFatherBoarId"]!=null)
				{
					model.ParFatherBoarId=row["ParFatherBoarId"].ToString();
				}
				if(row["ParMotherBoarId"]!=null)
				{
					model.ParMotherBoarId=row["ParMotherBoarId"].ToString();
				}
				if(row["ParF_barcode"]!=null)
				{
					model.ParF_barcode=row["ParF_barcode"].ToString();
				}
				if(row["ParF_name"]!=null)
				{
					model.ParF_name=row["ParF_name"].ToString();
				}
				if(row["ParF_remark"]!=null)
				{
					model.ParF_remark=row["ParF_remark"].ToString();
				}
				if(row["ParM_barcode"]!=null)
				{
					model.ParM_barcode=row["ParM_barcode"].ToString();
				}
				if(row["ParM_name"]!=null)
				{
					model.ParM_name=row["ParM_name"].ToString();
				}
				if(row["ParM_remark"]!=null)
				{
					model.ParM_remark=row["ParM_remark"].ToString();
				}
				if(row["levels"]!=null && row["levels"].ToString()!="")
				{
					model.levels=int.Parse(row["levels"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select id,boarId_origin,boarId,ParFatherBoarId,ParMotherBoarId,ParF_barcode,ParF_name,ParF_remark,ParM_barcode,ParM_name,ParM_remark,levels ");
			strSql.Append(" FROM Basic_Boar_sys_Genealogy ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" id,boarId_origin,boarId,ParFatherBoarId,ParMotherBoarId,ParF_barcode,ParF_name,ParF_remark,ParM_barcode,ParM_name,ParM_remark,levels ");
			strSql.Append(" FROM Basic_Boar_sys_Genealogy ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Basic_Boar_sys_Genealogy ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.id desc");
			}
			strSql.Append(")AS Row, T.*  from Basic_Boar_sys_Genealogy T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Basic_Boar_sys_Genealogy";
			parameters[1].Value = "id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod
		public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, out string Total)
		{
			StringBuilder strSql_grid = new StringBuilder();
			StringBuilder strSql_total = new StringBuilder();
			strSql_total.Append(" SELECT COUNT(id) FROM Basic_Boar_sys_Genealogy ");
			strSql_grid.Append("SELECT ");
			strSql_grid.Append("      n,* ");
			strSql_grid.Append(" FROM ( SELECT *, ROW_NUMBER() OVER( Order by " + filedOrder + " ) AS n from Basic_Boar_sys_Genealogy");
			if (strWhere.Trim() != "")
			{
				strSql_grid.Append(" WHERE " + strWhere);
				strSql_total.Append(" WHERE " + strWhere);
			}
			strSql_grid.Append("  ) as w1  ");
			strSql_grid.Append("WHERE n BETWEEN " + (PageSize * (PageIndex - 1) + 1) + " AND " + PageSize * PageIndex);
			strSql_grid.Append(" ORDER BY " + filedOrder);
			Total = DbHelperSQL.Query(strSql_total.ToString()).Tables[0].Rows[0][0].ToString();
			return DbHelperSQL.Query(strSql_grid.ToString());
		}

		public DataSet GetList(string bid, string pid, int level)
		{
			StringBuilder sb = new StringBuilder();
			sb.AppendLine("  SELECT C.*,'公' AS gmsex, A.levels,A.boarId,A.id AS gid,ParFatherBoarId AS pid FROM dbo.Basic_Boar_sys_Genealogy A   ");
			sb.AppendLine("  INNER JOIN  dbo.Basic_boar C ON	 A.ParFatherBoarId=C.id  ");
			sb.AppendLine("  WHERE A.boarId_origin='" + bid + "' AND	 A.levels=" + level + " ");
			if (level != 1)
				sb.AppendLine(" and boarId='" + pid + "'");
			sb.AppendLine("  UNION ALL	   ");
			sb.AppendLine("  SELECT C.*,'母' AS gmsex, A.levels,A.boarId,A.id AS gid,ParMotherBoarId as pid FROM dbo.Basic_Boar_sys_Genealogy A   ");
			sb.AppendLine("  INNER JOIN  dbo.Basic_boar C ON	 A.ParMotherBoarId=C.id  ");
			sb.AppendLine("   WHERE A.boarId_origin='" + bid + "' AND	 A.levels=" + level + "  ");
			if (level != 1)
				sb.AppendLine(" and boarId='" + pid + "'");
			return DbHelperSQL.Query(sb.ToString());
		}

		public DataSet GetList2(string bid, string pid, int level)
		{
			StringBuilder sb = new StringBuilder();
			sb.AppendLine(" SELECT  ParFatherBoarId AS id,ParF_barcode AS barcode,ParF_name AS name ,ParF_remark AS remark,levels  ,'M' as sex   ");
			sb.AppendLine(" ,'公' AS gmsex, A.levels,A.boarId,A.id AS gid,ParFatherBoarId AS pid ");
			sb.AppendLine(" FROM dbo.Basic_Boar_sys_Genealogy A");
			sb.AppendLine("  WHERE A.boarId_origin='" + bid + "' AND	 A.levels=" + level + " ");
			if (level != 1)
				sb.AppendLine(" and boarId='" + pid + "'");
			sb.AppendLine(" UNION ALL	  ");
			sb.AppendLine(" SELECT  ParMotherBoarId AS id,ParM_barcode AS barcode,ParM_name AS name ,ParM_remark AS remark,levels,'F' as sex   ");
			sb.AppendLine(" ,'母' AS gmsex, A.levels,A.boarId,A.id AS gid,ParMotherBoarId as pid ");
			sb.AppendLine(" FROM dbo.Basic_Boar_sys_Genealogy A ");
			sb.AppendLine("   ");
			sb.AppendLine("  WHERE A.boarId_origin='" + bid + "' AND	 A.levels=" + level + " ");
			if (level != 1)
				sb.AppendLine(" and boarId='" + pid + "'");
			//sb.AppendLine("  SELECT C.*,'公' AS gmsex, A.levels,A.boarId,A.id AS gid,ParFatherBoarId AS pid FROM dbo.Basic_Boar_sys_Genealogy A   ");
			//sb.AppendLine("  INNER JOIN  dbo.Basic_boar C ON	 A.ParFatherBoarId=C.id  ");
			//sb.AppendLine("  WHERE A.boarId_origin='" + bid + "' AND	 A.levels=" + level + " ");
			//if (level != 1)
			//	sb.AppendLine(" and boarId='" + pid + "'");
			//sb.AppendLine("  UNION ALL	   ");
			//sb.AppendLine("  SELECT C.*,'母' AS gmsex, A.levels,A.boarId,A.id AS gid,ParMotherBoarId as pid FROM dbo.Basic_Boar_sys_Genealogy A   ");
			//sb.AppendLine("  INNER JOIN  dbo.Basic_boar C ON	 A.ParMotherBoarId=C.id  ");
			//sb.AppendLine("   WHERE A.boarId_origin='" + bid + "' AND	 A.levels=" + level + "  ");
			//if (level != 1)
			//	sb.AppendLine(" and boarId='" + pid + "'");
			return DbHelperSQL.Query(sb.ToString());
		}

		public DataSet GetForm(string oid, string bid, int level, string id)
		{
			StringBuilder sb = new StringBuilder();
			sb.AppendLine("   ");
			sb.AppendLine("    SELECT  B.id AS fid,B.barcode AS fbarcode, B.product_name AS fname,B.specifications AS flb,B.remarks AS fbz,  ");
			sb.AppendLine("     c.id AS mid,c.barcode AS mbarcode,c.product_name AS mname,c.specifications AS mlb,c.remarks AS mbz  ");
			sb.AppendLine(" 	 FROM dbo.Basic_Boar_sys_Genealogy A   ");
			sb.AppendLine("     INNER JOIN  dbo.Basic_boar B ON	 A.ParFatherBoarId=B.id  ");
			sb.AppendLine("  INNER JOIN  dbo.Basic_boar C ON	 A.ParMotherBoarId=C.id  ");
			sb.AppendLine("  WHERE 1=1 ");
			if (id != "") sb.AppendLine("   AND A.id='" + id + "'  ");
			else sb.AppendLine("  WHERE A.boarId_origin='=" + oid + "' AND A.boarId='=" + bid + "' AND	 A.levels=" + level + "  ");
			return DbHelperSQL.Query(sb.ToString());
		}
		#endregion  ExtensionMethod
	}
}

