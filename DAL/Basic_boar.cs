﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using XHD.DBUtility;//Please add references
namespace XHD.DAL
{
	/// <summary>
	/// 数据访问类:Basic_boar
	/// </summary>
	public partial class Basic_boar
	{
		public Basic_boar()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string id,string barcode)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Basic_boar");
			strSql.Append(" where id=@id and barcode=@barcode ");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.VarChar,50),
					new SqlParameter("@barcode", SqlDbType.VarChar,50)			};
			parameters[0].Value = id;
			parameters[1].Value = barcode;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(XHD.Model.Basic_boar model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Basic_boar(");
			strSql.Append("id,product_name,category_id,status,unit,cost,price,agio,remarks,specifications,create_id,create_time,barcode,CollectionStatus,EfficacyState,LastCollecTime,LastEfficaTime,Drug_id,img_url,birthday,shortcode,origin,place,dormId,corralId,sex,InDate,sysId,productId,Pro_SeriesId,Pro_Series_LevelId,unit_id)");
			strSql.Append(" values (");
			strSql.Append("@id,@product_name,@category_id,@status,@unit,@cost,@price,@agio,@remarks,@specifications,@create_id,@create_time,@barcode,@CollectionStatus,@EfficacyState,@LastCollecTime,@LastEfficaTime,@Drug_id,@img_url,@birthday,@shortcode,@origin,@place,@dormId,@corralId,@sex,@InDate,@sysId,@productId,@Pro_SeriesId,@Pro_Series_LevelId,@unit_id)");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.VarChar,50),
					new SqlParameter("@product_name", SqlDbType.VarChar,250),
					new SqlParameter("@category_id", SqlDbType.VarChar,50),
					new SqlParameter("@status", SqlDbType.VarChar,5),
					new SqlParameter("@unit", SqlDbType.VarChar,250),
					new SqlParameter("@cost", SqlDbType.Decimal,9),
					new SqlParameter("@price", SqlDbType.Decimal,9),
					new SqlParameter("@agio", SqlDbType.Decimal,9),
					new SqlParameter("@remarks", SqlDbType.VarChar,-1),
					new SqlParameter("@specifications", SqlDbType.VarChar,250),
					new SqlParameter("@create_id", SqlDbType.VarChar,50),
					new SqlParameter("@create_time", SqlDbType.DateTime),
					new SqlParameter("@barcode", SqlDbType.VarChar,50),
					new SqlParameter("@CollectionStatus", SqlDbType.SmallInt,2),
					new SqlParameter("@EfficacyState", SqlDbType.SmallInt,2),
					new SqlParameter("@LastCollecTime", SqlDbType.DateTime),
					new SqlParameter("@LastEfficaTime", SqlDbType.DateTime),
					new SqlParameter("@Drug_id", SqlDbType.VarChar,50),
					new SqlParameter("@img_url", SqlDbType.VarChar,500),
					new SqlParameter("@birthday", SqlDbType.DateTime),
					new SqlParameter("@shortcode", SqlDbType.VarChar,50),
					new SqlParameter("@origin", SqlDbType.VarChar,50),
					new SqlParameter("@place", SqlDbType.VarChar,50),
					new SqlParameter("@dormId", SqlDbType.VarChar,50),
					new SqlParameter("@corralId", SqlDbType.VarChar,50),
					new SqlParameter("@sex", SqlDbType.VarChar,5),
					new SqlParameter("@InDate", SqlDbType.DateTime),
					new SqlParameter("@sysId", SqlDbType.VarChar,50),
					new SqlParameter("@productId", SqlDbType.VarChar,50),
					new SqlParameter("@Pro_SeriesId", SqlDbType.VarChar,50),
					new SqlParameter("@Pro_Series_LevelId", SqlDbType.VarChar,50),
						new SqlParameter("@unit_id", SqlDbType.VarChar,50)};
			parameters[0].Value = model.id;
			parameters[1].Value = model.product_name;
			parameters[2].Value = model.category_id;
			parameters[3].Value = model.status;
			parameters[4].Value = model.unit;
			parameters[5].Value = model.cost;
			parameters[6].Value = model.price;
			parameters[7].Value = model.agio;
			parameters[8].Value = model.remarks;
			parameters[9].Value = model.specifications;
			parameters[10].Value = model.create_id;
			parameters[11].Value = model.create_time;
			parameters[12].Value = model.barcode;
			parameters[13].Value = model.CollectionStatus;
			parameters[14].Value = model.EfficacyState;
			parameters[15].Value = model.LastCollecTime;
			parameters[16].Value = model.LastEfficaTime;
			parameters[17].Value = model.Drug_id;
			parameters[18].Value = model.img_url;
			parameters[19].Value = model.birthday;
			parameters[20].Value = model.shortcode;
			parameters[21].Value = model.origin;
			parameters[22].Value = model.place;
			parameters[23].Value = model.dormId;
			parameters[24].Value = model.corralId;
			parameters[25].Value = model.sex;
			parameters[26].Value = model.InDate;
			parameters[27].Value = model.sysId;
			parameters[28].Value = model.productId;
			parameters[29].Value = model.Pro_SeriesId;
			parameters[30].Value = model.Pro_Series_LevelId;
			parameters[31].Value = model.unit_id;
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XHD.Model.Basic_boar model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Basic_boar set ");
			strSql.Append("product_name=@product_name,");
			strSql.Append("category_id=@category_id,");
			//strSql.Append("status=@status,");
			strSql.Append("unit=@unit,");
			strSql.Append("cost=@cost,");
			strSql.Append("price=@price,");
			strSql.Append("agio=@agio,");
			strSql.Append("remarks=@remarks,");
			strSql.Append("specifications=@specifications,");
			strSql.Append("create_id=@create_id,");
			strSql.Append("create_time=@create_time,");
			//strSql.Append("CollectionStatus=@CollectionStatus,");
			//strSql.Append("EfficacyState=@EfficacyState,");
			//strSql.Append("LastCollecTime=@LastCollecTime,");
			//strSql.Append("LastEfficaTime=@LastEfficaTime,");
			strSql.Append("Drug_id=@Drug_id,");
			strSql.Append("img_url=@img_url,");
			strSql.Append("birthday=@birthday,");
			strSql.Append("shortcode=@shortcode,");
			strSql.Append("origin=@origin,");
			strSql.Append("place=@place,");
			strSql.Append("dormId=@dormId,");
			strSql.Append("corralId=@corralId,");
			strSql.Append("sex=@sex,");
			strSql.Append("InDate=@InDate,");
			strSql.Append("sysId=@sysId,");
			strSql.Append("productId=@productId,");
			strSql.Append("Pro_SeriesId=@Pro_SeriesId,");
			strSql.Append("Pro_Series_LevelId=@Pro_Series_LevelId,");
			strSql.Append("unit_id=@unit_id ");
			strSql.Append(" where id=@id and barcode=@barcode ");
			SqlParameter[] parameters = {
					new SqlParameter("@product_name", SqlDbType.VarChar,250),
					new SqlParameter("@category_id", SqlDbType.VarChar,50),
					new SqlParameter("@status", SqlDbType.VarChar,5),
					new SqlParameter("@unit", SqlDbType.VarChar,250),
					new SqlParameter("@cost", SqlDbType.Decimal,9),
					new SqlParameter("@price", SqlDbType.Decimal,9),
					new SqlParameter("@agio", SqlDbType.Decimal,9),
					new SqlParameter("@remarks", SqlDbType.VarChar,-1),
					new SqlParameter("@specifications", SqlDbType.VarChar,250),
					new SqlParameter("@create_id", SqlDbType.VarChar,50),
					new SqlParameter("@create_time", SqlDbType.DateTime),
					new SqlParameter("@CollectionStatus", SqlDbType.SmallInt,2),
					new SqlParameter("@EfficacyState", SqlDbType.SmallInt,2),
					new SqlParameter("@LastCollecTime", SqlDbType.DateTime),
					new SqlParameter("@LastEfficaTime", SqlDbType.DateTime),
					new SqlParameter("@Drug_id", SqlDbType.VarChar,50),
					new SqlParameter("@img_url", SqlDbType.VarChar,500),
					new SqlParameter("@birthday", SqlDbType.DateTime),
					new SqlParameter("@shortcode", SqlDbType.VarChar,50),
					new SqlParameter("@origin", SqlDbType.VarChar,50),
					new SqlParameter("@place", SqlDbType.VarChar,50),
					new SqlParameter("@dormId", SqlDbType.VarChar,50),
					new SqlParameter("@corralId", SqlDbType.VarChar,50),
					new SqlParameter("@sex", SqlDbType.VarChar,5),
					new SqlParameter("@InDate", SqlDbType.DateTime),
					new SqlParameter("@sysId", SqlDbType.VarChar,50),
					new SqlParameter("@productId", SqlDbType.VarChar,50),
					new SqlParameter("@Pro_SeriesId", SqlDbType.VarChar,50),
					new SqlParameter("@Pro_Series_LevelId", SqlDbType.VarChar,50),
						new SqlParameter("@unit_id", SqlDbType.VarChar,50),
					new SqlParameter("@id", SqlDbType.VarChar,50),
					new SqlParameter("@barcode", SqlDbType.VarChar,50)};
			parameters[0].Value = model.product_name;
			parameters[1].Value = model.category_id;
			parameters[2].Value = model.status;
			parameters[3].Value = model.unit;
			parameters[4].Value = model.cost;
			parameters[5].Value = model.price;
			parameters[6].Value = model.agio;
			parameters[7].Value = model.remarks;
			parameters[8].Value = model.specifications;
			parameters[9].Value = model.create_id;
			parameters[10].Value = model.create_time;
			parameters[11].Value = model.CollectionStatus;
			parameters[12].Value = model.EfficacyState;
			parameters[13].Value = model.LastCollecTime;
			parameters[14].Value = model.LastEfficaTime;
			parameters[15].Value = model.Drug_id;
			parameters[16].Value = model.img_url;
			parameters[17].Value = model.birthday;
			parameters[18].Value = model.shortcode;
			parameters[19].Value = model.origin;
			parameters[20].Value = model.place;
			parameters[21].Value = model.dormId;
			parameters[22].Value = model.corralId;
			parameters[23].Value = model.sex;
			parameters[24].Value = model.InDate;
			parameters[25].Value = model.sysId;
			parameters[26].Value = model.productId;
			parameters[27].Value = model.Pro_SeriesId;
			parameters[28].Value = model.Pro_Series_LevelId;
			parameters[29].Value = model.unit_id; 
			parameters[30].Value = model.id;
			parameters[31].Value = model.barcode;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string id,string barcode)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Basic_boar ");
			strSql.Append(" where id=@id and barcode=@barcode ");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.VarChar,50),
					new SqlParameter("@barcode", SqlDbType.VarChar,50)			};
			parameters[0].Value = id;
			parameters[1].Value = barcode;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public bool DeleteByid(string id)
		{

			StringBuilder strSql = new StringBuilder();
			strSql.Append("delete from Basic_boar ");
			strSql.Append(" where id=@id   ");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.VarChar,50)         };
			parameters[0].Value = id; 

			int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XHD.Model.Basic_boar GetModel(string id,string barcode)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 id,product_name,category_id,status,unit,cost,price,agio,remarks,specifications,create_id,create_time,barcode,CollectionStatus,EfficacyState,LastCollecTime,LastEfficaTime,Drug_id,img_url,birthday,shortcode,origin,place,dormId,corralId,sex,InDate,sysId,productId,Pro_SeriesId,Pro_Series_LevelId,unit_id from Basic_boar ");
			strSql.Append(" where id=@id and barcode=@barcode ");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.VarChar,50),
					new SqlParameter("@barcode", SqlDbType.VarChar,50)			};
			parameters[0].Value = id;
			parameters[1].Value = barcode;

			XHD.Model.Basic_boar model=new XHD.Model.Basic_boar();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XHD.Model.Basic_boar DataRowToModel(DataRow row)
		{
			XHD.Model.Basic_boar model=new XHD.Model.Basic_boar();
			if (row != null)
			{
				if(row["id"]!=null)
				{
					model.id=row["id"].ToString();
				}
				if(row["product_name"]!=null)
				{
					model.product_name=row["product_name"].ToString();
				}
				if(row["category_id"]!=null)
				{
					model.category_id=row["category_id"].ToString();
				}
				if(row["status"]!=null)
				{
					model.status=row["status"].ToString();
				}
				if(row["unit"]!=null)
				{
					model.unit=row["unit"].ToString();
				}
				if(row["cost"]!=null && row["cost"].ToString()!="")
				{
					model.cost=decimal.Parse(row["cost"].ToString());
				}
				if(row["price"]!=null && row["price"].ToString()!="")
				{
					model.price=decimal.Parse(row["price"].ToString());
				}
				if(row["agio"]!=null && row["agio"].ToString()!="")
				{
					model.agio=decimal.Parse(row["agio"].ToString());
				}
				if(row["remarks"]!=null)
				{
					model.remarks=row["remarks"].ToString();
				}
				if(row["specifications"]!=null)
				{
					model.specifications=row["specifications"].ToString();
				}
				if(row["create_id"]!=null)
				{
					model.create_id=row["create_id"].ToString();
				}
				if(row["create_time"]!=null && row["create_time"].ToString()!="")
				{
					model.create_time=DateTime.Parse(row["create_time"].ToString());
				}
				if(row["barcode"]!=null)
				{
					model.barcode=row["barcode"].ToString();
				}
				if(row["CollectionStatus"]!=null && row["CollectionStatus"].ToString()!="")
				{
					model.CollectionStatus=int.Parse(row["CollectionStatus"].ToString());
				}
				if(row["EfficacyState"]!=null && row["EfficacyState"].ToString()!="")
				{
					model.EfficacyState=int.Parse(row["EfficacyState"].ToString());
				}
				if(row["LastCollecTime"]!=null && row["LastCollecTime"].ToString()!="")
				{
					model.LastCollecTime=DateTime.Parse(row["LastCollecTime"].ToString());
				}
				if(row["LastEfficaTime"]!=null && row["LastEfficaTime"].ToString()!="")
				{
					model.LastEfficaTime=DateTime.Parse(row["LastEfficaTime"].ToString());
				}
				if(row["Drug_id"]!=null)
				{
					model.Drug_id=row["Drug_id"].ToString();
				}
				if(row["img_url"]!=null)
				{
					model.img_url=row["img_url"].ToString();
				}
				if(row["birthday"]!=null && row["birthday"].ToString()!="")
				{
					model.birthday=DateTime.Parse(row["birthday"].ToString());
				}
				if(row["shortcode"]!=null)
				{
					model.shortcode=row["shortcode"].ToString();
				}
				if(row["origin"]!=null)
				{
					model.origin=row["origin"].ToString();
				}
				if(row["place"]!=null)
				{
					model.place=row["place"].ToString();
				}
				if(row["dormId"]!=null)
				{
					model.dormId=row["dormId"].ToString();
				}
				if(row["corralId"]!=null)
				{
					model.corralId=row["corralId"].ToString();
				}
				if(row["sex"]!=null)
				{
					model.sex=row["sex"].ToString();
				}
				if(row["InDate"]!=null && row["InDate"].ToString()!="")
				{
					model.InDate=DateTime.Parse(row["InDate"].ToString());
				}
				if(row["sysId"]!=null)
				{
					model.sysId=row["sysId"].ToString();
				}
				if(row["productId"]!=null)
				{
					model.productId=row["productId"].ToString();
				}
				if(row["Pro_SeriesId"]!=null)
				{
					model.Pro_SeriesId=row["Pro_SeriesId"].ToString();
				}
				if(row["Pro_Series_LevelId"]!=null)
				{
					model.Pro_Series_LevelId=row["Pro_Series_LevelId"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select id,product_name,category_id,status,unit,cost,price,agio,remarks,specifications,create_id,create_time,barcode,CollectionStatus,EfficacyState,LastCollecTime,LastEfficaTime,Drug_id,img_url,birthday,shortcode,origin,place,dormId,corralId,sex,InDate,sysId,productId,Pro_SeriesId,Pro_Series_LevelId ,unit_id");
			strSql.Append(" FROM Basic_boar ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" id,product_name,category_id,status,unit,cost,price,agio,remarks,specifications,create_id,create_time,barcode,CollectionStatus,EfficacyState,LastCollecTime,LastEfficaTime,Drug_id,img_url,birthday,shortcode,origin,place,dormId,corralId,sex,InDate,sysId,productId,Pro_SeriesId,Pro_Series_LevelId,unit_id ");
			strSql.Append(" FROM Basic_boar ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Basic_boar ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.barcode desc");
			}
			strSql.Append(")AS Row, T.*  from Basic_boar T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Basic_boar";
			parameters[1].Value = "barcode";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, out string Total)
		{
			StringBuilder strSql_grid = new StringBuilder();
			StringBuilder strSql_total = new StringBuilder();
			strSql_total.Append(" SELECT COUNT(id) FROM Basic_boar ");
			strSql_grid.Append("SELECT ");
			strSql_grid.Append("      n,id,dormId,corralId,product_name,category_id,status,unit,cost,price,agio,remarks,specifications,create_id,create_time,barcode,CollectionStatus,EfficacyState,LastCollecTime,LastEfficaTime,pickcycle, isnull(pickcycle-datediff(day,LastCollecTime, getdate()),0) as okdate ,drugName ,drugcycle,img_url,birthday,DATEDIFF(DAY, birthday,GETDATE()) nlday,shortcode  ");
			//strSql_grid.Append(",(SELECT params_name   FROM  dbo.Sys_Param where id = w1.category_id) as category_name");
			strSql_grid.Append(",(select product_name from Product where Product.id = category_id)  as category_name,drugName,drugcycle");

			strSql_grid.Append(" FROM ( SELECT id,dormId,corralId,product_name,category_id,status,A.unit,cost,price,agio,a.remarks,specifications,create_id,create_time,barcode,shortcode,CollectionStatus,EfficacyState,birthday,DATEDIFF(DAY, birthday,GETDATE()) nlday, c.drugName , c.drugcycle, img_url, ROW_NUMBER() OVER( Order by " + filedOrder + " ) AS n,LastCollecTime,LastEfficaTime,b.pickcycle   from Basic_boar a LEFT JOIN (SELECT id AS productid,pickcycle FROM  dbo.Product) b ON a.category_id=b.productid");
			strSql_grid.Append("   LEFT JOIN dbo.Basic_Drug C ON	a.Drug_id=C.IDCode");
			if (strWhere.Trim() != "")
			{
				strSql_grid.Append(" WHERE " + strWhere);
				strSql_total.Append(" WHERE " + strWhere);
			}
			strSql_grid.Append("  ) as w1  ");
			strSql_grid.Append("WHERE n BETWEEN " + (PageSize * (PageIndex - 1) + 1) + " AND " + PageSize * PageIndex);
			strSql_grid.Append(" ORDER BY " + filedOrder);
			Total = DbHelperSQL.Query(strSql_total.ToString()).Tables[0].Rows[0][0].ToString();
			return DbHelperSQL.Query(strSql_grid.ToString());
		}

		#endregion  ExtensionMethod
	}
}

