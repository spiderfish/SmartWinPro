﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using XHD.DBUtility;//Please add references
namespace XHD.DAL
{
	/// <summary>
	/// 数据访问类:boar_collection
	/// </summary>
	public partial class boar_collection
	{
		public boar_collection()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from boar_collection");
			strSql.Append(" where id=@id ");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.VarChar,50)			};
			parameters[0].Value = id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(XHD.Model.boar_collection model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into boar_collection(");
			strSql.Append("id,orderid,deviceid,boarid,dotime,doperson,remarks,collectStation,WorkstationNo,collectDate,collector,IsStatus,ordermxid)");
			strSql.Append(" values (");
			strSql.Append("@id,@orderid,@deviceid,@boarid,@dotime,@doperson,@remarks,@collectStation,@WorkstationNo,@collectDate,@collector,@IsStatus,@ordermxid)");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.VarChar,50),
					new SqlParameter("@orderid", SqlDbType.VarChar,50),
					new SqlParameter("@deviceid", SqlDbType.VarChar,50),
					new SqlParameter("@boarid", SqlDbType.VarChar,50),
					new SqlParameter("@dotime", SqlDbType.DateTime),
					new SqlParameter("@doperson", SqlDbType.VarChar,50),
					new SqlParameter("@remarks", SqlDbType.VarChar,50),
					new SqlParameter("@collectStation", SqlDbType.VarChar,50),
					new SqlParameter("@WorkstationNo", SqlDbType.VarChar,50),
					new SqlParameter("@collectDate", SqlDbType.DateTime),
					new SqlParameter("@collector", SqlDbType.VarChar,50),
					new SqlParameter("@IsStatus", SqlDbType.VarChar,5),
               new SqlParameter("@ordermxid", SqlDbType.VarChar,50) };
			parameters[0].Value = model.id;
			parameters[1].Value = model.orderid;
			parameters[2].Value = model.deviceid;
			parameters[3].Value = model.boarid;
			parameters[4].Value = model.dotime;
			parameters[5].Value = model.doperson;
			parameters[6].Value = model.remarks;
			parameters[7].Value = model.collectStation;
			parameters[8].Value = model.WorkstationNo;
			parameters[9].Value = model.collectDate;
			parameters[10].Value = model.collector;
			parameters[11].Value = model.IsStatus;
            parameters[12].Value = model.ordermxid;
            int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XHD.Model.boar_collection model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update boar_collection set ");
			strSql.Append("orderid=@orderid,");
            strSql.Append("ordermxid=@ordermxid,");
            strSql.Append("deviceid=@deviceid,");
			strSql.Append("boarid=@boarid,");
			strSql.Append("dotime=@dotime,");
			strSql.Append("doperson=@doperson,");
			strSql.Append("remarks=@remarks,");
			strSql.Append("collectStation=@collectStation,");
			strSql.Append("WorkstationNo=@WorkstationNo,");
			strSql.Append("collectDate=@collectDate,");
			strSql.Append("collector=@collector "); 
			strSql.Append(" where id=@id ");
			SqlParameter[] parameters = {
					new SqlParameter("@orderid", SqlDbType.VarChar,50),  
                    new SqlParameter("@deviceid", SqlDbType.VarChar,50),
					new SqlParameter("@boarid", SqlDbType.VarChar,50),
					new SqlParameter("@dotime", SqlDbType.DateTime),
					new SqlParameter("@doperson", SqlDbType.VarChar,50),
					new SqlParameter("@remarks", SqlDbType.VarChar,50),
					new SqlParameter("@collectStation", SqlDbType.VarChar,50),
					new SqlParameter("@WorkstationNo", SqlDbType.VarChar,50),
					new SqlParameter("@collectDate", SqlDbType.DateTime),
					new SqlParameter("@collector", SqlDbType.VarChar,50), 
					new SqlParameter("@id", SqlDbType.VarChar,50),
               new SqlParameter("@ordermxid", SqlDbType.VarChar,50)};
			parameters[0].Value = model.orderid;
			parameters[1].Value = model.deviceid;
			parameters[2].Value = model.boarid;
			parameters[3].Value = model.dotime;
			parameters[4].Value = model.doperson;
			parameters[5].Value = model.remarks;
			parameters[6].Value = model.collectStation;
			parameters[7].Value = model.WorkstationNo;
			parameters[8].Value = model.collectDate;
			parameters[9].Value = model.collector; 
			parameters[10].Value = model.id;
            parameters[11].Value = model.ordermxid;

            int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from boar_collection ");
			strSql.Append(" where id=@id ");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.VarChar,50)			};
			parameters[0].Value = id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from boar_collection ");
			strSql.Append(" where id in ("+idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XHD.Model.boar_collection GetModel(string id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 id,orderid,deviceid,boarid,dotime,doperson,remarks,collectStation,WorkstationNo,collectDate,collector,IsStatus,ordermxid,weights,para1,para2 from boar_collection ");
			strSql.Append(" where id=@id ");
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.VarChar,50)			};
			parameters[0].Value = id;

			XHD.Model.boar_collection model=new XHD.Model.boar_collection();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XHD.Model.boar_collection DataRowToModel(DataRow row)
		{
			XHD.Model.boar_collection model=new XHD.Model.boar_collection();
			if (row != null)
			{
				if(row["id"]!=null)
				{
					model.id=row["id"].ToString();
				}
				if(row["orderid"]!=null)
				{
					model.orderid=row["orderid"].ToString();
				}
				if(row["deviceid"]!=null)
				{
					model.deviceid=row["deviceid"].ToString();
				}
				if(row["boarid"]!=null)
				{
					model.boarid=row["boarid"].ToString();
				}
				if(row["dotime"]!=null && row["dotime"].ToString()!="")
				{
					model.dotime=DateTime.Parse(row["dotime"].ToString());
				}
				if(row["doperson"]!=null)
				{
					model.doperson=row["doperson"].ToString();
				}
				if(row["remarks"]!=null)
				{
					model.remarks=row["remarks"].ToString();
				}
				if(row["collectStation"]!=null)
				{
					model.collectStation=row["collectStation"].ToString();
				}
				if(row["WorkstationNo"]!=null)
				{
					model.WorkstationNo=row["WorkstationNo"].ToString();
				}
				if(row["collectDate"]!=null && row["collectDate"].ToString()!="")
				{
					model.collectDate=DateTime.Parse(row["collectDate"].ToString());
				}
				if(row["collector"]!=null)
				{
					model.collector=row["collector"].ToString();
				}
				if(row["IsStatus"]!=null)
				{
					model.IsStatus=row["IsStatus"].ToString();
				}
                if (row["ordermxid"] != null)
                {
                    model.IsStatus = row["ordermxid"].ToString();
                }
                if (row["para1"] != null)
                {
                    model.para1 = row["para1"].ToString();
                }

                if (row["para2"] != null)
                {
                    model.IsStatus = row["para2"].ToString();
                }
                if (row["weights"] != null)
                {
                    model.weights = decimal.Parse( row["weights"].ToString());
                }

            }
            return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(" SELECT A.*,B.id,B.Serialnumber,mx.product_id AS mxID,B.Customer_id,E.cus_name,C.product_name DeviceName,D.barcode,D.shortcode,D.product_name BoarName,D.barcode,f.*,g.*, mx.capacity,b.create_time,p.price,p.dailyoutput,p.pickcycle, ");
           // rows[i].quantity / rows[i].dailyoutput * (rows[i].capacity / rows[i].price));
            sb.AppendLine("case when p.dailyoutput=0 then 0  when p.price=0 then 0 else ceiling(mx.[quantity]*1.000/p.dailyoutput*(mx.capacity/p.price)) end as mrp  ");
            //sb.AppendLine(" , (select params_name from Sys_Param where id = A.collectStation) as collectStationName  ");
            //sb.AppendLine("  , (select params_name from Sys_Param where id = A.WorkstationNo) as WorkstationName  ");
            sb.AppendLine(" ,mx.agio,mx.quantity,p.product_name,p.unit,p.specifications,mx.capacity,h.product_category ");
            sb.AppendLine("  ,(SELECT sys_value FROM  dbo.Sys_info WHERE sys_key='sys_name') companyName");
            sb.AppendLine("   FROM dbo.boar_collection A  ");
            sb.AppendLine(" left  JOIN  dbo.Sale_order_details mx ON	 A.orderid=mx.order_id  AND mx.product_id=A.ordermxid");
            sb.AppendLine("   left JOIN  dbo.Product p ON A.ordermxid = p.id ");
            sb.AppendLine(" left JOIN  dbo.Sale_order B ON	 mx.order_id=B.id  ");
            sb.AppendLine(" left JOIN dbo.Basic_device C ON	A.deviceid=C.id  ");
            sb.AppendLine(" left JOIN dbo.Basic_boar D ON	 A.boarid=D.id  ");
            sb.AppendLine(" left JOIN  dbo.CRM_Customer E ON	 B.Customer_id=E.id  ");
            sb.AppendLine(" left JOIN  dbo.V_boar_collection_log_all F ON	 F.allid=a.id  ");
            sb.AppendLine(" left JOIN  dbo.V_ISASpSUS G ON	 g.collectionID=a.id  ");
            sb.AppendLine(" LEFT JOIN  dbo.Product_category h ON p.category_id=h.id  ");

            if (strWhere.Trim()!="")
			{
                sb.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(sb.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" id,orderid,deviceid,boarid,dotime,doperson,remarks,collectStation,WorkstationNo,collectDate,collector,IsStatus,ordermxid,weights,para1,para2 ");
			strSql.Append(" FROM boar_collection ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM boar_collection ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.id desc");
			}
			strSql.Append(")AS Row, T.*  from boar_collection T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

        /*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "boar_collection";
			parameters[1].Value = "id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

        #endregion  BasicMethod
        #region  ExtensionMethod
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, out string Total)
        {
            StringBuilder strSql_inner = new StringBuilder();
            StringBuilder strSql_grid = new StringBuilder();
            StringBuilder strSql_total = new StringBuilder();

            //联合数据
            strSql_inner.Append("( SELECT * ");
            strSql_inner.Append($"        ,ROW_NUMBER() OVER(Order by {filedOrder}) AS n FROM ");
            strSql_inner.Append("   (");
            strSql_inner.Append("        SELECT  ");
            strSql_inner.Append("            * ");
            //strSql_inner.Append("            , (select params_name from Sys_Param where id = w1.cus_industry_id) as cus_industry ");
            //strSql_inner.Append("            , (select params_name from Sys_Param where id = w1.cus_level_id) as cus_level ");
            strSql_inner.Append("            , (select params_name from Sys_Param where id = w1.collectStation) as collectStationName ");//站点
            strSql_inner.Append("            , (select params_name from Sys_Param where id = w1.WorkstationNo) as WorkstationName ");//工位号           
            strSql_inner.Append("        FROM(");
            strSql_inner.Append(" SELECT A.*,mx.product_id AS mxID,B.Customer_id,B.Serialnumber,E.cus_name,C.product_name DeviceName,D.product_name BoarName  ");
            strSql_inner.Append("       ,mx.agio,mx.quantity,p.product_name,p.unit,p.specifications ");
            strSql_inner.Append("   ,ISNULL(S.StatusName,'未知')StatusName,d.barcode,ISA.*,blog.*,p.pickcycle ");
            strSql_inner.Append("     FROM dbo.boar_collection A  ");
            strSql_inner.Append("  LEFT JOIN  dbo.Sale_order_details mx ON A.orderid = mx.order_id  AND	 A.ordermxid=mx.product_id ");
            strSql_inner.Append("  LEFT JOIN  dbo.Sale_order B ON	 mx.order_id=B.id  ");
            strSql_inner.Append("    LEFT JOIN  dbo.Product p ON A.ordermxid = p.id ");
            strSql_inner.Append(" LEFT JOIN dbo.Basic_device C ON	A.deviceid=C.id  ");
            strSql_inner.Append(" LEFT JOIN dbo.Basic_boar D ON	 A.boarid=D.id  ");
            strSql_inner.Append(" LEFT JOIN  dbo.CRM_Customer E ON	 B.Customer_id=E.id  ");
            strSql_inner.Append(" LEFT JOIN dbo.DocStatus_Name S ON S.IsStatusCode = A.IsStatus ");

            strSql_inner.Append(" LEFT JOIN dbo.V_ISASpSUS ISA ON ISA.collectionID = A.ID ");
            strSql_inner.Append(" LEFT JOIN dbo.V_boar_collection_log blog ON blog.EventTitle = A.ID and a.IsStatus=blog.EventType ");

            strSql_inner.Append("  ) as w1  ");

            strSql_inner.Append("   ) w2 ");
            if (strWhere.Trim() != "")
            {
                strSql_inner.Append(" WHERE " + strWhere);
            }
            strSql_inner.Append(") W3");

            //Total数据
            strSql_total.Append(" SELECT COUNT(ID) FROM ");
            strSql_total.Append(strSql_inner.ToString());

            //grid数据
            strSql_grid.Append(" SELECT * FROM ");
            strSql_grid.Append(strSql_inner.ToString());
            strSql_grid.Append(" WHERE n BETWEEN " + PageSize * (PageIndex - 1) + " AND " + PageSize * PageIndex);

            Total = DbHelperSQL.Query(strSql_total.ToString()).Tables[0].Rows[0][0].ToString();
            return DbHelperSQL.Query(strSql_grid.ToString());
        }

        public DataSet GetListCollection(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * ");
            strSql.Append("FROM[dbo].[boar_collection] ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        public DataSet GetListBoars(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT A.*,DATEDIFF(DAY,birthday,GETDATE()) AS age ");
            strSql.Append(" ,B.product_name  ");
            strSql.Append("FROM[dbo].[Basic_boar]  A");
            strSql.Append("  INNER JOIN dbo.Product B ON	A.category_id=B.id");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }
        public bool AddPrintTimes(string id)
        {

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("  UPDATE boar_collection SET	PrintTimes=ISNULL(PrintTimes,0)+1  WHERE id=@id ");
            //sb.AppendLine(" WHERE order_id=@id  and product_id=@ordermxid");

            SqlParameter[] parameters = {
                    new SqlParameter("@id", SqlDbType.VarChar,50)  
            };
            parameters[0].Value = id; 
            int rows = DbHelperSQL.ExecuteSql(sb.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool UpdateBoar_CollectionStatus(string id, string IsStatus)
        {

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("  UPDATE boar_collection SET	 IsStatus=@IsStatus  ");
            sb.AppendLine(" WHERE id=@id  ");

            if (IsStatus == "NN")
            {
                //一旦删除，恢复在途   
                sb.AppendLine("  UPDATE B SET	B.status='Y' ");//更新为课使用
                sb.AppendLine("  FROM dbo.boar_collection A ");
                sb.AppendLine("  INNER JOIN  dbo.Basic_boar B ON  A.boarid = B.id");
                sb.AppendLine(" WHERE A.id=@id  ");
            }
            //一旦提交不能修改，因为采集后不可逆
            if (IsStatus == "20")//采集提交(暂时采集没有保存，先放这里）
            {
                sb.AppendLine("   UPDATE B SET	B.CollectionStatus=3 ,B.LastCollecTime=getdate()");
                sb.AppendLine("  FROM dbo.boar_collection A ");
                sb.AppendLine("  INNER JOIN  dbo.Basic_boar B ON  A.boarid = B.id");
                sb.AppendLine(" WHERE A.id=@id  ");
            }
            if (IsStatus == "0")
            {
                //一旦删除，恢复在途   
                sb.AppendLine("   UPDATE boar_collection SET	IsStatus='0' ");//更新为课使用
              
                sb.AppendLine(" WHERE id=@id  ");
            }

            SqlParameter[] parameters = {
                    new SqlParameter("@id", SqlDbType.VarChar,50)  ,
              new SqlParameter("@IsStatus", SqlDbType.VarChar,50) 
            };
            parameters[0].Value = id;
            parameters[1].Value = IsStatus; 
            int rows = DbHelperSQL.ExecuteSql(sb.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string CreateAutoDoc(string orderid )
        {

            StringBuilder sb = new StringBuilder(); 
            sb.AppendLine("  DECLARE @Msg VARCHAR(100) EXEC  USP_Create_AutoDocument  '" + orderid + "' ,@Msg output  ");
            sb.AppendLine("  SELECT @Msg  ");
 
            SqlParameter[] parameters = { };
            DataSet ds = DbHelperSQL.Query(sb.ToString(), parameters);
            if (ds.Tables[0].Rows.Count> 0)
            {
                return ds.Tables[0].Rows[0][0].ToString();
            }
            else
            {
                return "";
            }
        }



        /// <summary>
        /// 更新订单明细状态
        /// </summary>
        public bool UpdateOrderStatus(string Orderid,string ordermxid,string dstatus)
        {

            StringBuilder sb = new StringBuilder();
            //加入数量更新
            sb.AppendLine(" UPDATE Sale_order_details SET	detail_status=@dstatus  ");
            //if (dstatus == "0")
            //    sb.AppendLine(" ,PreWorkSum=");
            sb.AppendLine(" WHERE order_id=@id  and product_id=@ordermxid");

            SqlParameter[] parameters = {
                    new SqlParameter("@id", SqlDbType.VarChar,50)  ,
              new SqlParameter("@dstatus", SqlDbType.VarChar,50),
               new SqlParameter("@ordermxid", SqlDbType.VarChar,50)
            };
            parameters[0].Value = Orderid;
            parameters[1].Value = dstatus;
            parameters[2].Value = ordermxid;
            int rows = DbHelperSQL.ExecuteSql(sb.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 更新订单状态
        /// </summary>
        public bool UpdateWeights(string id , decimal weights)
        {

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("  UPDATE boar_collection SET	 weights=@weights ,IsStatus='30' ");
            sb.AppendLine(" WHERE id=@id  ");

            SqlParameter[] parameters = {
                    new SqlParameter("@id", SqlDbType.VarChar,50)  ,
              new SqlParameter("@weights",SqlDbType.Decimal) 
            };
            parameters[0].Value = id;
            parameters[1].Value = weights; 
            int rows = DbHelperSQL.ExecuteSql(sb.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool UpdateParas(string type,string id, string weights, string density, string shape, string VIT
            ,string Out_quantity,string Out_Warehouse,string Out_Warehouse_kw,string Out_logistics
            , string In_quantity, string In_Warehouse,string In_Warehouse_kw
            ,string dilution_quantity,string dilution_result,string dilution_weights)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("  UPDATE boar_collection ");
            if (type == "collection")//采集
                sb.AppendLine("   SET  IsStatus = '15' ");//采集保存
            if (type=="weights")
                sb.AppendLine("   SET weights = @weights, IsStatus = '30' ");//称重保存
            if (type=="density")
                sb.AppendLine("   SET density = @density, IsStatus = '50' ");//密度保存
            if (type=="VIT")
                sb.AppendLine("   SET shape = @shape,VIT=@VIT, IsStatus = '70' ");//活力保存
            if (type=="dilution")
                sb.AppendLine("   SET  dilution_result=@dilution_result,dilution_weights=@dilution_weights,dilution_Diff_weights=weights-@dilution_weights, IsStatus = '90' ");//稀释
            if (type == "packing")//打包
                sb.AppendLine("   SET dilution_quantity = @dilution_quantity, IsStatus = '96' ");//打包
            if (type=="In")
                sb.AppendLine("   SET In_quantity = @In_quantity,In_Warehouse=@In_Warehouse,In_Warehouse_kw=@In_Warehouse_kw ,IsStatus = 'A0' ");//入库
            if (type=="Out")
                sb.AppendLine("   SET Out_quantity = @Out_quantity,Out_Warehouse=@Out_Warehouse,Out_Warehouse_kw=@Out_Warehouse_kw,Out_logistics=@Out_logistics, IsStatus = 'B0' ");//出库
            sb.AppendLine(" WHERE id=@id  ");

            //一旦提交不能修改，因为采集后不可逆
            if (type == "collection")//采集保存(后续添加保存功能才能生效）
            {
                sb.AppendLine("   UPDATE B SET	B.CollectionStatus=3 ,B.LastCollecTime=getdate()");
                sb.AppendLine("  FROM dbo.boar_collection A ");
                sb.AppendLine("  INNER JOIN  dbo.Basic_boar B ON  A.boarid = B.id");
                sb.AppendLine(" WHERE A.id=@id  ");
            }
            if (type == "Out")//出库流程或删除，在途恢复
            {
                sb.AppendLine("   UPDATE B SET	B.status='Y' ");//更新为在途状态
                sb.AppendLine("  FROM dbo.boar_collection A ");
                sb.AppendLine("  INNER JOIN  dbo.Basic_boar B ON  A.boarid = B.id");
                sb.AppendLine(" WHERE A.id=@id  ");
            }
            SqlParameter[] parameters = {
                    new SqlParameter("@id", SqlDbType.VarChar,50)  ,
              new SqlParameter("@weights",SqlDbType.VarChar,50),
               new SqlParameter("@density", SqlDbType.VarChar,50),
                 new SqlParameter("@shape", SqlDbType.VarChar,50),
                new SqlParameter("@VIT", SqlDbType.VarChar,50),
                new SqlParameter("@Out_quantity", SqlDbType.VarChar,50),
                new SqlParameter("@Out_Warehouse", SqlDbType.VarChar,50),
                new SqlParameter("@Out_Warehouse_kw", SqlDbType.VarChar,50),
                new SqlParameter("@In_quantity", SqlDbType.VarChar,50),
                new SqlParameter("@In_Warehouse", SqlDbType.VarChar,50),
                new SqlParameter("@In_Warehouse_kw", SqlDbType.VarChar,50),
                new SqlParameter("@dilution_quantity", SqlDbType.VarChar,50),
                new SqlParameter("@dilution_result", SqlDbType.VarChar,50),
                new SqlParameter("@dilution_weights", SqlDbType.VarChar,50),
                  new SqlParameter("@Out_logistics", SqlDbType.VarChar,250)//物流信息
            };
            parameters[0].Value = id;
            parameters[1].Value = weights;
            parameters[2].Value = density;
            parameters[3].Value = shape;
            parameters[4].Value = VIT;
            parameters[5].Value = Out_quantity;
            parameters[6].Value = Out_Warehouse;
            parameters[7].Value = Out_Warehouse_kw;
            parameters[8].Value = In_quantity;
            parameters[9].Value = In_Warehouse;
            parameters[10].Value = In_Warehouse_kw;
            parameters[11].Value = dilution_quantity;
            parameters[12].Value = dilution_result;
            parameters[13].Value = dilution_weights;
            parameters[14].Value = Out_logistics;
            int rows = DbHelperSQL.ExecuteSql(sb.ToString(), parameters);
            if (rows > 0)
            {
                if (type == "packing")
                {
                    sb.Length = 0;//清空
                    sb.AppendLine(" DELETE boar_collection_dilution_barcode WHERE bc_id='"+id+"'"); 
                    for (int i = 0; i < int.Parse(dilution_quantity); i++)
                    {
                        string carcode = id + i.ToString("0000");
                        sb.AppendLine(" INSERT INTO dbo.boar_collection_dilution_barcode  ");
                        sb.AppendLine("         ( bc_id, barcode, dotime, remarks )  ");
                        sb.AppendLine(" VALUES  ( '"+ id + "', -- bc_id - varchar(50)  ");
                        sb.AppendLine("           '"+ carcode + "', -- barcode - varchar(50)  ");
                        sb.AppendLine("           GETDATE(), -- dotime - datetime  ");
                        sb.AppendLine("           ''  -- remarks - varchar(50)  ");
                        sb.AppendLine("           )   ");

                    }
                    int rr = DbHelperSQL.ExecuteSql(sb.ToString(), parameters);
                    if (rows > 0) return true;
                    else return false;
                }
                 else   return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetListAuto(string SaleOrderID, string bt, string et)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("  EXEC USP_Create_PreWorkDocument '"+ SaleOrderID + "','"+bt+"', '"+et+"' ");

        
            return DbHelperSQL.Query(sb.ToString());
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetListLog(string sql)
        {
            string sql1 = "SELECT * FROM     dbo.boar_collection_dilution_barcode " + sql;
              

            
            return DbHelperSQL.Query(sql1);
        }

        public DataSet GetBarList(string barcode)
        {
            string sql1 = "SELECT top 5 * FROM dbo.V_boar WHERE barcode = '"+barcode+"' ORDER BY createtime desc";


            return DbHelperSQL.Query(sql1);
        }



        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetSAS_InfoDataList(string strWhere)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(" SELECT * FROM dbo.SAS_InfoData   ");

            if (strWhere.Trim() != "")
            {
                sb.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(sb.ToString());
        }
        public DataSet GetSAS_InfoImgDataList(string strWhere)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(" SELECT * FROM dbo.SAS_InfoImgData   ");

            if (strWhere.Trim() != "")
            {
                sb.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(sb.ToString());
        }
        #endregion  ExtensionMethod
    }


}

