﻿using System;
namespace XHD.Model
{
	/// <summary>
	/// Basic_Boar_Drug:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class Basic_Boar_Drug
	{
		public Basic_Boar_Drug()
		{}
		#region Model
		private string _id;
		private string _boarid;
		private decimal? _dosage;
		private string _createid;
		private string _drugId;
		private string _remarks;
		private DateTime? _createdate;
		private DateTime? _drugTime; 
		/// <summary>
		/// 
		/// </summary>
		public string id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string boarId
		{
			set{ _boarid=value;}
			get{return _boarid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? Dosage
		{
			set{ _dosage=value;}
			get{return _dosage;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string createId
		{
			set{ _createid=value;}
			get{return _createid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? createDate
		{
			set{ _createdate=value;}
			get{return _createdate;}
		}

        public string remarks { get => _remarks; set => _remarks = value; }
        public string drugId { get => _drugId; set => _drugId = value; }
        public DateTime? drugTime { get => _drugTime; set => _drugTime = value; }
        #endregion Model

    }
}

