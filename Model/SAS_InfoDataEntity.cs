﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XHD.Model
{
[Serializable]
public partial class SAS_InfoDataEntity
 {
public string casereference{ get; set; }
public string sperm{ get; set; }
public string breed{ get; set; }
public string strain{ get; set; }
public string donorname{ get; set; }
public string subject{ get; set; }
public string additionalnumber{ get; set; }
public string barn{ get; set; }
public string lastcollection{ get; set; }
public string state{ get; set; }
public string geneticline{ get; set; }
public string batchnumber{ get; set; }
public string spermnumber{ get; set; }
public int? initialdilutorvolume{ get; set; }
public int? analysisdilution{ get; set; }
public string collector{ get; set; }
public string Operator{ get; set; }
public int? dosisvolume{ get; set; }
public int? usefulspermperdosis{ get; set; }
public string dilutor{ get; set; }
public int? ejaculatedvolume{ get; set; }
public string sementemp{ get; set; }
public string spermsdsispayunits{ get; set; }
public int? produceddosis{ get; set; }
public int? diluentvolume{ get; set; }
public int? dilutionratio{ get; set; }//这个变了
public int? totalspermsperdosis{ get; set; }
public int? totalfinalvolume{ get; set; }
public string date{ get; set; }
public int? totnumber{ get; set; }
public double? totpercen{ get; set; }
public int? totmillions{ get; set; }
public double? totmml{ get; set; }
public int? motnumber{ get; set; }
public double? motpercen{ get; set; }
public int? motmillions{ get; set; }
public double? motmml{ get; set; }
public int? nornumber{ get; set; }
public double? norpercen{ get; set; }
public int? normillions{ get; set; }
public int? normml{ get; set; }
public int? mnonumber{ get; set; }
public double? mnopercen{ get; set; }
public int? mnomillions{ get; set; }
public double? mnomml{ get; set; }
public int? usenumber{ get; set; }
public double? usepercen{ get; set; }
public int? usemillions{ get; set; }
public double? usemml{ get; set; }
public int? pronumber{ get; set; }
public double? propercen{ get; set; }
public int? promillions{ get; set; }
public double? promml{ get; set; }
public int? rapnumber{ get; set; }
public double? rappercen{ get; set; }
public int? rapmillions{ get; set; }
public double? rapmml{ get; set; }
public int? slonumber{ get; set; }
public double? slopercen{ get; set; }
public int? slomillions{ get; set; }
public double? slomml{ get; set; }
public int? cirnumber{ get; set; }
public double? cirpercen{ get; set; }
public int? cirmillions{ get; set; }
public double? cirmml{ get; set; }
public int? locnumber{ get; set; }
public double? locpercen{ get; set; }
public int? locmillions{ get; set; }
public double? locmml{ get; set; }
public int? immnumber{ get; set; }
public double? immpercen{ get; set; }
public int? immmillions{ get; set; }
public double? immmml{ get; set; }
public int? mornumber{ get; set; }
public double? morpercen{ get; set; }
public int? mormillions{ get; set; }
public double? mormml{ get; set; }
public int? bennumber{ get; set; }
public double? benpercen{ get; set; }
public int? benmillions{ get; set; }
public double? benmml{ get; set; }
public int? coinumber{ get; set; }
public double? coipercen{ get; set; }
public int? coimillions{ get; set; }
public double? coimml{ get; set; }
public int? dmrnumber{ get; set; }
public double? dmrpercen{ get; set; }
public int? dmrmillions{ get; set; }
public double? dmrmml{ get; set; }
public int? disnumber{ get; set; }
public double? dispercen{ get; set; }
public int? dismillions{ get; set; }
public double? dismml{ get; set; }
public int? concentration{ get; set; }

public int? iTotalNum { get; set; }
        public int? malnumber { get; set; }
        public double? malpercen { get; set; }
        public int? prognumber { get; set; }
        public double? progpercen { get; set; } 
    }

    public class SAS_InfoImgDataEntity {
        public string casereference { get; set; }
        public string img_Name { get; set; }
        public string orgin_Name { get; set; }
        public DateTime createtime { get; set; }

    }


    public class SAS_MFEntity
    {
        public double mnomml;
        public double morpercen;

        public int dNear { get; set; }
        public int dFastForw { get; set; }
        public int dAnnular { get; set; }
        public int dInactive { get; set; }
        public int dTailBend { get; set; }
        public int dEndRoll { get; set; }
        public int dDMR { get; set; }
        public int dFar { get; set; }

        public int iTotalNum { get; set; }
        public int iNormalNum { get; set; }
        public double dNormalRatio { get; set; }

        public string vbatch { get; set; }
        public DateTime tcheckoutDate { get; set; }
        public string vcheckoutTime { get; set; }
        public string voperator { get; set; } 
        public string vtype { get; set; }
        public decimal ddensity { get; set; }
        public decimal dvitality { get; set; }
        public int icopies { get; set; }
        public decimal dadd { get; set; }
        public string vresult { get; set; }
        public decimal dcapacity { get; set; }
        public decimal dmilliliter { get; set; }
        public decimal dmotileSperms { get; set; }
        public decimal dmalformation { get; set; }
        public bool bstandard { get; set; }
        public List<SAS_MF_VFileEntity> vfile { get; set; }
        public List<SAS_MF_mxlistEntity> mxlist { get; set;}
        public double dMalformaRatio { get; set; }
        public int dForwVitality { get; set; }
        public double progpercen { get; set; }
        public int iMalformaNum { get; set; }
        public int dSlowForw { get; set; }
        public int dPlaceMove { get; set; }
        public string donorname { get; set; }
        public string additionalnumber{ get; set; }
public string barn{ get; set; }
public string lastcollection{ get; set; }
public string state{ get; set; }
public string geneticline{ get; set; }
public string batchnumber{ get; set; }
public string spermnumber{ get; set; }
public int? initialdilutorvolume{ get; set; }
public int? analysisdilution{ get; set; }
public string collector{ get; set; }
public string Operator{ get; set; }
public int? dosisvolume{ get; set; }
public int? usefulspermperdosis{ get; set; }
public string dilutor{ get; set; }
public int? ejaculatedvolume{ get; set; }
public string sementemp{ get; set; }
public string spermsdsispayunits{ get; set; }
public int? produceddosis{ get; set; }
public int? diluentvolume{ get; set; }
public int? dilutionratio{ get; set; }//这个变了
public int? totalspermsperdosis{ get; set; }
public int? totalfinalvolume{ get; set; }
public string date{ get; set; }
public int? totnumber{ get; set; }
public double? totpercen{ get; set; }
public int? totmillions{ get; set; }
public double? totmml{ get; set; }
public int? motnumber{ get; set; }
public double? motpercen{ get; set; }
public int? motmillions{ get; set; }
public double? motmml{ get; set; }
public int? nornumber{ get; set; }
public double? norpercen{ get; set; }
public int? normillions{ get; set; }
public int? normml{ get; set; }
public int? mnonumber{ get; set; }
public double? mnopercen{ get; set; }
public int? mnomillions{ get; set; } 
public int? usenumber{ get; set; }
public double? usepercen{ get; set; }
public int? usemillions{ get; set; }
public double? usemml{ get; set; }
public int? pronumber{ get; set; }
public double? propercen{ get; set; }
public int? promillions{ get; set; }
public double? promml{ get; set; }
public int? rapnumber{ get; set; }
public double? rappercen{ get; set; }
public int? rapmillions{ get; set; }
public double? rapmml{ get; set; }
public int? slonumber{ get; set; }
public double? slopercen{ get; set; }
public int? slomillions{ get; set; }
public double? slomml{ get; set; }
public int? cirnumber{ get; set; }
public double? cirpercen{ get; set; }
public int? cirmillions{ get; set; }
public double? cirmml{ get; set; }
public int? locnumber{ get; set; }
public double? locpercen{ get; set; }
public int? locmillions{ get; set; }
public double? locmml{ get; set; }
public int? immnumber{ get; set; }
public double? immpercen{ get; set; }
public int? immmillions{ get; set; }
public double? immmml{ get; set; }
public int? mornumber{ get; set; } 
public int? mormillions{ get; set; }
public double? mormml{ get; set; }
public int? bennumber{ get; set; }
public double? benpercen{ get; set; }
public int? benmillions{ get; set; }
public double? benmml{ get; set; }
public int? coinumber{ get; set; }
public double? coipercen{ get; set; }
public int? coimillions{ get; set; }
public double? coimml{ get; set; }
public int? dmrnumber{ get; set; }
public double? dmrpercen{ get; set; }
public int? dmrmillions{ get; set; }
public double? dmrmml{ get; set; }
public int? disnumber{ get; set; }
public double? dispercen{ get; set; }
public int? dismillions{ get; set; }
public double? dismml{ get; set; }
    }

    public class SAS_MF_VFileEntity {
        public string vtpmc { get; set; }
        public string vfile { get; set; }
        
    }
    public class SAS_MF_mxlistEntity
    {
        public string vgzgp { get; set; }
        public DateTime tcsrq { get; set; }
        public string vpz { get; set; }
        public string vpx { get; set; }
        
    }
}

