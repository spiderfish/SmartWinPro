﻿/*
* Sale_order_details.cs
*
* 功 能： N/A
* 类 名： Sale_order_details
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2016-03-08 08:53:10    黄润伟    
*
* Copyright © 2015 www.xhdcrm.com All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：小黄豆                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace XHD.Model
{
    /// <summary>
    /// Sale_order_details:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class Sale_order_details
    {
        public Sale_order_details()
        { }
        #region Model
        private string _order_id;
        private string _product_id;
        private decimal? _agio;
        private int? _quantity;
        private decimal? _amount;
        private string _detail_status;

        private decimal? _capacity;
        private int? _uspd;
        /// <summary>
        /// 
        /// </summary>
        public string order_id
        {
            set { _order_id = value; }
            get { return _order_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string product_id
        {
            set { _product_id = value; }
            get { return _product_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? agio
        {
            set { _agio = value; }
            get { return _agio; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? quantity
        {
            set { _quantity = value; }
            get { return _quantity; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? amount
        {
            set { _amount = value; }
            get { return _amount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string detail_status
        {
            set { _detail_status = value; }
            get { return _detail_status; }
        }

        public int? uspd
        {
            set { _uspd = value; }
            get { return _uspd; }
        }

        public decimal? capacity { get => _capacity; set => _capacity = value; }
        #endregion Model

    }
}

