﻿using System;
namespace XHD.Model
{
	/// <summary>
	/// Basic_Boar_changeLog:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class Basic_Boar_changeLog
	{
		public Basic_Boar_changeLog()
		{}
		#region Model
		private string _id;
		private string _boarid;
		private string _inDormId;
		private string _incorralid;
		private DateTime? _intime;
		private string _outDormId;
		private string _outcorralid;
		private DateTime? _outtime;
		private string _accpetperson;
		private string _remarks;
		/// <summary>
		/// 
		/// </summary>
		public string id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 产品编号
		/// </summary>
		public string BoarId
		{
			set{ _boarid=value;}
			get{return _boarid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string InDormId
		{
			set{ _inDormId=value;}
			get{return _inDormId;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string InCorralId
		{
			set{ _incorralid=value;}
			get{return _incorralid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? InTime
		{
			set{ _intime=value;}
			get{return _intime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string OutDormId
		{
			set{ _outDormId=value;}
			get{return _outDormId;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string OutCorralId
		{
			set{ _outcorralid=value;}
			get{return _outcorralid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? OutTime
		{
			set{ _outtime=value;}
			get{return _outtime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string accpetPerson
		{
			set{ _accpetperson=value;}
			get{return _accpetperson;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string remarks
		{
			set{ _remarks=value;}
			get{return _remarks;}
		}
		#endregion Model

	}
}

