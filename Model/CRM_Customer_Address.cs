﻿using System;
namespace XHD.Model
{
	/// <summary>
	/// CRM_Customer_Address:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class CRM_Customer_Address
	{
		public CRM_Customer_Address()
		{}
		#region Model
		private string _id;
		private string _cusid;
		private string _addName;
		private string _addtel;
		private string _addmail;
		private string _address;
		private string _isdefault;
		private DateTime? _creattime= DateTime.Now;
		/// <summary>
		/// 
		/// </summary>
		public string Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CusId
		{
			set{ _cusid=value;}
			get{return _cusid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string addTel
		{
			set{ _addtel=value;}
			get{return _addtel;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string addMail
		{
			set{ _addmail=value;}
			get{return _addmail;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string address
		{
			set{ _address=value;}
			get{return _address;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string IsDefault
		{
			set{ _isdefault=value;}
			get{return _isdefault;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreatTime
		{
			set{ _creattime=value;}
			get{return _creattime;}
		}

        public string addName { get => _addName; set => _addName = value; }
        #endregion Model

    }
}

