﻿using System;
namespace XHD.Model
{
	/// <summary>
	/// Basic_Boar_sys_Genealogy:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class Basic_Boar_sys_Genealogy
	{
		public Basic_Boar_sys_Genealogy()
		{}
		#region Model
		private string _id;
		private string _boarid_origin;
		private string _boarid;
		private string _parfatherboarid;
		private string _parmotherboarid;
		private string _parf_barcode;
		private string _parf_name;
		private string _parf_remark;
		private string _parm_barcode;
		private string _parm_name;
		private string _parm_remark;
		private int? _levels;
		/// <summary>
		/// 
		/// </summary>
		public string id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string boarId_origin
		{
			set{ _boarid_origin=value;}
			get{return _boarid_origin;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string boarId
		{
			set{ _boarid=value;}
			get{return _boarid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ParFatherBoarId
		{
			set{ _parfatherboarid=value;}
			get{return _parfatherboarid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ParMotherBoarId
		{
			set{ _parmotherboarid=value;}
			get{return _parmotherboarid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ParF_barcode
		{
			set{ _parf_barcode=value;}
			get{return _parf_barcode;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ParF_name
		{
			set{ _parf_name=value;}
			get{return _parf_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ParF_remark
		{
			set{ _parf_remark=value;}
			get{return _parf_remark;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ParM_barcode
		{
			set{ _parm_barcode=value;}
			get{return _parm_barcode;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ParM_name
		{
			set{ _parm_name=value;}
			get{return _parm_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ParM_remark
		{
			set{ _parm_remark=value;}
			get{return _parm_remark;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? levels
		{
			set{ _levels=value;}
			get{return _levels;}
		}
		#endregion Model

	}
}

