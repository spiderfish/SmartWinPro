﻿using System;
namespace XHD.Model
{
	/// <summary>
	/// Basic_boar:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class Basic_boar
	{
		public Basic_boar()
		{}
		#region Model
		private string _id;
		private string _product_name;
		private string _category_id;
		private string _status;
		private string _unit;
		private string _unit_id; 
		private decimal? _cost;
		private decimal? _price;
		private decimal? _agio;
		private string _remarks;
		private string _specifications;
		private string _create_id;
		private DateTime? _create_time;
		private string _barcode;
		private int? _collectionstatus;
		private int? _efficacystate;
		private DateTime? _lastcollectime;
		private DateTime? _lastefficatime;
		private string _drug_id;
		private string _img_url;
		private DateTime? _birthday;
		private string _shortcode;
		private string _origin;
		private string _place;
		private string _dormid;
		private string _corralid;
		private string _sex;
		private DateTime? _indate;
		private string _sysid;
		private string _productid;
		private string _pro_seriesid;
		private string _pro_series_levelid;
		/// <summary>
		/// 
		/// </summary>
		public string id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string product_name
		{
			set{ _product_name=value;}
			get{return _product_name;}
		}
		/// <summary>
		/// 产品Prouct表的ID
		/// </summary>
		public string category_id
		{
			set{ _category_id=value;}
			get{return _category_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string status
		{
			set{ _status=value;}
			get{return _status;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string unit
		{
			set{ _unit=value;}
			get{return _unit;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? cost
		{
			set{ _cost=value;}
			get{return _cost;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? price
		{
			set{ _price=value;}
			get{return _price;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? agio
		{
			set{ _agio=value;}
			get{return _agio;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string remarks
		{
			set{ _remarks=value;}
			get{return _remarks;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string specifications
		{
			set{ _specifications=value;}
			get{return _specifications;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string create_id
		{
			set{ _create_id=value;}
			get{return _create_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? create_time
		{
			set{ _create_time=value;}
			get{return _create_time;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string barcode
		{
			set{ _barcode=value;}
			get{return _barcode;}
		}
		/// <summary>
		/// 采精状态1、可采精 2、采精中 3、养身期
		/// </summary>
		public int? CollectionStatus
		{
			set{ _collectionstatus=value;}
			get{return _collectionstatus;}
		}
		/// <summary>
		/// 药效状态：1、非药效期 2、药效期
		/// </summary>
		public int? EfficacyState
		{
			set{ _efficacystate=value;}
			get{return _efficacystate;}
		}
		/// <summary>
		/// 最后采集日期
		/// </summary>
		public DateTime? LastCollecTime
		{
			set{ _lastcollectime=value;}
			get{return _lastcollectime;}
		}
		/// <summary>
		/// 最后喂药时间
		/// </summary>
		public DateTime? LastEfficaTime
		{
			set{ _lastefficatime=value;}
			get{return _lastefficatime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Drug_id
		{
			set{ _drug_id=value;}
			get{return _drug_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string img_url
		{
			set{ _img_url=value;}
			get{return _img_url;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? birthday
		{
			set{ _birthday=value;}
			get{return _birthday;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string shortcode
		{
			set{ _shortcode=value;}
			get{return _shortcode;}
		}
		/// <summary>
		/// 来源
		/// </summary>
		public string origin
		{
			set{ _origin=value;}
			get{return _origin;}
		}
		/// <summary>
		/// 场地
		/// </summary>
		public string place
		{
			set{ _place=value;}
			get{return _place;}
		}
		/// <summary>
		/// 畜舍
		/// </summary>
		public string dormId
		{
			set{ _dormid=value;}
			get{return _dormid;}
		}
		/// <summary>
		/// 畜栏
		/// </summary>
		public string corralId
		{
			set{ _corralid=value;}
			get{return _corralid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string sex
		{
			set{ _sex=value;}
			get{return _sex;}
		}
		/// <summary>
		/// 进厂时间
		/// </summary>
		public DateTime? InDate
		{
			set{ _indate=value;}
			get{return _indate;}
		}
		/// <summary>
		/// 系谱编号
		/// </summary>
		public string sysId
		{
			set{ _sysid=value;}
			get{return _sysid;}
		}
		/// <summary>
		/// 品种
		/// </summary>
		public string productId
		{
			set{ _productid=value;}
			get{return _productid;}
		}
		/// <summary>
		/// 品系
		/// </summary>
		public string Pro_SeriesId
		{
			set{ _pro_seriesid=value;}
			get{return _pro_seriesid;}
		}
		/// <summary>
		/// 等级
		/// </summary>
		public string Pro_Series_LevelId
		{
			set{ _pro_series_levelid=value;}
			get{return _pro_series_levelid;}
		}

        public string unit_id { get => _unit_id; set => _unit_id = value; }
        #endregion Model

    }
}

