﻿using System;

namespace XHD.Model
{
    [Serializable]
    public class VitBI_ExtenEntity1
    {
        private int? totnumber;
        private decimal? totpercen;
        private int? totmillions;
        private decimal? totmml;
        private int? nornumber;
        private decimal? norpercen;
        private int? normillions;
        private decimal? normml;
        private int? motnumber;
        private decimal?  motpercen;
        private int? motmillions;
        private decimal? motmml;
        private int? mnonumber;
        private decimal? mnopercen;
        private int? mnomillions;
        private decimal? mnomml;
        private int? usenumber;
        private decimal? usepercen;
        private int? usemillions;
        private decimal? usemml;

        public int? Totnumber
        {
            set { totnumber = value; }
            get { return totnumber; }
        }

        public decimal? Totpercen
        {
            set { totpercen = value; }
            get { return totpercen; }
        }
        public int? Totmillions
        {
            set { totmillions = value; }
            get { return totmillions; }
        }
        public decimal? Totmml
        {
            set { totmml = value; }
            get { return totmml; }
        }
        public int? Nornumber
        {
            set { nornumber = value; }
            get { return nornumber; }
        }
        public decimal? Norpercen
        {
            set { norpercen = value; }
            get { return norpercen; }
        }
        public int? Normillions
        {
            set { normillions = value; }
            get { return normillions; }
        }
        public decimal? Normml
        {
            set { normml = value; }
            get { return normml; }
        }
        public int? Motnumber
        {
            set { motnumber = value; }
            get { return motnumber; }
        }
        public decimal? Motpercen
        {
            set { motpercen = value; }
            get { return motpercen; }
        }
        public int? Motmillions
        {
            set { motmillions = value; }
            get { return motmillions; }
        }
        public decimal? Motmml
        {
            set { motmml = value; }
            get { return motmml; }
        }
        public int? Mnonumber
        {
            set { mnonumber = value; }
            get { return mnonumber; }
        }
        public decimal? Mnopercen
        {
            set { mnopercen = value; }
            get { return mnopercen; }
        }
        public int? Mnomillions
        {
            set { mnomillions = value; }
            get { return mnomillions; }
        }
        public decimal? Mnomml
        {
            set { mnomml = value; }
            get { return mnomml; }
        }
        public int? Usenumber
        {
            set { usenumber = value; }
            get { return usenumber; }
        }
        public decimal? Usepercen
        {
            set { usepercen = value; }
            get { return usepercen; }
        }
        public int? Usemillions
        {
            set { usemillions = value; }
            get { return usemillions; }
        }
        public decimal? Usemml
        {
            set { usemml = value; }
            get { return usemml; }
        }
        /// <
            }
}