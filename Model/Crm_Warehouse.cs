﻿using System;
namespace XHD.Model
{
	/// <summary>
	/// Crm_Warehouse:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class Crm_Warehouse
	{
		public Crm_Warehouse()
		{}
		#region Model
		private int _id;
		private string _name;
		private string _address;
		private string _lxrid;
		private string _lxr;
		private string _lxrdh;
		private string _remark;
		private string _inempid;
		private DateTime? _indate;
		private string _editempid;
		private DateTime? _editdate;
		private string _isdel;
		private string _delempid;
		private DateTime? _deldate;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Name
		{
			set{ _name=value;}
			get{return _name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Address
		{
			set{ _address=value;}
			get{return _address;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Lxrid
		{
			set{ _lxrid=value;}
			get{return _lxrid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Lxr
		{
			set{ _lxr=value;}
			get{return _lxr;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Lxrdh
		{
			set{ _lxrdh=value;}
			get{return _lxrdh;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Remark
		{
			set{ _remark=value;}
			get{return _remark;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string InEmpID
		{
			set{ _inempid=value;}
			get{return _inempid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? InDate
		{
			set{ _indate=value;}
			get{return _indate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string EditEmpID
		{
			set{ _editempid=value;}
			get{return _editempid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? EditDate
		{
			set{ _editdate=value;}
			get{return _editdate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string IsDel
		{
			set{ _isdel=value;}
			get{return _isdel;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string DelEmpID
		{
			set{ _delempid=value;}
			get{return _delempid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? DelDate
		{
			set{ _deldate=value;}
			get{return _deldate;}
		}
		#endregion Model

	}
}

