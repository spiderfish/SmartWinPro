﻿/*
* Product.cs
*
* 功 能： N/A
* 类 名： Product
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2016-03-06 13:36:04    黄润伟    
*
* Copyright © 2015 www.xhdcrm.com All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：小黄豆                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace XHD.Model
{
    /// <summary>
    /// Product:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class Product
    {
        public Product()
        { }
        #region Model
        private string _id;
        private string _product_name;
        private string _category_id;
        private string _status;
        private string _unit;
        private string _unit_id;
        private string _dosageUnit_id;
        private string _dosageUnit;
        private string _specifications_id;
        private decimal? _cost;
        private decimal? _price;
        private decimal? _agio;
        private decimal? _OnlinePrice; 
        private string _remarks;
        private string _specifications;
        private string _create_id;
        private DateTime? _create_time;
        private string _barcode;
        private int? _dailyoutput;
        private int? _pickcycle;
        private string _Series_Id;
        private string _Series_Name;
        private string _Level_Id;
        private string _Level_Name;
        private string _img;
        private string _richText;
        private int? _waitDay;
        private decimal? _Quantity;
        private decimal? _Dose;
        private decimal? _EffectiveSperm;
        /// <summary>
        /// 
        /// </summary>
        public string id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string product_name
        {
            set { _product_name = value; }
            get { return _product_name; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string category_id
        {
            set { _category_id = value; }
            get { return _category_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string status
        {
            set { _status = value; }
            get { return _status; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string unit
        {
            set { _unit = value; }
            get { return _unit; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? cost
        {
            set { _cost = value; }
            get { return _cost; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? price
        {
            set { _price = value; }
            get { return _price; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? agio
        {
            set { _agio = value; }
            get { return _agio; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string remarks
        {
            set { _remarks = value; }
            get { return _remarks; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string specifications
        {
            set { _specifications = value; }
            get { return _specifications; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string create_id
        {
            set { _create_id = value; }
            get { return _create_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? create_time
        {
            set { _create_time = value; }
            get { return _create_time; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string barcode
        {
            set { _barcode = value; }
            get { return _barcode; }
        }
        /// <summary>
        /// 日产量
        /// </summary>
        public int? dailyoutput
        {
            set { _dailyoutput = value; }
            get { return _dailyoutput; }
        }
        /// <summary>
        /// 采集周期
        /// </summary>
        public int? pickcycle
        {
            set { _pickcycle = value; }
            get { return _pickcycle; }
        }

        public string unit_id { get => _unit_id; set => _unit_id = value; }
        public string dosageUnit_id { get => _dosageUnit_id; set => _dosageUnit_id = value; }
        public string dosageUnit { get => _dosageUnit; set => _dosageUnit = value; }
        public string specifications_id { get => _specifications_id; set => _specifications_id = value; }
        public string Img { get => _img; set => _img = value; }
        public string Level_Name { get => _Level_Name; set => _Level_Name = value; }
        public string Level_Id { get => _Level_Id; set => _Level_Id = value; }
        public string Series_Name { get => _Series_Name; set => _Series_Name = value; }
        public string Series_Id { get => _Series_Id; set => _Series_Id = value; }
        public string RichText { get => _richText; set => _richText = value; }
        public decimal? OnlinePrice { get => _OnlinePrice; set => _OnlinePrice = value; }
        public int? WaitDay { get => _waitDay; set => _waitDay = value; }
        public decimal? EffectiveSperm { get => _EffectiveSperm; set => _EffectiveSperm = value; }
        public decimal? Dose { get => _Dose; set => _Dose = value; }
        public decimal? Quantity { get => _Quantity; set => _Quantity = value; }
        #endregion Model

    }
}

