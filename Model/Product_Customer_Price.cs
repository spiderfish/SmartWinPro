﻿using System;
namespace XHD.Model
{
	/// <summary>
	/// Product_Customer_Price:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class Product_Customer_Price
	{
		public Product_Customer_Price()
		{}
		#region Model
		private string _product_id;
		private string _customer_id;
		private decimal? _price;
		private DateTime? _createtime;
		private string _createname;
		private string _remarks;
		/// <summary>
		/// 
		/// </summary>
		public string product_id
		{
			set{ _product_id=value;}
			get{return _product_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string customer_id
		{
			set{ _customer_id=value;}
			get{return _customer_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? price
		{
			set{ _price=value;}
			get{return _price;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? createTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string createName
		{
			set{ _createname=value;}
			get{return _createname;}
		}

        public string remarks { get => _remarks; set => _remarks = value; }
        #endregion Model

    }
}

