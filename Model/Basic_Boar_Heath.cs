﻿using System;
namespace XHD.Model
{
	/// <summary>
	/// Basic_Boar_Heath:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class Basic_Boar_Heath
	{
		public Basic_Boar_Heath()
		{}
		#region Model
		private string _id;
		private string _boarid;
		private decimal? _weight;
		private decimal? _backfat;
		private decimal? _eyearea;
		private decimal? _temperature;
		private string _serum;
		private string _semen;
		private string _drinkwater;
		private string _feed;
		private string _feces;
		private string _saliva;
		private string _pigheath;
		private string _dormheath;
		private string _others;
		private string _remarks;
		private string _monitorperson;
		private DateTime? _monitordate;
		private string _createperson;
		private DateTime? _createdate;
		/// <summary>
		/// 
		/// </summary>
		public string id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string boarId
		{
			set{ _boarid=value;}
			get{return _boarid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? weight
		{
			set{ _weight=value;}
			get{return _weight;}
		}
		/// <summary>
		/// 背膘
		/// </summary>
		public decimal? backfat
		{
			set{ _backfat=value;}
			get{return _backfat;}
		}
		/// <summary>
		///  眼肌面积
		/// </summary>
		public decimal? eyeArea
		{
			set{ _eyearea=value;}
			get{return _eyearea;}
		}
		/// <summary>
		/// 体温
		/// </summary>
		public decimal? temperature
		{
			set{ _temperature=value;}
			get{return _temperature;}
		}
		/// <summary>
		/// 血清 阴性 或 阳性
		/// </summary>
		public string serum
		{
			set{ _serum=value;}
			get{return _serum;}
		}
		/// <summary>
		/// 精液 阴性 或 阳性
		/// </summary>
		public string semen
		{
			set{ _semen=value;}
			get{return _semen;}
		}
		/// <summary>
		/// 饮水  阴性 或 阳性
		/// </summary>
		public string drinkWater
		{
			set{ _drinkwater=value;}
			get{return _drinkwater;}
		}
		/// <summary>
		///  饲料：选择，阴性 或 阳性
		/// </summary>
		public string feed
		{
			set{ _feed=value;}
			get{return _feed;}
		}
		/// <summary>
		///  粪便：选择，阴性 或 阳性
		/// </summary>
		public string Feces
		{
			set{ _feces=value;}
			get{return _feces;}
		}
		/// <summary>
		/// 选择，阴性 或 阳性
		/// </summary>
		public string saliva
		{
			set{ _saliva=value;}
			get{return _saliva;}
		}
		/// <summary>
		/// 合格 或 不合格
		/// </summary>
		public string pigHeath
		{
			set{ _pigheath=value;}
			get{return _pigheath;}
		}
		/// <summary>
		///  舍内卫生 合格 或 不合格
		/// </summary>
		public string dormHeath
		{
			set{ _dormheath=value;}
			get{return _dormheath;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string others
		{
			set{ _others=value;}
			get{return _others;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string remarks
		{
			set{ _remarks=value;}
			get{return _remarks;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string monitorPerson
		{
			set{ _monitorperson=value;}
			get{return _monitorperson;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? monitorDate
		{
			set{ _monitordate=value;}
			get{return _monitordate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string createPerson
		{
			set{ _createperson=value;}
			get{return _createperson;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? createDate
		{
			set{ _createdate=value;}
			get{return _createdate;}
		}
		#endregion Model

	}
}

