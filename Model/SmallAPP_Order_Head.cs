﻿using System;
namespace XHD.Model
{
	/// <summary>
	/// SmallAPP_Order_Head:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class SmallAPP_Order_Head
	{
		public SmallAPP_Order_Head()
		{}
		#region Model
		private string _orderid;
		private DateTime? _createtime;
		private string _createname;
		private string _wx_id;
		private string _cusid;
		private string _orderstatus;
		private string _orderstatusname;
		private string _payRemarks;
		private string _remarks;
		private string _addressId;
		/// <summary>
		/// 
		/// </summary>
		public string OrderId
		{
			set{ _orderid=value;}
			get{return _orderid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreateTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CreateName
		{
			set{ _createname=value;}
			get{return _createname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string WX_ID
		{
			set{ _wx_id=value;}
			get{return _wx_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CusId
		{
			set{ _cusid=value;}
			get{return _cusid;}
		}
		/// <summary>
		/// 0待付款，1已付款，2备货中，3已发货
		/// </summary>
		public string orderStatus
		{
			set{ _orderstatus=value;}
			get{return _orderstatus;}
		}
		/// <summary>
		/// 0待付款，1已付款，2备货中，3已发货
		/// </summary>
		public string orderStatusName
		{
			set{ _orderstatusname=value;}
			get{return _orderstatusname;}
		}

        public string Remarks { get => _remarks; set => _remarks = value; }
        public string PayRemarks { get => _payRemarks; set => _payRemarks = value; }
        public string AddressId { get => _addressId; set => _addressId = value; }
        #endregion Model

    }
}

