﻿using System;
namespace XHD.Model
{
	/// <summary>
	/// Basic_Boar_sys:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class Basic_Boar_sys
	{
		public Basic_Boar_sys()
		{}
		#region Model
		private string _id;
		private string _boarid;
		private decimal? _weight;
		private string _birthplace;
		private int? _leftnip;
		private int? _rightnip;
		private DateTime? _stopmilkdate;
		private decimal? _stopmilkweight;
		private int? _samenestsnumber;
		private int? _bithsnumber;
		private DateTime? _outdate;
		private string _levels;
		private string _description;
		private string _license;
		/// <summary>
		/// 
		/// </summary>
		public string id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 产品编号
		/// </summary>
		public string BoarId
		{
			set{ _boarid=value;}
			get{return _boarid;}
		}
		/// <summary>
		/// 体重
		/// </summary>
		public decimal? weight
		{
			set{ _weight=value;}
			get{return _weight;}
		}
		/// <summary>
		/// 出生地
		/// </summary>
		public string birthPlace
		{
			set{ _birthplace=value;}
			get{return _birthplace;}
		}
		/// <summary>
		/// 左边乳头
		/// </summary>
		public int? leftNip
		{
			set{ _leftnip=value;}
			get{return _leftnip;}
		}
		/// <summary>
		/// 右边乳头
		/// </summary>
		public int? rightNip
		{
			set{ _rightnip=value;}
			get{return _rightnip;}
		}
		/// <summary>
		/// 断奶日期
		/// </summary>
		public DateTime? stopMilkDate
		{
			set{ _stopmilkdate=value;}
			get{return _stopmilkdate;}
		}
		/// <summary>
		/// 断奶体重
		/// </summary>
		public decimal? stopMilkWeight
		{
			set{ _stopmilkweight=value;}
			get{return _stopmilkweight;}
		}
		/// <summary>
		/// 同窝头数
		/// </summary>
		public int? sameNestsNumber
		{
			set{ _samenestsnumber=value;}
			get{return _samenestsnumber;}
		}
		/// <summary>
		/// 出生胎次
		/// </summary>
		public int? bithsNumber
		{
			set{ _bithsnumber=value;}
			get{return _bithsnumber;}
		}
		/// <summary>
		/// 出厂日期
		/// </summary>
		public DateTime? outDate
		{
			set{ _outdate=value;}
			get{return _outdate;}
		}
		/// <summary>
		/// 级别
		/// </summary>
		public string levels
		{
			set{ _levels=value;}
			get{return _levels;}
		}
		/// <summary>
		/// 外形特征
		/// </summary>
		public string description
		{
			set{ _description=value;}
			get{return _description;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string license
		{
			set{ _license=value;}
			get{return _license;}
		}
		#endregion Model

	}
}

