﻿using System;
namespace XHD.Model
{
	/// <summary>
	/// Basic_Product_Series_Level:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class Basic_Product_Series_Level
	{
		public Basic_Product_Series_Level()
		{}
		#region Model
		private string _id;
		private string _levelname;
		private string _productseriesid;
		private string _create_id;
		private DateTime? _create_time;
		private string _remarks;
		private string _def1;
		private string _def2;
		/// <summary>
		/// 
		/// </summary>
		public string id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 登记
		/// </summary>
		public string LevelName
		{
			set{ _levelname=value;}
			get{return _levelname;}
		}
		/// <summary>
		/// 品系
		/// </summary>
		public string ProductSeriesId
		{
			set{ _productseriesid=value;}
			get{return _productseriesid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string create_id
		{
			set{ _create_id=value;}
			get{return _create_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? create_time
		{
			set{ _create_time=value;}
			get{return _create_time;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string remarks
		{
			set{ _remarks=value;}
			get{return _remarks;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string def1
		{
			set{ _def1=value;}
			get{return _def1;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string def2
		{
			set{ _def2=value;}
			get{return _def2;}
		}
		#endregion Model

	}
}

