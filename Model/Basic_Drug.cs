﻿using System;
namespace XHD.Model
{
	/// <summary>
	/// Basic_Drug:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class Basic_Drug
	{
		public Basic_Drug()
		{}
		#region Model
		private string _idcode;
		private string _drugname;
		private decimal? _drugcycle;
        private string _remarks;
		private string _unit_id;
		private string _unit;
		/// <summary>
		/// 
		/// </summary>
		public string IDCode
		{
			set{ _idcode=value;}
			get{return _idcode;}
		}
		/// <summary>
		/// 药品名称
		/// </summary>
		public string drugName
		{
			set{ _drugname=value;}
			get{return _drugname;}
		}
		/// <summary>
		/// 用药周期
		/// </summary>
		public decimal? drugcycle
		{
			set{ _drugcycle=value;}
			get{return _drugcycle;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Remarks
		{
			set{ _remarks=value;}
			get{return _remarks;}
		}

        public string unit_id { get => _unit_id; set => _unit_id = value; }
        public string unit { get => _unit; set => _unit = value; }
        #endregion Model

    }
}

