﻿using System;
namespace XHD.Model
{
	/// <summary>
	/// Boar_ISASpSUS:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class Boar_ISASpSUS
	{
		public Boar_ISASpSUS()
		{}
		#region Model
		private string _collectionid;
		private string _casereference;
		private string _collector;
		private string _c_operator;
		private int? _dosisvolume;
		private string _c_subject;
		private int? _ejaculatedvolume;
		private int? _produceddosis;
		private int? _diluentvolume;
        private int? _totalfinalvolume;
        private int? _dilutionratio;
        private int? _totalspermsperdosis;
        //  private double? _usemml;
    
        public int? malnumber { get; set; }
        public double? malpercen { get; set; }
        public int? prognumber { get; set; }
        public double? progpercen { get; set; }

        private DateTime? _createtime;

      

        /// <summary>
        /// 
        /// </summary>
        public string collectionID
		{
			set{ _collectionid=value;}
			get{return _collectionid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string casereference
		{
			set{ _casereference=value;}
			get{return _casereference;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string collector
		{
			set{ _collector=value;}
			get{return _collector;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string c_operator
		{
			set{ _c_operator=value;}
			get{return _c_operator;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? dosisvolume
		{
			set{ _dosisvolume=value;}
			get{return _dosisvolume;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string c_subject
		{
			set{ _c_subject=value;}
			get{return _c_subject;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? ejaculatedvolume
		{
			set{ _ejaculatedvolume=value;}
			get{return _ejaculatedvolume;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? produceddosis
		{
			set{ _produceddosis=value;}
			get{return _produceddosis;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? diluentvolume
		{
			set{ _diluentvolume=value;}
			get{return _diluentvolume;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? totalfinalvolume
		{
			set{ _totalfinalvolume=value;}
			get{return _totalfinalvolume;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreateTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}

        public int? Dilutionratio { get => _dilutionratio; set => _dilutionratio = value; }
        public int? Totalspermsperdosis { get => _totalspermsperdosis; set => _totalspermsperdosis = value; }
      //  public double? Usemml { get => _usemml; set => _usemml = value; }
       
        #endregion Model
   
            private int? totnumber;
            private double? totpercen;
            private int? totmillions;
            private double? totmml;
            private int? nornumber;
            private double? norpercen;
            private int? normillions;
            private double? normml;
            private int? motnumber;
            private double? motpercen;
            private int? motmillions;
            private double? motmml;
            private int? mnonumber;
            private double? mnopercen;
            private int? mnomillions;
            private double? mnomml;
            private int? usenumber;
            private double? usepercen;
            private int? usemillions;
            private decimal? usemml;
        private string freshreference;
        private string subject;
        private string _operator;
        private int? initialdilutorvolume;
        private int? analysisdilution;
        private int? usefulspermperdosis;

            public int? Totnumber
            {
                set { totnumber = value; }
                get { return totnumber; }
            }

            public double? Totpercen
            {
                set { totpercen = value; }
                get { return totpercen; }
            }
            public int? Totmillions
            {
                set { totmillions = value; }
                get { return totmillions; }
            }
            public double? Totmml
            {
                set { totmml = value; }
                get { return totmml; }
            }
            public int? Nornumber
            {
                set { nornumber = value; }
                get { return nornumber; }
            }
            public double? Norpercen
            {
                set { norpercen = value; }
                get { return norpercen; }
            }
            public int? Normillions
            {
                set { normillions = value; }
                get { return normillions; }
            }
            public double? Normml
            {
                set { normml = value; }
                get { return normml; }
            }
            public int? Motnumber
            {
                set { motnumber = value; }
                get { return motnumber; }
            }
            public double? Motpercen
            {
                set { motpercen = value; }
                get { return motpercen; }
            }
            public int? Motmillions
            {
                set { motmillions = value; }
                get { return motmillions; }
            }
            public double? Motmml
            {
                set { motmml = value; }
                get { return motmml; }
            }
            public int? Mnonumber
            {
                set { mnonumber = value; }
                get { return mnonumber; }
            }
            public double? Mnopercen
            {
                set { mnopercen = value; }
                get { return mnopercen; }
            }
            public int? Mnomillions
            {
                set { mnomillions = value; }
                get { return mnomillions; }
            }
            public double? Mnomml
            {
                set { mnomml = value; }
                get { return mnomml; }
            }
            public int? Usenumber
            {
                set { usenumber = value; }
                get { return usenumber; }
            }
            public double? Usepercen
            {
                set { usepercen = value; }
                get { return usepercen; }
            }
            public int? Usemillions
            {
                set { usemillions = value; }
                get { return usemillions; }
            }
            public decimal? Usemml
            {
                set { usemml = value; }
                get { return usemml; }
            }

        public string Freshreference { get => freshreference; set => freshreference = value; }
        public string Subject { get => subject; set => subject = value; }
        public string Operator { get => _operator; set => _operator = value; }
        public int? Initialdilutorvolume { get => initialdilutorvolume; set => initialdilutorvolume = value; }
        public int? Usefulspermperdosis { get => usefulspermperdosis; set => usefulspermperdosis = value; }
        public int? Analysisdilution { get => analysisdilution; set => analysisdilution = value; }

        /// <
    }

    
}

