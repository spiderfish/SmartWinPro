﻿using System;
namespace XHD.Model
{
	/// <summary>
	/// Basic_dorm:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class Basic_dorm
	{
		public Basic_dorm()
		{}
		#region Model
		private string _id;
		private string _dormname;
		private decimal? _area;
		private string _product_icon;
		private string _remarks;
		private string _create_id;
		private DateTime? _create_time;
		private string _def1;
		private string _def2;
		/// <summary>
		/// 
		/// </summary>
		public string id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 畜舍名称
		/// </summary>
		public string dormName
		{
			set{ _dormname=value;}
			get{return _dormname;}
		}
		/// <summary>
		/// 畜舍面积
		/// </summary>
		public decimal? area
		{
			set{ _area=value;}
			get{return _area;}
		}
		/// <summary>
		/// 图标
		/// </summary>
		public string product_icon
		{
			set{ _product_icon=value;}
			get{return _product_icon;}
		}
		/// <summary>
		/// 备注
		/// </summary>
		public string remarks
		{
			set{ _remarks=value;}
			get{return _remarks;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string create_id
		{
			set{ _create_id=value;}
			get{return _create_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? create_time
		{
			set{ _create_time=value;}
			get{return _create_time;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string def1
		{
			set{ _def1=value;}
			get{return _def1;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string def2
		{
			set{ _def2=value;}
			get{return _def2;}
		}
		#endregion Model

	}
}

