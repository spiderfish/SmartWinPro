﻿using System;
namespace XHD.Model
{
	/// <summary>
	/// smallApp_Order_Body:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class smallApp_Order_Body
	{
		public smallApp_Order_Body()
		{}
		#region Model
		private string _orderid;
		private string _productid;
		private decimal? _totalamount;
		private decimal? _price;
		private decimal? _totalsum;
		/// <summary>
		/// 
		/// </summary>
		public string OrderId
		{
			set{ _orderid=value;}
			get{return _orderid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ProductId
		{
			set{ _productid=value;}
			get{return _productid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? TotalAmount
		{
			set{ _totalamount=value;}
			get{return _totalamount;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? price
		{
			set{ _price=value;}
			get{return _price;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? TotalSum
		{
			set{ _totalsum=value;}
			get{return _totalsum;}
		}
		#endregion Model

	}
}

