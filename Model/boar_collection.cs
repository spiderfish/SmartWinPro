﻿using System;
namespace XHD.Model
{
	/// <summary>
	/// boar_collection:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class boar_collection
	{
		public boar_collection()
		{}
		#region Model
		private string _id;
		private string _orderid;
        private string _ordermxid; 
        private string _deviceid;
		private string _boarid;
		private DateTime? _dotime;
		private string _doperson;
		private string _remarks;
		private string _collectstation;
		private string _workstationno;
		private DateTime? _collectdate;
		private string _collector;
		private string _isstatus;
        private decimal? _weights;  
        private string _para1;
        private string _para2;
        /// <summary>
        /// 
        /// </summary>
        public string id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string orderid
		{
			set{ _orderid=value;}
			get{return _orderid;}
		}
        /// <summary>
        /// 
        /// </summary>
        public string ordermxid
        {
            set { _ordermxid = value; }
            get { return _ordermxid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string deviceid
		{
			set{ _deviceid=value;}
			get{return _deviceid;}
		}
		/// <summary>
		/// 种禽号
		/// </summary>
		public string boarid
		{
			set{ _boarid=value;}
			get{return _boarid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? dotime
		{
			set{ _dotime=value;}
			get{return _dotime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string doperson
		{
			set{ _doperson=value;}
			get{return _doperson;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string remarks
		{
			set{ _remarks=value;}
			get{return _remarks;}
		}
		/// <summary>
		/// 站点
		/// </summary>
		public string collectStation
		{
			set{ _collectstation=value;}
			get{return _collectstation;}
		}
		/// <summary>
		/// 工位号
		/// </summary>
		public string WorkstationNo
		{
			set{ _workstationno=value;}
			get{return _workstationno;}
		}
		/// <summary>
		/// 采集日期
		/// </summary>
		public DateTime? collectDate
		{
			set{ _collectdate=value;}
			get{return _collectdate;}
		}
		/// <summary>
		/// 采集员
		/// </summary>
		public string collector
		{
			set{ _collector=value;}
			get{return _collector;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string IsStatus
		{
			set{ _isstatus=value;}
			get{return _isstatus;}
		}
        /// <summary>
        /// 
        /// </summary>
        public string para1
        {
            set { _para1 = value; }
            get { return _para1; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string para2
        {
            set { _para2 = value; }
            get { return _para2; }
        }
        public decimal? weights
        {
            set { _weights = value; }
            get { return _weights; }
        }
     
        #endregion Model

    }
}

