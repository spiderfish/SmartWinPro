﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;

namespace XHD.Common
{
    public static class SmartDataTableExtensions
    {
        /// <summary>  
        /// 利用反射和泛型,将DataTable转为List
        /// </summary>  
        /// <param name="dt"></param>  
        /// <returns></returns>  
        public static List<T> ToList<T>(this DataTable dt) where T : class,new()
        {
            List<T> ls = new List<T>();
            try
            {
                // 获得此模型的类型  
                Type type = typeof(T);
                //遍历DataTable中所有的数据行  
                foreach (DataRow dr in dt.Rows)
                {
                    T t = dr.ToModel<T>();
                    //对象添加到泛型集合中  
                    ls.Add(t);
                }
            }catch(Exception ex) 
            { 
            }
            return ls;
        }

        public static T ToModel<T>(this DataRow dr) where T : class,new()
        {
            //定义一个临时变量  
            string tempName = string.Empty;
            T t = new T();
            // 获得此模型的公共属性  
            PropertyInfo[] propertys = t.GetType().GetProperties();
            //遍历该对象的所有属性  
            foreach (PropertyInfo pi in propertys)
            {
                tempName = pi.Name;//将属性名称赋值给临时变量  
                //检查DataTable是否包含此列（列名==对象的属性名）    
                if (dr.Table.Columns.Contains(tempName))
                {
                    // 判断此属性是否有Setter  
                    if (!pi.CanWrite) continue;//该属性不可写，直接跳出  
                    //取值  
                    object value = dr[tempName];
                    //如果非空，则赋给对象的属性  
                    if (value != DBNull.Value)
                        pi.SetValue(t, value, null);
                }
            }
            return t;
        }


        //泛型+反射
        public static T ConvertModel<T, P>(P pModel)
        {

           
                T ret = System.Activator.CreateInstance<T>();
            try
            {
                if (pModel == null) return ret;
                List<PropertyInfo> p_pis = pModel.GetType().GetProperties().ToList();
                PropertyInfo[] t_pis = typeof(T).GetProperties();

                foreach (PropertyInfo pi in t_pis)
                {
                    //可写入数据
                    if (pi.CanWrite)
                    {
                        //忽略大小写
                        var name = p_pis.Find(s => s.Name.ToLower() == pi.Name.ToLower());
                        if (name != null)//&& pi.PropertyType.Name == name.PropertyType.Name
                        {
                            pi.SetValue(ret, name.GetValue(pModel, null), null);
                        }

                    }
                }

            }
            catch(Exception ex)
            {


            }
            return ret;
        }

    }
}
