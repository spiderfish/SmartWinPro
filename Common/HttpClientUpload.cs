﻿using System.Net.Http;
using System.IO;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using System.Collections.Generic;
using System;

public class FileUploadExample
{
    public static async Task UploadFileAsync(string url, string filePath, string paramName, Dictionary<string, string> formData)
    {
        using (var client = new HttpClient())
        using (var form = new MultipartFormDataContent())
        {
            // 添加表单数据
            foreach (var data in formData)
            {
                form.Add(new StringContent(data.Value), String.Format("\"{0}\"", data.Key));
            }

            // 添加文件
            using (var fs = File.OpenRead(filePath))
            {
                var stream = new StreamContent(fs);
                var content = new ByteArrayContent(fs.ReadToEnd());
                content.Headers.ContentType = MediaTypeHeaderValue.Parse("multipart/form-data");
                form.Add(content, paramName);

                // 执行上传
                var response = await client.PostAsync(url, form);
                response.EnsureSuccessStatusCode();
            }
        }
    }
}